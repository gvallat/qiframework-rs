# qiframework-rs - A port of libqi in Rust

[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/gvallat/qiframework-rs)

Status: **Early developement**

`qiframework-rs` is a port of [libqi](https://github.com/aldebaran/libqi) in Rust.

A summary of the qi-messaging protocol can be found [here](https://github.com/lugu/qiloop/blob/master/doc/about-qimessaging.md)

Minimal Rust version: **1.39**

## qimessaging-rs

This is the RPC part of `libqi`.

### Current state

- [X] Simple data serialization
- [X] Simple any value
- [X] Basic connection in TCP and TCPS
- [ ] Access `service` info (Client side)
- [ ] Call a `service` method
- [ ] Register to a `service` event (signal)
- [ ] READ/Edit/Watch a `service` property
- [ ] Support `libqi` `AnyObject` (Client side)
- [ ] Register a `service` (Server side)
- [ ] ...

### Serialization

Using serde:

- [ ] Serialize to `qimessaging` binary format
  - [X] basic types
  - [X] `Any`
  - [ ] `Object`
  - [ ] support integer conversion
  - [ ] support structure as map
  - [ ] support structure as list
- [ ] Deserialize from `qimessaging` binary format
  - [ ] basic types
  - [ ] `Any`
  - [ ] `Object`
  - [ ] support integer conversion
  - [ ] support structure as map
  - [ ] support structure as list
- [ ] Serialize to `Any` value
  - [X] basic types
  - [X] `Any`
  - [ ] `Object`
  - [ ] support integer conversion
  - [ ] support structure as map
  - [ ] support structure as list
- [ ] Deserialize from `Any` value
  - [X] basic types
  - [ ] `Any`
  - [ ] `Object`
  - [ ] support integer conversion
  - [ ] support structure as map
  - [ ] support structure as list
- [ ] Serialize `Any` value
  - [X] generic case
  - [X] optimized for serialization to `Any`
  - [ ] follow expected signature for tuple
- [ ] Deserialize `Any` value
  - [ ] generic case
  - [ ] optimized for deserialization from `Any`

## Tools and helpers

At some point, we should add tools and helpers:

- [ ] A utility crate (connection arguments...)
- [ ] A CLI tools for:
  - listing services and methods
  - making calls
  - watching signals
  - interacting with properties
  - accessing logs
- [ ] Qi-messaging tracing tool
- [ ] Qi-messaging statistics tool
- [ ] ...
