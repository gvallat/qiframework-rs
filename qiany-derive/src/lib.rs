#[macro_use]
extern crate quote;
#[macro_use]
extern crate syn;
extern crate proc_macro;

mod anyenum;
mod anystruct;

// extern crate proc_macro2;

#[proc_macro_derive(AnyStruct)]
pub fn derive_anystruct(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as anystruct::Input);
    anystruct::impl_anystruct(input).into()
}

#[proc_macro_derive(AnyEnum, attributes(qiany))]
pub fn derive_anyenum(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as anyenum::Input);
    anyenum::impl_anyenum(input).into()
}
