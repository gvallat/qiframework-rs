use heck::MixedCase;
use proc_macro2::{Span, TokenStream};
use syn::parse::{Error, Parse, ParseStream, Result};
use syn::{spanned::Spanned, Data, DeriveInput, Fields, Ident, Type};

pub struct Input {
    name: Ident,
    fields: StructFields,
}

enum StructFields {
    Named(Vec<(Ident, Type)>),
    Unnamed(Vec<Type>),
}

impl Parse for Input {
    fn parse(input: ParseStream) -> Result<Self> {
        let call_site = Span::call_site();
        let derive_input = DeriveInput::parse(input)?;

        let data = match derive_input.data {
            Data::Struct(data) => data,
            _ => return Err(Error::new(call_site, "input must be a struct")),
        };

        let fields = match data.fields {
            Fields::Named(fields) => {
                let fields = fields
                    .named
                    .into_iter()
                    .map(|f| (f.ident.unwrap(), f.ty))
                    .collect::<Vec<(Ident, Type)>>();
                StructFields::Named(fields)
            }
            Fields::Unnamed(fields) => {
                let fields = fields
                    .unnamed
                    .into_iter()
                    .map(|f| f.ty)
                    .collect::<Vec<Type>>();
                StructFields::Unnamed(fields)
            }
            _ => return Err(Error::new(call_site, "input can't be an unit type")),
        };

        Ok(Input {
            name: derive_input.ident,
            fields,
        })
    }
}

fn impl_typed_tuple(name: &Ident) -> TokenStream {
    quote! {
        impl qiany::Typed for #name {
            fn type_interface() -> qiany::TypeInterfacePtr {
                qiany::interfaces::TupleTypeInterface::<Self>::get().0
            }
        }

        impl qiany::ToAnyValue for #name {
            fn into_anyvalue(self) -> qiany::AnyValue {
                let data: Box<dyn std::any::Any + Send> = Box::new(self);
                let interface = qiany::interfaces::TupleTypeInterface::<Self>::get().1;
                qiany::AnyValue::Tuple(data, interface)
            }

            fn as_anyref(&self) -> qiany::AnyRef<'_> {
                let data: &(dyn std::any::Any + Send) = self;
                let interface = qiany::interfaces::TupleTypeInterface::<Self>::get().1;
                qiany::AnyRef::Tuple(data, interface)
            }

            fn as_anyrefmut(&mut self) -> qiany::AnyRefMut<'_> {
                let data: &mut (dyn std::any::Any + Send) = self;
                let interface = qiany::interfaces::TupleTypeInterface::<Self>::get().1;
                qiany::AnyRefMut::Tuple(data, interface)
            }
        }
    }
}

struct DeriveFragments {
    len: usize,
    signature: TokenStream,
    names: TokenStream,
    contains_enum: TokenStream,
    as_ref: TokenStream,
    as_refmut: TokenStream,
    into_vec: TokenStream,
}

fn struct_fragments(fields: Vec<(Ident, Type)>) -> DeriveFragments {
    let len = fields.len();

    let names = fields.iter().map(|(n, _)| {
        // IMPROVE add support for renaming
        let name = n.to_string().to_mixed_case();
        quote_spanned! { n.span() => #name, }
    });
    let names: TokenStream = quote! { #(#names)* }.into();

    let enums = fields.iter().map(|(_, ty)| {
        quote_spanned! { ty.span() => <#ty as qiany::Typed>::type_interface().contains_enum() || }
    });
    let contains_enum: TokenStream = quote! {
        #(#enums)* false
    }
    .into();

    let sigs = fields.iter().map(|(_, ty)| {
        quote_spanned! { ty.span() => <#ty as qiany::Typed>::signature(), }
    });
    let signature: TokenStream = quote! {
        let sigs = [#(#sigs)*];
        qiany::Signature::make_struct(&sigs, Self::name(), Self::field_names()).unwrap()
    }
    .into();

    let as_ref = fields.iter().enumerate().map(|(i, (n, _))| {
        let name = &n;
        quote_spanned! { n.span() => #i => Ok(d.#name.as_anyref()), }
    });
    let as_ref: TokenStream = quote! { #(#as_ref)* }.into();

    let as_refmut = fields.iter().enumerate().map(|(i, (n, _))| {
        let name = &n;
        quote_spanned! { n.span() => #i => Ok(d.#name.as_anyrefmut()), }
    });
    let as_refmut: TokenStream = quote! { #(#as_refmut)* }.into();

    let into_vec = fields.iter().map(|(n, _)| {
        let name = &n;
        quote_spanned! { n.span() => vec.push(d.#name.into_anyvalue()); }
    });
    let into_vec: TokenStream = quote! { #(#into_vec)* }.into();

    DeriveFragments {
        len,
        signature,
        names,
        contains_enum,
        as_ref,
        as_refmut,
        into_vec,
    }
}

fn tuple_fragments(fields: Vec<Type>) -> DeriveFragments {
    let len = fields.len();

    let names: TokenStream = quote! {}.into();

    let sigs = fields.iter().map(|ty| {
        quote_spanned! { ty.span() => <#ty as qiany::Typed>::signature(), }
    });
    let signature: TokenStream = quote! {
        let sigs = [#(#sigs)*];
        qiany::Signature::make_tuple(&sigs)
    }
    .into();

    let enums = fields.iter().map(|ty| {
        quote_spanned! { ty.span() => <#ty as qiany::Typed>::type_interface().contains_enum() || }
    });
    let contains_enum: TokenStream = quote! {
        #(#enums)* false
    }
    .into();

    let as_ref = fields.iter().enumerate().map(|(i, ty)| {
        let idx = syn::Index::from(i);
        quote_spanned! { ty.span() => #i => Ok(d.#idx.as_anyref()), }
    });
    let as_ref: TokenStream = quote! { #(#as_ref)* }.into();

    let as_refmut = fields.iter().enumerate().map(|(i, ty)| {
        let idx = syn::Index::from(i);
        quote_spanned! { ty.span() => #i => Ok(d.#idx.as_anyrefmut()), }
    });
    let as_refmut: TokenStream = quote! { #(#as_refmut)* }.into();

    let into_vec = fields.iter().enumerate().map(|(i, ty)| {
        let idx = syn::Index::from(i);
        quote_spanned! { ty.span() => vec.push(d.#idx.into_anyvalue()); }
    });
    let into_vec: TokenStream = quote! { #(#into_vec)* }.into();

    DeriveFragments {
        len,
        signature,
        names,
        contains_enum,
        as_ref,
        as_refmut,
        into_vec,
    }
}

pub fn impl_anystruct(input: Input) -> TokenStream {
    // IMPROVE Add support for renaming
    let name = &input.name;
    let typed = impl_typed_tuple(&input.name);

    let DeriveFragments {
        len,
        signature,
        names,
        contains_enum,
        as_ref,
        as_refmut,
        into_vec,
    } = match input.fields {
        StructFields::Named(fields) => struct_fragments(fields),
        StructFields::Unnamed(fields) => tuple_fragments(fields),
    };

    quote! {
        impl qiany::interfaces::Indexer for #name {
            fn signature() -> qiany::Signature {
                #signature
            }

            fn name() -> &'static str {
                stringify!(#name)
            }

            fn field_names() -> &'static [&'static str] {
                &[#names]
            }

            fn contains_enum() -> bool {
                #contains_enum
            }

            fn len() -> usize {
                #len
            }

            fn as_ref<'a>(data: &'a dyn std::any::Any, idx: usize) -> qiany::Result<qiany::AnyRef<'a>> {
                use std::any::Any;
                use qiany::ToAnyValue;
                match data.downcast_ref::<Self>() {
                    Some(d) => match idx {
                        #as_ref
                        _ => Err(qiany::AnyError::InvalidOperation),
                    }
                    None => Err(qiany::AnyError::InvalidOperation),
                }
            }

            fn as_refmut<'a>(data: &'a mut dyn std::any::Any, idx: usize) -> qiany::Result<qiany::AnyRefMut<'a>> {
                use std::any::Any;
                use qiany::ToAnyValue;
                match data.downcast_mut::<Self>() {
                    Some(d) => match idx {
                        #as_refmut
                        _ => Err(qiany::AnyError::InvalidOperation),
                    }
                    None => Err(qiany::AnyError::InvalidOperation),
                }
            }

            fn into_iter(data: Box<dyn std::any::Any + Send>) -> qiany::Result<Box<dyn Iterator<Item = qiany::AnyValue>>> {
                match data.downcast::<Self>() {
                    Ok(data) => {
                        let mut vec = Vec::with_capacity(Self::len());
                        let d = *data;
                        #into_vec
                        Ok(Box::new(vec.into_iter()))
                    },
                    _ => Err(qiany::AnyError::InvalidOperation),
                }
            }
        }

        #typed
    }
}
