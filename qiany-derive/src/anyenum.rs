use heck::ShoutySnakeCase;
use proc_macro2::{Span, TokenStream};
use syn::parse::{Error, Parse, ParseStream, Parser, Result};
use syn::{parenthesized, Data, DeriveInput, Fields, Ident, Meta, NestedMeta};

pub struct Input {
    ident: Ident,
    repr: Ident,
    variants: Vec<Variant>,
    default_variant: Variant,
}

#[derive(Clone)]
struct Variant {
    ident: Ident,
    discriminant: Ident,
    attrs: VariantAttrs,
}

#[derive(Clone)]
struct VariantAttrs {
    is_default: bool,
}

fn parse_meta(attrs: &mut VariantAttrs, meta: &Meta) {
    if let Meta::List(value) = meta {
        for meta in &value.nested {
            if let NestedMeta::Meta(Meta::Path(path)) = meta {
                if path.is_ident("default") {
                    attrs.is_default = true;
                }
            }
        }
    }
}

fn parse_attrs(variant: &syn::Variant) -> Result<VariantAttrs> {
    let mut attrs = VariantAttrs { is_default: false };
    for attr in &variant.attrs {
        if attr.path.is_ident("qiany") {
            parse_meta(&mut attrs, &attr.parse_meta()?);
        }
    }
    Ok(attrs)
}

impl Parse for Input {
    fn parse(input: ParseStream) -> Result<Self> {
        let call_site = Span::call_site();
        let derive_input = DeriveInput::parse(input)?;

        let data = match derive_input.data {
            Data::Enum(data) => data,
            _ => return Err(Error::new(call_site, "input must be an enum")),
        };

        let variants = data
            .variants
            .into_iter()
            .map(|variant| match variant.fields {
                Fields::Unit => {
                    let attrs = parse_attrs(&variant)?;
                    let name = variant.ident.to_string().to_shouty_snake_case();
                    let discriminant = Ident::new(&name, variant.ident.span());
                    Ok(Variant {
                        ident: variant.ident,
                        discriminant,
                        attrs,
                    })
                }
                Fields::Named(_) | Fields::Unnamed(_) => {
                    Err(Error::new(variant.ident.span(), "must be a unit variant"))
                }
            })
            .collect::<Result<Vec<Variant>>>()?;

        if variants.is_empty() {
            return Err(Error::new(call_site, "there must be at least on variant"));
        }

        let generics = derive_input.generics;
        if !generics.params.is_empty() || generics.where_clause.is_some() {
            return Err(Error::new(call_site, "generic enum is not supported"));
        }

        let mut repr = None;
        for attr in derive_input.attrs {
            if attr.path.is_ident("repr") {
                fn repr_arg(input: ParseStream) -> Result<Ident> {
                    let content;
                    parenthesized!(content in input);
                    content.parse()
                }
                let ty = repr_arg.parse2(attr.tokens)?;
                repr = Some(ty);
                break;
            }
        }
        let repr = repr.ok_or_else(|| Error::new(call_site, "missing #[repr(...)] attribute"))?;

        let mut iter = variants.iter().filter(|v| v.attrs.is_default);
        let default_variant = match iter.next() {
            Some(v) => match iter.next() {
                Some(_) => {
                    return Err(Error::new(
                        call_site,
                        "only one variant can be #[qiany(default)]",
                    ));
                }
                None => v.clone(),
            },
            None => variants[0].clone(),
        };

        Ok(Input {
            ident: derive_input.ident,
            repr,
            variants,
            default_variant,
        })
    }
}

pub fn impl_anyenum(input: Input) -> TokenStream {
    let name = input.ident;
    let repr = input.repr;
    let default_variant = input.default_variant.ident;

    let discriminants = input.variants.iter().map(|v| {
        let var = &v.ident;
        let discriminant = &v.discriminant;
        quote! { const #discriminant: #repr = #name::#var as #repr; }
    });

    let matchings = input.variants.iter().map(|v| {
        let var = &v.ident;
        let discriminant = &v.discriminant;
        quote! { #discriminant => #name::#var, }
    });

    quote! {
        impl Default for #name {
            fn default() -> #name {
                #name::#default_variant
            }
        }

        impl qiany::interfaces::EnumInterface for #name {
            type Repr = #repr;

            fn new_any() -> Box<dyn std::any::Any + Send> {
                Box::new(#name::#default_variant)
            }

            fn from_repr(val: Self::Repr) -> Self {
                #(#discriminants)*
                match val {
                    #(#matchings)*
                    _ => #name::#default_variant,
                }
            }

            fn to_repr(self) -> Self::Repr {
                self as #repr
            }
        }

        impl qiany::Typed for #name {
            fn type_interface() -> qiany::TypeInterfacePtr {
                qiany::interfaces::EnumTypeInterface::<Self>::get().0
            }
        }

        impl qiany::ToAnyValue for #name {
            fn into_anyvalue(self) -> qiany::AnyValue {
                let data: Box<dyn std::any::Any + Send> = Box::new(self);
                let interface = qiany::interfaces::EnumTypeInterface::<Self>::get().1;
                qiany::AnyValue::Enum(data, interface)
            }

            fn as_anyref(&self) -> qiany::AnyRef<'_> {
                let data: &(dyn std::any::Any + Send) = self;
                let interface = qiany::interfaces::EnumTypeInterface::<Self>::get().1;
                qiany::AnyRef::Enum(data, interface)
            }

            fn as_anyrefmut(&mut self) -> qiany::AnyRefMut<'_> {
                let data: &mut (dyn std::any::Any + Send) = self;
                let interface = qiany::interfaces::EnumTypeInterface::<Self>::get().1;
                qiany::AnyRefMut::Enum(data, interface)
            }
        }
    }
}
