
fn main() {
    let args = AppArgs::read();
    let mut session = Session::new();

    let stop_source = Arc::new(StopSource::new());
    let link = session.disconnected.connect(
        |_| {
            Box::new(async {
                stop_source.stop();
            })
        }
    );

    let future = async {
        session.connect(args.url).await?;
        let service = session.service::<ServiceStuff>("ALStuff").await?;
        let x = service.do_it().await?;
        let service_2 = session.service::<ServiceBidule>("ALBidule").await?;
        service_2.bidule(x).await?;
        Ok(())
    };

    let stop_token = stop_source.stop_token();
    let future = stop_token.stop_future(future);
    block_on(future);

    link.drop();
}