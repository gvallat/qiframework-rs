use {
    qimessaging_rs::{
        any::{self, Any, IntoAny},
        buffer::Buffer,
        serde::to_any,
        signature::{GetTypeSignature, Signature},
    },
    std::collections::HashMap,
};

#[derive(serde::Serialize)]
#[repr(u16)]
#[allow(unused)]
enum Fruit {
    Apple = 1,
    Banana = 2,
}

impl GetTypeSignature for Fruit {
    fn signature() -> String {
        "W".to_string()
    }
}

#[derive(serde::Serialize)]
#[repr(i8)]
#[allow(unused)]
enum Level {
    Low = 1,
    Medium = 2,
    High = 3,
}

impl GetTypeSignature for Level {
    fn signature() -> String {
        "c".to_string()
    }
}

#[derive(serde::Serialize)]
#[serde(rename_all = "camelCase")]
struct MyStruct {
    number: u32,
    some_info: String,
}

impl GetTypeSignature for MyStruct {
    fn signature() -> String {
        "(Is)<MyStruct,number,someInfo>".to_string()
    }
}

#[derive(serde::Serialize)]
#[serde(rename_all = "camelCase")]
struct OtherStruct {
    ratio: f32,
    data: MyStruct,
}

impl GetTypeSignature for OtherStruct {
    fn signature() -> String {
        "(f(Is)<MyStruct,number,someInfo>)<OtherStruct,ratio,data>".to_string()
    }
}

macro_rules! test_integer {
    ($name:ident) => {
        let a = Any::$name(5);
        println!("{}", serde_json::to_string(&a).unwrap());
    };
}

fn test_serialize_any_to_json() {
    let a = Any::from_bool(true);
    println!("{}", serde_json::to_string(&a).unwrap());

    test_integer!(from_i8);
    test_integer!(from_u8);
    test_integer!(from_i16);
    test_integer!(from_u16);
    test_integer!(from_i32);
    test_integer!(from_u32);
    test_integer!(from_i64);
    test_integer!(from_u64);

    let a = Any::from_f32(5.);
    println!("{}", serde_json::to_string(&a).unwrap());

    let a = Any::from_f64(5.);
    println!("{}", serde_json::to_string(&a).unwrap());

    let a = Any::from_string("stuff".to_string());
    println!("{}", serde_json::to_string(&a).unwrap());

    let a = Any::from_buffer(Buffer::from_vec(vec![1, 2, 3, 4, 5]));
    println!("{}", serde_json::to_string(&a).unwrap());

    let a = Any::from_f64(5.);
    let a = Any::into_dynamic(a);
    println!("{}", serde_json::to_string(&a).unwrap());

    let vec = vec![1, 2, 3, 4, 5];
    let a = vec.into_any();
    println!("{}", serde_json::to_string(&a).unwrap());

    let o: Option<u8> = None;
    let a = o.into_any();
    println!("{}", serde_json::to_string(&a).unwrap());

    let o: Option<u8> = Some(8);
    let a = o.into_any();
    println!("{}", serde_json::to_string(&a).unwrap());

    let mut m = HashMap::new();
    m.insert("a".to_string(), 5);
    m.insert("b".to_string(), 8);
    let a = m.into_any();
    println!("{}", serde_json::to_string(&a).unwrap());
}

fn as_json_and_back(any: Any) {
    let json = serde_json::to_string(&any).unwrap();
    let res: Any = serde_json::from_str(&json).unwrap();
    println!("{:?}", res);
}

fn test_deserialize_any_from_json() {
    as_json_and_back(Any::from_bool(true));
    as_json_and_back(Any::from_i8(5));
    as_json_and_back(Any::from_u8(5));
    as_json_and_back(Any::from_i16(5));
    as_json_and_back(Any::from_u16(5));
    as_json_and_back(Any::from_i32(5));
    as_json_and_back(Any::from_u32(5));
    as_json_and_back(Any::from_i64(5));
    as_json_and_back(Any::from_u64(5));
    as_json_and_back(Any::from_f32(5.));
    as_json_and_back(Any::from_f64(5.));

    as_json_and_back(Any::from_string("Some stuff".to_string()));
    as_json_and_back(Any::from_buffer(Buffer::from_vec(vec![1, 2, 3, 4])));
    as_json_and_back(Any::into_dynamic(Any::from_f64(5.3)));

    let o: Option<u8> = None;
    as_json_and_back(o.into_any());

    let o: Option<u8> = Some(42);
    as_json_and_back(o.into_any());

    let vec = vec![1, 2, 3, 4, 5];
    as_json_and_back(vec.into_any());

    let mut m = HashMap::new();
    m.insert("a".to_string(), 5);
    m.insert("b".to_string(), 8);
    as_json_and_back(m.into_any());

    let sig = Signature::new("{sI}".to_string()).unwrap();
    let content = "{ \"apple\": 0, \"banana\": 1 }";
    let mut de = serde_json::Deserializer::from_str(content);
    let res = any::deserialize_to_any_with_signature(&sig, &mut de);
    println!("{:?}", res);

    let content = "{ \"apple\": 0, \"banana\": 1 }";
    let mut de = serde_json::Deserializer::from_str(content);
    let res = any::deserialize_to_any(&mut de);
    println!("{:?}", res);

    let json = "{ \"signature\" : \"I\", \"data\": 45  }";
    let res: Any = serde_json::from_str(&json).unwrap();
    println!("{:?}", res);
}

macro_rules! test_integer {
    ($t:ty) => {
        let v: $t = 5;
        let a = to_any(&v).unwrap();
        println!("{:?}", a);
    };
}

#[allow(unused)]
#[allow(clippy::many_single_char_names)]
fn test_serialize_to_any() {
    let a = to_any(&true).unwrap();
    println!("{:?}", a);

    test_integer!(i8);
    test_integer!(u8);
    test_integer!(i16);
    test_integer!(u16);
    test_integer!(i32);
    test_integer!(u32);
    test_integer!(i64);
    test_integer!(u64);

    let a = to_any(&5.0f32).unwrap();
    println!("{:?}", a);

    let a = to_any(&5.0f64).unwrap();
    println!("{:?}", a);

    let a = to_any(&"stuff").unwrap();
    println!("{:?}", a);

    let buf = Buffer::from_vec(vec![1, 2, 3, 4, 5]);
    let a = to_any(&buf).unwrap();
    println!("{:?}", a);

    let vec = vec![1, 2, 3, 4, 5];
    let a = to_any(&vec).unwrap();
    println!("{:?}", a);

    let o: Option<u8> = None;
    let a = to_any(&o).unwrap();
    println!("{:?}", a);

    let o: Option<u8> = Some(5);
    let a = to_any(&o).unwrap();
    println!("{:?}", a);

    let mut m = HashMap::new();
    m.insert("a".to_string(), 5);
    m.insert("b".to_string(), 8);
    let a = to_any(&m).unwrap();
    println!("\n{:#?}", a);

    let ms = MyStruct {
        number: 42,
        some_info: "it's working".to_string(),
    };
    let a = to_any(&ms).unwrap();
    println!("\n{:#?}", a);
    println!("{}", serde_json::to_string(&a).unwrap());

    let os = OtherStruct {
        ratio: 5.3,
        data: ms,
    };
    let a = to_any(&os).unwrap();
    println!("\n{:#?}", a);
    println!("{}", serde_json::to_string(&a).unwrap());

    let vec = vec![
        Any::from_u8(5),
        Any::from_u32(6),
        Any::from_string("text".to_string()),
    ];
    let a = to_any(&vec).unwrap();
    println!("\n{:#?}", a);
    println!("{}", serde_json::to_string(&a).unwrap());

    let f = Fruit::Apple;
    let a = to_any(&f).unwrap();
    println!("\n{:?}", a);

    let l = Level::High;
    let a = to_any(&l).unwrap();
    println!("\n{:?}", a);
}

fn main() {
    test_serialize_any_to_json();
    println!();
    //test_serialize_to_any();
    test_deserialize_any_from_json();
}
