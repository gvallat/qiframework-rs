// use futures::stream::StreamExt;
use qimessaging_rs::{rpc::PasswordClientAuthenticator, url::Url, Session};
use tokio::runtime;

async fn run() -> std::io::Result<()> {
    let session = Session::new();

    let authenticator = Box::new(PasswordClientAuthenticator::new(
        "nao".to_string(),
        "nao".to_string(),
    ));
    let url: Url = "tcps://10.0.165.69:9503".parse().unwrap();
    session.connect(url, Some(authenticator)).await?;
    tracing::info!("Connected !!!");
    let services = session.services_info().await?;
    for service in services {
        println!("[{}] {}", service.service_id, service.name);
        println!(
            "\t- Process ID: {}\n\t- Machine ID: {}\n\t- Session ID: {}",
            service.process_id, service.machine_id, service.session_id
        )
    }

    // if let Some(name) = session.service_removed_receiver().next().await {
    //     println!("Service removed: {}", name);
    // }

    Ok(())
}

fn main() {
    tracing_log::LogTracer::init().unwrap();
    let filter = tracing_subscriber::EnvFilter::from_default_env()
        .add_directive("qimessaging_rs=trace".parse().unwrap())
        .add_directive("simple_connect=trace".parse().unwrap());
    let subscriber = tracing_subscriber::fmt::Subscriber::builder()
        .with_max_level(tracing_subscriber::filter::LevelFilter::INFO)
        .with_env_filter(filter)
        .finish();
    tracing::subscriber::set_global_default(subscriber).unwrap();

    let mut rt = runtime::Builder::new()
        .threaded_scheduler()
        .enable_all()
        .build()
        .unwrap();
    if let Err(e) = rt.block_on(run()) {
        tracing::error!("Ended in error: {}", e);
    }
    //rt.shutdown_on_idle();
}
