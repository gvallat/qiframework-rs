use crate::{
    convert,
    interfaces::{
        AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
        AnyTupleInterface, AnyVecInterface,
    },
    AnyError, AnyKind, AnyRef, AnyRefMut, Buffer, Signature, Typed,
};
use std::any::Any;

pub enum AnyValue {
    Unit,
    Bool(bool),
    Int8(i8),
    Int16(i16),
    Int32(i32),
    Int64(i64),
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    UInt64(u64),
    Float(f32),
    Double(f64),
    String(String),
    Raw(Buffer),
    Dynamic(Box<AnyValue>),
    Vec(Box<dyn Any + Send>, &'static (dyn AnyVecInterface + Sync)),
    Map(Box<dyn Any + Send>, &'static (dyn AnyMapInterface + Sync)),
    Option(
        Box<dyn Any + Send>,
        &'static (dyn AnyOptionInterface + Sync),
    ),
    Tuple(Box<dyn Any + Send>, &'static (dyn AnyTupleInterface + Sync)),
    Enum(Box<dyn Any + Send>, &'static (dyn AnyEnumInterface + Sync)),
    Object(
        Box<dyn Any + Send>,
        &'static (dyn AnyObjectInterface + Sync),
    ),
}

impl AnyValue {
    pub fn as_anyref(&self) -> AnyRef<'_> {
        match self {
            AnyValue::Unit => AnyRef::Unit,
            AnyValue::Bool(val) => AnyRef::Bool(val),
            AnyValue::Int8(val) => AnyRef::Int8(val),
            AnyValue::Int16(val) => AnyRef::Int16(val),
            AnyValue::Int32(val) => AnyRef::Int32(val),
            AnyValue::Int64(val) => AnyRef::Int64(val),
            AnyValue::UInt8(val) => AnyRef::UInt8(val),
            AnyValue::UInt16(val) => AnyRef::UInt16(val),
            AnyValue::UInt32(val) => AnyRef::UInt32(val),
            AnyValue::UInt64(val) => AnyRef::UInt64(val),
            AnyValue::Float(val) => AnyRef::Float(val),
            AnyValue::Double(val) => AnyRef::Double(val),
            AnyValue::String(val) => AnyRef::String(&*val),
            AnyValue::Raw(val) => AnyRef::Raw(val),
            AnyValue::Dynamic(any) => AnyRef::Dynamic(any),
            AnyValue::Vec(data, interface) => AnyRef::Vec(&**data, *interface),
            AnyValue::Map(data, interface) => AnyRef::Map(&**data, *interface),
            AnyValue::Option(data, interface) => AnyRef::Option(&**data, *interface),
            AnyValue::Tuple(data, interface) => AnyRef::Tuple(&**data, *interface),
            AnyValue::Enum(data, interface) => AnyRef::Enum(&**data, *interface),
            AnyValue::Object(data, interface) => AnyRef::Object(&**data, *interface),
        }
    }

    pub fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        match self {
            AnyValue::Unit => AnyRefMut::Unit,
            AnyValue::Bool(val) => AnyRefMut::Bool(val),
            AnyValue::Int8(val) => AnyRefMut::Int8(val),
            AnyValue::Int16(val) => AnyRefMut::Int16(val),
            AnyValue::Int32(val) => AnyRefMut::Int32(val),
            AnyValue::Int64(val) => AnyRefMut::Int64(val),
            AnyValue::UInt8(val) => AnyRefMut::UInt8(val),
            AnyValue::UInt16(val) => AnyRefMut::UInt16(val),
            AnyValue::UInt32(val) => AnyRefMut::UInt32(val),
            AnyValue::UInt64(val) => AnyRefMut::UInt64(val),
            AnyValue::Float(val) => AnyRefMut::Float(val),
            AnyValue::Double(val) => AnyRefMut::Double(val),
            AnyValue::String(val) => AnyRefMut::String(val),
            AnyValue::Raw(val) => AnyRefMut::Raw(val),
            AnyValue::Dynamic(any) => AnyRefMut::Dynamic(any),
            AnyValue::Vec(data, interface) => AnyRefMut::Vec(&mut **data, *interface),
            AnyValue::Map(data, interface) => AnyRefMut::Map(&mut **data, *interface),
            AnyValue::Option(data, interface) => AnyRefMut::Option(&mut **data, *interface),
            AnyValue::Tuple(data, interface) => AnyRefMut::Tuple(&mut **data, *interface),
            AnyValue::Enum(data, interface) => AnyRefMut::Enum(&mut **data, *interface),
            AnyValue::Object(data, interface) => AnyRefMut::Object(&mut **data, *interface),
        }
    }

    pub fn kind(&self) -> AnyKind {
        self.as_anyref().kind()
    }

    pub fn signature(&self) -> &'static Signature {
        self.as_anyref().signature()
    }

    pub fn downcast<T: 'static>(self) -> Result<T, AnyValue> {
        match self {
            AnyValue::Unit => downcast_value(()).map_err(|_| AnyValue::Unit),
            AnyValue::Bool(val) => downcast_value(val).map_err(|v| AnyValue::Bool(v)),
            AnyValue::Int8(val) => downcast_value(val).map_err(|v| AnyValue::Int8(v)),
            AnyValue::Int16(val) => downcast_value(val).map_err(|v| AnyValue::Int16(v)),
            AnyValue::Int32(val) => downcast_value(val).map_err(|v| AnyValue::Int32(v)),
            AnyValue::Int64(val) => downcast_value(val).map_err(|v| AnyValue::Int64(v)),
            AnyValue::UInt8(val) => downcast_value(val).map_err(|v| AnyValue::UInt8(v)),
            AnyValue::UInt16(val) => downcast_value(val).map_err(|v| AnyValue::UInt16(v)),
            AnyValue::UInt32(val) => downcast_value(val).map_err(|v| AnyValue::UInt32(v)),
            AnyValue::UInt64(val) => downcast_value(val).map_err(|v| AnyValue::UInt64(v)),
            AnyValue::Float(val) => downcast_value(val).map_err(|v| AnyValue::Float(v)),
            AnyValue::Double(val) => downcast_value(val).map_err(|v| AnyValue::Double(v)),
            AnyValue::String(val) => downcast_value(val).map_err(|v| AnyValue::String(v)),
            AnyValue::Raw(val) => downcast_value(val).map_err(|v| AnyValue::Raw(v)),
            AnyValue::Dynamic(any) => downcast_any(any).map_err(|v| AnyValue::Dynamic(v)),
            AnyValue::Vec(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Vec(v, interface))
            }
            AnyValue::Map(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Map(v, interface))
            }
            AnyValue::Option(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Option(v, interface))
            }
            AnyValue::Tuple(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Tuple(v, interface))
            }
            AnyValue::Enum(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Enum(v, interface))
            }
            AnyValue::Object(data, interface) => {
                downcast_box(data).map_err(|v| AnyValue::Object(v, interface))
            }
        }
    }

    pub fn try_into<T: Typed + 'static>(self) -> Result<T, AnyError> {
        convert::try_from_value(self)
    }
}

fn downcast_any<T: 'static>(b: Box<AnyValue>) -> Result<T, Box<AnyValue>> {
    let b: Box<dyn Any + 'static> = b;
    match b.downcast::<T>() {
        Ok(b) => Ok(*b),
        Err(b) => Err(b.downcast::<AnyValue>().unwrap()),
    }
}

fn downcast_box<T: 'static>(
    b: Box<dyn Any + 'static + Send>,
) -> Result<T, Box<dyn Any + 'static + Send>> {
    match b.downcast::<T>() {
        Ok(b) => Ok(*b),
        Err(b) => Err(b),
    }
}

fn downcast_value<D: 'static + Send, T: 'static>(d: D) -> Result<T, D> {
    let b = Box::new(d);
    match downcast_box(b) {
        Ok(t) => Ok(t),
        Err(b) => Err(*b.downcast::<D>().unwrap()),
    }
}

impl Clone for AnyValue {
    fn clone(&self) -> AnyValue {
        match self {
            AnyValue::Unit => AnyValue::Unit,
            AnyValue::Bool(val) => AnyValue::Bool(*val),
            AnyValue::Int8(val) => AnyValue::Int8(*val),
            AnyValue::Int16(val) => AnyValue::Int16(*val),
            AnyValue::Int32(val) => AnyValue::Int32(*val),
            AnyValue::Int64(val) => AnyValue::Int64(*val),
            AnyValue::UInt8(val) => AnyValue::UInt8(*val),
            AnyValue::UInt16(val) => AnyValue::UInt16(*val),
            AnyValue::UInt32(val) => AnyValue::UInt32(*val),
            AnyValue::UInt64(val) => AnyValue::UInt64(*val),
            AnyValue::Float(val) => AnyValue::Float(*val),
            AnyValue::Double(val) => AnyValue::Double(*val),
            AnyValue::String(val) => AnyValue::String(val.clone()),
            AnyValue::Raw(val) => AnyValue::Raw(val.clone()),
            AnyValue::Dynamic(any) => AnyValue::Dynamic(any.clone()),
            AnyValue::Vec(data, interface) => {
                AnyValue::Vec(interface.clone_data(&**data), *interface)
            }
            AnyValue::Map(data, interface) => {
                AnyValue::Map(interface.clone_data(&**data), *interface)
            }
            AnyValue::Option(data, interface) => {
                AnyValue::Option(interface.clone_data(&**data), *interface)
            }
            AnyValue::Tuple(data, interface) => {
                AnyValue::Tuple(interface.clone_data(&**data), *interface)
            }
            AnyValue::Enum(data, interface) => {
                AnyValue::Enum(interface.clone_data(&**data), *interface)
            }
            AnyValue::Object(data, interface) => {
                AnyValue::Object(interface.clone_data(&**data), *interface)
            }
        }
    }
}

impl std::fmt::Debug for AnyValue {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.as_anyref(), f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn downcast() {
        let v = AnyValue::Bool(true);
        v.downcast::<bool>().unwrap();

        let v = AnyValue::String("text".to_string());
        v.downcast::<String>().unwrap();

        let v = AnyValue::Dynamic(Box::new(AnyValue::Bool(true)));
        v.downcast::<AnyValue>().unwrap();
    }

    macro_rules! impl_downcast_test {
        ($name:ident, $t:ty) => {
            #[test]
            fn $name() {
                let v = <$t as Typed>::new_anyvalue();
                v.downcast::<$t>().unwrap();
            }
        };
    }

    // unit
    impl_downcast_test!(downcast_unit, ());
    // bool
    impl_downcast_test!(downcast_bool, bool);
    // signed integer
    impl_downcast_test!(downcast_i8, i8);
    impl_downcast_test!(downcast_i16, i16);
    impl_downcast_test!(downcast_i32, i32);
    impl_downcast_test!(downcast_i64, i64);
    // unsigned integer
    impl_downcast_test!(downcast_u8, u8);
    impl_downcast_test!(downcast_u16, u16);
    impl_downcast_test!(downcast_u32, u32);
    impl_downcast_test!(downcast_u64, u64);
    // float
    impl_downcast_test!(downcast_f32, f32);
    impl_downcast_test!(downcast_f64, f64);
    // string
    impl_downcast_test!(downcast_string, String);
    // buffer
    impl_downcast_test!(downcast_raw, crate::Buffer);
    // dynamic
    impl_downcast_test!(downcast_dynamic, AnyValue);
}
