use crate::{AnyError, Result};

/// Describe an `AnyValue` content
///
/// Used by the qi-messaging protocol to describe structures and functions parameters and return type.
///
/// Examples:
/// - `b` : `bool`
/// - `i` : `i32`
/// - `+i` : `Option<i32>`
/// - `(ib)` : `(i32, bool)`
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Signature {
    sig: String,
}

impl Signature {
    /// Create a checked signature
    ///
    /// # Error
    ///
    /// This function will return a `AnyError::InvalidSignature` if it fails to validate
    pub fn new(sig: String) -> Result<Signature> {
        match parser::validate(&sig) {
            Ok(_) => Ok(Signature { sig }),
            Err(_) => Err(AnyError::InvalidSignature(sig)),
        }
    }

    /// Returns the string representation of the signature
    pub fn as_string(&self) -> &str {
        &self.sig
    }

    /// Returns a graph representation of the signature
    pub fn to_graph(&self) -> SignatureGraph {
        parser::to_graph(&self.sig)
            .expect("a valid signature should always be convertible to a graph")
            .1
    }

    /// Checks that the signature does not contain any unknown
    pub fn is_valid(&self) -> bool {
        !self.sig.contains('_') && !self.sig.contains('X')
    }

    /// Constructs the signature of an option of `elem`
    pub fn make_optional(elem: &Signature) -> Signature {
        Signature {
            sig: format!("+{}", elem.sig),
        }
    }

    /// Constructs the signature of a vector of `elem`
    pub fn make_vec(elem: &Signature) -> Signature {
        Signature {
            sig: format!("[{}]", elem.sig),
        }
    }

    /// Constructs the signature of a map of `key` => `value`
    pub fn make_map(key: &Signature, value: &Signature) -> Signature {
        Signature {
            sig: format!("{{{}{}}}", key.sig, value.sig),
        }
    }

    /// Constructs the signature of a tuple
    pub fn make_tuple(fields: &[&Signature]) -> Signature {
        let len: usize = fields.iter().map(|s| s.sig.len()).sum();
        let mut sig = String::with_capacity(len + 2);
        sig += "(";
        let mut sig = fields.iter().fold(sig, |res, field| res + &field.sig);
        sig += ")";

        Signature { sig }
    }

    /// Constructs the signature of a struct
    ///
    /// # Errors
    ///
    /// Returns `AnyError::InvalidSignatureName` if one of the name is not a valid name.
    /// Names can only contains ascii alphnumeric characters and '_'
    ///
    /// Returns `AnyError::InvalidSignatureNameCount` if `fields.len() != names.len()`
    pub fn make_struct<S: AsRef<str>>(
        fields: &[&Signature],
        name: S,
        names: &[S],
    ) -> Result<Signature> {
        if fields.len() != names.len() {
            return Err(AnyError::InvalidSignatureNameCount);
        }

        let name = name.as_ref();
        if !name.chars().all(parser::is_valid_name_char) {
            return Err(AnyError::InvalidSignatureName(name.to_owned()));
        }

        for name in names {
            let name = name.as_ref();
            if !name.chars().all(parser::is_valid_name_char) {
                return Err(AnyError::InvalidSignatureName(name.to_owned()));
            }
        }

        let mut sig = String::new();
        sig += "(";
        let mut sig = fields.iter().fold(sig, |res, field| res + &field.sig);
        sig += ")<";
        sig += name;
        let mut sig = names
            .iter()
            .fold(sig, |res, name| res + "," + name.as_ref());
        sig += ">";
        Ok(Signature { sig })
    }
}

/// An AST representation of a `Signature`
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum SignatureGraph {
    Unknown,
    None,
    Unit,
    Bool,
    Int8,
    UInt8,
    Int16,
    UInt16,
    Int32,
    UInt32,
    Int64,
    UInt64,
    Float,
    Double,
    String,
    List(Box<SignatureGraph>),
    Map(Box<SignatureGraph>, Box<SignatureGraph>),
    Tuple(Vec<SignatureGraph>, Option<(String, Vec<String>)>),
    Dynamic,
    Raw,
    Object,
    Optional(Box<SignatureGraph>),
    // Pointer, VarArgs, Tag::KwArgs,
}

impl SignatureGraph {
    /// Convert the signature graph into a signature
    pub fn to_signature(&self) -> Signature {
        match self {
            SignatureGraph::Unknown => Signature {
                sig: "X".to_string(),
            },
            SignatureGraph::None => Signature {
                sig: "_".to_string(),
            },
            SignatureGraph::Unit => Signature {
                sig: "v".to_string(),
            },
            SignatureGraph::Bool => Signature {
                sig: "b".to_string(),
            },
            SignatureGraph::Int8 => Signature {
                sig: "c".to_string(),
            },
            SignatureGraph::UInt8 => Signature {
                sig: "C".to_string(),
            },
            SignatureGraph::Int16 => Signature {
                sig: "w".to_string(),
            },
            SignatureGraph::UInt16 => Signature {
                sig: "W".to_string(),
            },
            SignatureGraph::Int32 => Signature {
                sig: "i".to_string(),
            },
            SignatureGraph::UInt32 => Signature {
                sig: "I".to_string(),
            },
            SignatureGraph::Int64 => Signature {
                sig: "l".to_string(),
            },
            SignatureGraph::UInt64 => Signature {
                sig: "L".to_string(),
            },
            SignatureGraph::Float => Signature {
                sig: "f".to_string(),
            },
            SignatureGraph::Double => Signature {
                sig: "d".to_string(),
            },
            SignatureGraph::String => Signature {
                sig: "s".to_string(),
            },
            SignatureGraph::List(elem) => Signature {
                sig: format!("[{}]", elem.to_signature().sig),
            },
            SignatureGraph::Map(key, value) => Signature {
                sig: format!("{{{}{}}}", key.to_signature().sig, value.to_signature().sig),
            },
            SignatureGraph::Tuple(elems, names) => Signature {
                sig: {
                    let elems = elems
                        .iter()
                        .fold(String::new(), |s, e| s + &e.to_signature().sig);
                    match names {
                        Some((name, names)) => {
                            let names = names.iter().fold(String::new(), |s, e| s + "," + e);
                            format!("({})<{}{}>", elems, name, names)
                        }
                        None => format!("({})", elems),
                    }
                },
            },
            SignatureGraph::Dynamic => Signature {
                sig: "m".to_string(),
            },
            SignatureGraph::Raw => Signature {
                sig: "r".to_string(),
            },
            SignatureGraph::Object => Signature {
                sig: "o".to_string(),
            },
            SignatureGraph::Optional(elem) => Signature {
                sig: format!("+{}", elem.to_signature().sig),
            },
        }
    }
}

mod parser {
    use super::SignatureGraph;
    use nom::{
        branch::alt, bytes::complete::take_while1, character::complete, combinator::opt, multi,
        sequence, IResult,
    };

    pub fn end_of_string(s: &str) -> IResult<&str, ()> {
        if !s.is_empty() {
            Err(nom::Err::Error(nom::error::make_error(
                s,
                nom::error::ErrorKind::Eof,
            )))
        } else {
            Ok((s, ()))
        }
    }

    pub fn validate(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = validate_type(sig)?;
        end_of_string(sig)
    }

    fn validate_type(sig: &str) -> IResult<&str, ()> {
        alt((
            validate_builtin,
            validate_list,
            validate_map,
            validate_tuple,
            validate_option,
        ))(sig)
    }

    fn validate_builtin(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = complete::one_of("_vbcwilCWILfdsmroX")(sig)?;
        Ok((sig, ()))
    }

    fn validate_option(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = complete::char('+')(sig)?;
        let (sig, _) = validate_type(sig)?;
        Ok((sig, ()))
    }

    fn validate_list(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = complete::char('[')(sig)?;
        let (sig, _) = validate_type(sig)?;
        let (sig, _) = complete::char(']')(sig)?;
        Ok((sig, ()))
    }

    fn validate_map(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = complete::char('{')(sig)?;
        let (sig, _) = validate_type(sig)?;
        let (sig, _) = validate_type(sig)?;
        let (sig, _) = complete::char('}')(sig)?;
        Ok((sig, ()))
    }

    fn validate_tuple(sig: &str) -> IResult<&str, ()> {
        let (sig, _) = complete::char('(')(sig)?;
        let (sig, inners) = multi::many1(validate_type)(sig)?;
        let (sig, _) = complete::char(')')(sig)?;
        let (sig, _) = opt(validate_tuple_names(inners.len()))(sig)?;
        Ok((sig, ()))
    }

    pub fn is_valid_name_char(c: char) -> bool {
        (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_'
    }

    fn validate_tuple_names(count: usize) -> impl Fn(&str) -> IResult<&str, ()> {
        move |sig| {
            let (sig, _) = complete::char('<')(sig)?;
            // read class name
            // IMPROVE support template class name
            let (sig, _) = take_while1(is_valid_name_char)(sig)?;
            // read field names
            let (sig, _) = multi::count(
                sequence::tuple((complete::char(','), take_while1(is_valid_name_char))),
                count,
            )(sig)?;
            let (sig, _) = complete::char('>')(sig)?;
            Ok((sig, ()))
        }
    }

    pub fn to_graph(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, g) = to_type(sig)?;
        end_of_string(sig)?;
        Ok((sig, g))
    }

    fn to_type(sig: &str) -> IResult<&str, SignatureGraph> {
        alt((to_builtin, to_list, to_map, to_tuple, to_option))(sig)
    }

    fn to_builtin(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, c) = complete::one_of("_vbcwilCWILfdsmroX")(sig)?;
        use SignatureGraph as SG;
        let g = match c {
            'b' => SG::Bool,
            'c' => SG::Int8,
            'w' => SG::Int16,
            'i' => SG::Int32,
            'l' => SG::Int64,
            'C' => SG::UInt8,
            'W' => SG::UInt16,
            'I' => SG::UInt32,
            'L' => SG::UInt64,
            'f' => SG::Float,
            'd' => SG::Double,
            's' => SG::String,
            'm' => SG::Dynamic,
            'r' => SG::Raw,
            'o' => SG::Object,
            'v' => SG::Unit,
            '_' => SG::None,
            'X' => SG::Unknown,
            _ => unreachable!(),
        };
        Ok((sig, g))
    }

    fn to_list(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, _) = complete::char('[')(sig)?;
        let (sig, elem) = to_type(sig)?;
        let (sig, _) = complete::char(']')(sig)?;
        Ok((sig, SignatureGraph::List(Box::new(elem))))
    }

    fn to_map(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, _) = complete::char('{')(sig)?;
        let (sig, key) = to_type(sig)?;
        let (sig, val) = to_type(sig)?;
        let (sig, _) = complete::char('}')(sig)?;
        Ok((sig, SignatureGraph::Map(Box::new(key), Box::new(val))))
    }

    fn to_tuple(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, _) = complete::char('(')(sig)?;
        let (sig, fields) = multi::many1(to_type)(sig)?;
        let (sig, _) = complete::char(')')(sig)?;
        let (sig, names) = opt(read_tuple_names(fields.len()))(sig)?;
        Ok((sig, SignatureGraph::Tuple(fields, names)))
    }

    fn read_tuple_names(count: usize) -> impl Fn(&str) -> IResult<&str, (String, Vec<String>)> {
        move |sig| {
            let (sig, _) = complete::char('<')(sig)?;
            // read class name
            // IMPROVE support template class name
            let (sig, name) = take_while1(is_valid_name_char)(sig)?;
            // read field names
            let (sig, names) = multi::count(
                sequence::tuple((complete::char(','), take_while1(is_valid_name_char))),
                count,
            )(sig)?;
            let (sig, _) = complete::char('>')(sig)?;

            let names = names.iter().map(|(_, name)| (*name).to_string()).collect();
            Ok((sig, (name.to_string(), names)))
        }
    }

    fn to_option(sig: &str) -> IResult<&str, SignatureGraph> {
        let (sig, _) = complete::char('+')(sig)?;
        let (sig, elem) = to_type(sig)?;
        Ok((sig, SignatureGraph::Optional(Box::new(elem))))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use SignatureGraph as SG;

    #[test]
    fn validate_builtin() {
        // unit
        parser::validate("v").unwrap();
        // bool
        parser::validate("b").unwrap();
        // unsigned integers
        parser::validate("c").unwrap();
        parser::validate("w").unwrap();
        parser::validate("i").unwrap();
        parser::validate("l").unwrap();
        // signed integers
        parser::validate("C").unwrap();
        parser::validate("W").unwrap();
        parser::validate("I").unwrap();
        parser::validate("L").unwrap();
        // floats
        parser::validate("f").unwrap();
        parser::validate("d").unwrap();
        // string
        parser::validate("s").unwrap();
        // dynamic
        parser::validate("m").unwrap();
        // object
        parser::validate("o").unwrap();
        // raw
        parser::validate("r").unwrap();
        // unknown
        parser::validate("_").unwrap();
        parser::validate("X").unwrap();
    }

    #[test]
    fn validate_simple_vec() {
        parser::validate("[i]").unwrap();
        parser::validate("[o]").unwrap();
        parser::validate("[b]").unwrap();
    }

    #[test]
    fn validate_complex_vec() {
        parser::validate("[+i]").unwrap();
        parser::validate("[[l]]").unwrap();
        parser::validate("[{is}]").unwrap();
    }

    #[test]
    fn validate_simple_map() {
        parser::validate("{is}").unwrap();
        parser::validate("{so}").unwrap();
        parser::validate("{lb}").unwrap();
    }

    #[test]
    fn validate_complex_map() {
        parser::validate("{i[s]}").unwrap();
        parser::validate("{s{is}}").unwrap();
    }

    #[test]
    fn validate_simple_option() {
        parser::validate("+i").unwrap();
        parser::validate("+o").unwrap();
        parser::validate("+b").unwrap();
    }

    #[test]
    fn validate_complex_option() {
        parser::validate("+[i]").unwrap();
        parser::validate("+{is}").unwrap();
    }

    #[test]
    fn validate_simple_tuple() {
        parser::validate("(i)").unwrap();
        parser::validate("(so)").unwrap();
        parser::validate("(Issb)").unwrap();
    }

    #[test]
    fn validate_complex_tuple() {
        parser::validate("(+i)").unwrap();
        parser::validate("(s[o])").unwrap();
        parser::validate("({Is}sb)").unwrap();
    }

    #[test]
    fn validate_simple_struct() {
        parser::validate("(ll)<Point,x,y>").unwrap();
        parser::validate("(s)<Signature,text>").unwrap();
    }

    #[test]
    fn graph_builtin() {
        // unit
        assert_eq!(parser::to_graph("v").unwrap().1, SG::Unit);
        // bool
        assert_eq!(parser::to_graph("b").unwrap().1, SG::Bool);
        // unsigned integers
        assert_eq!(parser::to_graph("c").unwrap().1, SG::Int8);
        assert_eq!(parser::to_graph("w").unwrap().1, SG::Int16);
        assert_eq!(parser::to_graph("i").unwrap().1, SG::Int32);
        assert_eq!(parser::to_graph("l").unwrap().1, SG::Int64);
        // signed integers
        assert_eq!(parser::to_graph("C").unwrap().1, SG::UInt8);
        assert_eq!(parser::to_graph("W").unwrap().1, SG::UInt16);
        assert_eq!(parser::to_graph("I").unwrap().1, SG::UInt32);
        assert_eq!(parser::to_graph("L").unwrap().1, SG::UInt64);
        // floats
        assert_eq!(parser::to_graph("f").unwrap().1, SG::Float);
        assert_eq!(parser::to_graph("d").unwrap().1, SG::Double);
        // string
        assert_eq!(parser::to_graph("s").unwrap().1, SG::String);
        // dynamic
        assert_eq!(parser::to_graph("m").unwrap().1, SG::Dynamic);
        // object
        assert_eq!(parser::to_graph("o").unwrap().1, SG::Object);
        // raw
        assert_eq!(parser::to_graph("r").unwrap().1, SG::Raw);
        // unknown
        assert_eq!(parser::to_graph("_").unwrap().1, SG::None);
        assert_eq!(parser::to_graph("X").unwrap().1, SG::Unknown);
    }

    #[test]
    fn graph_simple_vec() {
        assert_eq!(
            parser::to_graph("[i]").unwrap().1,
            SG::List(Box::new(SG::Int32))
        );
        assert_eq!(
            parser::to_graph("[o]").unwrap().1,
            SG::List(Box::new(SG::Object))
        );
        assert_eq!(
            parser::to_graph("[b]").unwrap().1,
            SG::List(Box::new(SG::Bool))
        );
    }

    #[test]
    fn graph_complex_vec() {
        assert_eq!(
            parser::to_graph("[+i]").unwrap().1,
            SG::List(Box::new(SG::Optional(Box::new(SG::Int32))))
        );
        assert_eq!(
            parser::to_graph("[[l]]").unwrap().1,
            SG::List(Box::new(SG::List(Box::new(SG::Int64))))
        );
        assert_eq!(
            parser::to_graph("[{is}]").unwrap().1,
            SG::List(Box::new(SG::Map(Box::new(SG::Int32), Box::new(SG::String))))
        );
    }

    #[test]
    fn graph_simple_map() {
        assert_eq!(
            parser::to_graph("{is}").unwrap().1,
            SG::Map(Box::new(SG::Int32), Box::new(SG::String))
        );
        assert_eq!(
            parser::to_graph("{so}").unwrap().1,
            SG::Map(Box::new(SG::String), Box::new(SG::Object))
        );
        assert_eq!(
            parser::to_graph("{lb}").unwrap().1,
            SG::Map(Box::new(SG::Int64), Box::new(SG::Bool))
        );
    }

    #[test]
    fn graph_complex_map() {
        assert_eq!(
            parser::to_graph("{i[s]}").unwrap().1,
            SG::Map(
                Box::new(SG::Int32),
                Box::new(SG::List(Box::new(SG::String)))
            )
        );
        assert_eq!(
            parser::to_graph("{s{is}}").unwrap().1,
            SG::Map(
                Box::new(SG::String),
                Box::new(SG::Map(Box::new(SG::Int32), Box::new(SG::String)))
            )
        );
    }

    #[test]
    fn graph_simple_option() {
        assert_eq!(
            parser::to_graph("+i").unwrap().1,
            SG::Optional(Box::new(SG::Int32))
        );
        assert_eq!(
            parser::to_graph("+o").unwrap().1,
            SG::Optional(Box::new(SG::Object))
        );
        assert_eq!(
            parser::to_graph("+b").unwrap().1,
            SG::Optional(Box::new(SG::Bool))
        );
    }

    #[test]
    fn graph_complex_option() {
        assert_eq!(
            parser::to_graph("+[i]").unwrap().1,
            SG::Optional(Box::new(SG::List(Box::new(SG::Int32))))
        );
        assert_eq!(
            parser::to_graph("+{is}").unwrap().1,
            SG::Optional(Box::new(SG::Map(Box::new(SG::Int32), Box::new(SG::String))))
        );
    }

    #[test]
    fn graph_simple_tuple() {
        assert_eq!(
            parser::to_graph("(i)").unwrap().1,
            SG::Tuple(vec![SG::Int32], None)
        );
        assert_eq!(
            parser::to_graph("(so)").unwrap().1,
            SG::Tuple(vec![SG::String, SG::Object], None)
        );
        assert_eq!(
            parser::to_graph("(Issb)").unwrap().1,
            SG::Tuple(vec![SG::UInt32, SG::String, SG::String, SG::Bool,], None)
        );
    }

    #[test]
    fn graph_complex_tuple() {
        assert_eq!(
            parser::to_graph("(+i)").unwrap().1,
            SG::Tuple(vec![SG::Optional(Box::new(SG::Int32))], None)
        );
        assert_eq!(
            parser::to_graph("(s[o])").unwrap().1,
            SG::Tuple(vec![SG::String, SG::List(Box::new(SG::Object))], None)
        );
    }

    #[test]
    fn graph_simple_struct() {
        assert_eq!(
            parser::to_graph("(ll)<Point,x,y>").unwrap().1,
            SG::Tuple(
                vec![SG::Int64, SG::Int64,],
                Some(("Point".to_string(), vec!["x".to_string(), "y".to_string()]))
            )
        );
        assert_eq!(
            parser::to_graph("(s)<Signature,text>").unwrap().1,
            SG::Tuple(
                vec![SG::String],
                Some(("Signature".to_string(), vec!["text".to_string()]))
            )
        );
    }

    #[test]
    fn make_optional() {
        let elem = Signature::new("i".to_string()).unwrap();
        let sig = Signature::make_optional(&elem);
        assert_eq!("+i", sig.as_string());

        let elem = Signature::new("(is)".to_string()).unwrap();
        let sig = Signature::make_optional(&elem);
        assert_eq!("+(is)", sig.as_string());
    }

    #[test]
    fn make_list() {
        let elem = Signature::new("i".to_string()).unwrap();
        let sig = Signature::make_vec(&elem);
        assert_eq!("[i]", sig.as_string());

        let elem = Signature::new("(is)".to_string()).unwrap();
        let sig = Signature::make_vec(&elem);
        assert_eq!("[(is)]", sig.as_string());
    }

    #[test]
    fn make_map() {
        let key = Signature::new("i".to_string()).unwrap();
        let value = Signature::new("i".to_string()).unwrap();
        let sig = Signature::make_map(&key, &value);
        assert_eq!("{ii}", sig.as_string());

        let key = Signature::new("s".to_string()).unwrap();
        let value = Signature::new("[i]".to_string()).unwrap();
        let sig = Signature::make_map(&key, &value);
        assert_eq!("{s[i]}", sig.as_string());
    }

    #[test]
    fn make_tuple() {
        let elem_1 = Signature::new("i".to_string()).unwrap();
        let elem_2 = Signature::new("s".to_string()).unwrap();
        let elem_3 = Signature::new("o".to_string()).unwrap();
        let sig = Signature::make_tuple(&[&elem_1, &elem_2, &elem_3]);
        assert_eq!("(iso)", sig.as_string());

        let elem_1 = Signature::new("m".to_string()).unwrap();
        let elem_2 = Signature::new("[s]".to_string()).unwrap();
        let elem_3 = Signature::new("{io}".to_string()).unwrap();
        let sig = Signature::make_tuple(&[&elem_1, &elem_2, &elem_3]);
        assert_eq!("(m[s]{io})", sig.as_string());
    }

    #[test]
    fn make_struct() {
        let elem_1 = Signature::new("i".to_string()).unwrap();
        let elem_2 = Signature::new("s".to_string()).unwrap();
        let elem_3 = Signature::new("o".to_string()).unwrap();
        let sig = Signature::make_struct(
            &[&elem_1, &elem_2, &elem_3],
            "MyType",
            &["val", "name", "object"],
        )
        .unwrap();
        assert_eq!("(iso)<MyType,val,name,object>", sig.as_string());
    }
}
