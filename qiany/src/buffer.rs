use crate::{
    AnyRef, AnyRefMut, AnyValue, Signature, ToAnyValue, TypeInterface, TypeInterfacePtr, Typed,
};

#[derive(Default, Clone, Debug, PartialEq, Eq)]
pub struct Buffer(pub Vec<u8>);

impl Buffer {
    pub fn new() -> Buffer {
        Buffer::default()
    }

    pub fn from(vec: Vec<u8>) -> Buffer {
        Buffer(vec)
    }

    pub fn into_inner(self) -> Vec<u8> {
        self.0
    }
}

impl std::ops::Deref for Buffer {
    type Target = Vec<u8>;
    fn deref(&self) -> &Vec<u8> {
        &self.0
    }
}

impl std::ops::DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut Vec<u8> {
        &mut self.0
    }
}

impl ToAnyValue for Buffer {
    fn into_anyvalue(self) -> AnyValue {
        AnyValue::Raw(self)
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        AnyRef::Raw(&self)
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        AnyRefMut::Raw(self)
    }
}

struct BufferTypeInterface {
    signature: Signature,
}

impl BufferTypeInterface {
    fn get() -> TypeInterfacePtr {
        use once_cell::sync::OnceCell;
        static INTERFACE: OnceCell<TypeInterfacePtr> = OnceCell::new();
        *INTERFACE.get_or_init(|| {
            let b = Box::new(BufferTypeInterface {
                signature: Signature::new("r".to_string()).unwrap(),
            });
            Box::leak(b) as &'static _
        })
    }
}

impl TypeInterface for BufferTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        AnyValue::Raw(Buffer::default())
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl Typed for Buffer {
    fn type_interface() -> TypeInterfacePtr {
        BufferTypeInterface::get()
    }
}
