#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum AnyKind {
    Unit,
    Bool,
    Integer,
    Float,
    String,
    Vec,
    Map,
    Option,
    Tuple,
    Raw,
    Enum,
    Dynamic,
    Object,
}
