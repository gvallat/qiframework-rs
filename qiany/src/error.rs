use crate::Signature;

pub enum SerializationError {
    SizeDoesNotFitU32(usize),
    WriteFail(std::io::Error),
    EndOfBuffer,
}

pub enum DeserializationError {
    ReadFail(std::io::Error),
    InvalidUtf8String,
    EndOfBuffer,
}

pub enum AnyError {
    InvalidOperation,
    OutOfRange,
    InvalidPrint,
    InvalidConversion,
    UnexpectedType, // Add expected and actual ?
    UnsupportedType,
    IntegerDoesNotFit,
    ObjectNotRegistered,
    InvalidSignature(String),
    InvalidSignatureName(String),
    InvalidSignatureNameCount,
    Serialization(SerializationError),
    Deserialization(DeserializationError),
    InvalidDeserialization(&'static Signature, &'static Signature),
}

impl std::fmt::Display for AnyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AnyError::InvalidOperation => f.write_str("invalid operation"),
            AnyError::OutOfRange => f.write_str("out of range acces"),
            AnyError::InvalidPrint => f.write_str("invalid cast during debug print"),
            AnyError::InvalidConversion => f.write_str("invalid conversion"),
            AnyError::UnexpectedType => f.write_str("unexpected type"),
            AnyError::UnsupportedType => f.write_str("unsupported type"),
            AnyError::IntegerDoesNotFit => f.write_str("integer doesn't fit"),
            AnyError::ObjectNotRegistered => {
                f.write_str("object type interface was not registered")
            }
            AnyError::InvalidSignature(sig) => write!(f, "invalid signature ({})", sig),
            AnyError::InvalidSignatureName(name) => {
                write!(f, "invalid name for signature ({})", name)
            }
            AnyError::InvalidSignatureNameCount => {
                f.write_str("name and field counts do not match")
            }
            AnyError::Serialization(error) => {
                f.write_str("serialization error: ")?;
                match error {
                    SerializationError::SizeDoesNotFitU32(size) => {
                        write!(f, "container size doesn't fit in u32 ({})", size)
                    }
                    SerializationError::WriteFail(error) => write!(f, "write failure ({})", error),
                    SerializationError::EndOfBuffer => write!(f, "unexpected end of buffer"),
                }
            }
            AnyError::Deserialization(error) => {
                f.write_str("deserialization error: ")?;
                match error {
                    DeserializationError::ReadFail(error) => write!(f, "read failure ({})", error),
                    DeserializationError::EndOfBuffer => write!(f, "unexpected end of buffer"),
                    DeserializationError::InvalidUtf8String => write!(f, "invalid utf8 string"),
                }
            }
            AnyError::InvalidDeserialization(from, to) => write!(
                f,
                "invalid convertion from '{}' to '{}'",
                from.as_string(),
                to.as_string()
            ),
        }
    }
}

impl std::fmt::Debug for AnyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Display>::fmt(self, f)
    }
}

impl std::error::Error for AnyError {}

pub type Result<T> = std::result::Result<T, AnyError>;
