use crate::{
    interfaces::{
        DynamicMapTypeInterface, DynamicOptionTypeInterface, DynamicTupleTypeInterface,
        DynamicVecTypeInterface,
    },
    AnyError, AnyValue, Buffer, Result, Signature, SignatureGraph, TypeInterfacePtr, Typed,
};
use once_cell::sync::OnceCell;
use std::{collections::HashMap, sync::Mutex};

/// A Registry for `TypeInterfacePtr`
///
/// It is used to find a `TypeInterface` based on a signature when converting
/// a `qi-messaging` buffer into an `AnyValue`.
///
/// It should not be use directly. Instead the global Registry should be used
/// through `qiany::register_type()` and `qiany::type_interface_for()`.
pub struct Registry {
    interfaces: HashMap<Signature, TypeInterfacePtr>,
    dynamic_interfaces: HashMap<Signature, TypeInterfacePtr>,
}

impl Registry {
    /// Creates a new registery
    ///
    /// Builtin types are automatically registered.
    /// Only object ('o') is not automatically registered.
    pub fn new() -> Registry {
        let mut registry = Registry {
            interfaces: HashMap::new(),
            dynamic_interfaces: HashMap::new(),
        };

        // unit
        registry.register_type(<()>::type_interface());
        // bool
        registry.register_type(bool::type_interface());
        // signed integer
        registry.register_type(i8::type_interface());
        registry.register_type(i16::type_interface());
        registry.register_type(i32::type_interface());
        registry.register_type(i64::type_interface());
        // unsigned integer
        registry.register_type(u8::type_interface());
        registry.register_type(u16::type_interface());
        registry.register_type(u32::type_interface());
        registry.register_type(u64::type_interface());
        // unsigned integer
        registry.register_type(f32::type_interface());
        registry.register_type(f64::type_interface());
        // string
        registry.register_type(String::type_interface());
        // raw
        registry.register_type(Buffer::type_interface());
        // dynamic
        registry.register_type(AnyValue::type_interface());

        registry
    }

    /// Register a new type interface.
    ///
    /// A type should only be registered once.
    /// Signature should be unique.
    ///
    /// # Enums
    ///
    /// Interface containing enums will not be registered, as they might cause data lost.
    ///
    /// Both `Vec<u32>` and `Vec<MyEnum>` (where MyEnum use repr(u32)) have the signature `[I]`.
    /// When deserializing a dynamic value from binary format we want to deserialize into a `Vec<u32>`
    /// as it is the general case, it will be converted into `Vec<MyEnum>` down the line if need be.
    /// Deserializing into an `MyEnum` where we do expect a `u32` would result in data lost (as all
    /// unknown values would have been converted to the default variant for that enum).
    pub fn register_type(&mut self, interface: TypeInterfacePtr) {
        if !interface.contains_enum() {
            // TODO check if not something is already registered with the same signature (error)
            self.interfaces
                .insert(interface.signature().clone(), interface);
        }
    }

    /// Find the TypeInterfacePtr for the given signature.
    ///
    /// If no type was registered with this signature, it will create a dynamic type
    /// (aka type based on vectors of `AnyValue`).
    ///
    /// # Fails
    ///
    /// * If the signature is not value.
    /// * If the signature contains an object ('o') and no type interface was registered for objects.
    pub fn get(&mut self, signature: &Signature) -> Result<TypeInterfacePtr> {
        match self.interfaces.get(signature) {
            Some(interface) => Ok(*interface),
            None => match self.dynamic_interfaces.get(signature) {
                Some(interface) => Ok(*interface),
                None => {
                    if signature.as_string() == "o" {
                        return Err(AnyError::ObjectNotRegistered);
                    }

                    use SignatureGraph as SG;
                    match signature.to_graph() {
                        SG::List(inner) => {
                            let inner = self.get(&inner.to_signature())?;
                            let interface = DynamicVecTypeInterface::make(inner);
                            self.register_dynamic_type(interface);
                            Ok(interface)
                        }
                        SG::Map(key, value) => {
                            let key = self.get(&key.to_signature())?;
                            let value = self.get(&value.to_signature())?;
                            let interface = DynamicMapTypeInterface::make(key, value);
                            self.register_dynamic_type(interface);
                            Ok(interface)
                        }
                        SG::Tuple(elements, names) => {
                            let mut interfaces = Vec::with_capacity(elements.len());
                            for elem in elements {
                                interfaces.push(self.get(&elem.to_signature())?);
                            }
                            let interface = DynamicTupleTypeInterface::make(interfaces, names);
                            self.register_dynamic_type(interface);
                            Ok(interface)
                        }
                        SG::Optional(inner) => {
                            let inner = self.get(&inner.to_signature())?;
                            let interface = DynamicOptionTypeInterface::make(inner);
                            self.register_dynamic_type(interface);
                            Ok(interface)
                        }
                        SG::None | SG::Unknown => {
                            Err(AnyError::InvalidSignature(signature.as_string().to_owned()))
                        }
                        _ => unreachable!("all builtin types should be registered"),
                    }
                }
            },
        }
    }

    fn register_dynamic_type(&mut self, interface: TypeInterfacePtr) {
        self.dynamic_interfaces
            .insert(interface.signature().clone(), interface);
    }
}

static REGISTRY: OnceCell<Mutex<Registry>> = OnceCell::new();

/// Calls Registry::register_type(interface) on the global registry.
pub fn register_type(interface: TypeInterfacePtr) {
    let mutex = REGISTRY.get_or_init(|| Mutex::new(Registry::new()));
    let mut registry = mutex.lock().unwrap();
    registry.register_type(interface);
}

/// Calls Registry::get(signature) on the global registry.
///
/// Prefer `select_type_interface_for()` whenever possible
pub fn type_interface_for(signature: &Signature) -> Result<TypeInterfacePtr> {
    let mutex = REGISTRY.get_or_init(|| Mutex::new(Registry::new()));
    let mut registry = mutex.lock().unwrap();
    registry.get(signature)
}

/// Returns the best type_interface matching the signature
///
/// If the `ExpectedType` signature match the `actual_signature`, its type interface
/// will be returned, otherwise `type_interface_for(actual_signature)` will be used
/// to find or created an adequate type interface.
///
/// This allow binary deserialization to use the optimal storage for the created AnyValue.
pub fn select_type_interface_for<ExpectedType: Typed>(
    actual_signature: &Signature,
) -> Result<TypeInterfacePtr> {
    // IMPROVE use equivalence instead of equality
    // (is) ~= (is)<Type,val,name>
    if ExpectedType::signature() == actual_signature {
        Ok(ExpectedType::type_interface())
    } else {
        type_interface_for(actual_signature)
    }
}
