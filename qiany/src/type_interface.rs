use crate::{AnyRef, AnyRefMut, AnyValue, Signature};

pub trait TypeInterface {
    fn new_anyvalue(&self) -> AnyValue;
    fn signature(&self) -> &Signature;
    fn contains_enum(&self) -> bool {
        false
    }
}

pub type TypeInterfacePtr = &'static (dyn TypeInterface + Sync);

pub trait Typed {
    fn type_interface() -> TypeInterfacePtr;

    fn new_anyvalue() -> AnyValue {
        Self::type_interface().new_anyvalue()
    }

    fn signature() -> &'static Signature {
        Self::type_interface().signature()
    }
}

// + Typed ?
pub trait ToAnyValue: Clone {
    fn into_anyvalue(self) -> AnyValue;
    fn as_anyref(&self) -> AnyRef<'_>;
    fn as_anyrefmut(&mut self) -> AnyRefMut<'_>;

    fn to_anyvalue(&self) -> AnyValue {
        self.clone().into_anyvalue()
    }
}
