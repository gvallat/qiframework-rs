use crate::{
    interfaces::{
        AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
        AnyTupleInterface, AnyVecInterface,
    },
    AnyRef, AnyRefMut, AnyValue, Buffer,
};
use std::any::Any;

mod from_binary;
mod to_binary;

pub use from_binary::FromBinaryDeserializer;
pub use to_binary::ToBinarySerializer;

pub trait Visitor<T>: Sized {
    fn visit(self, any: AnyRef<'_>) -> T {
        match any {
            AnyRef::Unit => self.visit_unit(),
            AnyRef::Bool(val) => self.visit_bool(*val),
            AnyRef::Int8(val) => self.visit_i8(*val),
            AnyRef::Int16(val) => self.visit_i16(*val),
            AnyRef::Int32(val) => self.visit_i32(*val),
            AnyRef::Int64(val) => self.visit_i64(*val),
            AnyRef::UInt8(val) => self.visit_u8(*val),
            AnyRef::UInt16(val) => self.visit_u16(*val),
            AnyRef::UInt32(val) => self.visit_u32(*val),
            AnyRef::UInt64(val) => self.visit_u64(*val),
            AnyRef::Float(val) => self.visit_f32(*val),
            AnyRef::Double(val) => self.visit_f64(*val),
            AnyRef::String(val) => self.visit_str(&*val),
            AnyRef::Raw(val) => self.visit_raw(&*val),
            AnyRef::Dynamic(any) => self.visit_dynamic(any),
            AnyRef::Vec(data, interface) => self.visit_vec(data, interface),
            AnyRef::Map(data, interface) => self.visit_map(data, interface),
            AnyRef::Option(data, interface) => self.visit_option(data, interface),
            AnyRef::Tuple(data, interface) => self.visit_tuple(data, interface),
            AnyRef::Enum(data, interface) => self.visit_enum(data, interface),
            AnyRef::Object(data, interface) => self.visit_object(data, interface),
        }
    }

    fn visit_unit(self) -> T;
    fn visit_bool(self, val: bool) -> T;
    fn visit_i8(self, val: i8) -> T;
    fn visit_i16(self, val: i16) -> T;
    fn visit_i32(self, val: i32) -> T;
    fn visit_i64(self, val: i64) -> T;
    fn visit_u8(self, val: u8) -> T;
    fn visit_u16(self, val: u16) -> T;
    fn visit_u32(self, val: u32) -> T;
    fn visit_u64(self, val: u64) -> T;
    fn visit_f32(self, val: f32) -> T;
    fn visit_f64(self, val: f64) -> T;
    fn visit_str(self, val: &str) -> T;
    fn visit_raw(self, val: &[u8]) -> T;
    fn visit_dynamic(self, val: &AnyValue) -> T;
    fn visit_vec(self, data: &dyn Any, interface: &'static (dyn AnyVecInterface + Sync)) -> T;
    fn visit_map(self, data: &dyn Any, interface: &'static (dyn AnyMapInterface + Sync)) -> T;
    fn visit_option(self, data: &dyn Any, interface: &'static (dyn AnyOptionInterface + Sync))
        -> T;
    fn visit_tuple(self, data: &dyn Any, interface: &'static (dyn AnyTupleInterface + Sync)) -> T;
    fn visit_enum(self, data: &dyn Any, interface: &'static (dyn AnyEnumInterface + Sync)) -> T;
    fn visit_object(self, data: &dyn Any, interface: &'static (dyn AnyObjectInterface + Sync))
        -> T;
}

pub trait VisitorMut<T>: Sized {
    fn visit(self, any: AnyRefMut<'_>) -> T {
        match any {
            AnyRefMut::Unit => self.visit_unit(),
            AnyRefMut::Bool(val) => self.visit_bool(val),
            AnyRefMut::Int8(val) => self.visit_i8(val),
            AnyRefMut::Int16(val) => self.visit_i16(val),
            AnyRefMut::Int32(val) => self.visit_i32(val),
            AnyRefMut::Int64(val) => self.visit_i64(val),
            AnyRefMut::UInt8(val) => self.visit_u8(val),
            AnyRefMut::UInt16(val) => self.visit_u16(val),
            AnyRefMut::UInt32(val) => self.visit_u32(val),
            AnyRefMut::UInt64(val) => self.visit_u64(val),
            AnyRefMut::Float(val) => self.visit_f32(val),
            AnyRefMut::Double(val) => self.visit_f64(val),
            AnyRefMut::String(val) => self.visit_str(val),
            AnyRefMut::Raw(val) => self.visit_raw(val),
            AnyRefMut::Dynamic(any) => self.visit_dynamic(any),
            AnyRefMut::Vec(data, interface) => self.visit_vec(data, interface),
            AnyRefMut::Map(data, interface) => self.visit_map(data, interface),
            AnyRefMut::Option(data, interface) => self.visit_option(data, interface),
            AnyRefMut::Tuple(data, interface) => self.visit_tuple(data, interface),
            AnyRefMut::Enum(data, interface) => self.visit_enum(data, interface),
            AnyRefMut::Object(data, interface) => self.visit_object(data, interface),
        }
    }

    fn visit_unit(self) -> T;
    fn visit_bool(self, val: &mut bool) -> T;
    fn visit_i8(self, val: &mut i8) -> T;
    fn visit_i16(self, val: &mut i16) -> T;
    fn visit_i32(self, val: &mut i32) -> T;
    fn visit_i64(self, val: &mut i64) -> T;
    fn visit_u8(self, val: &mut u8) -> T;
    fn visit_u16(self, val: &mut u16) -> T;
    fn visit_u32(self, val: &mut u32) -> T;
    fn visit_u64(self, val: &mut u64) -> T;
    fn visit_f32(self, val: &mut f32) -> T;
    fn visit_f64(self, val: &mut f64) -> T;
    fn visit_str(self, val: &mut String) -> T;
    fn visit_raw(self, val: &mut Buffer) -> T;
    fn visit_dynamic(self, val: &mut AnyValue) -> T;
    fn visit_vec(self, data: &mut dyn Any, interface: &'static (dyn AnyVecInterface + Sync)) -> T;
    fn visit_map(self, data: &mut dyn Any, interface: &'static (dyn AnyMapInterface + Sync)) -> T;
    fn visit_option(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyOptionInterface + Sync),
    ) -> T;
    fn visit_tuple(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyTupleInterface + Sync),
    ) -> T;
    fn visit_enum(self, data: &mut dyn Any, interface: &'static (dyn AnyEnumInterface + Sync))
        -> T;
    fn visit_object(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyObjectInterface + Sync),
    ) -> T;
}
