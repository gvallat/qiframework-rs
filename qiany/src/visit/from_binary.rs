use {
    crate::{
        interfaces::{
            AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
            AnyTupleInterface, AnyVecInterface, EnumRepr, EnumReprKind,
        },
        type_interface_for,
        visit::VisitorMut,
        AnyError, AnyValue, Buffer, DeserializationError, Result, Signature,
    },
    byteorder::{LittleEndian, ReadBytesExt},
    std::{
        any::Any,
        io::{Cursor, Read},
    },
};

pub struct FromBinaryDeserializer<'b, F> {
    buf: Cursor<&'b [u8]>,
    read_object: F,
}

impl<'b, F> FromBinaryDeserializer<'b, F>
where
    F: FnMut(
        &mut Cursor<&'b [u8]>,
        &mut dyn Any,
        &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()>,
{
    pub fn new(buf: &'b [u8], read_object: F) -> FromBinaryDeserializer<'b, F> {
        FromBinaryDeserializer {
            buf: Cursor::new(buf),
            read_object,
        }
    }

    pub fn is_eof(&self) -> bool {
        self.buf.position() == self.buf.get_ref().len() as u64
    }

    fn read_size(&mut self) -> Result<usize> {
        let val = self.buf.read_u32::<LittleEndian>().map_err(io_error)?;
        Ok(val as usize)
    }
}

fn io_error(error: std::io::Error) -> AnyError {
    AnyError::Deserialization(DeserializationError::ReadFail(error))
}

fn eof_error() -> AnyError {
    AnyError::Deserialization(DeserializationError::EndOfBuffer)
}

fn utf8_error(_: std::string::FromUtf8Error) -> AnyError {
    AnyError::Deserialization(DeserializationError::InvalidUtf8String)
}

impl<'b, F> VisitorMut<Result<()>> for &mut FromBinaryDeserializer<'b, F>
where
    F: FnMut(
        &mut Cursor<&'b [u8]>,
        &mut dyn Any,
        &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()>,
{
    fn visit_unit(self) -> Result<()> {
        Ok(())
    }

    fn visit_bool(self, val: &mut bool) -> Result<()> {
        let ok = self.buf.read_u8().map_err(io_error)?;
        if ok == 0 {
            *val = false;
        } else {
            *val = true;
        }
        Ok(())
    }

    fn visit_i8(self, val: &mut i8) -> Result<()> {
        *val = self.buf.read_i8().map_err(io_error)?;
        Ok(())
    }

    fn visit_i16(self, val: &mut i16) -> Result<()> {
        *val = self.buf.read_i16::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_i32(self, val: &mut i32) -> Result<()> {
        *val = self.buf.read_i32::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_i64(self, val: &mut i64) -> Result<()> {
        *val = self.buf.read_i64::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_u8(self, val: &mut u8) -> Result<()> {
        *val = self.buf.read_u8().map_err(io_error)?;
        Ok(())
    }

    fn visit_u16(self, val: &mut u16) -> Result<()> {
        *val = self.buf.read_u16::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_u32(self, val: &mut u32) -> Result<()> {
        *val = self.buf.read_u32::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_u64(self, val: &mut u64) -> Result<()> {
        *val = self.buf.read_u64::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_f32(self, val: &mut f32) -> Result<()> {
        *val = self.buf.read_f32::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_f64(self, val: &mut f64) -> Result<()> {
        *val = self.buf.read_f64::<LittleEndian>().map_err(io_error)?;
        Ok(())
    }

    fn visit_str(self, val: &mut String) -> Result<()> {
        let mut vec = Buffer::new();
        self.visit_raw(&mut vec)?;
        *val = String::from_utf8(vec.0).map_err(utf8_error)?;
        Ok(())
    }

    fn visit_raw(self, val: &mut Buffer) -> Result<()> {
        let len = self.read_size()?;
        val.0.resize(len, 0);
        let readen_len = self.buf.read(&mut val.0).map_err(io_error)?;
        if readen_len != len {
            Err(eof_error())
        } else {
            Ok(())
        }
    }

    fn visit_dynamic(self, val: &mut AnyValue) -> Result<()> {
        let mut sig = String::new();
        self.visit_str(&mut sig)?;
        let sig = Signature::new(sig)?;
        *val = type_interface_for(&sig)?.new_anyvalue();
        self.visit(val.as_anyrefmut())?;
        Ok(())
    }

    fn visit_vec(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyVecInterface + Sync),
    ) -> Result<()> {
        let len = self.read_size()?;
        interface.reserve(data, len);
        for _ in 0..len {
            let mut value = interface.new_element();
            self.visit(value.as_anyrefmut())?;
            interface.push(data, value)?;
        }
        Ok(())
    }

    fn visit_map(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyMapInterface + Sync),
    ) -> Result<()> {
        let len = self.read_size()?;
        interface.reserve(data, len);
        for _ in 0..len {
            let mut key = interface.new_key();
            self.visit(key.as_anyrefmut())?;
            let mut value = interface.new_value();
            self.visit(value.as_anyrefmut())?;
            interface.insert(data, key, value)?;
        }
        Ok(())
    }

    fn visit_option(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyOptionInterface + Sync),
    ) -> Result<()> {
        let is_some = self.buf.read_u8().map_err(io_error)?;
        if is_some != 0 {
            let mut value = interface.new_element();
            self.visit(value.as_anyrefmut())?;
            interface.set_some(data, value)?;
            Ok(())
        } else {
            Ok(())
        }
    }

    fn visit_tuple(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyTupleInterface + Sync),
    ) -> Result<()> {
        let len = interface.len();
        for idx in 0..len {
            let anyref = interface.get_mut(data, idx)?;
            self.visit(anyref)?;
        }
        Ok(())
    }

    fn visit_enum(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyEnumInterface + Sync),
    ) -> Result<()> {
        match interface.kind() {
            EnumReprKind::Int8 => {
                let val = self.buf.read_i8().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::Int8(val))?;
            }
            EnumReprKind::Int16 => {
                let val = self.buf.read_i16::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::Int16(val))?;
            }
            EnumReprKind::Int32 => {
                let val = self.buf.read_i32::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::Int32(val))?;
            }
            EnumReprKind::Int64 => {
                let val = self.buf.read_i64::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::Int64(val))?;
            }
            EnumReprKind::UInt8 => {
                let val = self.buf.read_u8().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::UInt8(val))?;
            }
            EnumReprKind::UInt16 => {
                let val = self.buf.read_u16::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::UInt16(val))?;
            }
            EnumReprKind::UInt32 => {
                let val = self.buf.read_u32::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::UInt32(val))?;
            }
            EnumReprKind::UInt64 => {
                let val = self.buf.read_u64::<LittleEndian>().map_err(io_error)?;
                interface.try_from_repr(data, EnumRepr::UInt64(val))?;
            }
        }
        Ok(())
    }

    fn visit_object(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()> {
        (self.read_object)(&mut self.buf, data, interface)
    }
}
