use {
    crate::{
        interfaces::{
            AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
            AnyTupleInterface, AnyVecInterface, EnumRepr,
        },
        visit::Visitor,
        AnyError, AnyValue, Result, SerializationError,
    },
    byteorder::{LittleEndian, WriteBytesExt},
    std::{
        any::Any,
        convert::TryFrom,
        io::{Cursor, Write},
    },
};

pub struct ToBinarySerializer<F> {
    buf: Cursor<Vec<u8>>,
    write_object: F,
}

impl<F> ToBinarySerializer<F>
where
    F: FnMut(
        &mut Cursor<Vec<u8>>,
        &dyn Any,
        &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()>,
{
    pub fn new(write_object: F) -> ToBinarySerializer<F> {
        ToBinarySerializer {
            buf: Cursor::new(Vec::new()),
            write_object,
        }
    }

    pub fn into_inner(self) -> Vec<u8> {
        self.buf.into_inner()
    }

    fn write_size(&mut self, len: usize) -> Result<()> {
        match u32::try_from(len) {
            Ok(size) => self.buf.write_u32::<LittleEndian>(size).map_err(io_error),
            Err(_) => Err(size_error(len)),
        }
    }
}

fn io_error(error: std::io::Error) -> AnyError {
    AnyError::Serialization(SerializationError::WriteFail(error))
}

fn size_error(size: usize) -> AnyError {
    AnyError::Serialization(SerializationError::SizeDoesNotFitU32(size))
}

fn eof_error() -> AnyError {
    AnyError::Serialization(SerializationError::EndOfBuffer)
}

impl<F> Visitor<Result<()>> for &mut ToBinarySerializer<F>
where
    F: FnMut(
        &mut Cursor<Vec<u8>>,
        &dyn Any,
        &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()>,
{
    fn visit_unit(self) -> Result<()> {
        Ok(())
    }

    fn visit_bool(self, val: bool) -> Result<()> {
        if val {
            self.buf.write_u8(1).map_err(io_error)
        } else {
            self.buf.write_u8(0).map_err(io_error)
        }
    }

    fn visit_i8(self, val: i8) -> Result<()> {
        self.buf.write_i8(val).map_err(io_error)
    }

    fn visit_i16(self, val: i16) -> Result<()> {
        self.buf.write_i16::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_i32(self, val: i32) -> Result<()> {
        self.buf.write_i32::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_i64(self, val: i64) -> Result<()> {
        self.buf.write_i64::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_u8(self, val: u8) -> Result<()> {
        self.buf.write_u8(val).map_err(io_error)
    }

    fn visit_u16(self, val: u16) -> Result<()> {
        self.buf.write_u16::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_u32(self, val: u32) -> Result<()> {
        self.buf.write_u32::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_u64(self, val: u64) -> Result<()> {
        self.buf.write_u64::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_f32(self, val: f32) -> Result<()> {
        self.buf.write_f32::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_f64(self, val: f64) -> Result<()> {
        self.buf.write_f64::<LittleEndian>(val).map_err(io_error)
    }

    fn visit_str(self, val: &str) -> Result<()> {
        self.visit_raw(val.as_bytes())
    }

    fn visit_raw(self, val: &[u8]) -> Result<()> {
        self.write_size(val.len())?;
        let size = self.buf.write(val).map_err(io_error)?;
        if size == val.len() {
            Ok(())
        } else {
            Err(eof_error())
        }
    }

    fn visit_dynamic(self, val: &AnyValue) -> Result<()> {
        self.visit_str(&val.signature().as_string())?;
        self.visit(val.as_anyref())
    }

    fn visit_vec(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyVecInterface + Sync),
    ) -> Result<()> {
        self.write_size(interface.len(data)?)?;
        for val in interface.iter(data)? {
            self.visit(val)?;
        }
        Ok(())
    }

    fn visit_map(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyMapInterface + Sync),
    ) -> Result<()> {
        self.write_size(interface.len(data)?)?;
        for (key, val) in interface.iter(data)? {
            self.visit(key)?;
            self.visit(val)?;
        }
        Ok(())
    }

    fn visit_option(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyOptionInterface + Sync),
    ) -> Result<()> {
        match interface.as_ref(data)? {
            Some(d) => {
                self.visit_u8(1)?;
                self.visit(d)
            }
            None => self.visit_u8(0),
        }
    }

    fn visit_tuple(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyTupleInterface + Sync),
    ) -> Result<()> {
        let len = interface.len();
        for i in 0..len {
            self.visit(interface.get(data, i)?)?;
        }
        Ok(())
    }

    fn visit_enum(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyEnumInterface + Sync),
    ) -> Result<()> {
        let repr = interface.to_repr(data)?;
        match repr {
            EnumRepr::Int8(val) => self.visit_i8(val),
            EnumRepr::Int16(val) => self.visit_i16(val),
            EnumRepr::Int32(val) => self.visit_i32(val),
            EnumRepr::Int64(val) => self.visit_i64(val),
            EnumRepr::UInt8(val) => self.visit_u8(val),
            EnumRepr::UInt16(val) => self.visit_u16(val),
            EnumRepr::UInt32(val) => self.visit_u32(val),
            EnumRepr::UInt64(val) => self.visit_u64(val),
        }
    }

    fn visit_object(
        self,
        data: &dyn Any,
        interface: &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()> {
        (self.write_object)(&mut self.buf, data, interface)
    }
}
