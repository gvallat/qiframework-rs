use crate::{
    interfaces::{
        AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
        AnyTupleInterface, AnyVecInterface,
    },
    AnyKind, AnyValue, Buffer, Signature, Typed,
};
use std::any::Any;

pub enum AnyRef<'a> {
    Unit,
    Bool(&'a bool),
    Int8(&'a i8),
    Int16(&'a i16),
    Int32(&'a i32),
    Int64(&'a i64),
    UInt8(&'a u8),
    UInt16(&'a u16),
    UInt32(&'a u32),
    UInt64(&'a u64),
    Float(&'a f32),
    Double(&'a f64),
    String(&'a String),
    Raw(&'a Buffer),
    Dynamic(&'a AnyValue),
    Vec(&'a dyn Any, &'static (dyn AnyVecInterface + Sync)),
    Map(&'a dyn Any, &'static (dyn AnyMapInterface + Sync)),
    Option(&'a dyn Any, &'static (dyn AnyOptionInterface + Sync)),
    Tuple(&'a dyn Any, &'static (dyn AnyTupleInterface + Sync)),
    Enum(&'a dyn Any, &'static (dyn AnyEnumInterface + Sync)),
    Object(&'a dyn Any, &'static (dyn AnyObjectInterface + Sync)),
}

impl<'a> AnyRef<'a> {
    pub fn to_owned(&self) -> AnyValue {
        match self {
            AnyRef::Unit => AnyValue::Unit,
            AnyRef::Bool(val) => AnyValue::Bool(**val),
            AnyRef::Int8(val) => AnyValue::Int8(**val),
            AnyRef::Int16(val) => AnyValue::Int16(**val),
            AnyRef::Int32(val) => AnyValue::Int32(**val),
            AnyRef::Int64(val) => AnyValue::Int64(**val),
            AnyRef::UInt8(val) => AnyValue::UInt8(**val),
            AnyRef::UInt16(val) => AnyValue::UInt16(**val),
            AnyRef::UInt32(val) => AnyValue::UInt32(**val),
            AnyRef::UInt64(val) => AnyValue::UInt64(**val),
            AnyRef::Float(val) => AnyValue::Float(**val),
            AnyRef::Double(val) => AnyValue::Double(**val),
            AnyRef::String(val) => AnyValue::String((*val).clone()),
            AnyRef::Raw(val) => AnyValue::Raw((*val).clone()),
            AnyRef::Dynamic(any) => AnyValue::Dynamic(Box::new((*any).clone())),
            AnyRef::Vec(data, interface) => AnyValue::Vec(interface.clone_data(*data), *interface),
            AnyRef::Map(data, interface) => AnyValue::Map(interface.clone_data(*data), *interface),
            AnyRef::Option(data, interface) => {
                AnyValue::Option(interface.clone_data(*data), *interface)
            }
            AnyRef::Tuple(data, interface) => {
                AnyValue::Tuple(interface.clone_data(*data), *interface)
            }
            AnyRef::Enum(data, interface) => {
                AnyValue::Enum(interface.clone_data(*data), *interface)
            }
            AnyRef::Object(data, interface) => {
                AnyValue::Object(interface.clone_data(*data), *interface)
            }
        }
    }

    pub fn kind(&self) -> AnyKind {
        match self {
            AnyRef::Unit => AnyKind::Unit,
            AnyRef::Bool(_) => AnyKind::Bool,
            AnyRef::Int8(_) => AnyKind::Integer,
            AnyRef::Int16(_) => AnyKind::Integer,
            AnyRef::Int32(_) => AnyKind::Integer,
            AnyRef::Int64(_) => AnyKind::Integer,
            AnyRef::UInt8(_) => AnyKind::Integer,
            AnyRef::UInt16(_) => AnyKind::Integer,
            AnyRef::UInt32(_) => AnyKind::Integer,
            AnyRef::UInt64(_) => AnyKind::Integer,
            AnyRef::Float(_) => AnyKind::Float,
            AnyRef::Double(_) => AnyKind::Float,
            AnyRef::String(_) => AnyKind::String,
            AnyRef::Raw(_) => AnyKind::Raw,
            AnyRef::Dynamic(_) => AnyKind::Dynamic,
            AnyRef::Vec(_, _) => AnyKind::Vec,
            AnyRef::Map(_, _) => AnyKind::Map,
            AnyRef::Option(_, _) => AnyKind::Option,
            AnyRef::Tuple(_, _) => AnyKind::Tuple,
            AnyRef::Enum(_, _) => AnyKind::Enum,
            AnyRef::Object(_, _) => AnyKind::Object,
        }
    }

    pub fn downcast_ref<T: 'static>(&self) -> Option<&T> {
        match self {
            AnyRef::Unit => None,
            AnyRef::Bool(val) => downcast_ref(*val),
            AnyRef::Int8(val) => downcast_ref(*val),
            AnyRef::Int16(val) => downcast_ref(*val),
            AnyRef::Int32(val) => downcast_ref(*val),
            AnyRef::Int64(val) => downcast_ref(*val),
            AnyRef::UInt8(val) => downcast_ref(*val),
            AnyRef::UInt16(val) => downcast_ref(*val),
            AnyRef::UInt32(val) => downcast_ref(*val),
            AnyRef::UInt64(val) => downcast_ref(*val),
            AnyRef::Float(val) => downcast_ref(*val),
            AnyRef::Double(val) => downcast_ref(*val),
            AnyRef::String(val) => downcast_ref(*val as &String),
            AnyRef::Raw(val) => downcast_ref(*val),
            AnyRef::Dynamic(val) => downcast_ref(*val),
            AnyRef::Vec(data, _) => (*data).downcast_ref(),
            AnyRef::Map(data, _) => (*data).downcast_ref(),
            AnyRef::Option(data, _) => (*data).downcast_ref(),
            AnyRef::Tuple(data, _) => (*data).downcast_ref(),
            AnyRef::Enum(data, _) => (*data).downcast_ref(),
            AnyRef::Object(data, _) => (*data).downcast_ref(),
        }
    }

    pub fn signature(&self) -> &'static Signature {
        match self {
            AnyRef::Unit => <() as Typed>::signature(),
            AnyRef::Bool(_) => <bool as Typed>::signature(),
            AnyRef::Int8(_) => <i8 as Typed>::signature(),
            AnyRef::Int16(_) => <i16 as Typed>::signature(),
            AnyRef::Int32(_) => <i32 as Typed>::signature(),
            AnyRef::Int64(_) => <i64 as Typed>::signature(),
            AnyRef::UInt8(_) => <u8 as Typed>::signature(),
            AnyRef::UInt16(_) => <u16 as Typed>::signature(),
            AnyRef::UInt32(_) => <u32 as Typed>::signature(),
            AnyRef::UInt64(_) => <u64 as Typed>::signature(),
            AnyRef::Float(_) => <f32 as Typed>::signature(),
            AnyRef::Double(_) => <f64 as Typed>::signature(),
            AnyRef::String(_) => <String as Typed>::signature(),
            AnyRef::Raw(_) => <Buffer as Typed>::signature(),
            AnyRef::Dynamic(_) => <AnyValue as Typed>::signature(),
            AnyRef::Vec(_, interface) => interface.signature(),
            AnyRef::Map(_, interface) => interface.signature(),
            AnyRef::Option(_, interface) => interface.signature(),
            AnyRef::Tuple(_, interface) => interface.signature(),
            AnyRef::Enum(_, interface) => interface.signature(),
            AnyRef::Object(_, interface) => interface.signature(),
        }
    }
}

fn downcast_ref<T: 'static>(d: &dyn Any) -> Option<&T> {
    d.downcast_ref::<T>()
}

impl<'a> std::fmt::Debug for AnyRef<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AnyRef::Unit => f.write_str("AnyValue{ unit }"),
            AnyRef::Bool(val) => write!(f, "AnyValue{{ bool: {:?} }}", val),
            AnyRef::Int8(val) => write!(f, "AnyValue{{ i8: {:?} }}", val),
            AnyRef::Int16(val) => write!(f, "AnyValue{{ i16: {:?} }}", val),
            AnyRef::Int32(val) => write!(f, "AnyValue{{ i32: {:?} }}", val),
            AnyRef::Int64(val) => write!(f, "AnyValue{{ i64: {:?} }}", val),
            AnyRef::UInt8(val) => write!(f, "AnyValue{{ u8: {:?} }}", val),
            AnyRef::UInt16(val) => write!(f, "AnyValue{{ u16: {:?} }}", val),
            AnyRef::UInt32(val) => write!(f, "AnyValue{{ u32: {:?} }}", val),
            AnyRef::UInt64(val) => write!(f, "AnyValue{{ u64: {:?} }}", val),
            AnyRef::Float(val) => write!(f, "AnyValue{{ f32: {:?} }}", val),
            AnyRef::Double(val) => write!(f, "AnyValue{{ f64: {:?} }}", val),
            AnyRef::String(val) => write!(f, "AnyValue{{ str: {:?} }}", val),
            AnyRef::Raw(val) => write!(f, "AnyValue{{ raw: {:?} }}", val),
            AnyRef::Dynamic(any) => write!(f, "AnyValue{{ dynamic: {:?} }}", any),
            AnyRef::Vec(data, interface) => {
                write!(f, "AnyValue{{ vec: {} }}", interface.debug_print(*data))
            }
            AnyRef::Map(data, interface) => {
                write!(f, "AnyValue{{ map: {} }}", interface.debug_print(*data))
            }
            AnyRef::Option(data, interface) => {
                write!(f, "AnyValue{{ option: {} }}", interface.debug_print(*data))
            }
            AnyRef::Tuple(data, interface) => {
                write!(f, "AnyValue{{ tuple: {} }}", interface.debug_print(*data))
            }
            AnyRef::Enum(data, interface) => {
                write!(f, "AnyValue{{ enum: {} }}", interface.debug_print(*data))
            }
            AnyRef::Object(data, interface) => {
                write!(f, "AnyValue{{ object: {} }}", interface.debug_print(*data))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn downcast_ref() {
        let v = AnyValue::Bool(true);
        let r = v.as_anyref();
        r.downcast_ref::<bool>().unwrap();

        let v = AnyValue::String("text".to_string());
        let r = v.as_anyref();
        r.downcast_ref::<String>().unwrap();

        let v = AnyValue::Dynamic(Box::new(AnyValue::Bool(true)));
        let r = v.as_anyref();
        r.downcast_ref::<AnyValue>().unwrap();
    }
}
