use crate::{
    interfaces::{
        AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
        AnyTupleInterface, AnyVecInterface,
    },
    AnyKind, AnyRef, AnyValue, Buffer, Signature,
};
use std::any::Any;

pub enum AnyRefMut<'a> {
    Unit,
    Bool(&'a mut bool),
    Int8(&'a mut i8),
    Int16(&'a mut i16),
    Int32(&'a mut i32),
    Int64(&'a mut i64),
    UInt8(&'a mut u8),
    UInt16(&'a mut u16),
    UInt32(&'a mut u32),
    UInt64(&'a mut u64),
    Float(&'a mut f32),
    Double(&'a mut f64),
    String(&'a mut String),
    Raw(&'a mut Buffer),
    Dynamic(&'a mut AnyValue),
    Vec(&'a mut dyn Any, &'static (dyn AnyVecInterface + Sync)),
    Map(&'a mut dyn Any, &'static (dyn AnyMapInterface + Sync)),
    Option(&'a mut dyn Any, &'static (dyn AnyOptionInterface + Sync)),
    Tuple(&'a mut dyn Any, &'static (dyn AnyTupleInterface + Sync)),
    Enum(&'a mut dyn Any, &'static (dyn AnyEnumInterface + Sync)),
    Object(&'a mut dyn Any, &'static (dyn AnyObjectInterface + Sync)),
}

impl<'a> AnyRefMut<'a> {
    pub fn as_anyref(&self) -> AnyRef<'_> {
        match self {
            AnyRefMut::Unit => AnyRef::Unit,
            AnyRefMut::Bool(val) => AnyRef::Bool(val),
            AnyRefMut::Int8(val) => AnyRef::Int8(val),
            AnyRefMut::Int16(val) => AnyRef::Int16(val),
            AnyRefMut::Int32(val) => AnyRef::Int32(val),
            AnyRefMut::Int64(val) => AnyRef::Int64(val),
            AnyRefMut::UInt8(val) => AnyRef::UInt8(val),
            AnyRefMut::UInt16(val) => AnyRef::UInt16(val),
            AnyRefMut::UInt32(val) => AnyRef::UInt32(val),
            AnyRefMut::UInt64(val) => AnyRef::UInt64(val),
            AnyRefMut::Float(val) => AnyRef::Float(val),
            AnyRefMut::Double(val) => AnyRef::Double(val),
            AnyRefMut::String(val) => AnyRef::String(val),
            AnyRefMut::Raw(val) => AnyRef::Raw(val),
            AnyRefMut::Dynamic(any) => AnyRef::Dynamic(any),
            AnyRefMut::Vec(data, interface) => AnyRef::Vec(*data, *interface),
            AnyRefMut::Map(data, interface) => AnyRef::Map(*data, *interface),
            AnyRefMut::Option(data, interface) => AnyRef::Option(*data, *interface),
            AnyRefMut::Tuple(data, interface) => AnyRef::Tuple(*data, *interface),
            AnyRefMut::Enum(data, interface) => AnyRef::Enum(*data, *interface),
            AnyRefMut::Object(data, interface) => AnyRef::Object(*data, *interface),
        }
    }

    pub fn to_owned(&self) -> AnyValue {
        self.as_anyref().to_owned()
    }

    pub fn kind(&self) -> AnyKind {
        self.as_anyref().kind()
    }

    pub fn signature(&self) -> &'static Signature {
        self.as_anyref().signature()
    }

    pub fn downcast_mut<T: 'static>(&mut self) -> Option<&mut T> {
        match self {
            AnyRefMut::Unit => None,
            AnyRefMut::Bool(val) => downcast_mut(*val),
            AnyRefMut::Int8(val) => downcast_mut(*val),
            AnyRefMut::Int16(val) => downcast_mut(*val),
            AnyRefMut::Int32(val) => downcast_mut(*val),
            AnyRefMut::Int64(val) => downcast_mut(*val),
            AnyRefMut::UInt8(val) => downcast_mut(*val),
            AnyRefMut::UInt16(val) => downcast_mut(*val),
            AnyRefMut::UInt32(val) => downcast_mut(*val),
            AnyRefMut::UInt64(val) => downcast_mut(*val),
            AnyRefMut::Float(val) => downcast_mut(*val),
            AnyRefMut::Double(val) => downcast_mut(*val),
            AnyRefMut::String(val) => downcast_mut(*val as &mut String),
            AnyRefMut::Raw(val) => downcast_mut(*val),
            AnyRefMut::Dynamic(val) => downcast_mut(*val),
            AnyRefMut::Vec(data, _) => (*data).downcast_mut(),
            AnyRefMut::Map(data, _) => (*data).downcast_mut(),
            AnyRefMut::Option(data, _) => (*data).downcast_mut(),
            AnyRefMut::Tuple(data, _) => (*data).downcast_mut(),
            AnyRefMut::Enum(data, _) => (*data).downcast_mut(),
            AnyRefMut::Object(data, _) => (*data).downcast_mut(),
        }
    }
}

fn downcast_mut<T: 'static>(d: &mut dyn Any) -> Option<&mut T> {
    d.downcast_mut::<T>()
}

impl<'a> std::fmt::Debug for AnyRefMut<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.as_anyref(), f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn downcast_mut() {
        let mut v = AnyValue::Bool(true);
        let mut r = v.as_anyrefmut();
        r.downcast_mut::<bool>().unwrap();

        let mut v = AnyValue::String("text".to_string());
        let mut r = v.as_anyrefmut();
        r.downcast_mut::<String>().unwrap();

        let mut v = AnyValue::Dynamic(Box::new(AnyValue::Bool(true)));
        let mut r = v.as_anyrefmut();
        r.downcast_mut::<AnyValue>().unwrap();
    }
}
