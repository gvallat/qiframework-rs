use crate::{
    interfaces::{
        AnyEnumInterface, EnumInterface, EnumRepr, EnumReprInterface, EnumReprKind, InterfaceCache,
    },
    AnyError, AnyValue, Result, Signature, TypeInterface, TypeInterfacePtr, Typed,
};
use std::any::{Any, TypeId};

struct EnumAnyEnumInterface<T> {
    _phantom: std::marker::PhantomData<T>,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl<T> Sync for EnumAnyEnumInterface<T> {}

impl<T: EnumInterface> AnyEnumInterface for EnumAnyEnumInterface<T> {
    fn signature(&self) -> &Signature {
        <<T as EnumInterface>::Repr as Typed>::signature()
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<T>() {
            Some(e) => format!("{:?}", e),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let e = data.downcast_ref::<T>().unwrap();
        Box::new(*e)
    }

    fn kind(&self) -> EnumReprKind {
        <T as EnumInterface>::Repr::kind()
    }

    fn new_repr(&self) -> Box<dyn Any + Send> {
        T::new_any()
    }

    fn to_repr(&self, data: &dyn Any) -> Result<EnumRepr> {
        match data.downcast_ref::<T>() {
            Some(val) => Ok(T::Repr::to_reprenum(val.to_repr())),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn try_from_repr<'a>(&self, data: &'a mut dyn Any, val: EnumRepr) -> Result<()> {
        match (data.downcast_mut::<T>(), T::Repr::try_from_reprenum(val)) {
            (Some(val), Ok(repr)) => {
                *val = T::from_repr(repr);
                Ok(())
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

pub struct EnumTypeInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    any_interface: &'static (dyn AnyEnumInterface + Sync),
    signature: Signature,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl<T> Sync for EnumTypeInterface<T> {}

impl<T: EnumInterface> TypeInterface for EnumTypeInterface<T> {
    fn new_anyvalue(&self) -> AnyValue {
        let v: T = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Enum(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn contains_enum(&self) -> bool {
        true
    }
}

impl<T: EnumInterface> EnumTypeInterface<T> {
    pub fn get() -> (TypeInterfacePtr, &'static (dyn AnyEnumInterface + Sync)) {
        use once_cell::sync::OnceCell;
        type Entry = (TypeInterfacePtr, &'static (dyn AnyEnumInterface + Sync));
        static INTERFACES: OnceCell<InterfaceCache<TypeId, Entry>> = OnceCell::new();
        let interfaces = INTERFACES.get_or_init(Default::default);

        let id = TypeId::of::<T>();
        interfaces.get(id, || {
            let any_interface = Box::new(EnumAnyEnumInterface {
                _phantom: std::marker::PhantomData::<T>,
            });
            let any_interface = Box::leak(any_interface) as &'static _;

            let signature = <T::Repr as Typed>::signature().clone();
            let interface = Box::new(EnumTypeInterface {
                _phantom: std::marker::PhantomData::<T>,
                any_interface,
                signature,
            });
            let interface = Box::leak(interface) as &'static _;

            crate::register_type(interface);
            (interface, any_interface)
        })
    }
}
