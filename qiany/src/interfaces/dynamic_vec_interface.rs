use crate::{
    convert, interfaces::AnyVecInterface, AnyError, AnyRef, AnyValue, Result, Signature,
    TypeInterface, TypeInterfacePtr,
};
use std::any::Any;

struct DynamicAnyVecInterface {
    signature: Signature,
    element_type: TypeInterfacePtr,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl Sync for DynamicAnyVecInterface {}

impl AnyVecInterface for DynamicAnyVecInterface {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(vec) => format!("{:?}", vec),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let vec = data.downcast_ref::<Vec<AnyValue>>().unwrap();
        Box::new(vec.clone())
    }

    fn reserve(&self, data: &mut dyn Any, capacity: usize) {
        if let Some(vec) = data.downcast_mut::<Vec<AnyValue>>() {
            vec.reserve(capacity);
        }
    }

    fn len(&self, data: &dyn Any) -> Result<usize> {
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(vec) => Ok(vec.len()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn set(
        &self,
        data: &mut dyn Any,
        vec: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<Vec<AnyValue>>(),
            vec.downcast::<Vec<AnyValue>>(),
        ) {
            (Some(vec), Ok(new_vec)) => {
                // validate content signatures ?
                *vec = *new_vec;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn push(&self, data: &mut dyn Any, value: AnyValue) -> Result<()> {
        match (
            data.downcast_mut::<Vec<AnyValue>>(),
            value.signature() == self.element_type.signature(),
        ) {
            (Some(vec), true) => {
                vec.push(value);
                Ok(())
            }
            (Some(vec), false) => {
                let mut new_val = self.new_element();
                convert::convert_any(value, new_val.as_anyrefmut())?;
                vec.push(new_val);
                Ok(())
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>> {
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(vec) => match vec.get(idx) {
                Some(val) => Ok(val.as_anyref()),
                _ => Err(AnyError::OutOfRange),
            },
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_element(&self) -> AnyValue {
        self.element_type.new_anyvalue()
    }

    fn iter<'a>(&self, data: &'a dyn Any) -> Result<Box<dyn Iterator<Item = AnyRef<'a>> + 'a>> {
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(vec) => Ok(Box::new(vec.iter().map(|v| v.as_anyref()))),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>> {
        match data.downcast::<Vec<AnyValue>>() {
            Ok(vec) => Ok(Box::new(vec.into_iter())),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

pub(crate) struct DynamicVecTypeInterface {
    signature: Signature,
    any_interface: &'static (dyn AnyVecInterface + Sync),
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl Sync for DynamicVecTypeInterface {}

impl TypeInterface for DynamicVecTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Vec<AnyValue> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Vec(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl DynamicVecTypeInterface {
    // should only be used from Registry::get()
    pub(crate) fn make(element_type: TypeInterfacePtr) -> TypeInterfacePtr {
        debug_assert!(!element_type.contains_enum());
        let signature = Signature::make_vec(element_type.signature());
        let any_interface = Box::new(DynamicAnyVecInterface {
            signature: signature.clone(),
            element_type,
        });
        let any_interface = Box::leak(any_interface) as &'static _;

        let interface = Box::new(DynamicVecTypeInterface {
            signature,
            any_interface,
        });
        Box::leak(interface) as &'static _
    }
}
