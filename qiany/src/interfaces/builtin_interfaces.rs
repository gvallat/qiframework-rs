use crate::{
    AnyRef, AnyRefMut, AnyValue, Signature, ToAnyValue, TypeInterface, TypeInterfacePtr, Typed,
};

impl ToAnyValue for () {
    fn into_anyvalue(self) -> AnyValue {
        AnyValue::Unit
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        AnyRef::Unit
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        AnyRefMut::Unit
    }
}

struct UnitTypeInterface {
    signature: Signature,
}

impl UnitTypeInterface {
    fn get() -> TypeInterfacePtr {
        use once_cell::sync::OnceCell;
        static INTERFACE: OnceCell<TypeInterfacePtr> = OnceCell::new();
        *INTERFACE.get_or_init(|| {
            let b = Box::new(UnitTypeInterface {
                signature: Signature::new("v".to_string()).unwrap(),
            });
            Box::leak(b) as &'static _
        })
    }
}

impl TypeInterface for UnitTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        AnyValue::Unit
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl Typed for () {
    fn type_interface() -> TypeInterfacePtr {
        UnitTypeInterface::get()
    }
}

macro_rules! impl_builtin_interface {
    ($t:ident $name:ident $tag:ident $sig:expr) => {
        impl ToAnyValue for $t {
            fn into_anyvalue(self) -> AnyValue {
                AnyValue::$tag(self)
            }

            fn as_anyref(&self) -> AnyRef<'_> {
                AnyRef::$tag(self)
            }

            fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
                AnyRefMut::$tag(self)
            }
        }

        struct $name {
            signature: Signature,
        }

        impl $name {
            fn get() -> TypeInterfacePtr {
                use once_cell::sync::OnceCell;
                static INTERFACE: OnceCell<TypeInterfacePtr> = OnceCell::new();
                *INTERFACE.get_or_init(|| {
                    let b = Box::new($name {
                        signature: Signature::new($sig.to_string()).unwrap(),
                    });
                    Box::leak(b) as &'static _
                })
            }
        }

        impl TypeInterface for $name {
            fn new_anyvalue(&self) -> AnyValue {
                AnyValue::$tag(Default::default())
            }

            fn signature(&self) -> &Signature {
                &self.signature
            }
        }

        impl Typed for $t {
            fn type_interface() -> TypeInterfacePtr {
                $name::get()
            }
        }
    };
}

// bool
impl_builtin_interface!(bool BoolTypeInterface Bool "b");

// signed integers
impl_builtin_interface!(i8  Int8TypeInterface  Int8  "c");
impl_builtin_interface!(i16 Int16TypeInterface Int16 "w");
impl_builtin_interface!(i32 Int32TypeInterface Int32 "i");
impl_builtin_interface!(i64 Int64TypeInterface Int64 "l");

// unsigned integers
impl_builtin_interface!(u8  UInt8TypeInterface  UInt8  "C");
impl_builtin_interface!(u16 UInt16TypeInterface UInt16 "W");
impl_builtin_interface!(u32 UInt32TypeInterface UInt32 "I");
impl_builtin_interface!(u64 UInt64TypeInterface UInt64 "L");

// floats
impl_builtin_interface!(f32 FloatTypeInterface  Float  "f");
impl_builtin_interface!(f64 DoubleTypeInterface Double "d");

// string
impl_builtin_interface!(String StringTypeInterface String "s");

impl ToAnyValue for AnyValue {
    fn into_anyvalue(self) -> AnyValue {
        AnyValue::Dynamic(Box::new(self))
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        AnyRef::Dynamic(self)
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        AnyRefMut::Dynamic(self)
    }
}

struct AnyValueTypeInterface {
    signature: Signature,
}

impl AnyValueTypeInterface {
    fn get() -> TypeInterfacePtr {
        use once_cell::sync::OnceCell;
        static INTERFACE: OnceCell<TypeInterfacePtr> = OnceCell::new();
        *INTERFACE.get_or_init(|| {
            let b = Box::new(AnyValueTypeInterface {
                signature: Signature::new("m".to_string()).unwrap(),
            });
            Box::leak(b) as &'static _
        })
    }
}

impl TypeInterface for AnyValueTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        AnyValue::Dynamic(Box::new(AnyValue::Unit))
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl Typed for AnyValue {
    fn type_interface() -> TypeInterfacePtr {
        AnyValueTypeInterface::get()
    }
}

#[cfg(test)]
mod tests {
    use crate::{AnyValue, Typed};

    #[test]
    fn builtin_signatures() {
        // unit
        assert_eq!("v", <()>::signature().as_string());
        // bool
        assert_eq!("b", bool::signature().as_string());
        // signed integer
        assert_eq!("c", i8::signature().as_string());
        assert_eq!("w", i16::signature().as_string());
        assert_eq!("i", i32::signature().as_string());
        assert_eq!("l", i64::signature().as_string());
        // unsigned integer
        assert_eq!("C", u8::signature().as_string());
        assert_eq!("W", u16::signature().as_string());
        assert_eq!("I", u32::signature().as_string());
        assert_eq!("L", u64::signature().as_string());
        // floats
        assert_eq!("f", f32::signature().as_string());
        assert_eq!("d", f64::signature().as_string());
        // string
        assert_eq!("s", String::signature().as_string());
        // dynamic
        assert_eq!("m", <AnyValue as Typed>::signature().as_string());
    }
}
