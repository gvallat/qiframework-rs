use crate::{
    convert,
    interfaces::{AnyMapInterface, InterfaceCache},
    AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, ToAnyValue, TypeInterface,
    TypeInterfacePtr, Typed,
};
use std::{
    any::{Any, TypeId},
    collections::{hash_map::RandomState, HashMap},
    fmt::Debug,
    hash::Hash,
};

#[allow(unused)]
struct MapAnyMapInterface<K, V> {
    _key: std::marker::PhantomData<K>,
    _value: std::marker::PhantomData<V>,
    signature: Signature,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl<K, V> Sync for MapAnyMapInterface<K, V> {}

pub trait MapKeyTraits: Hash + Eq + ToAnyValue + Typed + Debug + Send + Clone + 'static {}
impl<T> MapKeyTraits for T where T: Hash + Eq + ToAnyValue + Typed + Debug + Send + Clone + 'static {}

pub trait MapValueTraits: ToAnyValue + Typed + Debug + Send + Clone + 'static {}
impl<T> MapValueTraits for T where T: ToAnyValue + Typed + Debug + Send + Clone + 'static {}

impl<K: MapKeyTraits, V: MapValueTraits> AnyMapInterface for MapAnyMapInterface<K, V> {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<HashMap<K, V>>() {
            Some(map) => format!("{:?}", map),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let map = data.downcast_ref::<HashMap<K, V>>().unwrap();
        Box::new(map.clone())
    }

    fn reserve(&self, data: &mut dyn Any, capacity: usize) {
        if let Some(map) = data.downcast_mut::<HashMap<K, V>>() {
            map.reserve(capacity);
        }
    }

    fn len(&self, data: &dyn Any) -> Result<usize> {
        match data.downcast_ref::<HashMap<K, V>>() {
            Some(map) => Ok(map.len()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn set(
        &self,
        data: &mut dyn Any,
        map: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<HashMap<K, V>>(),
            map.downcast::<HashMap<K, V>>(),
        ) {
            (Some(map), Ok(new_map)) => {
                *map = *new_map;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn insert(
        &self,
        data: &mut dyn Any,
        key: AnyValue,
        value: AnyValue,
    ) -> Result<Option<AnyValue>> {
        match (
            data.downcast_mut::<HashMap<K, V>>(),
            key.downcast::<K>(),
            value.downcast::<V>(),
        ) {
            (Some(map), Ok(key), Ok(value)) => {
                let v = map.insert(key, value);
                Ok(v.map(|v| v.into_anyvalue()))
            }
            (Some(map), Err(key), Ok(value)) => {
                let mut new_key = self.new_key();
                convert::convert_any(key, new_key.as_anyrefmut())?;
                match new_key.downcast::<K>() {
                    Ok(key) => {
                        let v = map.insert(key, value);
                        Ok(v.map(|v| v.into_anyvalue()))
                    }
                    _ => Err(AnyError::InvalidOperation),
                }
            }
            (Some(map), Ok(key), Err(val)) => {
                let mut new_val = self.new_value();
                convert::convert_any(val, new_val.as_anyrefmut())?;
                match new_val.downcast::<V>() {
                    Ok(val) => {
                        let v = map.insert(key, val);
                        Ok(v.map(|v| v.into_anyvalue()))
                    }
                    _ => Err(AnyError::InvalidOperation),
                }
            }
            (Some(map), Err(key), Err(val)) => {
                let mut new_key = self.new_key();
                convert::convert_any(key, new_key.as_anyrefmut())?;
                let mut new_val = self.new_value();
                convert::convert_any(val, new_val.as_anyrefmut())?;
                match (new_key.downcast::<K>(), new_val.downcast::<V>()) {
                    (Ok(key), Ok(val)) => {
                        let v = map.insert(key, val);
                        Ok(v.map(|v| v.into_anyvalue()))
                    }
                    _ => Err(AnyError::InvalidOperation),
                }
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_key(&self) -> AnyValue {
        K::new_anyvalue()
    }

    fn new_value(&self) -> AnyValue {
        V::new_anyvalue()
    }

    fn iter<'a>(
        &self,
        data: &'a dyn Any,
    ) -> Result<Box<dyn Iterator<Item = (AnyRef<'a>, AnyRef<'a>)> + 'a>> {
        match data.downcast_ref::<HashMap<K, V>>() {
            Some(map) => Ok(Box::new(
                map.iter().map(|(k, v)| (k.as_anyref(), v.as_anyref())),
            )),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(
        &self,
        data: Box<dyn Any + Send>,
    ) -> Result<Box<dyn Iterator<Item = (AnyValue, AnyValue)>>> {
        match data.downcast::<HashMap<K, V>>() {
            Ok(map) => Ok(Box::new(
                map.into_iter()
                    .map(|(k, v)| (k.into_anyvalue(), v.into_anyvalue())),
            )),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

#[allow(unused)]
struct MapTypeInterface<K, V> {
    _key: std::marker::PhantomData<K>,
    _value: std::marker::PhantomData<V>,
    any_interface: &'static (dyn AnyMapInterface + Sync),
    signature: Signature,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl<K, V> Sync for MapTypeInterface<K, V> {}

impl<K: MapKeyTraits, V: MapValueTraits> TypeInterface for MapTypeInterface<K, V> {
    fn new_anyvalue(&self) -> AnyValue {
        let v: HashMap<K, V> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Map(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn contains_enum(&self) -> bool {
        K::type_interface().contains_enum() || V::type_interface().contains_enum()
    }
}

impl<K: MapKeyTraits, V: MapValueTraits> MapTypeInterface<K, V> {
    fn get() -> (TypeInterfacePtr, &'static (dyn AnyMapInterface + Sync)) {
        use once_cell::sync::OnceCell;
        type Key = (TypeId, TypeId);
        type Entry = (TypeInterfacePtr, &'static (dyn AnyMapInterface + Sync));
        static INTERFACES: OnceCell<InterfaceCache<Key, Entry>> = OnceCell::new();
        let interfaces = INTERFACES.get_or_init(Default::default);

        let id = (TypeId::of::<K>(), TypeId::of::<V>());
        interfaces.get(id, || {
            let signature = Signature::make_map(K::signature(), V::signature());

            let any_interface = Box::new(MapAnyMapInterface {
                _key: std::marker::PhantomData::<K>,
                _value: std::marker::PhantomData::<V>,
                signature: signature.clone(),
            });
            let any_interface = Box::leak(any_interface) as &'static _;

            let interface = Box::new(MapTypeInterface {
                _key: std::marker::PhantomData::<K>,
                _value: std::marker::PhantomData::<V>,
                any_interface,
                signature,
            });
            let interface = Box::leak(interface) as &'static _;

            crate::register_type(interface);
            (interface, any_interface)
        })
    }
}

impl<K: MapKeyTraits, V: MapValueTraits> Typed for HashMap<K, V, RandomState> {
    fn type_interface() -> TypeInterfacePtr {
        MapTypeInterface::<K, V>::get().0
    }
}

impl<K: MapKeyTraits, V: MapValueTraits> ToAnyValue for HashMap<K, V, RandomState> {
    fn into_anyvalue(self) -> AnyValue {
        let data: Box<dyn Any + Send> = Box::new(self);
        let interface = MapTypeInterface::<K, V>::get().1;
        AnyValue::Map(data, interface)
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        let data: &(dyn Any + Send) = self;
        let interface = MapTypeInterface::<K, V>::get().1;
        AnyRef::Map(data, interface)
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        let data: &mut (dyn Any + Send) = self;
        let interface = MapTypeInterface::<K, V>::get().1;
        AnyRefMut::Map(data, interface)
    }
}
