use crate::{
    convert, interfaces::AnyMapInterface, AnyError, AnyRef, AnyValue, Result, Signature,
    TypeInterface, TypeInterfacePtr,
};
use std::any::Any;

struct DynamicAnyMapInterface {
    signature: Signature,
    key_type: TypeInterfacePtr,
    value_type: TypeInterfacePtr,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl Sync for DynamicAnyMapInterface {}

impl AnyMapInterface for DynamicAnyMapInterface {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Vec<(AnyValue, AnyValue)>>() {
            Some(map) => format!("{:?}", map),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let map = data.downcast_ref::<Vec<(AnyValue, AnyValue)>>().unwrap();
        Box::new(map.clone())
    }

    fn reserve(&self, data: &mut dyn Any, capacity: usize) {
        if let Some(map) = data.downcast_mut::<Vec<(AnyValue, AnyValue)>>() {
            map.reserve(capacity);
        }
    }

    fn len(&self, data: &dyn Any) -> Result<usize> {
        match data.downcast_ref::<Vec<(AnyValue, AnyValue)>>() {
            Some(map) => Ok(map.len()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn set(
        &self,
        data: &mut dyn Any,
        map: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<Vec<(AnyValue, AnyValue)>>(),
            map.downcast::<Vec<(AnyValue, AnyValue)>>(),
        ) {
            (Some(map), Ok(new_map)) => {
                // validate content signatures ?
                *map = *new_map;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn insert(
        &self,
        data: &mut dyn Any,
        key: AnyValue,
        value: AnyValue,
    ) -> Result<Option<AnyValue>> {
        // check key already exists ?
        match (
            data.downcast_mut::<Vec<(AnyValue, AnyValue)>>(),
            key.signature() == self.key_type.signature(),
            value.signature() == self.value_type.signature(),
        ) {
            (Some(map), true, true) => {
                map.push((key, value));
                Ok(None)
            }
            (Some(map), false, true) => {
                let mut new_key = self.new_key();
                convert::convert_any(key, new_key.as_anyrefmut())?;
                map.push((new_key, value));
                Ok(None)
            }
            (Some(map), true, false) => {
                let mut new_value = self.new_key();
                convert::convert_any(value, new_value.as_anyrefmut())?;
                map.push((key, new_value));
                Ok(None)
            }
            (Some(map), false, false) => {
                let mut new_key = self.new_key();
                convert::convert_any(key, new_key.as_anyrefmut())?;
                let mut new_value = self.new_key();
                convert::convert_any(value, new_value.as_anyrefmut())?;
                map.push((new_key, new_value));
                Ok(None)
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_key(&self) -> AnyValue {
        self.key_type.new_anyvalue()
    }

    fn new_value(&self) -> AnyValue {
        self.value_type.new_anyvalue()
    }

    fn iter<'a>(
        &self,
        data: &'a dyn Any,
    ) -> Result<Box<dyn Iterator<Item = (AnyRef<'a>, AnyRef<'a>)> + 'a>> {
        match data.downcast_ref::<Vec<(AnyValue, AnyValue)>>() {
            Some(map) => Ok(Box::new(
                map.iter().map(|(k, v)| (k.as_anyref(), v.as_anyref())),
            )),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(
        &self,
        data: Box<dyn Any + Send>,
    ) -> Result<Box<dyn Iterator<Item = (AnyValue, AnyValue)>>> {
        match data.downcast::<Vec<(AnyValue, AnyValue)>>() {
            Ok(map) => Ok(Box::new(map.into_iter())),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

pub(crate) struct DynamicMapTypeInterface {
    signature: Signature,
    any_interface: &'static (dyn AnyMapInterface + Sync),
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl Sync for DynamicMapTypeInterface {}

impl TypeInterface for DynamicMapTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Vec<(AnyValue, AnyValue)> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Map(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl DynamicMapTypeInterface {
    // should only be used from Registry::get()
    pub(crate) fn make(
        key_type: TypeInterfacePtr,
        value_type: TypeInterfacePtr,
    ) -> TypeInterfacePtr {
        debug_assert!(!key_type.contains_enum());
        debug_assert!(!value_type.contains_enum());
        let signature = Signature::make_map(key_type.signature(), value_type.signature());
        let any_interface = Box::new(DynamicAnyMapInterface {
            signature: signature.clone(),
            key_type,
            value_type,
        });
        let any_interface = Box::leak(any_interface) as &'static _;

        let interface = Box::new(DynamicMapTypeInterface {
            signature,
            any_interface,
        });
        Box::leak(interface) as &'static _
    }
}
