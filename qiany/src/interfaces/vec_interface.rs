use crate::{
    convert,
    interfaces::{AnyVecInterface, InterfaceCache},
    AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, ToAnyValue, TypeInterface,
    TypeInterfacePtr, Typed,
};
use std::{
    any::{Any, TypeId},
    fmt::Debug,
};

struct VecAnyVecInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    signature: Signature,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl<T> Sync for VecAnyVecInterface<T> {}

pub trait VecInnerTraits: ToAnyValue + Typed + Debug + Send + Clone + 'static {}
impl<T> VecInnerTraits for T where T: ToAnyValue + Typed + Debug + Send + Clone + 'static {}

impl<T: VecInnerTraits> AnyVecInterface for VecAnyVecInterface<T> {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Vec<T>>() {
            Some(vec) => format!("{:?}", vec),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let vec = data.downcast_ref::<Vec<T>>().unwrap();
        Box::new(vec.clone())
    }

    fn reserve(&self, data: &mut dyn Any, capacity: usize) {
        if let Some(vec) = data.downcast_mut::<Vec<T>>() {
            vec.reserve(capacity);
        }
    }

    fn len(&self, data: &dyn Any) -> Result<usize> {
        match data.downcast_ref::<Vec<T>>() {
            Some(vec) => Ok(vec.len()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn set(
        &self,
        data: &mut dyn Any,
        vec: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (data.downcast_mut::<Vec<T>>(), vec.downcast::<Vec<T>>()) {
            (Some(vec), Ok(new_vec)) => {
                *vec = *new_vec;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn push(&self, data: &mut dyn Any, value: AnyValue) -> Result<()> {
        match (data.downcast_mut::<Vec<T>>(), value.downcast::<T>()) {
            (Some(vec), Ok(val)) => {
                vec.push(val);
                Ok(())
            }
            (Some(vec), Err(val)) => {
                let mut new_val = self.new_element();
                convert::convert_any(val, new_val.as_anyrefmut())?;
                match new_val.downcast::<T>() {
                    Ok(val) => {
                        vec.push(val);
                        Ok(())
                    }
                    _ => Err(AnyError::InvalidOperation),
                }
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>> {
        match data.downcast_ref::<Vec<T>>() {
            Some(vec) => match vec.get(idx) {
                Some(val) => Ok(val.as_anyref()),
                _ => Err(AnyError::OutOfRange),
            },
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_element(&self) -> AnyValue {
        T::new_anyvalue()
    }

    fn iter<'a>(&self, data: &'a dyn Any) -> Result<Box<dyn Iterator<Item = AnyRef<'a>> + 'a>> {
        match data.downcast_ref::<Vec<T>>() {
            Some(vec) => Ok(Box::new(vec.iter().map(|v| v.as_anyref()))),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>> {
        match data.downcast::<Vec<T>>() {
            Ok(vec) => Ok(Box::new(vec.into_iter().map(|v| v.into_anyvalue()))),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

struct VecTypeInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    any_interface: &'static (dyn AnyVecInterface + Sync),
    signature: Signature,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl<T> Sync for VecTypeInterface<T> {}

impl<T: VecInnerTraits> TypeInterface for VecTypeInterface<T> {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Vec<T> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Vec(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn contains_enum(&self) -> bool {
        T::type_interface().contains_enum()
    }
}

impl<T: VecInnerTraits> VecTypeInterface<T> {
    fn get() -> (TypeInterfacePtr, &'static (dyn AnyVecInterface + Sync)) {
        use once_cell::sync::OnceCell;
        type Entry = (TypeInterfacePtr, &'static (dyn AnyVecInterface + Sync));
        static INTERFACES: OnceCell<InterfaceCache<TypeId, Entry>> = OnceCell::new();
        let interfaces = INTERFACES.get_or_init(Default::default);

        let id = TypeId::of::<T>();
        interfaces.get(id, || {
            let signature = Signature::make_vec(T::signature());

            let any_interface = Box::new(VecAnyVecInterface {
                _phantom: std::marker::PhantomData::<T>,
                signature: signature.clone(),
            });
            let any_interface = Box::leak(any_interface) as &'static _;

            let interface = Box::new(VecTypeInterface {
                _phantom: std::marker::PhantomData::<T>,
                any_interface,
                signature,
            });
            let interface = Box::leak(interface) as &'static _;

            crate::register_type(interface);
            (interface, any_interface)
        })
    }
}

impl<T: VecInnerTraits> Typed for Vec<T> {
    fn type_interface() -> TypeInterfacePtr {
        VecTypeInterface::<T>::get().0
    }
}

impl<T: VecInnerTraits> ToAnyValue for Vec<T> {
    fn into_anyvalue(self) -> AnyValue {
        let data: Box<dyn Any + Send> = Box::new(self);
        let interface = VecTypeInterface::<T>::get().1;
        AnyValue::Vec(data, interface)
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        let data: &(dyn Any + Send) = self;
        let interface = VecTypeInterface::<T>::get().1;
        AnyRef::Vec(data, interface)
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        let data: &mut (dyn Any + Send) = self;
        let interface = VecTypeInterface::<T>::get().1;
        AnyRefMut::Vec(data, interface)
    }
}
