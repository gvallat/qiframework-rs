use parking_lot::ReentrantMutex as Mutex;
use std::{cell::RefCell, collections::HashMap};

pub struct InterfaceCache<I, T> {
    map: Mutex<RefCell<HashMap<I, T>>>,
}

impl<I: std::hash::Hash + Eq, T: Copy> InterfaceCache<I, T> {
    pub fn new() -> InterfaceCache<I, T> {
        InterfaceCache {
            map: Mutex::new(RefCell::new(HashMap::new())),
        }
    }

    pub fn get<Func>(&self, id: I, create: Func) -> T
    where
        Func: FnOnce() -> T,
    {
        let lock = self.map.lock();

        let map = lock.borrow();
        if let Some(pair) = map.get(&id) {
            *pair
        } else {
            std::mem::drop(map);

            let entry = create();

            let mut map = lock.borrow_mut();
            map.insert(id, entry);
            entry
        }
    }
}

impl<I: std::hash::Hash + Eq, T: Copy> Default for InterfaceCache<I, T> {
    fn default() -> Self {
        InterfaceCache::new()
    }
}
