use crate::{
    interfaces::AnyTupleInterface, AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature,
    TypeInterface, TypeInterfacePtr,
};
use std::any::Any;

struct DynamicAnyTupleInterface {
    signature: Signature,
    field_names: Vec<&'static str>,
    len: usize,
}

impl AnyTupleInterface for DynamicAnyTupleInterface {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn field_names(&self) -> &[&'static str] {
        &self.field_names
    }

    fn len(&self) -> usize {
        self.len
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(tuple) => format!("{:?}", tuple),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let tuple = data.downcast_ref::<Vec<AnyValue>>().unwrap();
        Box::new(tuple.clone())
    }

    fn set(
        &self,
        data: &mut dyn Any,
        tuple: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<Vec<AnyValue>>(),
            tuple.downcast::<Vec<AnyValue>>(),
        ) {
            (Some(tuple), Ok(new_tuple)) => {
                // validate content signatures ?
                *tuple = *new_tuple;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>> {
        if idx >= self.len {
            return Err(AnyError::InvalidOperation);
        }
        match data.downcast_ref::<Vec<AnyValue>>() {
            Some(tuple) => Ok(tuple[idx].as_anyref()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn get_mut<'a>(&self, data: &'a mut dyn Any, idx: usize) -> Result<AnyRefMut<'a>> {
        if idx >= self.len {
            return Err(AnyError::InvalidOperation);
        }
        match data.downcast_mut::<Vec<AnyValue>>() {
            Some(tuple) => Ok(tuple[idx].as_anyrefmut()),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>> {
        match data.downcast::<Vec<AnyValue>>() {
            Ok(vec) => Ok(Box::new(vec.into_iter())),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

pub(crate) struct DynamicTupleTypeInterface {
    signature: Signature,
    any_interface: &'static (dyn AnyTupleInterface + Sync),
    elements_types: Vec<TypeInterfacePtr>,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl Sync for DynamicTupleTypeInterface {}

impl TypeInterface for DynamicTupleTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Vec<AnyValue> = self
            .elements_types
            .iter()
            .map(|i| i.new_anyvalue())
            .collect();

        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Tuple(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl DynamicTupleTypeInterface {
    // should only be used from Registry::get()
    /// Panics: If elements_types.len() != Some(names.len())
    pub(crate) fn make(
        elements_types: Vec<TypeInterfacePtr>,
        names: Option<(String, Vec<String>)>,
    ) -> TypeInterfacePtr {
        debug_assert!(elements_types.iter().all(|i| !i.contains_enum()));
        let sigs: Vec<&_> = elements_types.iter().map(|i| i.signature()).collect();
        let (signature, field_names) = match names {
            Some((name, names)) => {
                let signature = Signature::make_struct(&sigs, name, &names).unwrap();
                let field_names = names
                    .iter()
                    .map(|n| Box::leak(n.clone().into_boxed_str()) as &'static _)
                    .collect();
                (signature, field_names)
            }
            None => {
                let signature = Signature::make_tuple(&sigs);
                (signature, Vec::new())
            }
        };

        let any_interface = Box::new(DynamicAnyTupleInterface {
            signature: signature.clone(),
            field_names,
            len: elements_types.len(),
        });
        let any_interface = Box::leak(any_interface) as &'static _;

        let interface = Box::new(DynamicTupleTypeInterface {
            signature,
            any_interface,
            elements_types,
        });
        Box::leak(interface) as &'static _
    }
}
