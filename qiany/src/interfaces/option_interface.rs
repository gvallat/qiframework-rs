use crate::{
    convert,
    interfaces::{AnyOptionInterface, InterfaceCache},
    AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, ToAnyValue, TypeInterface,
    TypeInterfacePtr, Typed,
};
use std::{
    any::{Any, TypeId},
    fmt::Debug,
};

struct OptionAnyOptionInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    signature: Signature,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl<T> Sync for OptionAnyOptionInterface<T> {}

pub trait OptionInnerTraits: ToAnyValue + Typed + Debug + Send + Clone + 'static {}
impl<T> OptionInnerTraits for T where T: ToAnyValue + Typed + Debug + Send + Clone + 'static {}

impl<T: OptionInnerTraits> AnyOptionInterface for OptionAnyOptionInterface<T> {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Option<T>>() {
            Some(vec) => format!("{:?}", vec),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let vec = data.downcast_ref::<Option<T>>().unwrap();
        Box::new(vec.clone())
    }

    fn set(
        &self,
        data: &mut dyn Any,
        option: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<Option<T>>(),
            option.downcast::<Option<T>>(),
        ) {
            (Some(opt), Ok(new_opt)) => {
                *opt = *new_opt;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn set_some(&self, data: &mut dyn Any, value: AnyValue) -> Result<()> {
        match (data.downcast_mut::<Option<T>>(), value.downcast::<T>()) {
            (Some(opt), Ok(val)) => {
                *opt = Some(val);
                Ok(())
            }
            (Some(opt), Err(val)) => {
                let mut new_val = self.new_element();
                convert::convert_any(val, new_val.as_anyrefmut())?;
                match new_val.downcast::<T>() {
                    Ok(val) => {
                        *opt = Some(val);
                        Ok(())
                    }
                    _ => Err(AnyError::InvalidOperation),
                }
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_element(&self) -> AnyValue {
        T::new_anyvalue()
    }

    fn as_ref<'a>(&self, data: &'a dyn Any) -> Result<Option<AnyRef<'a>>> {
        match data.downcast_ref::<Option<T>>() {
            Some(Some(val)) => Ok(Some(val.as_anyref())),
            Some(None) => Ok(None),
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn as_refmut<'a>(&self, data: &'a mut dyn Any) -> Result<Option<AnyRefMut<'a>>> {
        match data.downcast_mut::<Option<T>>() {
            Some(Some(val)) => Ok(Some(val.as_anyrefmut())),
            Some(None) => Ok(None),
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn into_inner(&self, data: Box<dyn Any + Send>) -> Result<Option<AnyValue>> {
        match data.downcast::<Option<T>>() {
            Ok(opt) => Ok(opt.map(|e| e.into_anyvalue())),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

struct OptionTypeInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    any_interface: &'static (dyn AnyOptionInterface + Sync),
    signature: Signature,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl<T> Sync for OptionTypeInterface<T> {}

impl<T: OptionInnerTraits> TypeInterface for OptionTypeInterface<T> {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Option<T> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Option(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn contains_enum(&self) -> bool {
        T::type_interface().contains_enum()
    }
}

impl<T: OptionInnerTraits> OptionTypeInterface<T> {
    fn get() -> (TypeInterfacePtr, &'static (dyn AnyOptionInterface + Sync)) {
        use once_cell::sync::OnceCell;
        type Entry = (TypeInterfacePtr, &'static (dyn AnyOptionInterface + Sync));
        static INTERFACES: OnceCell<InterfaceCache<TypeId, Entry>> = OnceCell::new();
        let interfaces = INTERFACES.get_or_init(Default::default);

        let id = TypeId::of::<T>();
        interfaces.get(id, || {
            let signature = Signature::make_optional(T::signature());

            let any_interface = Box::new(OptionAnyOptionInterface {
                _phantom: std::marker::PhantomData::<T>,
                signature: signature.clone(),
            });
            let any_interface = Box::leak(any_interface) as &'static _;

            let interface = Box::new(OptionTypeInterface {
                _phantom: std::marker::PhantomData::<T>,
                any_interface,
                signature,
            });
            let interface = Box::leak(interface) as &'static _;

            crate::register_type(interface);
            (interface, any_interface)
        })
    }
}

impl<T: OptionInnerTraits> Typed for Option<T> {
    fn type_interface() -> TypeInterfacePtr {
        OptionTypeInterface::<T>::get().0
    }
}

impl<T: OptionInnerTraits> ToAnyValue for Option<T> {
    fn into_anyvalue(self) -> AnyValue {
        let data: Box<dyn Any + Send> = Box::new(self);
        let interface = OptionTypeInterface::<T>::get().1;
        AnyValue::Option(data, interface)
    }

    fn as_anyref(&self) -> AnyRef<'_> {
        let data: &(dyn Any + Send) = self;
        let interface = OptionTypeInterface::<T>::get().1;
        AnyRef::Option(data, interface)
    }

    fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
        let data: &mut (dyn Any + Send) = self;
        let interface = OptionTypeInterface::<T>::get().1;
        AnyRefMut::Option(data, interface)
    }
}
