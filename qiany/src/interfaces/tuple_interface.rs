use crate::{
    interfaces::{AnyTupleInterface, Indexer, InterfaceCache},
    AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, ToAnyValue, TypeInterface,
    TypeInterfacePtr, Typed,
};
use std::{
    any::{Any, TypeId},
    fmt::Debug,
};

struct TupleAnyTupleInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    signature: Signature,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl<T> Sync for TupleAnyTupleInterface<T> {}

impl<T: Indexer + Debug + Default + Send + Clone + 'static> AnyTupleInterface
    for TupleAnyTupleInterface<T>
{
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn field_names(&self) -> &[&'static str] {
        <T as Indexer>::field_names()
    }

    fn len(&self) -> usize {
        <T as Indexer>::len()
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<T>() {
            Some(tuple) => format!("{:?}", tuple),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let tuple = data.downcast_ref::<T>().unwrap();
        Box::new(tuple.clone())
    }

    fn set(
        &self,
        data: &mut dyn Any,
        tuple: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (data.downcast_mut::<T>(), tuple.downcast::<T>()) {
            (Some(tuple), Ok(new_tuple)) => {
                *tuple = *new_tuple;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>> {
        <T as Indexer>::as_ref(data, idx)
    }

    fn get_mut<'a>(&self, data: &'a mut dyn Any, idx: usize) -> Result<AnyRefMut<'a>> {
        <T as Indexer>::as_refmut(data, idx)
    }

    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>> {
        <T as Indexer>::into_iter(data)
    }
}

pub struct TupleTypeInterface<T> {
    _phantom: std::marker::PhantomData<T>,
    any_interface: &'static (dyn AnyTupleInterface + Sync),
    signature: Signature,
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl<T> Sync for TupleTypeInterface<T> {}

impl<T: Indexer + Debug + Default + Send + Clone + 'static> TypeInterface
    for TupleTypeInterface<T>
{
    fn new_anyvalue(&self) -> AnyValue {
        let v: T = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Tuple(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn contains_enum(&self) -> bool {
        <T as Indexer>::contains_enum()
    }
}

impl<T: Indexer + Debug + Default + Send + Clone + 'static> TupleTypeInterface<T> {
    pub fn get() -> (TypeInterfacePtr, &'static (dyn AnyTupleInterface + Sync)) {
        use once_cell::sync::OnceCell;
        type Entry = (TypeInterfacePtr, &'static (dyn AnyTupleInterface + Sync));
        static INTERFACES: OnceCell<InterfaceCache<TypeId, Entry>> = OnceCell::new();
        let interfaces = INTERFACES.get_or_init(Default::default);

        let id = TypeId::of::<T>();
        interfaces.get(id, || {
            let signature = <T as Indexer>::signature();

            let any_interface = Box::new(TupleAnyTupleInterface {
                _phantom: std::marker::PhantomData::<T>,
                signature: signature.clone(),
            });
            let any_interface = Box::leak(any_interface) as &'static _;

            let interface = Box::new(TupleTypeInterface {
                _phantom: std::marker::PhantomData::<T>,
                any_interface,
                signature,
            });
            let interface = Box::leak(interface) as &'static _;

            crate::register_type(interface);
            (interface, any_interface)
        })
    }
}

macro_rules! impl_typed_for_tuple {
    ($($t:ident)+) => {
        impl<$($t), +> Typed for ($($t),+)
        where
            Self: Indexer,
            $($t: Debug + Default + Send + Clone +'static),+
        {
            fn type_interface() -> TypeInterfacePtr {
                TupleTypeInterface::<Self>::get().0
            }
        }

        impl<$($t), +> ToAnyValue for ($($t),+)
        where
            Self: Indexer,
            $($t: Debug + Default + Send + Clone + 'static),+
        {
            fn into_anyvalue(self) -> AnyValue {
                let data: Box<dyn Any + Send> = Box::new(self);
                let interface = TupleTypeInterface::<Self>::get().1;
                AnyValue::Tuple(data, interface)
            }

            fn as_anyref(&self) -> AnyRef<'_> {
                let data: &(dyn Any + Send) = self;
                let interface = TupleTypeInterface::<Self>::get().1;
                AnyRef::Tuple(data, interface)
            }

            fn as_anyrefmut(&mut self) -> AnyRefMut<'_> {
                let data: &mut (dyn Any + Send) = self;
                let interface = TupleTypeInterface::<Self>::get().1;
                AnyRefMut::Tuple(data, interface)
            }
        }
    };
}

macro_rules! impl_typed_for_tuple_cascade {
    ($t0:ident $t1:ident $($ts:ident)+) => {
        impl_typed_for_tuple_cascade!(__impl $t0 $t1; $($ts)+);
    };
    (__impl $($head:ident)*; $tail:ident $($ts:ident)+) => {
        impl_typed_for_tuple!($($head)*);
        impl_typed_for_tuple_cascade!(__impl $($head)* $tail; $($ts)+);
    };
    (__impl $($head:ident)*; $tail:ident) => {
        impl_typed_for_tuple!($($head)*);
        impl_typed_for_tuple!($($head)* $tail);
    };
}

impl_typed_for_tuple_cascade!(T0 T1 T2 T3 T4 T5 T6 T7 T8 T9 T10 T11);
