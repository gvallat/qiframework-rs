use crate::{AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, ToAnyValue, Typed};
use std::any::Any;

pub trait Indexer {
    fn signature() -> Signature;

    fn name() -> &'static str {
        ""
    }

    fn field_names() -> &'static [&'static str] {
        &[]
    }

    fn contains_enum() -> bool;

    fn len() -> usize;
    fn as_ref<'a>(data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>>;
    fn as_refmut<'a>(data: &'a mut dyn Any, idx: usize) -> Result<AnyRefMut<'a>>;

    fn into_iter(data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>>;
}

macro_rules! replace_expr {
    ($_t:tt $sub:expr) => {
        $sub
    };
}

macro_rules! count_tts {
    ($($tts:tt)*) => {0usize $(+ replace_expr!($tts 1usize))*};
}

macro_rules! impl_indexer_for_tuple {
    ($($n:tt $t:ident),+) => {
        impl<$($t), +> Indexer for ($($t),+)
        where
            $($t: ToAnyValue + Typed + 'static),+
        {
            fn signature() -> Signature {
                Signature::make_tuple(&[$($t::signature()),+])
            }

            fn contains_enum() -> bool {
                $($t::type_interface().contains_enum())||+
            }

            fn len() -> usize {
                count_tts!($($t)+)
            }

            fn as_ref<'a>(data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>> {
                match data.downcast_ref::<Self>() {
                    Some(tuple) => match idx {
                        $($n => Ok(tuple.$n.as_anyref()),)+
                        _ => Err(AnyError::InvalidOperation),
                    },
                    None => Err(AnyError::InvalidOperation),
                }
            }

            fn as_refmut<'a>(data: &'a mut dyn Any, idx: usize) -> Result<AnyRefMut<'a>> {
                match data.downcast_mut::<Self>() {
                    Some(tuple) => match idx {
                        $($n => Ok(tuple.$n.as_anyrefmut()),)+
                        _ => Err(AnyError::InvalidOperation),
                    },
                    None => Err(AnyError::InvalidOperation),
                }
            }

            fn into_iter(data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>> {
                match data.downcast::<Self>() {
                    Ok(tuple) => {
                        let mut vec = Vec::with_capacity(Self::len());
                        let tuple = *tuple;
                        $(vec.push(tuple.$n.into_anyvalue());)+
                        Ok(Box::new(vec.into_iter()))
                    },
                    _ => Err(AnyError::InvalidOperation),
                }
            }
        }
    };
}

macro_rules! impl_indexer_for_tuple_cascade {
    ($n0:tt $t0:ident, $n1:tt $t1:ident, $($ns:tt $ts:ident),*) => {
        impl_indexer_for_tuple_cascade!(__impl $n0 $t0, $n1 $t1; $($ns $ts),*);
    };
    (__impl $($hns:tt $hts:ident),+; $n:tt $t:ident, $($ns:tt $ts:ident),*) => {
        impl_indexer_for_tuple!($($hns $hts),+);
        impl_indexer_for_tuple_cascade!(__impl $($hns $hts),+, $n $t; $($ns $ts),*);
    };
    (__impl $($hns:tt $hts:ident),+; $n:tt $t:ident) => {
        impl_indexer_for_tuple!($($hns $hts),+);
        impl_indexer_for_tuple!($($hns $hts),+, $n $t);
    };
}

impl_indexer_for_tuple_cascade!(0 T0, 1 T1, 2 T2, 3 T3, 4 T4, 5 T5, 6 T6, 7 T7, 8 T8, 9 T9, 10 T10, 11 T11);

#[test]
fn indexer_len() {
    assert_eq!(3, <(u32, u32, u32) as Indexer>::len());
    assert_eq!(4, <(u32, u32, u32, u32) as Indexer>::len());
    assert_eq!(5, <(u32, u32, u32, u32, u32) as Indexer>::len());
}
