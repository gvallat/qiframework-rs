use crate::{
    convert, interfaces::AnyOptionInterface, AnyError, AnyRef, AnyRefMut, AnyValue, Result,
    Signature, TypeInterface, TypeInterfacePtr,
};
use std::any::Any;

struct DynamicAnyOptionInterface {
    signature: Signature,
    element_type: TypeInterfacePtr,
}

// safety: doesn't store any value, just implement functions to manipulate them
unsafe impl Sync for DynamicAnyOptionInterface {}

impl AnyOptionInterface for DynamicAnyOptionInterface {
    fn signature(&self) -> &Signature {
        &self.signature
    }

    fn debug_print(&self, data: &dyn Any) -> String {
        match data.downcast_ref::<Option<AnyValue>>() {
            Some(vec) => format!("{:?}", vec),
            None => format!("\"{:?}\"", AnyError::InvalidPrint),
        }
    }

    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static> {
        let vec = data.downcast_ref::<Option<AnyValue>>().unwrap();
        Box::new(vec.clone())
    }

    fn set(
        &self,
        data: &mut dyn Any,
        option: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>> {
        match (
            data.downcast_mut::<Option<AnyValue>>(),
            option.downcast::<Option<AnyValue>>(),
        ) {
            (Some(opt), Ok(new_opt)) => {
                // validate content signatures ?
                *opt = *new_opt;
                Ok(())
            }
            (None, Ok(b)) => Err(b),
            (_, Err(b)) => Err(b),
        }
    }

    fn set_some(&self, data: &mut dyn Any, value: AnyValue) -> Result<()> {
        match (
            data.downcast_mut::<Option<AnyValue>>(),
            value.signature() == self.element_type.signature(),
        ) {
            (Some(opt), true) => {
                *opt = Some(value);
                Ok(())
            }
            (Some(opt), false) => {
                let mut new_val = self.new_element();
                convert::convert_any(value, new_val.as_anyrefmut())?;
                *opt = Some(new_val);
                Ok(())
            }
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn new_element(&self) -> AnyValue {
        self.element_type.new_anyvalue()
    }

    fn as_ref<'a>(&self, data: &'a dyn Any) -> Result<Option<AnyRef<'a>>> {
        match data.downcast_ref::<Option<AnyValue>>() {
            Some(Some(val)) => Ok(Some(val.as_anyref())),
            Some(None) => Ok(None),
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn as_refmut<'a>(&self, data: &'a mut dyn Any) -> Result<Option<AnyRefMut<'a>>> {
        match data.downcast_mut::<Option<AnyValue>>() {
            Some(Some(val)) => Ok(Some(val.as_anyrefmut())),
            Some(None) => Ok(None),
            _ => Err(AnyError::InvalidOperation),
        }
    }

    fn into_inner(&self, data: Box<dyn Any + Send>) -> Result<Option<AnyValue>> {
        match data.downcast::<Option<AnyValue>>() {
            Ok(opt) => Ok(*opt),
            _ => Err(AnyError::InvalidOperation),
        }
    }
}

pub(crate) struct DynamicOptionTypeInterface {
    signature: Signature,
    any_interface: &'static (dyn AnyOptionInterface + Sync),
}

// safety: doesn't store any values, just implement functions to manipulate them
unsafe impl Sync for DynamicOptionTypeInterface {}

impl TypeInterface for DynamicOptionTypeInterface {
    fn new_anyvalue(&self) -> AnyValue {
        let v: Option<AnyValue> = Default::default();
        let data: Box<dyn Any + Send> = Box::new(v);
        let interface = self.any_interface;
        AnyValue::Option(data, interface)
    }

    fn signature(&self) -> &Signature {
        &self.signature
    }
}

impl DynamicOptionTypeInterface {
    // should only be used from Registry::get()
    pub(crate) fn make(element_type: TypeInterfacePtr) -> TypeInterfacePtr {
        debug_assert!(!element_type.contains_enum());
        let signature = Signature::make_vec(element_type.signature());
        let any_interface = Box::new(DynamicAnyOptionInterface {
            signature: signature.clone(),
            element_type,
        });
        let any_interface = Box::leak(any_interface) as &'static _;

        let interface = Box::new(DynamicOptionTypeInterface {
            signature,
            any_interface,
        });
        Box::leak(interface) as &'static _
    }
}
