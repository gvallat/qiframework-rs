use crate::{AnyError, AnyRef, AnyRefMut, AnyValue, Result, Signature, Typed};
use std::any::Any;

mod builtin_interfaces;
mod dynamic_map_interface;
mod dynamic_option_interface;
mod dynamic_tuple_interface;
mod dynamic_vec_interface;
mod enum_interface;
mod indexer;
mod interface_cache;
mod map_interface;
mod option_interface;
mod tuple_interface;
mod vec_interface;

pub(crate) use dynamic_map_interface::DynamicMapTypeInterface;
pub(crate) use dynamic_option_interface::DynamicOptionTypeInterface;
pub(crate) use dynamic_tuple_interface::DynamicTupleTypeInterface;
pub(crate) use dynamic_vec_interface::DynamicVecTypeInterface;
pub use enum_interface::EnumTypeInterface;
pub use indexer::Indexer;
pub use interface_cache::InterfaceCache;
pub use tuple_interface::TupleTypeInterface;

pub trait AnyVecInterface {
    fn signature(&self) -> &Signature;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    fn reserve(&self, data: &mut dyn Any, capacity: usize);
    fn len(&self, data: &dyn Any) -> Result<usize>;

    /// Assigns the given vector to the current data
    ///
    /// Should not use `convert::convert_any()` as it use this function for
    /// a conversion free path, using it would result in infinit recursion.
    ///
    /// # Error
    ///
    /// If the given vector could not be cast into the expected type, it is
    /// returned as an error.
    fn set(
        &self,
        data: &mut dyn Any,
        vec: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>>;

    fn push(&self, data: &mut dyn Any, value: AnyValue) -> Result<()>;
    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>>;
    fn new_element(&self) -> AnyValue;

    fn iter<'a>(&self, data: &'a dyn Any) -> Result<Box<dyn Iterator<Item = AnyRef<'a>> + 'a>>;
    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>>;
}

pub trait AnyMapInterface {
    fn signature(&self) -> &Signature;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    fn reserve(&self, data: &mut dyn Any, capacity: usize);
    fn len(&self, data: &dyn Any) -> Result<usize>;

    /// Assigns the given map to the current data
    ///
    /// Should not use `convert::convert_any()` as it use this function for
    /// a conversion free path, using it would result in infinit recursion.
    ///
    /// # Error
    ///
    /// If the given map could not be cast into the expected type, it is
    /// returned as an error.
    fn set(
        &self,
        data: &mut dyn Any,
        map: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>>;

    fn insert(
        &self,
        data: &mut dyn Any,
        key: AnyValue,
        value: AnyValue,
    ) -> Result<Option<AnyValue>>;
    fn new_key(&self) -> AnyValue;
    fn new_value(&self) -> AnyValue;

    fn iter<'a>(
        &self,
        data: &'a dyn Any,
    ) -> Result<Box<dyn Iterator<Item = (AnyRef<'a>, AnyRef<'a>)> + 'a>>;
    fn into_iter(
        &self,
        data: Box<dyn Any + Send>,
    ) -> Result<Box<dyn Iterator<Item = (AnyValue, AnyValue)>>>;
}

#[allow(clippy::len_without_is_empty)]
pub trait AnyTupleInterface {
    fn signature(&self) -> &Signature;

    fn field_names(&self) -> &[&'static str];
    fn len(&self) -> usize;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    /// Assigns the given tuple to the current data
    ///
    /// Should not use `convert::convert_any()` as it use this function for
    /// a conversion free path, using it would result in infinit recursion.
    ///
    /// # Error
    ///
    /// If the given tuple could not be cast into the expected type, it is
    /// returned as an error.
    fn set(
        &self,
        data: &mut dyn Any,
        tuple: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>>;

    fn get<'a>(&self, data: &'a dyn Any, idx: usize) -> Result<AnyRef<'a>>;
    fn get_mut<'a>(&self, data: &'a mut dyn Any, idx: usize) -> Result<AnyRefMut<'a>>;

    fn get_by_name<'a>(&self, data: &'a dyn Any, name: &str) -> Result<AnyRef<'a>> {
        match self
            .field_names()
            .iter()
            .enumerate()
            .find(|(_, n)| **n == name)
        {
            Some((idx, _)) => self.get(data, idx),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn get_mut_by_name<'a>(&self, data: &'a mut dyn Any, name: &str) -> Result<AnyRefMut<'a>> {
        match self
            .field_names()
            .iter()
            .enumerate()
            .find(|(_, n)| **n == name)
        {
            Some((idx, _)) => self.get_mut(data, idx),
            None => Err(AnyError::InvalidOperation),
        }
    }

    fn into_iter(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Iterator<Item = AnyValue>>>;

    /// Try to convert the struct
    ///
    /// If possible tries to construct missing field and move them to the `fields` map.
    fn convert_to(
        &self,
        fields: &mut std::collections::HashMap<&'static str, AnyValue>,
        dropped_fields: &mut std::collections::HashMap<String, AnyValue>,
        missing_fields: &mut Vec<&'static str>,
    ) -> Result<()> {
        let _fields = fields;
        let _dropped_fields = dropped_fields;
        let _missing_fields = missing_fields;
        Ok(())
    }

    /// Try to convert the struct
    ///
    /// If possible tries to construct missing fields.
    fn convert_from(
        &self,
        data: &mut dyn Any,
        dropped_fields: std::collections::HashMap<String, AnyValue>,
        missing_fields: Vec<&'static str>,
    ) -> Result<bool> {
        let _data = data;
        let _dropped_fields = dropped_fields;
        let _missing_fields = missing_fields;
        Ok(false)
    }
}

pub trait AnyOptionInterface {
    fn signature(&self) -> &Signature;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    /// Assigns the given option to the current data
    ///
    /// Should not use `convert::convert_any()` as it use this function for
    /// a conversion free path, using it would result in infinit recursion.
    ///
    /// # Error
    ///
    /// If the given option could not be cast into the expected type, it is
    /// returned as an error.
    fn set(
        &self,
        data: &mut dyn Any,
        option: Box<dyn Any + Send>,
    ) -> std::result::Result<(), Box<dyn Any + Send>>;

    fn set_some(&self, data: &mut dyn Any, value: AnyValue) -> Result<()>;
    fn new_element(&self) -> AnyValue;

    fn as_ref<'a>(&self, data: &'a dyn Any) -> Result<Option<AnyRef<'a>>>;
    fn as_refmut<'a>(&self, data: &'a mut dyn Any) -> Result<Option<AnyRefMut<'a>>>;

    fn into_inner(&self, data: Box<dyn Any + Send>) -> Result<Option<AnyValue>>;
}

#[derive(Clone, Copy, Debug)]
pub enum EnumRepr {
    Int8(i8),
    Int16(i16),
    Int32(i32),
    Int64(i64),
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    UInt64(u64),
}

#[derive(Clone, Copy, Debug)]
pub enum EnumReprKind {
    Int8,
    Int16,
    Int32,
    Int64,
    UInt8,
    UInt16,
    UInt32,
    UInt64,
}

pub trait EnumReprInterface: Copy + Typed {
    fn signature() -> &'static Signature;
    fn to_reprenum(self) -> EnumRepr;
    fn try_from_reprenum(val: EnumRepr) -> Result<Self>;
    fn kind() -> EnumReprKind;
}

pub trait AnyEnumInterface {
    fn signature(&self) -> &Signature;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    fn kind(&self) -> EnumReprKind;
    fn new_repr(&self) -> Box<dyn Any + Send>;
    fn to_repr(&self, data: &dyn Any) -> Result<EnumRepr>;
    fn try_from_repr<'a>(&self, data: &'a mut dyn Any, val: EnumRepr) -> Result<()>;
}

pub trait EnumInterface: std::fmt::Debug + Default + Copy + Send + 'static {
    type Repr: EnumReprInterface; // integer;

    fn new_any() -> Box<dyn Any + Send>;
    fn from_repr(val: Self::Repr) -> Self;
    fn to_repr(self) -> Self::Repr;
}

macro_rules! impl_enum_repr_interface {
    ($t:ty, $tag:ident) => {
        impl EnumReprInterface for $t {
            fn signature() -> &'static Signature {
                use crate::Typed;
                <$t as Typed>::signature()
            }

            fn to_reprenum(self) -> EnumRepr {
                EnumRepr::$tag(self)
            }

            fn try_from_reprenum(val: EnumRepr) -> Result<Self> {
                match val {
                    EnumRepr::$tag(val) => Ok(val),
                    _ => Err(AnyError::InvalidOperation),
                }
            }

            fn kind() -> EnumReprKind {
                EnumReprKind::$tag
            }
        }
    };
}

impl_enum_repr_interface!(i8, Int8);
impl_enum_repr_interface!(i16, Int16);
impl_enum_repr_interface!(i32, Int32);
impl_enum_repr_interface!(i64, Int64);
impl_enum_repr_interface!(u8, UInt8);
impl_enum_repr_interface!(u16, UInt16);
impl_enum_repr_interface!(u32, UInt32);
impl_enum_repr_interface!(u64, UInt64);

pub trait AnyObjectInterface {
    fn signature(&self) -> &Signature;

    fn debug_print(&self, data: &dyn Any) -> String;
    fn clone_data(&self, data: &dyn Any) -> Box<dyn Any + Send + 'static>;

    fn as_anyobject<'a>(&self, data: &'a dyn Any) -> Result<&'a dyn Any>;
    fn from_anyobject(&self, data: &mut dyn Any, anyobject: &dyn Any) -> Result<()>;

    fn into_anyobject<'a>(&self, data: Box<dyn Any + Send>) -> Result<Box<dyn Any + Send>>;
}
