mod any_kind;
mod any_ref;
mod any_ref_mut;
mod any_value;
mod buffer;
pub mod convert;
mod error;
pub mod interfaces;
mod registry;
mod signature;
mod type_interface;
pub mod visit;

pub use any_kind::AnyKind;
pub use any_ref::AnyRef;
pub use any_ref_mut::AnyRefMut;
pub use any_value::AnyValue;
pub use buffer::Buffer;
pub use error::{AnyError, DeserializationError, Result, SerializationError};
pub use registry::{register_type, select_type_interface_for, type_interface_for};
pub use signature::{Signature, SignatureGraph};
pub use type_interface::{ToAnyValue, TypeInterface, TypeInterfacePtr, Typed};

pub use qiany_derive::*;

#[cfg(test)]
mod tests {
    use super::*;

    fn fake_send<T: Send>(_val: T) {}

    #[test]
    fn anyvalue_is_send() {
        let v = AnyValue::Unit;
        fake_send(v);
    }

    #[test]
    fn anyvalue_from_u32() {
        let v = 45u32;
        let _v = ToAnyValue::to_anyvalue(&v);
    }

    #[test]
    fn anyvalue_from_vec_u32() {
        let v = vec![45u32, 32, 56];
        let _v = v.to_anyvalue();
    }

    #[test]
    fn anyvalue_from_tuple() {
        let t = (45, 42, 12);
        let v = t.to_anyvalue();
        println!("{:?}", v);
    }

    #[test]
    fn test_clone() {
        let v = AnyValue::Unit;
        let _ = v.clone();

        let v = AnyValue::UInt32(45);
        let _ = v.clone();

        let v = AnyValue::Dynamic(Box::new(v));
        let _ = v.clone();

        let vec = vec![45u32, 32, 56];
        let v = vec.to_anyvalue();
        let _ = v.clone();
    }

    #[test]
    fn test_debug() {
        let v = AnyValue::Unit;
        println!("{:?}", v);

        let v = AnyValue::UInt32(45);
        println!("{:?}", v);

        let v = AnyValue::Dynamic(Box::new(v));
        println!("{:?}", v);

        let vec = vec![45u32, 32, 56];
        let v = vec.to_anyvalue();
        println!("{:?}", v);
    }
}
