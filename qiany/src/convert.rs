use crate::{
    interfaces::{
        AnyEnumInterface, AnyMapInterface, AnyObjectInterface, AnyOptionInterface,
        AnyTupleInterface, AnyVecInterface, EnumRepr, EnumReprKind,
    },
    visit::VisitorMut,
    AnyError, AnyKind, AnyRefMut, AnyValue, Buffer, Result, Typed,
};
use std::{any::Any, convert::TryFrom};

pub fn try_from_value<T: Typed + 'static>(value: AnyValue) -> Result<T> {
    match value.downcast::<T>() {
        Ok(t) => Ok(t),
        Err(value) => {
            let value_signature = value.signature();
            let mut new_value = T::new_anyvalue();
            if let Err(_) = convert_any(value, new_value.as_anyrefmut()) {
                return Err(AnyError::InvalidDeserialization(
                    value_signature,
                    T::signature(),
                ));
            }

            Ok(new_value
                .downcast::<T>()
                .expect("anyvalue constructed from type interface of T should downcast to T"))
        }
    }
}

pub fn convert_any(from: AnyValue, to: AnyRefMut<'_>) -> Result<()> {
    let convertor = Convertor { from };
    convertor.visit(to)
}

pub struct Convertor {
    from: AnyValue,
}

macro_rules! try_integer_conversion {
    ($src:ident, $dest:ident, $t:ty) => {
        match <$t>::try_from($src) {
            Ok(v) => {
                *$dest = v;
                Ok(())
            }
            Err(_) => Err(AnyError::InvalidConversion),
        }
    };
}

macro_rules! enum_into_integer {
    ($data:ident, $interface:ident, $dest:ident, $t:ty) => {
        match $interface.to_repr(&*$data)? {
            EnumRepr::Int8(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::Int16(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::Int32(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::Int64(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::UInt8(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::UInt16(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::UInt32(v) => try_integer_conversion!(v, $dest, $t),
            EnumRepr::UInt64(v) => try_integer_conversion!(v, $dest, $t),
        }
    };
}

macro_rules! convert_integer {
    ($src:expr, $dest:ident, $t:ty) => {
        match $src {
            AnyValue::Bool(v) => {
                *$dest = if v { 1 } else { 0 };
                Ok(())
            }
            AnyValue::Int8(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::Int16(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::Int32(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::Int64(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::UInt8(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::UInt16(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::UInt32(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::UInt64(v) => try_integer_conversion!(v, $dest, $t),
            AnyValue::Enum(data, interface) => enum_into_integer!(data, interface, $dest, $t),
            _ => Err(AnyError::InvalidConversion),
        }
    };
}

macro_rules! integer_to_float {
    ($src:ident, $dest:ident, $t:ty) => {{
        *$dest = $src as $t;
        Ok(())
    }};
}

macro_rules! into_bool {
    ($src:ident, $dest:ident) => {{
        *$dest = if $src == 0 { false } else { true };
        Ok(())
    }};
}

macro_rules! try_enum_from_integer_kind {
    ($data:ident, $interface:ident, $val:ident, $t:ty, $tag:ident) => {{
        let repr = match <$t>::try_from($val) {
            Ok(v) => Ok(EnumRepr::$tag(v)),
            Err(_) => Err(AnyError::InvalidConversion),
        }?;
        $interface.try_from_repr($data, repr)?;
        Ok(())
    }};
}

macro_rules! try_enum_from_integer {
    ($data:ident, $interface:ident, $val:ident) => {
        match $interface.kind() {
            EnumReprKind::Int8 => try_enum_from_integer_kind!($data, $interface, $val, i8, Int8),
            EnumReprKind::Int16 => try_enum_from_integer_kind!($data, $interface, $val, i16, Int16),
            EnumReprKind::Int32 => try_enum_from_integer_kind!($data, $interface, $val, i32, Int32),
            EnumReprKind::Int64 => try_enum_from_integer_kind!($data, $interface, $val, i64, Int64),
            EnumReprKind::UInt8 => try_enum_from_integer_kind!($data, $interface, $val, u8, UInt8),
            EnumReprKind::UInt16 => {
                try_enum_from_integer_kind!($data, $interface, $val, u16, UInt16)
            }
            EnumReprKind::UInt32 => {
                try_enum_from_integer_kind!($data, $interface, $val, u32, UInt32)
            }
            EnumReprKind::UInt64 => {
                try_enum_from_integer_kind!($data, $interface, $val, u64, UInt64)
            }
        }
    };
}

impl VisitorMut<Result<()>> for Convertor {
    fn visit_unit(self) -> Result<()> {
        match self.from {
            AnyValue::Unit => Ok(()),
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_bool(self, val: &mut bool) -> Result<()> {
        match self.from {
            AnyValue::Bool(v) => {
                *val = v;
                Ok(())
            }
            AnyValue::Int8(v) => into_bool!(v, val),
            AnyValue::Int16(v) => into_bool!(v, val),
            AnyValue::Int32(v) => into_bool!(v, val),
            AnyValue::Int64(v) => into_bool!(v, val),
            AnyValue::UInt8(v) => into_bool!(v, val),
            AnyValue::UInt16(v) => into_bool!(v, val),
            AnyValue::UInt32(v) => into_bool!(v, val),
            AnyValue::UInt64(v) => into_bool!(v, val),
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_i8(self, val: &mut i8) -> Result<()> {
        convert_integer!(self.from, val, i8)
    }

    fn visit_i16(self, val: &mut i16) -> Result<()> {
        convert_integer!(self.from, val, i16)
    }

    fn visit_i32(self, val: &mut i32) -> Result<()> {
        convert_integer!(self.from, val, i32)
    }

    fn visit_i64(self, val: &mut i64) -> Result<()> {
        convert_integer!(self.from, val, i64)
    }

    fn visit_u8(self, val: &mut u8) -> Result<()> {
        convert_integer!(self.from, val, u8)
    }

    fn visit_u16(self, val: &mut u16) -> Result<()> {
        convert_integer!(self.from, val, u16)
    }

    fn visit_u32(self, val: &mut u32) -> Result<()> {
        convert_integer!(self.from, val, u32)
    }

    fn visit_u64(self, val: &mut u64) -> Result<()> {
        convert_integer!(self.from, val, u64)
    }

    fn visit_f32(self, val: &mut f32) -> Result<()> {
        match self.from {
            AnyValue::Float(v) => {
                *val = v;
                Ok(())
            }
            AnyValue::Double(v) => {
                *val = v as f32;
                Ok(())
            }
            AnyValue::Int8(v) => integer_to_float!(v, val, f32),
            AnyValue::Int16(v) => integer_to_float!(v, val, f32),
            AnyValue::Int32(v) => integer_to_float!(v, val, f32),
            AnyValue::Int64(v) => integer_to_float!(v, val, f32),
            AnyValue::UInt8(v) => integer_to_float!(v, val, f32),
            AnyValue::UInt16(v) => integer_to_float!(v, val, f32),
            AnyValue::UInt32(v) => integer_to_float!(v, val, f32),
            AnyValue::UInt64(v) => integer_to_float!(v, val, f32),
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_f64(self, val: &mut f64) -> Result<()> {
        match self.from {
            AnyValue::Float(v) => {
                *val = v as f64;
                Ok(())
            }
            AnyValue::Double(v) => {
                *val = v;
                Ok(())
            }
            AnyValue::Int8(v) => integer_to_float!(v, val, f64),
            AnyValue::Int16(v) => integer_to_float!(v, val, f64),
            AnyValue::Int32(v) => integer_to_float!(v, val, f64),
            AnyValue::Int64(v) => integer_to_float!(v, val, f64),
            AnyValue::UInt8(v) => integer_to_float!(v, val, f64),
            AnyValue::UInt16(v) => integer_to_float!(v, val, f64),
            AnyValue::UInt32(v) => integer_to_float!(v, val, f64),
            AnyValue::UInt64(v) => integer_to_float!(v, val, f64),
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_str(self, val: &mut String) -> Result<()> {
        match self.from {
            AnyValue::String(s) => {
                *val = s;
                Ok(())
            }
            AnyValue::Raw(b) => match String::from_utf8(b.into_inner()) {
                Ok(s) => {
                    *val = s;
                    Ok(())
                }
                _ => Err(AnyError::InvalidConversion),
            },
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_raw(self, val: &mut Buffer) -> Result<()> {
        match self.from {
            AnyValue::Raw(b) => {
                *val = b;
                Ok(())
            }
            AnyValue::String(s) => {
                *val = Buffer::from(s.into_bytes());
                Ok(())
            }
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_dynamic(self, val: &mut AnyValue) -> Result<()> {
        match self.from.kind() {
            AnyKind::Dynamic => match self.from {
                AnyValue::Dynamic(any) => {
                    *val = *any;
                    Ok(())
                }
                _ => unreachable!(),
            },
            _ => {
                *val = AnyValue::Dynamic(Box::new(self.from));
                Ok(())
            }
        }
    }

    fn visit_vec(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyVecInterface + Sync),
    ) -> Result<()> {
        match self.from {
            AnyValue::Vec(src_data, src_interface) => {
                // try to assign the target directly if the types match
                let src_data = if std::ptr::eq(src_interface, interface) {
                    match interface.set(data, src_data) {
                        Ok(_) => return Ok(()),
                        Err(src_data) => src_data,
                    }
                } else {
                    src_data
                };
                // otherwise convert vector content
                interface.reserve(data, src_interface.len(&*src_data)?);
                let iter = src_interface.into_iter(src_data)?;
                for value in iter {
                    let mut new_value = interface.new_element();
                    convert_any(value, new_value.as_anyrefmut())?;
                    interface.push(data, new_value)?;
                }
                Ok(())
            }
            // Map<K, V> as Vec<(K, V)> ?
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_map(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyMapInterface + Sync),
    ) -> Result<()> {
        match self.from {
            AnyValue::Map(src_data, src_interface) => {
                // try to assign the target directly if the types match
                let src_data = if std::ptr::eq(src_interface, interface) {
                    match interface.set(data, src_data) {
                        Ok(_) => return Ok(()),
                        Err(src_data) => src_data,
                    }
                } else {
                    src_data
                };
                // otherwise convert map content
                interface.reserve(data, src_interface.len(&*src_data)?);
                let iter = src_interface.into_iter(src_data)?;
                for (key, value) in iter {
                    let mut new_key = interface.new_key();
                    convert_any(key, new_key.as_anyrefmut())?;
                    let mut new_value = interface.new_value();
                    convert_any(value, new_value.as_anyrefmut())?;
                    interface.insert(data, new_key, new_value)?;
                }
                Ok(())
            }
            AnyValue::Tuple(src_data, src_interface) => {
                if src_interface.field_names().is_empty()
                    || !interface.signature().as_string().starts_with("{s")
                {
                    Err(AnyError::InvalidConversion)
                } else {
                    interface.reserve(data, src_interface.len());
                    let names = src_interface
                        .field_names()
                        .iter()
                        .map(|n| AnyValue::String((*n).to_owned()));
                    let iter = names.zip(src_interface.into_iter(src_data)?);
                    for (key, value) in iter {
                        let mut new_value = interface.new_value();
                        convert_any(value, new_value.as_anyrefmut())?;
                        interface.insert(data, key, new_value)?;
                    }
                    Ok(())
                }
            }
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_option(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyOptionInterface + Sync),
    ) -> Result<()> {
        match self.from {
            AnyValue::Option(src_data, src_interface) => {
                // try to assign the target directly if the types match
                let src_data = if std::ptr::eq(src_interface, interface) {
                    match interface.set(data, src_data) {
                        Ok(_) => return Ok(()),
                        Err(src_data) => src_data,
                    }
                } else {
                    src_data
                };
                // otherwise convert option content
                match src_interface.into_inner(src_data)? {
                    Some(inner) => {
                        let mut new_inner = interface.new_element();
                        convert_any(inner, new_inner.as_anyrefmut())?;
                        interface.set_some(data, new_inner)?;
                        Ok(())
                    }
                    None => Ok(()),
                }
            }
            // consider anything else as Some(T) ?
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_tuple(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyTupleInterface + Sync),
    ) -> Result<()> {
        let len = interface.len();
        match self.from {
            AnyValue::Tuple(src_data, src_interface) => {
                // try to assign the target directly if the types match
                let src_data = if std::ptr::eq(src_interface, interface) {
                    match interface.set(data, src_data) {
                        Ok(_) => return Ok(()),
                        Err(src_data) => src_data,
                    }
                } else {
                    src_data
                };
                // otherwise convert tuple content
                if src_interface.field_names().is_empty()
                    || interface.field_names().is_empty()
                    || src_interface.field_names() == interface.field_names()
                {
                    // allow src.len() > dest.len() ?
                    if src_interface.len() == len {
                        let iter = src_interface.into_iter(src_data)?;
                        for (idx, value) in iter.enumerate() {
                            let element = interface.get_mut(data, idx)?;
                            convert_any(value, element)?;
                        }
                        Ok(())
                    } else {
                        Err(AnyError::InvalidConversion)
                    }
                } else {
                    convert_struct(src_data, src_interface, data, interface)
                }
            }
            AnyValue::Map(src_data, src_interface) => {
                if !interface.field_names().is_empty()
                    && src_interface.signature().as_string() == "{sm}"
                {
                    struct_from_map(src_data, src_interface, data, interface)
                } else {
                    Err(AnyError::InvalidConversion)
                }
            }
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_enum(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyEnumInterface + Sync),
    ) -> Result<()> {
        match self.from {
            AnyValue::Int8(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::Int16(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::Int32(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::Int64(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::UInt8(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::UInt16(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::UInt32(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::UInt64(val) => try_enum_from_integer!(data, interface, val),
            AnyValue::Enum(src_data, src_interface) => match src_interface.to_repr(&*src_data)? {
                EnumRepr::Int8(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::Int16(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::Int32(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::Int64(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::UInt8(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::UInt16(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::UInt32(val) => try_enum_from_integer!(data, interface, val),
                EnumRepr::UInt64(val) => try_enum_from_integer!(data, interface, val),
            },
            _ => Err(AnyError::InvalidConversion),
        }
    }

    fn visit_object(
        self,
        data: &mut dyn Any,
        interface: &'static (dyn AnyObjectInterface + Sync),
    ) -> Result<()> {
        match self.from {
            AnyValue::Object(src_data, src_interface) => {
                let anyobject = src_interface.as_anyobject(&*src_data)?;
                interface.from_anyobject(data, anyobject)?;
                Ok(())
            }
            _ => Err(AnyError::InvalidConversion),
        }
    }
}

fn convert_struct(
    src_data: Box<dyn Any + Send>,
    src_interface: &'static (dyn AnyTupleInterface + Sync),
    dst_data: &mut dyn Any,
    dst_interface: &'static (dyn AnyTupleInterface + Sync),
) -> Result<()> {
    use std::collections::HashMap;

    // Collect all available fields
    let names = src_interface.field_names().iter().map(|n| *n);
    let iter = names.zip(src_interface.into_iter(src_data)?);
    let elements = iter.collect::<HashMap<&'static str, AnyValue>>();

    // Separate expected fields from unexpected fields
    let mut fields = HashMap::new();
    let mut dropped_fields = HashMap::new();
    for (name, element) in elements.into_iter() {
        match dst_interface.field_names().iter().find(|n| **n == name) {
            Some(_) => {
                fields.insert(name, element);
            }
            None => {
                dropped_fields.insert(name.to_string(), element);
            }
        }
    }

    // List missing fields
    let mut missing_fields = Vec::new();
    for name in dst_interface.field_names().iter() {
        if let None = fields.iter().find(|(k, _)| *k == name) {
            missing_fields.push(*name);
        }
    }

    if !missing_fields.is_empty() {
        // Tries conversion using source type interface
        src_interface.convert_to(&mut fields, &mut dropped_fields, &mut missing_fields)?;

        // Convert all known fields
        for (name, element) in fields.into_iter() {
            let field = dst_interface.get_mut_by_name(dst_data, name)?;
            convert_any(element, field)?;
        }

        // If missing fields tries to finish struct using target type interface
        if !missing_fields.is_empty()
            && !dst_interface.convert_from(dst_data, dropped_fields, missing_fields)?
        {
            Err(AnyError::InvalidConversion)
        } else {
            Ok(())
        }
    } else {
        // All fields where found
        for (name, element) in fields.into_iter() {
            let field = dst_interface.get_mut_by_name(dst_data, name)?;
            convert_any(element, field)?;
        }
        Ok(())
    }
}

fn struct_from_map(
    src_data: Box<dyn Any + Send>,
    src_interface: &'static (dyn AnyMapInterface + Sync),
    dst_data: &mut dyn Any,
    dst_interface: &'static (dyn AnyTupleInterface + Sync),
) -> Result<()> {
    use std::collections::HashMap;

    // Convert the any map to a field map
    let elements = src_interface
        .into_iter(src_data)?
        .map(
            |(k, v)| match (k.downcast::<String>(), v.downcast::<AnyValue>()) {
                (Ok(k), Ok(v)) => Ok((k, v)),
                _ => Err(AnyError::InvalidConversion),
            },
        )
        .collect::<Result<HashMap<String, AnyValue>>>()
        .map_err(|_| AnyError::InvalidConversion)?;

    // Separate expected fields from unexpected fields
    let mut fields = HashMap::new();
    let mut dropped_fields = HashMap::new();
    for (name, element) in elements.into_iter() {
        match dst_interface.field_names().iter().find(|n| **n == name) {
            Some(_) => {
                fields.insert(name, element);
            }
            None => {
                dropped_fields.insert(name, element);
            }
        }
    }

    // List missing fields
    let mut missing_fields = Vec::new();
    for name in dst_interface.field_names().iter() {
        if let None = fields.iter().find(|(k, _)| *k == name) {
            missing_fields.push(*name);
        }
    }

    // Convert found fields
    for (name, element) in fields.into_iter() {
        let field = dst_interface.get_mut_by_name(dst_data, &name)?;
        convert_any(element, field)?;
    }

    if !missing_fields.is_empty() {
        // If missing fields tries to finish struct using target type interface
        if !dst_interface.convert_from(dst_data, dropped_fields, missing_fields)? {
            Err(AnyError::InvalidConversion)
        } else {
            Ok(())
        }
    } else {
        Ok(())
    }
}
