use qiany::{AnyValue, Buffer, ToAnyValue};
use std::collections::HashMap;

macro_rules! impl_convert_integer_inner {
    ($from:ty, $to:ty) => {
        let x: $from = 45;
        let v = x.to_anyvalue();
        let xx: $to = v.try_into().unwrap();
        assert_eq!(45, xx);
    };
}

macro_rules! impl_convert_integer {
    ($name:ident, $t:ty) => {
        #[test]
        fn $name() {
            impl_convert_integer_inner!(i8, $t);
            impl_convert_integer_inner!(i16, $t);
            impl_convert_integer_inner!(i32, $t);
            impl_convert_integer_inner!(i64, $t);
            impl_convert_integer_inner!(u8, $t);
            impl_convert_integer_inner!(u16, $t);
            impl_convert_integer_inner!(u32, $t);
            impl_convert_integer_inner!(u64, $t);
        }
    };
}

impl_convert_integer!(convert_i8, i8);
impl_convert_integer!(convert_i16, i16);
impl_convert_integer!(convert_i32, i32);
impl_convert_integer!(convert_i64, i64);
impl_convert_integer!(convert_u8, u8);
impl_convert_integer!(convert_u16, u16);
impl_convert_integer!(convert_u32, u32);
impl_convert_integer!(convert_u64, u64);

macro_rules! assert_approx {
    ($left:expr, $right:ident) => {{
        let delta = ($left - $right).abs();
        if !(delta < 1e-5) {
            panic!("assertion failed: `({} ≈ {})`", $left, $right)
        }
    }};
}

macro_rules! impl_convert_integer_to_float {
    ($from:ty, $to:ty) => {
        let x: $from = 45;
        let v = x.to_anyvalue();
        let xx: $to = v.try_into().unwrap();
        assert_approx!(45., xx);
    };
}

macro_rules! impl_convert_float {
    ($name:ident, $t:ty) => {
        #[test]
        fn $name() {
            let x: f32 = 45.;
            let v = x.to_anyvalue();
            let xx: $t = v.try_into().unwrap();
            assert_approx!(45., xx);

            let x: f64 = 45.;
            let v = x.to_anyvalue();
            let xx: $t = v.try_into().unwrap();
            assert_approx!(45., xx);

            impl_convert_integer_to_float!(i8, $t);
            impl_convert_integer_to_float!(i16, $t);
            impl_convert_integer_to_float!(i32, $t);
            impl_convert_integer_to_float!(i64, $t);
            impl_convert_integer_to_float!(u8, $t);
            impl_convert_integer_to_float!(u16, $t);
            impl_convert_integer_to_float!(u32, $t);
            impl_convert_integer_to_float!(u64, $t);
        }
    };
}

impl_convert_float!(convert_f32, f32);
impl_convert_float!(convert_f64, f64);

#[test]
fn convert_string() {
    let s = "some text".to_string();
    let v = s.to_anyvalue();
    let ss: String = v.try_into().unwrap();
    assert_eq!(s, ss);

    let b = Buffer::from(b"some text".to_vec());
    let v = b.to_anyvalue();
    let ss: String = v.try_into().unwrap();
    assert_eq!(s, ss);
}

#[test]
fn convert_buffer() {
    let b = Buffer::from(b"some text".to_vec());
    let v = b.to_anyvalue();
    let bb: Buffer = v.try_into().unwrap();
    assert_eq!(b, bb);

    let s = "some text".to_string();
    let v = s.to_anyvalue();
    let bb: Buffer = v.try_into().unwrap();
    assert_eq!(b, bb);
}

#[test]
fn convert_dynamic() {
    let x = 45u8;
    let d = x.to_anyvalue();
    let v = d.to_anyvalue();
    let dd: AnyValue = v.try_into().unwrap();
    let xx: u8 = dd.try_into().unwrap();
    assert_eq!(x, xx);
}

#[test]
fn convert_vec() {
    let v1 = vec![45u8, 32, 78];
    let any = v1.to_anyvalue();
    let vv: Vec<u8> = any.try_into().unwrap();
    assert_eq!(v1, vv);

    let v2 = vec![45u32, 32, 78];
    let any = v2.to_anyvalue();
    let vv: Vec<u8> = any.try_into().unwrap();
    assert_eq!(v1, vv);
}

#[test]
fn convert_vec_any() {
    let v1 = vec![AnyValue::Int8(45), AnyValue::UInt16(96)];
    let any = v1.to_anyvalue();
    let _vv: Vec<AnyValue> = any.try_into().unwrap();
}

#[test]
fn convert_map() {
    let vec = vec![
        (45, "Norway".to_string()),
        (16, "Denmark".to_string()),
        (84, "Iceland".to_string()),
    ];
    let map: HashMap<i32, String> = vec.iter().cloned().collect();
    let v = map.to_anyvalue();
    let m = v.try_into().unwrap();
    assert_eq!(map, m);

    let v = map.to_anyvalue();
    let m: HashMap<u64, Buffer> = v.try_into().unwrap();
    let vec = vec![
        (45, Buffer::from(b"Norway".to_vec())),
        (16, Buffer::from(b"Denmark".to_vec())),
        (84, Buffer::from(b"Iceland".to_vec())),
    ];
    let map: HashMap<u64, Buffer> = vec.iter().cloned().collect();
    assert_eq!(map, m);
}

// #[test]
// fn convert_map_to_vec() {
//     let vec = vec![
//         (45, "Norway".to_string()),
//         (16, "Denmark".to_string()),
//         (84, "Iceland".to_string()),
//     ];
//     let map: HashMap<i32, String> = vec.iter().cloned().collect();
//     let v = map.to_anyvalue();
//     let vec2: Vec<(i32, String)> = v.try_into().unwrap();
//     assert_eq!(vec, vec2);
// }

#[test]
fn convert_option() {
    let o = Some(45u8);
    let any = o.to_anyvalue();
    let oo: Option<i64> = any.try_into().unwrap();
    assert_eq!(Some(45), oo);

    let o: Option<u8> = None;
    let any = o.to_anyvalue();
    let oo: Option<i64> = any.try_into().unwrap();
    assert_eq!(None, oo);
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, qiany::AnyEnum)]
#[repr(u8)]
enum MyEnum {
    Unknown = 0,
    First = 1,
    Third = 3,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, qiany::AnyEnum)]
#[repr(i32)]
enum AnotherEnum {
    Unknown = 0,
    First = 1,
    Third = 3,
}

#[test]
fn convert_enum() {
    let x = 1;
    let any = x.to_anyvalue();
    let e: MyEnum = any.try_into().unwrap();
    assert_eq!(MyEnum::First, e);

    let x = AnotherEnum::First;
    let any = x.to_anyvalue();
    let e: MyEnum = any.try_into().unwrap();
    assert_eq!(MyEnum::First, e);

    let x = 45;
    let any = x.to_anyvalue();
    let e: MyEnum = any.try_into().unwrap();
    assert_eq!(MyEnum::Unknown, e);
}

#[test]
fn convert_tuple() {
    let x = (45u8, "text".to_string());
    let any = x.to_anyvalue();
    let t: (u32, Buffer) = any.try_into().unwrap();
    assert_eq!((45, Buffer::from(b"text".to_vec())), t);
}

#[derive(Clone, Default, Debug, PartialEq, Eq, qiany::AnyStruct)]
struct MyStruct {
    val: u8,
    text: String,
}

#[derive(Clone, Default, Debug, PartialEq, Eq, qiany::AnyStruct)]
struct AnotherStruct {
    text: Buffer,
    val: i64,
}

#[test]
fn convert_struct() {
    let s = MyStruct {
        val: 45,
        text: "text".to_string(),
    };
    let any = s.to_anyvalue();
    let ss: AnotherStruct = any.try_into().unwrap();
    let expect = AnotherStruct {
        text: Buffer::from(b"text".to_vec()),
        val: 45,
    };
    assert_eq!(expect, ss);

    let vec = vec![
        ("val".to_string(), AnyValue::Int16(45)),
        ("text".to_string(), AnyValue::String("text".to_string())),
        ("other".to_string(), AnyValue::Float(5.3)),
    ];
    let map: HashMap<String, AnyValue> = vec.iter().cloned().collect();
    let any = map.to_anyvalue();
    let ss: MyStruct = any.try_into().unwrap();
    assert_eq!(s, ss);
}

#[test]
fn convert_vec_of_struct() {
    let s = MyStruct {
        val: 45,
        text: "text".to_string(),
    };
    let vec = vec![s.clone(), s.clone()];
    let any = vec.to_anyvalue();
    let ss: Vec<AnotherStruct> = any.try_into().unwrap();
    assert_eq!(2, ss.len());
}

#[test]
fn convert_struct_to_map() {
    let s = MyStruct {
        val: 45,
        text: "text".to_string(),
    };
    let any = s.to_anyvalue();
    let _map: HashMap<String, AnyValue> = any.try_into().unwrap();
}

#[derive(Clone, Default, Debug, PartialEq, Eq, qiany::AnyStruct)]
struct MyPoint {
    x: u8,
    y: u8,
    z: u8,
}

#[test]
fn convert_struct_to_map_2() {
    let s = MyPoint {
        x: 45,
        y: 45,
        z: 45,
    };
    let any = s.to_anyvalue();
    let _map: HashMap<String, u8> = any.try_into().unwrap();
}
