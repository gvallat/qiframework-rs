# QiAny

Type erasure crate for use with `qimessaging`.

The main enum is `AnyValue`, it's the data struct use to serialize and deserialize messages within the `qimessaging` protocol.

## Advancement

- [X] `Signature` (struct, validation, graph representation)
- [X] `AnyValue` enum and associated `AnyRef` and `AnyRefMt`
- [X] Visit interface for `AnyValue`
- [X] Deserialize binary buffer to `AnyValue`
- [X] Serialize `AnyValue` to binary buffer
- [X] `TypeInterface` for builtin types
- [X] `TypeInterface` for `Vec<T>`
- [X] `TypeInterface` for `Map<K,V>`
- [X] `TypeInterface` for `Option<T>`
- [X] `TypeInterface` for tuples and structures
- [X] `TypeInterface` for enums
- [X] Dynamic `TypeInterface` for vectors
- [X] Dynamic `TypeInterface` for maps
- [X] Dynamic `TypeInterface` for options
- [X] Dynamic `TypeInterface` for tuples adn structures
- [X] Registry for types interfaces
- [X] Derive `AnyEnum`
- [X] Derive `AnyStruct`
- [X] Implement conversion from `AnyValue` to `T: Typed`
  - [X] Add `AnyValue::try_into()`
  - [X] Add convertion support
  - [ ] Add `convert_to` and `conver_from` in `Indexer` (to rename into `StructInterface`)
  - [ ] Add attributes `added_fields` and `dropped_fields` to derive `AnyStruct`
  - [ ] Add attributes `convert_to` and `convert_from` to derive `AnyStruct`

## Example

```rust
#[derive(Debug, Default, Clone, qiany::AnyStruct, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
struct MyData {
    count: u32,
    running: bool,
    name: String,
    point: (u32, u32),
    hosts: HashMap<u32, String>,
}

#[derive(Debug, Clone, Copy, qiany::AnyEnum)]
#[repr(i32)]
enum MyEnum {
    First,
    Second,
    Third,
}

fn main() {
    let s = MyData::default();
    let v = s.to_anyvalue();
    println!("MyData signature: {}", MyData::signature().as_string());
    println!("{:?}", v);

    let e = MyEnum::Second;
    let v = e.to_anyvalue();
    println!("MyEnum signature: {}", MyEnum::signature().as_string());
    println!("{:?}", v);
}
```
