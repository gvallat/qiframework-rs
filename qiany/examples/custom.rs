use qiany::{ToAnyValue, Typed};
use std::collections::HashMap;

#[derive(Debug, Default, Clone, qiany::AnyStruct)]
struct MyData {
    count: u32,
    running: bool,
    name: String,
    point: (u32, u32),
    hosts: HashMap<u32, String>,
}

#[derive(Debug, Default, Clone, qiany::AnyStruct)]
struct MyTuple(u32, String, Vec<bool>);

#[derive(Debug, Clone, Copy, qiany::AnyEnum)]
#[repr(i32)]
enum MyEnum {
    First,
    Second,
    Third,
}

fn main() {
    let mut hosts = HashMap::new();
    hosts.insert(45, "stuff".to_string());
    hosts.insert(52, "other".to_string());

    let d = MyData {
        count: 4,
        running: false,
        name: "me".to_string(),
        point: (12, 369),
        hosts,
    };

    let v = d.to_anyvalue();
    println!("MyData signature: {}", MyData::signature().as_string());
    println!("{:?}", v);

    let t = MyTuple(45, "Test".to_string(), vec![true, false]);
    let at = t.to_anyvalue();
    println!("MyTuple signature: {}", MyTuple::signature().as_string());
    println!("{:?}", at);

    let s = MyEnum::Second;
    let v = s.to_anyvalue();
    println!("MyEnum signature: {}", MyEnum::signature().as_string());
    println!("{:?}", v);
}
