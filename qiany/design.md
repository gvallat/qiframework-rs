# Design

```plantuml
interface Visitor<T> {
  visit(&mut self, AnyRef) -> T
  visit_map(&mut self, &Any, &static AnyMapInterface) -> T
  // visit_*() ...
}

class Convertor {
  convert(source: AnyRef, target: AnyValue) -> Result<()>
}
Visitor <-- Convertor

class BinarySerializer {
  serialize(source: AnyRef) -> Result<Bytes>
}
Visitor <-- BinarySerializer
```
