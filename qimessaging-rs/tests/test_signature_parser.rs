use qimessaging_rs::signature::Signature;

#[test]
fn parse_none() {
    let sig = Signature::new("_".to_owned()).unwrap();
    assert_eq!(sig.full_string(), "_");
    assert_eq!(sig.simplified_string(), "_");
}

#[test]
fn parse_unknown() {
    let sig = Signature::new("X".to_owned()).unwrap();
    assert_eq!(sig.full_string(), "X");
    assert_eq!(sig.simplified_string(), "X");
}

#[test]
fn parse_base_types() {
    for s in &[
        "v", "b", "c", "C", "w", "W", "i", "I", "l", "L", "f", "d", "s", "m", "r", "o",
    ] {
        let sig = Signature::new(s.to_string()).unwrap();
        assert_eq!(sig.full_string(), *s);
        assert_eq!(sig.simplified_string(), *s);
    }
}

#[test]
fn parse_invalid() {
    let res = Signature::new("8".to_owned());
    assert!(res.is_err());
}

#[test]
fn parse_extra_data() {
    let res = Signature::new("ii".to_owned());
    assert!(res.is_err());
}

#[test]
fn parse_optional() {
    let sig = Signature::new("+i".to_owned()).unwrap();
    assert_eq!(sig.full_string(), "+i");
    assert_eq!(sig.simplified_string(), "+i");
}

#[test]
fn parse_optional_missing_type() {
    let res = Signature::new("+".to_owned());
    assert!(res.is_err());
}

#[test]
fn parse_unexpected_closure() {
    for s in &[")", "]", "}"] {
        let res = Signature::new(s.to_string());
        assert!(res.is_err());
    }
}

#[test]
fn parse_list() {
    let raw = "[i]";
    let sig = Signature::new(raw.to_string()).unwrap();
    assert_eq!(sig.full_string(), raw);
    assert_eq!(sig.simplified_string(), raw);
}

#[test]
fn parse_list_eof() {
    let raw = "[";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_list_invalid() {
    let raw = "[8]";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_list_expected_end() {
    let raw = "[ii]";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_map() {
    let raw = "{is}";
    let sig = Signature::new(raw.to_string()).unwrap();
    assert_eq!(sig.full_string(), raw);
    assert_eq!(sig.simplified_string(), raw);
}

#[test]
fn parse_map_eof() {
    let raw = "{";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_map_invalid() {
    let raw = "{s8}";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_map_expected_end() {
    let raw = "{iis}";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_no_names() {
    let raw = "(il)";
    let sig = Signature::new(raw.to_string()).unwrap();
    assert_eq!(sig.full_string(), raw);
    assert_eq!(sig.simplified_string(), raw);
}

#[test]
fn parse_tuple_eof() {
    let raw = "(";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_invalid() {
    let raw = "(s8)";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple() {
    let raw = "(il)<My<x>,var1,var2>";
    let sig = Signature::new(raw.to_string()).unwrap();
    assert_eq!(sig.full_string(), raw);
    assert_eq!(sig.simplified_string(), "(il)");
}

#[test]
fn parse_tuple_invalid_name() {
    let raw = "(il)<My$,var1,var2>";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_invalid_field_name() {
    let raw = "(il)<My,va$r1,var2>";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_missing_names() {
    let raw = "(il)<My,var1>";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_to_mutch_names() {
    let raw = "(il)<My,var1,var2,var3>";
    let res = Signature::new(raw.to_string());
    assert!(res.is_err());
}

#[test]
fn parse_tuple_inner() {
    let raw = "(i(ll)<Stuff,var1,var2>)<Other,x,y>";
    let sig = Signature::new(raw.to_string()).unwrap();
    assert_eq!(sig.full_string(), raw);
    assert_eq!(sig.simplified_string(), "(i(ll))");
}

#[test]
fn parse_naoqi_signatures() {
    let signatures = include_str!("signatures.txt");
    let mut ok = true;
    for sig_str in signatures.split("\n") {
        if let Err(_) = Signature::new(sig_str.to_string()) {
            ok = false;
            eprintln!("Could not parse: '{}'", sig_str);
        }
    }
    assert!(ok);
}
