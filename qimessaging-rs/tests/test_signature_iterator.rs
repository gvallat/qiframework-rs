use qimessaging_rs::signature::{Signature, Tag};

#[test]
fn iter_list() {
    let raw = "[i]";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();
    assert_eq!(iter.next().unwrap().tag(), Tag::List);
    assert_eq!(iter.next().unwrap().tag(), Tag::Int32);
    assert_eq!(iter.next().unwrap().tag(), Tag::ListEnd);
    assert!(iter.next().is_none());
}

#[test]
fn iter_tuple_no_name() {
    let raw = "(il)";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);
    assert_eq!(item.struct_name(), "");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int32);
    assert_eq!(item.field_name(), "");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int64);
    assert_eq!(item.field_name(), "");

    assert_eq!(iter.next().unwrap().tag(), Tag::TupleEnd);
    assert!(iter.next().is_none());
}

#[test]
fn iter_tuple() {
    let raw = "(il)<Stuff,int,long>";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);
    assert_eq!(item.struct_name(), "Stuff");
    assert_eq!(item.index(), 0);
    assert_eq!(item.tag_char(), '(');

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int32);
    assert_eq!(item.field_name(), "int");
    assert_eq!(item.index(), 1);
    assert_eq!(item.tag_char(), 'i');

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int64);
    assert_eq!(item.field_name(), "long");
    assert_eq!(item.index(), 2);
    assert_eq!(item.tag_char(), 'l');

    assert_eq!(iter.next().unwrap().tag(), Tag::TupleEnd);
    assert!(iter.next().is_none());
}

#[test]
fn iter_tuple_inner() {
    let raw = "(i(ll)<Stuff,var1,var2>)<Other,x,y>";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);
    assert_eq!(item.struct_name(), "Other");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int32);
    assert_eq!(item.field_name(), "x");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);
    assert_eq!(item.struct_name(), "Stuff");
    assert_eq!(item.field_name(), "y");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int64);
    assert_eq!(item.field_name(), "var1");

    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Int64);
    assert_eq!(item.field_name(), "var2");

    assert_eq!(iter.next().unwrap().tag(), Tag::TupleEnd);
    assert_eq!(iter.next().unwrap().tag(), Tag::TupleEnd);
    assert!(iter.next().is_none());
}

#[test]
fn iter_tuple_sub_signature() {
    let raw = "(i(ll)<Stuff,var1,var2>)<Other,x,y>";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    iter.next().unwrap();
    iter.next().unwrap();
    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);

    let sig_2 = item.sub_signature().unwrap();
    assert_eq!(sig_2.full_string(), "(ll)<Stuff,var1,var2>");
    assert_eq!(sig_2.simplified_string(), "(ll)");
}

#[test]
fn iter_tuple_inner_sub_signature() {
    let raw = "(i(l(ii)<Me,v,w>)<Stuff,var1,var2>)<Other,x,y>";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    iter.next().unwrap();
    iter.next().unwrap();
    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Tuple);

    let sig_2 = item.sub_signature().unwrap();
    assert_eq!(sig_2.full_string(), "(l(ii)<Me,v,w>)<Stuff,var1,var2>");
    assert_eq!(sig_2.simplified_string(), "(l(ii))");
}

#[test]
fn iter_optional_sub_signature() {
    let raw = "(i+(l)<Stuff,var1>)<Other,x,y>";
    let sig = Signature::new(raw.to_string()).unwrap();
    let mut iter = sig.iter();

    iter.next().unwrap();
    iter.next().unwrap();
    let item = iter.next().unwrap();
    assert_eq!(item.tag(), Tag::Optional);

    let sig_2 = item.sub_signature().unwrap();
    assert_eq!(sig_2.full_string(), "+(l)<Stuff,var1>");
    assert_eq!(sig_2.simplified_string(), "+(l)");
}
