use qimessaging_rs::{serialization::serialize_value, signature::Signature};

#[test]
fn serialize_u8() {
    let sig = Signature::new("C".to_string()).unwrap();
    let buf = serialize_value(&sig, &5u8).unwrap();
    assert_eq!(5u8, buf[0]);
}

#[test]
fn serialize_vec_u8() {
    let sig = Signature::new("[C]".to_string()).unwrap();
    let data = vec![0u8, 1, 2, 3, 4];
    let buf = serialize_value(&sig, &data).unwrap();
    let data = &[5, 0, 0, 0, 0, 1, 2, 3, 4];
    assert_eq!(data, &buf[..]);
}

#[test]
fn serialize_map_u8() {
    let sig = Signature::new("{CC}".to_string()).unwrap();
    let data: std::collections::HashMap<u8, u8> =
        [(0, 1), (2, 3), (4, 5)].iter().cloned().collect();
    let buf = serialize_value(&sig, &data).unwrap();
    // size
    let data = &[3, 0, 0, 0];
    assert_eq!(data, &buf[..4]);
    // data
    let data = &[&[0, 1], &[2, 3], &[4, 5]];
    let buf = &buf[4..];
    for i in 0..3 {
        let b = &buf[i * 2..(i * 2) + 2];
        assert!(data.iter().find(|d| &d[..] == b).is_some());
    }
}

#[test]
fn serialize_optional() {
    let sig = Signature::new("+C".to_string()).unwrap();
    let data = Some(6u8);
    let buf = serialize_value(&sig, &data).unwrap();
    let data = &[1, 6];
    assert_eq!(data, &buf[..]);
}

#[test]
fn serialize_optional_empty() {
    let sig = Signature::new("+C".to_string()).unwrap();
    let data: Option<u8> = None;
    let buf = serialize_value(&sig, &data).unwrap();
    let data = &[0];
    assert_eq!(data, &buf[..]);
}
