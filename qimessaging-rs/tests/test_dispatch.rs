use {
    bytes::BytesMut,
    futures::{channel::mpsc, sink::SinkExt, stream::StreamExt},
    qimessaging_rs::{
        transport::{message_codec::MessageCodec, Dispatcher, Message, Socket},
        url::Url,
    },
    std::{
        marker::PhantomData,
        sync::{
            atomic::{AtomicBool, Ordering},
            Arc, Mutex,
        },
    },
    tokio_util::codec::Framed,
};

struct MockDispatcherState {
    closed: AtomicBool,
    closed_sender: mpsc::UnboundedSender<()>,
    closed_receiver: Mutex<mpsc::UnboundedReceiver<()>>,
    event_sender: mpsc::UnboundedSender<u8>,
    event_receiver: Mutex<mpsc::UnboundedReceiver<u8>>,
    message_sender: mpsc::UnboundedSender<Message>,
    message_receiver: Mutex<mpsc::UnboundedReceiver<Message>>,
}

impl Default for MockDispatcherState {
    fn default() -> Self {
        let (c_sender, c_receiver) = mpsc::unbounded();
        let (e_sender, e_receiver) = mpsc::unbounded();
        let (m_sender, m_receiver) = mpsc::unbounded();
        MockDispatcherState {
            closed: AtomicBool::new(false),
            closed_sender: c_sender,
            closed_receiver: Mutex::new(c_receiver),
            event_sender: e_sender,
            event_receiver: Mutex::new(e_receiver),
            message_sender: m_sender,
            message_receiver: Mutex::new(m_receiver),
        }
    }
}

struct MockDispatcher {
    state: Arc<MockDispatcherState>,
}

impl Dispatcher for MockDispatcher {
    type State = Arc<MockDispatcherState>;
    type Event = u8;
    fn new(_url: Url, state: Self::State) -> Self {
        MockDispatcher { state }
    }

    fn handle_event(&mut self, event: Self::Event) {
        let _ = self.state.event_sender.unbounded_send(event);
    }

    fn handle_received_message(&mut self, msg: Message) {
        let _ = self.state.message_sender.unbounded_send(msg);
    }

    fn close(&mut self) {
        self.state.closed.store(true, Ordering::SeqCst);
        let _ = self.state.closed_sender.unbounded_send(());
    }
}

#[test]
fn socket_closed_on_drop() {
    let state = Arc::new(MockDispatcherState::default());
    let mut rt = tokio::runtime::Builder::new().basic_scheduler().enable_all().build().unwrap();
    rt.block_on(async {
        let (_test_stream, stream) = mockstream::stream();
        let dispatcher: PhantomData<MockDispatcher> = PhantomData;
        let _socket = Socket::new(stream, Url::default(), state.clone(), dispatcher);
    });
    rt.block_on(async {
        let mut receiver = state.closed_receiver.lock().unwrap();
        let _ = receiver.next().await.unwrap();
    });
    assert!(state.closed.load(Ordering::SeqCst));
}

#[test]
fn socket_connected() {
    let state = Arc::new(MockDispatcherState::default());
    let mut rt = tokio::runtime::Builder::new().basic_scheduler().enable_all().build().unwrap();
    rt.block_on(async {
        let (_test_stream, stream) = mockstream::stream();
        let dispatcher: PhantomData<MockDispatcher> = PhantomData;
        let socket = Socket::new(stream, Url::default(), state.clone(), dispatcher);
        assert!(socket.is_connected());
    });
}

fn make_test_message() -> Message {
    let data = &[0, 1, 2, 3, 4, 5, 6, 7];
    let mut bytes = BytesMut::new();
    bytes.extend_from_slice(&data[..]);
    let mut msg = Message::new(bytes.freeze());
    msg.header.msg_id = 59;
    msg.header.size = data.len() as u32;
    msg
}

fn assert_msg_eq(msg_1: &Message, msg_2: &Message) {
    assert_eq!(msg_1.header, msg_2.header);
    assert_eq!(msg_1.payload(), msg_2.payload());
}

#[test]
fn messages_sent_in_socket_are_send_in_stream() {
    let state = Arc::new(MockDispatcherState::default());
    let mut rt = tokio::runtime::Builder::new().basic_scheduler().enable_all().build().unwrap();
    rt.block_on(async {
        let (test_stream, stream) = mockstream::stream();
        let dispatcher: PhantomData<MockDispatcher> = PhantomData;
        let socket = Socket::new(stream, Url::default(), state.clone(), dispatcher);

        let msg = make_test_message();
        let res = socket.send_message(msg.clone());
        assert!(res.is_ok());

        let mut stream = Framed::new(test_stream, MessageCodec);
        let msg_2 = stream.next().await.unwrap().unwrap();
        assert_msg_eq(&msg, &msg_2);
    });
}

#[test]
fn messages_received_drom_stream_are_dispatched() {
    let state = Arc::new(MockDispatcherState::default());
    let mut rt = tokio::runtime::Builder::new().basic_scheduler().enable_all().build().unwrap();

    rt.block_on(async {
        let (test_stream, stream) = mockstream::stream();
        let dispatcher: PhantomData<MockDispatcher> = PhantomData;
        let _socket = Socket::new(stream, Url::default(), state.clone(), dispatcher);

        let mut stream = Framed::new(test_stream, MessageCodec);
        let msg = make_test_message();
        stream.send(msg.clone()).await.unwrap();
        stream.close().await.unwrap();

        let mut receiver = state.message_receiver.lock().unwrap();
        let msg_2 = receiver.next().await.unwrap();
        assert_msg_eq(&msg, &msg_2);
    });
}

#[test]
fn events_send_to_dispatcher_are_handled() {
    let state = Arc::new(MockDispatcherState::default());
    let mut rt = tokio::runtime::Builder::new().basic_scheduler().enable_all().build().unwrap();

    rt.block_on(async {
        let (_test_stream, stream) = mockstream::stream();
        let dispatcher: PhantomData<MockDispatcher> = PhantomData;
        let socket = Socket::new(stream, Url::default(), state.clone(), dispatcher);

        socket.send_event(8).unwrap();

        let mut receiver = state.event_receiver.lock().unwrap();
        let event = receiver.next().await.unwrap();
        assert_eq!(event, 8);
    });
}

// TODO test close
