use {
    crate::{
        serialization::{Deserialize, Deserializer, Serialize, Serializer},
        signature::GetTypeSignature,
        url::Url,
    },
    std::io::Result,
};

#[derive(Default, Debug, Clone)]
pub struct ServiceInfo {
    pub name: String,
    pub service_id: u32,
    pub machine_id: String,
    pub process_id: u32,
    pub endpoints: Vec<Url>,
    pub session_id: String,
}

impl GetTypeSignature for ServiceInfo {
    fn signature() -> String {
        "(sIsI[s]s)<ServiceInfo,name,serviceId,machineId,processId,endpoints,sessionId>".to_string()
    }
}

impl Serialize for ServiceInfo {
    fn serialize_into(&self, serializer: &mut Serializer<'_, '_>) -> Result<()> {
        let mut tuple = serializer.tuple_serializer("ServiceInfo")?;
        tuple.write_field("name", &self.name)?;
        tuple.write_field("serviceId", &self.service_id)?;
        tuple.write_field("machineId", &self.machine_id)?;
        tuple.write_field("processId", &self.process_id)?;
        tuple.write_field("endpoints", &self.endpoints)?;
        tuple.write_field("sessionId", &self.session_id)?;
        tuple.end()
    }
}

impl Deserialize for ServiceInfo {
    fn deserialize_from(deserializer: &mut Deserializer<'_, '_>) -> Result<Self> {
        let mut tuple = deserializer.tuple_deserializer("ServiceInfo")?;
        let name = tuple.read_field("name")?;
        let service_id = tuple.read_field("serviceId")?;
        let machine_id = tuple.read_field("machineId")?;
        let process_id = tuple.read_field("processId")?;
        let endpoints = tuple.read_field("endpoints")?;
        let session_id = tuple.read_field("sessionId")?;
        tuple.end()?;
        Ok(ServiceInfo {
            name,
            service_id,
            machine_id,
            process_id,
            endpoints,
            session_id,
        })
    }
}
