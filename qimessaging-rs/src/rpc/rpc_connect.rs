use {
    crate::{
        any::{Any, AnyEnum},
        rpc::{
            ClientAuthenticator, RpcDispatcher, RpcSocket, AUTHENTICATION_CONTINUE,
            AUTHENTICATION_DONE, AUTHENTICATION_ERROR, AUTHENTICATION_KEY_ERROR,
            AUTHENTICATION_KEY_STATE,
        },
        serialization::serialize_value,
        session::SessionTx,
        signature::Signature,
        transport::{self, Message},
        url::Url,
    },
    std::{collections::HashMap, io},
};

type CapacityMap = HashMap<String, Any>;

fn is_authentication_done(map: &CapacityMap) -> io::Result<bool> {
    match map.get(AUTHENTICATION_KEY_STATE) {
        Some(Any(AnyEnum::UInt32(val))) if *val == AUTHENTICATION_DONE => Ok(true),
        Some(Any(AnyEnum::UInt32(val))) if *val == AUTHENTICATION_CONTINUE => Ok(false),
        Some(Any(AnyEnum::UInt32(val))) if *val == AUTHENTICATION_ERROR => {
            let error = match map.get(AUTHENTICATION_KEY_ERROR) {
                Some(Any(AnyEnum::String(text))) => text.clone(),
                _ => "authentication error".to_string(),
            };
            Err(io::Error::new(io::ErrorKind::ConnectionRefused, error))
        }
        _ => Err(io::Error::new(
            io::ErrorKind::ConnectionAborted,
            "invalid authentication response",
        )),
    }
}

async fn call_server_authenticate(socket: &RpcSocket, map: CapacityMap) -> io::Result<CapacityMap> {
    // IMPROVE sig as static (using OnceCell ?)
    let sig: Signature = "{sm}".parse().unwrap();
    let payload = serialize_value(&sig, &map)?;
    let mut msg = Message::new(payload);
    msg.header.service_id = super::SERVICE_ID_SERVER;
    msg.header.object_id = super::OBJECT_ID_MAIN;
    msg.header.action_id = super::ACTION_ID_SERVER_AUTHENTICATE;
    socket.call(msg, &sig).await
}

pub async fn connect_socket(
    url: &Url,
    authenticator: &dyn ClientAuthenticator,
    state: SessionTx,
) -> io::Result<RpcSocket> {
    let socket = transport::connect_socket::<RpcDispatcher>(url, state).await?;

    let map = authenticator.authentication_data();
    // TODO add capacities
    let reply = call_server_authenticate(&socket, map).await?;
    // TODO read server capacities
    let mut done = is_authentication_done(&reply)?;

    loop {
        if done {
            break;
        }

        // TODO filter reply
        let map = reply.clone();
        let map = authenticator.process_authentication(map);
        let reply = call_server_authenticate(&socket, map).await?;
        done = is_authentication_done(&reply)?;
    }

    Ok(socket)
}
