use {
    crate::{
        session::{SessionEvent, SessionTx},
        transport::{message, Dispatcher, Message, MessageHeader, MsgType},
        url::Url,
    },
    bytes::Bytes,
    std::collections::HashMap,
    tokio::sync::oneshot,
};

#[derive(Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct CallId {
    pub msg_id: u32,
    pub service_id: u32,
    pub object_id: u32,
    pub action_id: u32,
}

impl CallId {
    pub fn new(msg_id: u32, service_id: u32, object_id: u32, action_id: u32) -> Self {
        Self {
            msg_id,
            service_id,
            object_id,
            action_id,
        }
    }

    pub fn from_header(header: &MessageHeader) -> Self {
        Self {
            msg_id: header.msg_id,
            service_id: header.service_id,
            object_id: header.object_id,
            action_id: header.action_id,
        }
    }
}

#[derive(Default, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TripletId {
    pub service_id: u32,
    pub object_id: u32,
    pub action_id: u32,
}

impl TripletId {
    pub fn new(service_id: u32, object_id: u32, action_id: u32) -> Self {
        Self {
            service_id,
            object_id,
            action_id,
        }
    }

    pub fn from_header(header: &MessageHeader) -> Self {
        Self {
            service_id: header.service_id,
            object_id: header.object_id,
            action_id: header.action_id,
        }
    }
}

pub type CallNotifier = oneshot::Sender<Result<Bytes, Bytes>>;
pub type SignalNotifier = Box<dyn Fn(Bytes, bool) + Send>;

#[allow(unused)]
pub enum RpcEvent {
    CallMade(u32, CallNotifier),
    CallCancelled(u32),
    SignalRegistered(TripletId, SignalNotifier),
    SignalUnregistered(TripletId),
}

pub struct RpcDispatcher {
    url: Url,
    session_events: SessionTx,
    call_notifiers: HashMap<u32, CallNotifier>,
    signal_notifiers: HashMap<TripletId, SignalNotifier>,
}

impl RpcDispatcher {
    fn register_call(&mut self, id: u32, notifier: CallNotifier) {
        let old = self.call_notifiers.insert(id, notifier);
        debug_assert!(old.is_none());
    }

    fn remove_call(&mut self, id: u32) {
        self.call_notifiers.remove(&id);
    }

    fn register_signal(&mut self, id: TripletId, notifier: SignalNotifier) {
        let old = self.signal_notifiers.insert(id, notifier);
        debug_assert!(old.is_none());
    }

    fn remove_signal(&mut self, id: TripletId) {
        self.signal_notifiers.remove(&id);
    }

    fn dispatch_reply(&mut self, id: u32, data: Bytes) {
        match self.call_notifiers.remove(&id) {
            Some(notifier) => {
                let _ = notifier.send(Ok(data));
            }
            None => {
                tracing::warn!("RpcDispatcher: Received an unexpected call reply");
            }
        }
    }

    fn dispatch_error(&mut self, id: u32, data: Bytes) {
        match self.call_notifiers.remove(&id) {
            Some(notifier) => {
                let _ = notifier.send(Err(data));
            }
            None => {
                tracing::warn!("RpcDispatcher: Received an unexpected call error");
            }
        }
    }

    fn dispatch_event(&mut self, id: TripletId, data: Bytes, dynamic: bool) {
        match self.signal_notifiers.remove(&id) {
            Some(notifier) => {
                notifier(data, dynamic);
            }
            None => {
                tracing::warn!("RpcDispatcher: Received an unexpected event");
            }
        }
    }

    fn dispatch_message(&mut self, msg: Message) {
        match msg.header.msg_type {
            MsgType::Reply => self.dispatch_reply(msg.header.msg_id, msg.into_payload()),
            MsgType::Error => self.dispatch_error(msg.header.msg_id, msg.into_payload()),
            MsgType::Event => {
                let dynamic = (msg.header.flags & message::FLAG_DYNAMIC_PAYLOAD)
                    == message::FLAG_DYNAMIC_PAYLOAD;
                self.dispatch_event(
                    TripletId::from_header(&msg.header),
                    msg.into_payload(),
                    dynamic,
                );
            }
            t => tracing::warn!(
                "RpcDispatcher: Received an unsupported message type: {:?}",
                t
            ),
        }
    }

    fn cancel_all_calls(&mut self) {
        self.call_notifiers.clear();
    }
}

impl Dispatcher for RpcDispatcher {
    type State = SessionTx;
    type Event = RpcEvent;

    fn new(url: Url, state: Self::State) -> Self {
        RpcDispatcher {
            url,
            session_events: state,
            call_notifiers: HashMap::new(),
            signal_notifiers: HashMap::new(),
        }
    }

    fn handle_event(&mut self, event: Self::Event) {
        match event {
            RpcEvent::CallMade(id, notifier) => self.register_call(id, notifier),
            RpcEvent::CallCancelled(id) => self.remove_call(id),
            RpcEvent::SignalRegistered(id, notifier) => self.register_signal(id, notifier),
            RpcEvent::SignalUnregistered(id) => self.remove_signal(id),
        }
    }

    fn handle_received_message(&mut self, msg: Message) {
        self.dispatch_message(msg);
    }

    fn close(&mut self) {
        self.cancel_all_calls();
        let _ = self
            .session_events
            .unbounded_send(SessionEvent::SocketClosed(self.url.clone()));
    }
}
