use {
    crate::{
        any::{self, Any},
        rpc::{self, RpcEvent, SignalNotifier, TripletId},
        serialization::{deserialize_value, serialize_value, Deserialize},
        signature::Signature,
        transport::{Message, MsgType, Socket},
    },
    bytes::Bytes,
    once_cell::sync::OnceCell,
    std::io::{self, Result},
    tokio::sync::oneshot,
};

pub type RpcSocket = Socket<RpcEvent>;
pub type CallResult = std::result::Result<Bytes, Bytes>;

fn pipe_error(msg: &str) -> io::Error {
    io::Error::new(io::ErrorKind::BrokenPipe, msg)
}

fn socket_error() -> io::Error {
    pipe_error("Broken RPC socket")
}

fn deserialize_error<T: Deserialize>(data: Bytes) -> io::Result<T> {
    let sig = "m".parse().unwrap();
    let v: Any = deserialize_value(&sig, &data)?;
    match v.as_string() {
        Some(text) => Err(io::Error::new(
            io::ErrorKind::Other,
            format!("call failed with error: '{}'", text),
        )),
        None => Err(io::Error::new(
            io::ErrorKind::Other,
            format!("call failed: '{:?}'", v),
        )),
    }
}

fn deserialize_result<T: Deserialize>(result: CallResult, signature: &Signature) -> io::Result<T> {
    match result {
        Ok(data) => deserialize_value(&signature, &data),
        Err(data) => deserialize_error(data),
    }
}

fn deserialize_result_any(result: CallResult, signature: &Signature) -> io::Result<Any> {
    match result {
        Ok(data) => {
            let mut cursor = io::Cursor::new(&data);
            let mut iter = signature.iter();
            any::deserialize_any(&mut cursor, &mut iter)
        }
        Err(data) => deserialize_error(data),
    }
}

impl Socket<RpcEvent> {
    pub async fn raw_call(&self, mut msg: Message) -> Result<CallResult> {
        let (notifier, promise) = oneshot::channel();
        let inner = self.inner();
        let id = inner.next_message_id();

        msg.header.msg_type = MsgType::Call;
        msg.header.size = msg.payload().len() as u32;
        msg.header.msg_id = id;

        if inner.send_event(RpcEvent::CallMade(id, notifier)).is_err() {
            return Err(socket_error());
        }
        if inner.send_message(msg).is_err() {
            let _ = inner.send_event(RpcEvent::CallCancelled(id));
            return Err(socket_error());
        }

        match promise.await {
            Ok(result) => Ok(result),
            Err(_) => Err(pipe_error("broken promise")),
        }
    }

    pub async fn call<T: Deserialize>(
        &self,
        msg: Message,
        return_signature: &Signature,
    ) -> io::Result<T> {
        deserialize_result(self.raw_call(msg).await?, return_signature)
    }

    pub async fn call_any(&self, msg: Message, return_signature: &Signature) -> io::Result<Any> {
        deserialize_result_any(self.raw_call(msg).await?, return_signature)
    }

    pub async fn connect_remote_signal(
        &self,
        triplet: TripletId,
        notifier: SignalNotifier,
    ) -> Result<u64> {
        let id = self.next_signal_id();

        if self
            .send_event(RpcEvent::SignalRegistered(triplet, notifier))
            .is_err()
        {
            return Err(socket_error());
        }

        let args = (triplet.object_id, triplet.action_id, id);
        static SIG: OnceCell<Signature> = OnceCell::new();
        let sig = SIG.get_or_init(|| "(IIL)".parse().unwrap());
        let payload = serialize_value(&sig, &args).unwrap();
        let mut msg = Message::new(payload);
        msg.header.service_id = triplet.service_id;
        msg.header.object_id = rpc::OBJECT_ID_MAIN;
        msg.header.action_id = rpc::ACTION_ID_BOUND_REGISTER_EVENT;

        static RETURN_SIG: OnceCell<Signature> = OnceCell::new();
        let sig = RETURN_SIG.get_or_init(|| "L".parse().unwrap());
        let res: Result<u64> = self.call(msg, sig).await;

        match res {
            Ok(_) => Ok(id),
            Err(e) => {
                let _ = self.send_event(RpcEvent::SignalUnregistered(triplet));
                Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!("failed to connect signal ({})", e.to_string()),
                ))
            }
        }
    }
}
