use crate::any::Any;
use std::collections::HashMap;

pub const AUTHENTICATION_ERROR: u32 = 1;
pub const AUTHENTICATION_CONTINUE: u32 = 2;
pub const AUTHENTICATION_DONE: u32 = 3;

pub const AUTHENTICATION_KEY_STATE: &str = "__qi_auth_state";
pub const AUTHENTICATION_KEY_ERROR: &str = "__qi_auth_err_reason";

/// A trait to handle authentification when opening a new socket within a `Session`.
pub trait ClientAuthenticator: Sync + Send {
    /// Called at the start of the authentication process,
    /// should return the inital authentification data to send to the server.
    fn authentication_data(&self) -> HashMap<String, Any>;

    /// Called during the authentication process if the server send a continuation result,
    /// the input is the server response, the output is the next authentification data
    /// to send to the server. (Can be use for authentification with a challenge.)
    fn process_authentication(&self, data: HashMap<String, Any>) -> HashMap<String, Any>;
}

/// A empty `Authenticator`, doesn't do any authentication.
#[derive(Default, Clone)]
pub struct NoOpClientAuthenticator;
impl ClientAuthenticator for NoOpClientAuthenticator {
    fn authentication_data(&self) -> HashMap<String, Any> {
        HashMap::new()
    }

    fn process_authentication(&self, _data: HashMap<String, Any>) -> HashMap<String, Any> {
        HashMap::new()
    }
}

/// A simple `Authenticator`, does authentication with an username and a password.
#[derive(Clone)]
pub struct PasswordClientAuthenticator {
    username: String,
    password: String,
}

impl PasswordClientAuthenticator {
    /// Consturcts a new `PasswordClientAuthenticator` with the given username and password.
    pub fn new(username: String, password: String) -> Self {
        PasswordClientAuthenticator { username, password }
    }
}

impl ClientAuthenticator for PasswordClientAuthenticator {
    fn authentication_data(&self) -> HashMap<String, Any> {
        let mut map = HashMap::new();
        map.insert("auth_user".to_string(), Any::from(self.username.clone()));
        map.insert("auth_token".to_string(), Any::from(self.password.clone()));
        map
    }

    fn process_authentication(&self, _data: HashMap<String, Any>) -> HashMap<String, Any> {
        HashMap::new()
    }
}
