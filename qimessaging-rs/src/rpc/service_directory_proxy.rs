use {
    crate::{
        rpc::{self, RpcSocket, ServiceInfo, SignalNotifier, TripletId},
        serialization::serialize_value,
        signature::{GetTypeSignature, Signature},
        transport::Message,
    },
    once_cell::sync::OnceCell,
    std::io::Result,
};

#[allow(unused)]
const ACTION_ID_SERVICE: u32 = 100;
#[allow(unused)]
const ACTION_ID_SERVICES: u32 = 101;
#[allow(unused)]
const ACTION_ID_REGISTER_SERVICE: u32 = 102;
#[allow(unused)]
const ACTION_ID_UNREGISTER_SERVICE: u32 = 103;
#[allow(unused)]
const ACTION_ID_SERVICE_READY: u32 = 104;
#[allow(unused)]
const ACTION_ID_UPDATE_SERVICE_INFO: u32 = 105;
#[allow(unused)]
const ACTION_ID_SERVICE_ADDED: u32 = 106;
#[allow(unused)]
const ACTION_ID_SERVICE_REMOVED: u32 = 107;
#[allow(unused)]
const ACTION_ID_MACHINE_ID: u32 = 108;
#[allow(unused)]
const ACTION_ID_SOCKET_OF_SERVICE: u32 = 109;

static SERVICE_INFO_SIG: OnceCell<Signature> = OnceCell::new();
static SERVICE_INFO_VEC_SIG: OnceCell<Signature> = OnceCell::new();
static STRING_SIG: OnceCell<Signature> = OnceCell::new();
// static ID_SIG: OnceCell<Signature> = OnceCell::new();

pub struct ServiceDirectoryProxy<'a> {
    socket: &'a RpcSocket,
}

impl<'a> ServiceDirectoryProxy<'a> {
    pub fn new(socket: &'a RpcSocket) -> Self {
        Self { socket }
    }

    pub async fn service(&self, name: &str) -> Result<ServiceInfo> {
        let sig = STRING_SIG.get_or_init(|| "s".parse().unwrap());
        let payload = serialize_value(sig, &name)?;
        let mut msg = Message::new(payload);
        msg.header.service_id = rpc::SERVICE_ID_SERVICE_DIRECTORY;
        msg.header.object_id = rpc::OBJECT_ID_MAIN;
        msg.header.action_id = ACTION_ID_SERVICE;

        let sig = SERVICE_INFO_SIG.get_or_init(|| ServiceInfo::signature().parse().unwrap());
        self.socket.call(msg, sig).await
    }

    pub async fn services(&self) -> Result<Vec<ServiceInfo>> {
        let mut msg = Message::empty();
        msg.header.service_id = rpc::SERVICE_ID_SERVICE_DIRECTORY;
        msg.header.object_id = rpc::OBJECT_ID_MAIN;
        msg.header.action_id = ACTION_ID_SERVICES;

        let sig = SERVICE_INFO_VEC_SIG.get_or_init(|| {
            <Vec<ServiceInfo> as GetTypeSignature>::signature()
                .parse()
                .unwrap()
        });
        self.socket.call(msg, sig).await
    }

    pub async fn connect_service_added(&self, notifier: SignalNotifier) -> Result<u64> {
        let triplet = TripletId::new(
            rpc::SERVICE_ID_SERVICE_DIRECTORY,
            rpc::OBJECT_ID_MAIN,
            ACTION_ID_SERVICE_ADDED,
        );
        self.socket.connect_remote_signal(triplet, notifier).await
    }

    pub async fn connect_service_removed(&self, notifier: SignalNotifier) -> Result<u64> {
        let triplet = TripletId::new(
            rpc::SERVICE_ID_SERVICE_DIRECTORY,
            rpc::OBJECT_ID_MAIN,
            ACTION_ID_SERVICE_REMOVED,
        );
        self.socket.connect_remote_signal(triplet, notifier).await
    }
}
