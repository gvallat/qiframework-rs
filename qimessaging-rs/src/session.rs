use {
    crate::{
        ask::{AskFuture, AskPromise},
        rpc::{ClientAuthenticator, ServiceDirectoryProxy, ServiceInfo},
        serialization::deserialize_value,
        signal::{Signal, SignalReceiver},
        signature::Signature,
        url::Url,
    },
    futures::{
        channel::{
            mpsc::{self, UnboundedReceiver, UnboundedSender},
            oneshot,
        },
        stream::StreamExt,
    },
    once_cell::sync::OnceCell,
    std::{
        io,
        sync::{
            atomic::{AtomicBool, AtomicU32, Ordering},
            Arc,
        },
    },
    tracing_futures::Instrument,
};

pub type SessionTx = UnboundedSender<SessionEvent>;
type SessionRx = UnboundedReceiver<SessionEvent>;
pub type BoxedAuthenticator = Box<dyn ClientAuthenticator>;

#[allow(unused)]
pub enum SessionEvent {
    Close,
    Connect(Url, Option<BoxedAuthenticator>, AskPromise<()>),
    Disconnect(AskPromise<()>),
    ServiceAdded(String),
    ServiceRemoved(String),
    SocketClosed(Url),

    ServiceInfo(String, AskPromise<ServiceInfo>),
    ServicesInfo(AskPromise<Vec<ServiceInfo>>),
}

struct SessionShared {
    events: SessionTx,
    is_connected: AtomicBool,

    disconnected: Signal<()>, // IMPROVE add error message ?
    connected: Signal<()>,
    service_added: Signal<Arc<str>>,
    service_removed: Signal<Arc<str>>,
}

impl SessionShared {
    fn new(events: SessionTx) -> Self {
        SessionShared {
            events,
            is_connected: AtomicBool::new(false),

            disconnected: Signal::new(),
            connected: Signal::new(),
            service_added: Signal::new(),
            service_removed: Signal::new(),
        }
    }
}

pub type DisconnectedSignalReceiver = SignalReceiver<()>;
pub type ConnectedSignalReceiver = SignalReceiver<()>;
pub type ServiceAddedSignalReceiver = SignalReceiver<Arc<str>>;
pub type ServiceRemovedSignalReceiver = SignalReceiver<Arc<str>>;

pub type ConnectFuture = AskFuture<()>;
pub type DisconnectFuture = AskFuture<()>;
pub type ServiceInfoFuture = AskFuture<ServiceInfo>;
pub type ServicesInfoFuture = AskFuture<Vec<ServiceInfo>>;

pub struct Session {
    shared: Arc<SessionShared>,
}

type ArcSession = Arc<Session>;

impl Default for Session {
    fn default() -> Self {
        Session::new()
    }
}

impl Session {
    /// **Warning** Spawns a task, so it need to be called form within the runtime (in an async function)
    pub fn new() -> Session {
        let (sender, receiver) = mpsc::unbounded();
        let shared = Arc::new(SessionShared::new(sender));
        static COUNTER: AtomicU32 = AtomicU32::new(0);
        let id = COUNTER.fetch_add(1, Ordering::SeqCst);
        tokio::spawn(
            session_task(receiver, shared.clone())
                .instrument(tracing::trace_span!("Session", "{}", id)),
        );
        Session { shared }
    }

    pub fn arc_new() -> ArcSession {
        Arc::new(Session::new())
    }

    #[inline(always)]
    fn send(&self, event: SessionEvent) {
        let _ = self.shared.events.unbounded_send(event);
    }

    pub fn is_connected(&self) -> bool {
        self.shared.is_connected.load(Ordering::SeqCst)
    }

    pub fn connect(&self, url: Url, authenticator: Option<BoxedAuthenticator>) -> ConnectFuture {
        let (sender, receiver) = oneshot::channel();
        self.send(SessionEvent::Connect(url, authenticator, sender));
        AskFuture::new(receiver)
    }

    pub fn disconnect(&self) -> DisconnectFuture {
        let (sender, receiver) = oneshot::channel();
        self.send(SessionEvent::Disconnect(sender));
        AskFuture::new(receiver)
    }

    pub fn service_info(&self, name: String) -> ServiceInfoFuture {
        let (sender, receiver) = oneshot::channel();
        self.send(SessionEvent::ServiceInfo(name, sender));
        AskFuture::new(receiver)
    }

    pub fn services_info(&self) -> ServicesInfoFuture {
        let (sender, receiver) = oneshot::channel();
        self.send(SessionEvent::ServicesInfo(sender));
        AskFuture::new(receiver)
    }

    pub fn disconnected_receiver(&self) -> DisconnectedSignalReceiver {
        self.shared.disconnected.receiver()
    }

    pub fn connected_receiver(&self) -> ConnectedSignalReceiver {
        self.shared.connected.receiver()
    }

    pub fn service_added_receiver(&self) -> ServiceAddedSignalReceiver {
        self.shared.service_added.receiver()
    }

    pub fn service_removed_receiver(&self) -> ServiceRemovedSignalReceiver {
        self.shared.service_removed.receiver()
    }
}

impl Drop for Session {
    fn drop(&mut self) {
        self.send(SessionEvent::Close);
    }
}

mod internal {
    use super::*;
    use crate::rpc::{connect_socket, NoOpClientAuthenticator, RpcSocket};

    #[allow(unused)]
    pub(super) struct ConnectedData {
        pub(super) url: Url,
        pub(super) authenticator: BoxedAuthenticator,
        pub(super) main_socket: RpcSocket,
        // TODO sockets map
    }

    pub(super) struct Session {
        pub(super) shared: Arc<SessionShared>,
        pub(super) events: SessionRx,
        pub(super) connected: Option<ConnectedData>,
    }

    impl Session {
        pub(super) fn send_disconnected(&mut self) {
            if self.shared.is_connected.load(Ordering::SeqCst) {
                self.shared.is_connected.store(false, Ordering::SeqCst);
                self.shared.disconnected.send(());
            }
        }

        pub(super) fn send_connected(&mut self) {
            if !self.shared.is_connected.load(Ordering::SeqCst) {
                self.shared.is_connected.store(true, Ordering::SeqCst);
                self.shared.connected.send(());
            }
        }

        pub(super) fn send_service_added(&mut self, name: String) {
            self.shared.service_added.send(name.into());
        }

        pub(super) fn send_service_removed(&mut self, name: String) {
            self.shared.service_removed.send(name.into());
        }

        pub(super) async fn connect(
            &mut self,
            url: Url,
            authenticator: Option<BoxedAuthenticator>,
        ) -> io::Result<()> {
            if self.shared.is_connected.load(Ordering::SeqCst) {
                return Err(io::Error::new(
                    io::ErrorKind::ConnectionAborted,
                    "Already connected",
                ));
            }

            let authenticator = authenticator.unwrap_or_else(|| Box::new(NoOpClientAuthenticator));
            let socket =
                match connect_socket(&url, &*authenticator, self.shared.events.clone()).await {
                    Ok(socket) => socket,
                    Err(e) => return Err(e),
                };

            let sd = ServiceDirectoryProxy::new(&socket);

            static SIGNAL_SIG: OnceCell<Signature> = OnceCell::new();
            SIGNAL_SIG.get_or_init(|| "(Is)".parse().unwrap());

            let events = self.shared.events.clone();
            let notifier = Box::new(move |payload, _| {
                let sig = SIGNAL_SIG.get().unwrap();
                if let Ok((_, name)) = deserialize_value::<(u32, String)>(&sig, &payload) {
                    let _ = events.unbounded_send(SessionEvent::ServiceAdded(name));
                }
            });
            // no need to keep signal id => will never disconnect
            if let Err(e) = sd.connect_service_added(notifier).await {
                tracing::error!("Could not connect to service added: {}", e);
            }

            let events = self.shared.events.clone();
            let notifier = Box::new(move |payload, _| {
                let sig = SIGNAL_SIG.get().unwrap();
                if let Ok((_, name)) = deserialize_value::<(u32, String)>(&sig, &payload) {
                    let _ = events.unbounded_send(SessionEvent::ServiceRemoved(name));
                }
            });
            // no need to keep signal id => will never disconnect
            if let Err(e) = sd.connect_service_removed(notifier).await {
                tracing::error!("Could not connect to service removed: {}", e);
            }

            self.connected = Some(ConnectedData {
                url,
                authenticator,
                main_socket: socket,
            });
            self.send_connected();
            Ok(())
        }

        pub(super) fn disconnect(&mut self) {
            if !self.shared.is_connected.load(Ordering::SeqCst) {
                return;
            }
            if let Some(data) = &self.connected {
                data.main_socket.close();
            }
            self.connected = None;
            self.send_disconnected();
        }

        pub(super) fn handle_socket_closed(&mut self, url: Url) {
            if let Some(data) = &self.connected {
                if *data.main_socket.url() == url {
                    self.disconnect();
                }
            }
        }

        pub(super) fn service_info(&mut self, name: String, promise: AskPromise<ServiceInfo>) {
            match &self.connected {
                Some(data) => {
                    let socket = data.main_socket.clone();
                    tokio::spawn(async move {
                        let sd = ServiceDirectoryProxy::new(&socket);
                        let res = sd.service(&name).await;
                        let _ = promise.send(res);
                    });
                }
                None => {
                    let _ = promise.send(Err(error_not_connected()));
                }
            }
        }

        pub(super) fn services_info(&mut self, promise: AskPromise<Vec<ServiceInfo>>) {
            match &self.connected {
                Some(data) => {
                    let socket = data.main_socket.clone();
                    tokio::spawn(async move {
                        let sd = ServiceDirectoryProxy::new(&socket);
                        let res = sd.services().await;
                        let _ = promise.send(res);
                    });
                }
                None => {
                    let _ = promise.send(Err(error_not_connected()));
                }
            }
        }
    }
}

fn error_not_connected() -> io::Error {
    io::Error::new(io::ErrorKind::BrokenPipe, "Not connected")
}

async fn session_task(events: SessionRx, shared: Arc<SessionShared>) {
    let mut session = internal::Session {
        shared,
        events,
        connected: None,
    };

    while let Some(event) = session.events.next().await {
        match event {
            SessionEvent::Connect(url, authenticator, promise) => {
                let res = session.connect(url, authenticator).await;
                let _ = promise.send(res);
            }
            SessionEvent::Disconnect(promise) => {
                session.disconnect();
                let _ = promise.send(Ok(()));
            }
            SessionEvent::ServiceAdded(name) => session.send_service_added(name),
            SessionEvent::ServiceRemoved(name) => session.send_service_removed(name),
            SessionEvent::SocketClosed(url) => session.handle_socket_closed(url),

            SessionEvent::ServiceInfo(name, promise) => session.service_info(name, promise),
            SessionEvent::ServicesInfo(promise) => session.services_info(promise),

            SessionEvent::Close => break,
        }
    }

    session.send_disconnected();
}
