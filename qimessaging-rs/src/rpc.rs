mod authentication;
mod rpc_connect;
mod rpc_dispatcher;
mod rpc_socket;
mod service_directory_proxy;
mod service_info;

pub use authentication::{
    ClientAuthenticator, NoOpClientAuthenticator, PasswordClientAuthenticator,
    AUTHENTICATION_CONTINUE, AUTHENTICATION_DONE, AUTHENTICATION_ERROR, AUTHENTICATION_KEY_ERROR,
    AUTHENTICATION_KEY_STATE,
};
pub use rpc_connect::connect_socket;
pub use rpc_dispatcher::{CallId, RpcDispatcher, RpcEvent, SignalNotifier, TripletId};
pub use rpc_socket::{CallResult, RpcSocket};
pub use service_info::ServiceInfo;

pub(crate) use service_directory_proxy::ServiceDirectoryProxy;

#[allow(unused)]
const SERVICE_ID_SERVER: u32 = 0;
#[allow(unused)]
const SERVICE_ID_SERVICE_DIRECTORY: u32 = 1;

#[allow(unused)]
const OBJECT_ID_NONE: u32 = 0;
#[allow(unused)]
const OBJECT_ID_MAIN: u32 = 1;

// const ACTION_ID_SERVER_CONNECT: u32 = 4; // dead value
#[allow(unused)]
const ACTION_ID_SERVER_AUTHENTICATE: u32 = 8;

#[allow(unused)]
const ACTION_ID_SD_SERVICE: u32 = 100;
#[allow(unused)]
const ACTION_ID_SD_SERVICES: u32 = 101;
#[allow(unused)]
const ACTION_ID_SD_REGISTER_SERVICE: u32 = 102;
#[allow(unused)]
const ACTION_ID_SD_UNREGISTER_SERVICE: u32 = 103;
#[allow(unused)]
const ACTION_ID_SD_SERVICE_READY: u32 = 104;
#[allow(unused)]
const ACTION_ID_SD_UPDATE_SERVICE_INFO: u32 = 105;
#[allow(unused)]
const ACTION_ID_SD_SERVICE_ADDED: u32 = 106;
#[allow(unused)]
const ACTION_ID_SD_SERVICE_REMOVED: u32 = 107;
#[allow(unused)]
const ACTION_ID_SD_MACHINE_ID: u32 = 108;

#[allow(unused)]
const ACTION_ID_BOUND_REGISTER_EVENT: u32 = 0;
#[allow(unused)]
const ACTION_ID_BOUND_UNREGISTER_EVENT: u32 = 1;
#[allow(unused)]
const ACTION_ID_BOUND_META_OBJECT: u32 = 2;
#[allow(unused)]
const ACTION_ID_BOUND_TERMINATE: u32 = 3;
#[allow(unused)]
const ACTION_ID_BOUND_GET_PROPERTY: u32 = 5;
#[allow(unused)]
const ACTION_ID_BOUND_SET_PROPERTY: u32 = 6;
#[allow(unused)]
const ACTION_ID_BOUND_PROPERTIES: u32 = 7;
#[allow(unused)]
const ACTION_ID_BOUND_REGISTER_EVENT_WITH_SIGNATURE: u32 = 8;
