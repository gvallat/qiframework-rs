use {
    core::{
        pin::Pin,
        task::{Context, Poll},
    },
    futures::{
        channel::oneshot::{Receiver, Sender},
        Future,
    },
    std::io,
};

pub type AskPromise<T> = Sender<io::Result<T>>;

pub struct AskFuture<T> {
    receiver: Receiver<io::Result<T>>,
}

impl<T> AskFuture<T> {
    pub fn new(receiver: Receiver<io::Result<T>>) -> Self {
        AskFuture { receiver }
    }
}

impl<T> Future for AskFuture<T> {
    type Output = io::Result<T>;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let unpinned = Pin::get_mut(self);
        match Pin::new(&mut unpinned.receiver).poll(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(Ok(res)) => Poll::Ready(res),
            Poll::Ready(Err(_)) => Poll::Ready(Err(io::Error::new(
                io::ErrorKind::BrokenPipe,
                "Broken promise",
            ))),
        }
    }
}
