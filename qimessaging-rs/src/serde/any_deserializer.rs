use {
    crate::{
        any::{Any, AnyEnum},
        serde::Error,
        signature::Signature,
    },
    serde::de::{
        DeserializeSeed, Deserializer, EnumAccess, MapAccess, SeqAccess, VariantAccess, Visitor,
    },
};

pub(crate) struct AnyDeserializer<'a> {
    any: &'a Any,
}

impl<'a> AnyDeserializer<'a> {
    pub(crate) fn new(any: &'a Any) -> Self {
        Self { any }
    }
}

impl<'de> Deserializer<'de> for AnyDeserializer<'de> {
    type Error = Error;

    fn is_human_readable(&self) -> bool {
        false
    }

    fn deserialize_any<V: Visitor<'de>>(self, visitor: V) -> Result<V::Value, Self::Error> {
        match &self.any.0 {
            AnyEnum::None | AnyEnum::Unit => visitor.visit_unit(),
            AnyEnum::Boolean(val) => visitor.visit_bool(*val),
            AnyEnum::Int8(val) => visitor.visit_i8(*val),
            AnyEnum::UInt8(val) => visitor.visit_u8(*val),
            AnyEnum::Int16(val) => visitor.visit_i16(*val),
            AnyEnum::UInt16(val) => visitor.visit_u16(*val),
            AnyEnum::Int32(val) => visitor.visit_i32(*val),
            AnyEnum::UInt32(val) => visitor.visit_u32(*val),
            AnyEnum::Int64(val) => visitor.visit_i64(*val),
            AnyEnum::UInt64(val) => visitor.visit_u64(*val),
            AnyEnum::Float(val) => visitor.visit_f32(*val),
            AnyEnum::Double(val) => visitor.visit_f64(*val),
            AnyEnum::String(val) => visitor.visit_borrowed_str(&val[..]),
            AnyEnum::List(_sig, vec) => {
                let seq = AnySeqAccess::new(vec);
                visitor.visit_seq(seq)
            }
            AnyEnum::Map(_sig, vec) => {
                let map = AnyMapAccess::new(vec);
                visitor.visit_map(map)
            }
            AnyEnum::Tuple(sig, vec) => {
                // TODO check sig.struct_names.len() > 0
                let tuple = AnyTupleAccess::new(sig, vec);
                if sig.struct_names()[0].field_names.is_empty() {
                    visitor.visit_seq(tuple)
                } else {
                    visitor.visit_map(tuple)
                }
            }
            AnyEnum::Dynamic(any) => {
                let de = AnyDeserializer::new(&*any);
                de.deserialize_any(visitor)
            }
            AnyEnum::Raw(buf) => visitor.visit_borrowed_bytes(&buf[..]),
            AnyEnum::Optional(_sig, option) => match option {
                Some(any) => {
                    let deser = AnyDeserializer::new(any);
                    visitor.visit_some(deser)
                }
                None => visitor.visit_none(),
            },
            AnyEnum::Object => unimplemented!(),
            AnyEnum::Unknown => visitor.visit_unit(), // error ?
        }
    }

    serde::forward_to_deserialize_any! {
        bool i8 i16 i32 i64 i128 u8 u16 u32 u64 u128 f32 f64 char str string
        bytes byte_buf option unit unit_struct newtype_struct seq tuple
        tuple_struct map struct identifier ignored_any
    }

    fn deserialize_enum<V: Visitor<'de>>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value, Self::Error> {
        use std::convert::TryFrom;
        let idx: u32 = match &self.any.0 {
            AnyEnum::Int8(val) => u32::try_from(*val).map_err(|_| Error::IncompatibleWithU32),
            AnyEnum::UInt8(val) => Ok(*val as u32),
            AnyEnum::Int16(val) => u32::try_from(*val).map_err(|_| Error::IncompatibleWithU32),
            AnyEnum::UInt16(val) => Ok(*val as u32),
            AnyEnum::Int32(val) => u32::try_from(*val).map_err(|_| Error::IncompatibleWithU32),
            AnyEnum::UInt32(val) => Ok(*val),
            AnyEnum::Int64(val) => u32::try_from(*val).map_err(|_| Error::IncompatibleWithU32),
            AnyEnum::UInt64(val) => u32::try_from(*val).map_err(|_| Error::IncompatibleWithU32),
            _ => Err(Error::IncompatibleEnumType),
        }?;
        let de = AnyEnumAccess { idx };
        visitor.visit_enum(de)
    }

    // TODO deserialize_struct => handle deserialize to Any
}

struct AnySeqAccess<'de> {
    anys: &'de [Any],
    idx: usize,
}

impl<'de> AnySeqAccess<'de> {
    fn new(anys: &'de [Any]) -> Self {
        Self { anys, idx: 0 }
    }
}

impl<'de> SeqAccess<'de> for AnySeqAccess<'de> {
    type Error = Error;

    fn next_element_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<Option<T::Value>, Self::Error> {
        if self.idx >= self.anys.len() {
            Ok(None)
        } else {
            let de = AnyDeserializer::new(&self.anys[self.idx]);
            self.idx += 1;
            Ok(Some(seed.deserialize(de)?))
        }
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.anys.len())
    }
}

struct AnyMapAccess<'de> {
    anys: &'de [(Any, Any)],
    idx: usize,
}

impl<'de> AnyMapAccess<'de> {
    fn new(anys: &'de [(Any, Any)]) -> Self {
        Self { anys, idx: 0 }
    }
}

impl<'de> MapAccess<'de> for AnyMapAccess<'de> {
    type Error = Error;

    fn next_key_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<Option<T::Value>, Self::Error> {
        if self.idx >= self.anys.len() {
            Ok(None)
        } else {
            let de = AnyDeserializer::new(&self.anys[self.idx].0);
            Ok(Some(seed.deserialize(de)?))
        }
    }

    fn next_value_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<T::Value, Self::Error> {
        let de = AnyDeserializer::new(&self.anys[self.idx].1);
        self.idx += 1;
        seed.deserialize(de)
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.anys.len())
    }
}

struct AnyTupleAccess<'de> {
    sig: &'de Signature,
    anys: &'de [Any],
    idx: usize,
}

impl<'de> AnyTupleAccess<'de> {
    fn new(sig: &'de Signature, anys: &'de [Any]) -> Self {
        // copy sig in thread-local ?
        Self { sig, anys, idx: 0 }
    }
}

impl<'de> MapAccess<'de> for AnyTupleAccess<'de> {
    type Error = Error;

    fn next_key_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<Option<T::Value>, Self::Error> {
        if self.idx >= self.anys.len() {
            Ok(None)
        } else {
            use serde::de::value::BorrowedStrDeserializer;
            let names = &self.sig.struct_names()[0];
            let de: BorrowedStrDeserializer<'de, Error> =
                BorrowedStrDeserializer::new(names.field_names[self.idx]);
            Ok(Some(seed.deserialize(de)?))
        }
    }

    fn next_value_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<T::Value, Self::Error> {
        let de = AnyDeserializer::new(&self.anys[self.idx]);
        self.idx += 1;
        seed.deserialize(de)
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.anys.len())
    }
}

impl<'de> SeqAccess<'de> for AnyTupleAccess<'de> {
    type Error = Error;

    fn next_element_seed<T: DeserializeSeed<'de>>(
        &mut self,
        seed: T,
    ) -> Result<Option<T::Value>, Self::Error> {
        if self.idx >= self.anys.len() {
            Ok(None)
        } else {
            let de = AnyDeserializer::new(&self.anys[self.idx]);
            self.idx += 1;
            Ok(Some(seed.deserialize(de)?))
        }
    }

    fn size_hint(&self) -> Option<usize> {
        Some(self.anys.len())
    }
}

struct AnyEnumAccess {
    idx: u32,
}

impl<'de> EnumAccess<'de> for AnyEnumAccess {
    type Error = Error;
    type Variant = AnyVariantAccess;

    fn variant_seed<V: DeserializeSeed<'de>>(
        self,
        seed: V,
    ) -> Result<(V::Value, Self::Variant), Self::Error> {
        use serde::de::{value::U32Deserializer, IntoDeserializer};
        let de: U32Deserializer<Error> = IntoDeserializer::into_deserializer(self.idx);
        let val = seed.deserialize(de)?;
        Ok((val, AnyVariantAccess))
    }
}

struct AnyVariantAccess;
impl<'de> VariantAccess<'de> for AnyVariantAccess {
    type Error = Error;

    fn unit_variant(self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn newtype_variant_seed<T: DeserializeSeed<'de>>(
        self,
        _seed: T,
    ) -> Result<T::Value, Self::Error> {
        Err(Error::UnsupportedVariant)
    }

    fn tuple_variant<V: Visitor<'de>>(
        self,
        _len: usize,
        _visitor: V,
    ) -> Result<V::Value, Self::Error> {
        Err(Error::UnsupportedVariant)
    }

    fn struct_variant<V: Visitor<'de>>(
        self,
        _fields: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value, Self::Error> {
        Err(Error::UnsupportedVariant)
    }
}
