use {
    crate::{
        serde::Error,
        signature::{self, iterator::Iter as SIter, Tag},
    },
    byteorder::{LittleEndian, WriteBytesExt},
    serde::{ser, Serialize, Serializer},
    std::io::{Cursor, Write},
};

pub(crate) fn write_bytes_array(buf: &mut Cursor<Vec<u8>>, src: &[u8]) -> Result<(), Error> {
    let len = src.len();
    if len > u32::max_value() as usize {
        return Err(Error::SizeToBig(len));
    }
    buf.write_u32::<LittleEndian>(len as u32)?;
    let size = buf.write(src)?;
    if size == len {
        Ok(())
    } else {
        Err(Error::EndOfBuffer)
    }
}

pub(crate) struct BinarySerializer<'sig> {
    sig_iter: SIter<'sig>,
    buf: Cursor<Vec<u8>>,
    check_tag: bool,
}

impl<'sig> BinarySerializer<'sig> {
    pub(crate) fn new(sig_iter: SIter<'sig>) -> Self {
        let vec = Vec::with_capacity(128);
        Self {
            sig_iter,
            buf: Cursor::new(vec),
            check_tag: true,
        }
    }

    pub(crate) fn with_buffer(sig_iter: SIter<'sig>, buf: Cursor<Vec<u8>>) -> Self {
        Self {
            sig_iter,
            buf,
            check_tag: true,
        }
    }

    pub(crate) fn check_reached_end(&mut self) -> Result<(), Error> {
        match self.sig_iter.next() {
            Some(_) => Err(Error::ExpectedSignatureEnd),
            None => Ok(()),
        }
    }

    pub(crate) fn into_vec(self) -> Vec<u8> {
        self.buf.into_inner()
    }

    fn check_tag(&mut self, tag: Tag) -> Result<(), Error> {
        if self.check_tag {
            match self.sig_iter.next() {
                Some(item) => {
                    if item.tag() == tag {
                        Ok(())
                    } else {
                        Err(Error::UnexpectedType(item.tag(), tag))
                    }
                }
                None => Err(Error::UnexpectedType(Tag::None, tag)),
            }
        } else {
            Ok(())
        }
    }
}

impl<'ser, 'sig> Serializer for &'ser mut BinarySerializer<'sig> {
    type Ok = ();
    type Error = Error; // Improve use custom error type

    type SerializeSeq = BinaryMapSeqSerializer<'ser, 'sig>;
    type SerializeTuple = BinaryStructSerializer<'ser, 'sig>;
    type SerializeTupleStruct = BinaryStructSerializer<'ser, 'sig>;
    type SerializeStruct = BinaryStructSerializer<'ser, 'sig>;
    type SerializeMap = BinaryMapSeqSerializer<'ser, 'sig>;
    type SerializeTupleVariant = ser::Impossible<(), Error>;
    type SerializeStructVariant = ser::Impossible<(), Error>;

    fn is_human_readable(&self) -> bool {
        false
    }

    fn serialize_bool(self, val: bool) -> Result<(), Error> {
        self.check_tag(Tag::Bool)?;
        self.buf.write_u8(val as u8).map_err(Into::into)
    }

    fn serialize_i8(self, val: i8) -> Result<(), Error> {
        self.check_tag(Tag::Int8)?;
        self.buf.write_i8(val).map_err(Into::into)
    }

    fn serialize_u8(self, val: u8) -> Result<(), Error> {
        self.check_tag(Tag::UInt8)?;
        self.buf.write_u8(val).map_err(Into::into)
    }

    fn serialize_i16(self, val: i16) -> Result<(), Error> {
        self.check_tag(Tag::Int16)?;
        self.buf.write_i16::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_u16(self, val: u16) -> Result<(), Error> {
        self.check_tag(Tag::UInt16)?;
        self.buf.write_u16::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_i32(self, val: i32) -> Result<(), Error> {
        self.check_tag(Tag::Int32)?;
        self.buf.write_i32::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_u32(self, val: u32) -> Result<(), Error> {
        self.check_tag(Tag::UInt32)?;
        self.buf.write_u32::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_i64(self, val: i64) -> Result<(), Error> {
        self.check_tag(Tag::Int64)?;
        self.buf.write_i64::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_u64(self, val: u64) -> Result<(), Error> {
        self.check_tag(Tag::UInt64)?;
        self.buf.write_u64::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_f32(self, val: f32) -> Result<(), Error> {
        self.check_tag(Tag::Float)?;
        self.buf.write_f32::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_f64(self, val: f64) -> Result<(), Error> {
        self.check_tag(Tag::Double)?;
        self.buf.write_f64::<LittleEndian>(val).map_err(Into::into)
    }

    fn serialize_char(self, val: char) -> Result<(), Error> {
        let mut b = [0; 4];
        let as_str = val.encode_utf8(&mut b);
        self.serialize_str(as_str)
    }

    fn serialize_str(self, val: &str) -> Result<(), Error> {
        self.check_tag(Tag::String)?;
        write_bytes_array(&mut self.buf, val.as_bytes())
    }

    fn serialize_bytes(self, val: &[u8]) -> Result<(), Error> {
        self.check_tag(Tag::Raw)?;
        write_bytes_array(&mut self.buf, val)
    }

    fn serialize_none(self) -> Result<(), Error> {
        self.check_tag(Tag::Optional)?;
        signature::consume_next_element(&mut self.sig_iter);
        self.buf.write_u8(0).map_err(Into::into)
    }

    fn serialize_some<T: ?Sized + Serialize>(self, val: &T) -> Result<(), Error> {
        self.check_tag(Tag::Optional)?;
        self.buf.write_u8(1).map_err(Error::from)?;
        Serialize::serialize(val, self)
    }

    fn serialize_unit(self) -> Result<(), Error> {
        self.check_tag(Tag::Unit)
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<(), Error> {
        // could check signature struct name but libqi doesn't check struct names
        self.check_tag(Tag::Tuple)?;
        self.check_tag(Tag::Unit)?;
        self.check_tag(Tag::TupleEnd)
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        idx: u32,
        _variant: &'static str,
    ) -> Result<(), Error> {
        // C like enum variant
        use std::convert::TryFrom;
        match self.sig_iter.next() {
            Some(item) => match item.tag() {
                Tag::Int8 => match i8::try_from(idx as i32) {
                    Ok(idx) => self.buf.write_i8(idx).map_err(Into::into),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::Int8)),
                },
                Tag::UInt8 => match u8::try_from(idx) {
                    Ok(idx) => self.buf.write_u8(idx).map_err(Into::into),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::UInt8)),
                },
                Tag::Int16 => match i16::try_from(idx as i32) {
                    Ok(idx) => self.buf.write_i16::<LittleEndian>(idx).map_err(Into::into),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::Int16)),
                },
                Tag::UInt16 => match u16::try_from(idx) {
                    Ok(idx) => self.buf.write_u16::<LittleEndian>(idx).map_err(Into::into),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::UInt16)),
                },
                Tag::Int32 => self
                    .buf
                    .write_i32::<LittleEndian>(idx as i32)
                    .map_err(Into::into),
                Tag::UInt32 => self.buf.write_u32::<LittleEndian>(idx).map_err(Into::into),
                Tag::Int64 => self
                    .buf
                    .write_i64::<LittleEndian>(idx as i64)
                    .map_err(Into::into),
                Tag::UInt64 => self
                    .buf
                    .write_u64::<LittleEndian>(idx as u64)
                    .map_err(Into::into),
                _ => Err(Error::IncompatibleEnumType),
            },
            None => Err(Error::IncompatibleEnumType),
        }
    }

    fn serialize_newtype_struct<T: ?Sized + Serialize>(
        self,
        _name: &'static str,
        val: &T,
    ) -> Result<(), Error> {
        // serialize as if not wrapped
        // example: struct Millimetters(u8)
        Serialize::serialize(val, self)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
        self.check_tag(Tag::List)?;
        BinaryMapSeqSerializer::new(self, len)
    }

    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple, Error> {
        // anonymous tuple: (u32, i16, f64)
        if self.check_tag {
            match self.sig_iter.next() {
                Some(item) => {
                    if item.tag() == Tag::Tuple {
                        super::SERIALIZE_SIGNATURE.with(|s| *s.borrow_mut() = item.sub_signature());
                    } else {
                        return Err(Error::UnexpectedType(item.tag(), Tag::Tuple));
                    }
                }
                None => return Err(Error::UnexpectedType(Tag::None, Tag::Tuple)),
            }
        }
        Ok(BinaryStructSerializer::new(self, false))
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleStruct, Error> {
        // tuple struct: Rgb(u8, u8, u8)
        // could check signature struct name but libqi doesn't check struct names
        self.serialize_tuple(len)
    }

    fn serialize_struct(
        self,
        name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStruct, Error> {
        // struct: Rgb{ r: u8, g: u8, b: u8}
        // could check signature struct name but libqi doesn't check struct names

        match name {
            super::OBJECT_TOKEN => {
                self.check_tag(Tag::Object)?;
                Ok(BinaryStructSerializer::new(self, true))
            }
            super::ANYVALUE_TOKEN => {
                self.check_tag(Tag::Dynamic)?;
                Ok(BinaryStructSerializer::new(self, true))
            }
            _ => self.serialize_tuple(len),
        }
    }

    fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
        self.check_tag(Tag::Map)?;
        BinaryMapSeqSerializer::new(self, len)
    }

    fn serialize_newtype_variant<T: ?Sized + Serialize>(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _val: &T,
    ) -> Result<(), Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant, Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant, Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_i128(self, _val: i128) -> Result<(), Error> {
        Err(Error::UnsupportedI128)
    }

    fn serialize_u128(self, _val: u128) -> Result<(), Error> {
        Err(Error::UnsupportedU128)
    }
}

pub(crate) struct BinaryMapSeqSerializer<'ser, 'sig> {
    serializer: &'ser mut BinarySerializer<'sig>,

    begin_sig_iter: SIter<'sig>,
    len: usize,
    written_len: usize,
    len_pos: u64,
}

impl<'ser, 'sig> BinaryMapSeqSerializer<'ser, 'sig> {
    fn new(
        serializer: &'ser mut BinarySerializer<'sig>,
        len: Option<usize>,
    ) -> Result<Self, Error> {
        let len_pos = serializer.buf.position();
        let written_len = match len {
            Some(size) => {
                if size > u32::max_value() as usize {
                    return Err(Error::SizeToBig(size));
                }
                serializer.buf.write_u32::<LittleEndian>(size as u32)?;
                size
            }
            None => {
                serializer.buf.write_u32::<LittleEndian>(0)?;
                0
            }
        };

        let begin_sig_iter = serializer.sig_iter.clone();
        Ok(Self {
            serializer,
            begin_sig_iter,
            len: 0,
            written_len,
            len_pos,
        })
    }

    fn fix_written_len(&mut self) -> Result<(), Error> {
        if self.len != self.written_len {
            let pos = self.serializer.buf.position();
            self.serializer.buf.set_position(self.len_pos);
            if self.len > u32::max_value() as usize {
                return Err(Error::SizeToBig(self.len));
            }
            self.serializer
                .buf
                .write_u32::<LittleEndian>(self.len as u32)?;
            self.serializer.buf.set_position(pos);
        }
        Ok(())
    }
}

impl<'ser, 'sig> ser::SerializeSeq for BinaryMapSeqSerializer<'ser, 'sig> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Error> {
        self.len += 1;
        self.serializer.sig_iter = self.begin_sig_iter.clone();
        Serialize::serialize(val, &mut *self.serializer)
    }

    fn end(mut self) -> Result<Self::Ok, Error> {
        self.fix_written_len()?;
        // list might be empty
        self.serializer.sig_iter = self.begin_sig_iter;
        signature::consume_next_element(&mut self.serializer.sig_iter);
        self.serializer.check_tag(Tag::ListEnd)?;
        Ok(())
    }
}

impl<'ser, 'sig> ser::SerializeMap for BinaryMapSeqSerializer<'ser, 'sig> {
    type Ok = ();
    type Error = Error;

    fn serialize_key<T: ?Sized + Serialize>(&mut self, key: &T) -> Result<(), Self::Error> {
        self.len += 1;
        self.serializer.sig_iter = self.begin_sig_iter.clone();
        Serialize::serialize(key, &mut *self.serializer)
    }

    fn serialize_value<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), Self::Error> {
        Serialize::serialize(value, &mut *self.serializer)
    }

    fn end(mut self) -> Result<Self::Ok, Error> {
        self.fix_written_len()?;
        // map might be empty
        self.serializer.sig_iter = self.begin_sig_iter;
        signature::consume_next_element(&mut self.serializer.sig_iter);
        signature::consume_next_element(&mut self.serializer.sig_iter);
        self.serializer.check_tag(Tag::MapEnd)?;
        Ok(())
    }
}

pub(crate) struct BinaryStructSerializer<'ser, 'sig> {
    serializer: &'ser mut BinarySerializer<'sig>,
    disabled_check: bool,
}

impl<'ser, 'sig> BinaryStructSerializer<'ser, 'sig> {
    fn new(serializer: &'ser mut BinarySerializer<'sig>, disable_check: bool) -> Self {
        if disable_check {
            serializer.check_tag = false;
        }
        Self {
            serializer,
            disabled_check: disable_check,
        }
    }

    fn end_struct(mut self) -> Result<(), Error> {
        if self.disabled_check {
            self.serializer.check_tag = true;
        }
        self.serializer.check_tag(Tag::TupleEnd)?;
        Ok(())
    }
}

impl<'ser, 'sig> ser::SerializeTuple for BinaryStructSerializer<'ser, 'sig> {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Error> {
        Serialize::serialize(val, &mut *self.serializer)
    }

    fn end(self) -> Result<Self::Ok, Error> {
        self.end_struct()
    }
}

impl<'ser, 'sig> ser::SerializeTupleStruct for BinaryStructSerializer<'ser, 'sig> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Error> {
        Serialize::serialize(val, &mut *self.serializer)
    }

    fn end(self) -> Result<Self::Ok, Error> {
        self.end_struct()
    }
}

impl<'ser, 'sig> ser::SerializeStruct for BinaryStructSerializer<'ser, 'sig> {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T: ?Sized + Serialize>(
        &mut self,
        name: &'static str,
        val: &T,
    ) -> Result<(), Error> {
        if let Some(item) = self.serializer.sig_iter.clone().next() {
            let expected = item.field_name();
            if !expected.is_empty() && name != expected {
                return Err(Error::UnexpectedField(expected.to_string(), name));
            }
        }
        Serialize::serialize(val, &mut *self.serializer)
    }

    fn end(self) -> Result<Self::Ok, Error> {
        self.end_struct()
    }

    fn skip_field(&mut self, name: &'static str) -> Result<(), Error> {
        if let Some(item) = self.serializer.sig_iter.clone().next() {
            let expected = item.field_name();
            if !expected.is_empty() && name != expected {
                return Err(Error::UnexpectedField(expected.to_string(), name));
            }
        }
        default_binary(&mut self.serializer.sig_iter, &mut self.serializer.buf)
    }
}

pub(crate) fn default_binary(iter: &mut SIter<'_>, buf: &mut Cursor<Vec<u8>>) -> Result<(), Error> {
    match iter.next() {
        Some(item) => match item.tag() {
            Tag::None => Ok(()),
            Tag::Unit => Ok(()),
            Tag::Bool => buf.write_u8(0).map_err(Into::into),
            Tag::Int8 => buf.write_i8(0).map_err(Into::into),
            Tag::UInt8 => buf.write_u8(0).map_err(Into::into),
            Tag::Int16 => buf.write_i16::<LittleEndian>(0).map_err(Into::into),
            Tag::UInt16 => buf.write_u16::<LittleEndian>(0).map_err(Into::into),
            Tag::Int32 => buf.write_i32::<LittleEndian>(0).map_err(Into::into),
            Tag::UInt32 => buf.write_u32::<LittleEndian>(0).map_err(Into::into),
            Tag::Int64 => buf.write_i64::<LittleEndian>(0).map_err(Into::into),
            Tag::UInt64 => buf.write_u64::<LittleEndian>(0).map_err(Into::into),
            Tag::Float => buf.write_f32::<LittleEndian>(0.).map_err(Into::into),
            Tag::Double => buf.write_f64::<LittleEndian>(0.).map_err(Into::into),
            Tag::String => buf.write_u32::<LittleEndian>(0).map_err(Into::into),
            Tag::List => buf.write_u32::<LittleEndian>(0).map_err(Into::into),
            Tag::Map => buf.write_u32::<LittleEndian>(0).map_err(Into::into),
            Tag::Tuple => {
                loop {
                    match iter.clone().next().unwrap().tag() {
                        Tag::TupleEnd => {
                            let _ = iter.next();
                            break;
                        }
                        _ => default_binary(iter, buf)?,
                    }
                }
                Ok(())
            }
            Tag::Dynamic => write_bytes_array(buf, b"v"),
            Tag::Raw => buf.write_u32::<LittleEndian>(0).map_err(Into::into),
            Tag::Optional => buf.write_u8(0).map_err(Into::into),
            Tag::Object => unimplemented!(),
            Tag::ListEnd | Tag::MapEnd | Tag::TupleEnd => unreachable!(),
            Tag::Unknown => Ok(()), // error ?
        },
        None => Err(Error::UnexpectedEndOfSignature),
    }
}
