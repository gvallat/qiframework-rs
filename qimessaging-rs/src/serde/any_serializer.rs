use {
    crate::{
        any::{Any, AnyEnum},
        buffer::Buffer,
        serde::Error,
        signature::{self, Iter as SIter, Signature, Tag},
    },
    serde::{ser, Serialize, Serializer},
};

pub(crate) struct AnySerializer<'iter, 'sig> {
    iter: &'iter mut SIter<'sig>,
}

impl<'iter, 'sig> AnySerializer<'iter, 'sig> {
    pub(crate) fn new(iter: &'iter mut SIter<'sig>) -> Self {
        Self { iter }
    }
}

fn check_tag(iter: &mut SIter<'_>, tag: Tag) -> Result<(), Error> {
    match iter.next() {
        Some(item) => {
            if item.tag() == tag {
                Ok(())
            } else {
                Err(Error::UnexpectedType(item.tag(), tag))
            }
        }
        None => Err(Error::UnexpectedType(Tag::None, tag)),
    }
}

fn get_next_sub_signature(iter: &mut SIter<'_>, tag: Tag) -> Result<Signature, Error> {
    match iter.next() {
        Some(item) => {
            if item.tag() == tag {
                Ok(item.sub_signature().unwrap())
            } else {
                Err(Error::UnexpectedType(item.tag(), tag))
            }
        }
        None => Err(Error::UnexpectedType(Tag::None, tag)),
    }
}

impl<'iter, 'sig> Serializer for AnySerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error; // Improve use custom error type

    type SerializeSeq = AnySeqSerializer<'iter, 'sig>;
    type SerializeTuple = AnyStructSerializer<'iter, 'sig>;
    type SerializeTupleStruct = AnyStructSerializer<'iter, 'sig>;
    type SerializeStruct = AnyStructSerializer<'iter, 'sig>;
    type SerializeMap = AnyMapSerializer<'iter, 'sig>;
    type SerializeTupleVariant = ser::Impossible<Self::Ok, Self::Error>;
    type SerializeStructVariant = ser::Impossible<Self::Ok, Self::Error>;

    fn is_human_readable(&self) -> bool {
        false
    }

    fn serialize_bool(self, val: bool) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Bool)?;
        Ok(Any(AnyEnum::Boolean(val)))
    }

    fn serialize_i8(self, val: i8) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Int8)?;
        Ok(Any(AnyEnum::Int8(val)))
    }

    fn serialize_u8(self, val: u8) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::UInt8)?;
        Ok(Any(AnyEnum::UInt8(val)))
    }

    fn serialize_i16(self, val: i16) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Int16)?;
        Ok(Any(AnyEnum::Int16(val)))
    }

    fn serialize_u16(self, val: u16) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::UInt16)?;
        Ok(Any(AnyEnum::UInt16(val)))
    }

    fn serialize_i32(self, val: i32) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Int32)?;
        Ok(Any(AnyEnum::Int32(val)))
    }

    fn serialize_u32(self, val: u32) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::UInt32)?;
        Ok(Any(AnyEnum::UInt32(val)))
    }

    fn serialize_i64(self, val: i64) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Int64)?;
        Ok(Any(AnyEnum::Int64(val)))
    }

    fn serialize_u64(self, val: u64) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::UInt64)?;
        Ok(Any(AnyEnum::UInt64(val)))
    }

    fn serialize_f32(self, val: f32) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Float)?;
        Ok(Any(AnyEnum::Float(val)))
    }

    fn serialize_f64(self, val: f64) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Double)?;
        Ok(Any(AnyEnum::Double(val)))
    }

    fn serialize_char(self, val: char) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::String)?;
        Ok(Any(AnyEnum::String(val.to_string())))
    }

    fn serialize_str(self, val: &str) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::String)?;
        Ok(Any(AnyEnum::String(val.to_string())))
    }

    fn serialize_bytes(self, val: &[u8]) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Raw)?;
        Ok(Any(AnyEnum::Raw(Buffer::from_vec(val.to_vec()))))
    }

    fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
        let sig = get_next_sub_signature(self.iter, Tag::Optional)?;
        signature::consume_next_element(self.iter);
        Ok(Any(AnyEnum::Optional(sig, None)))
    }

    fn serialize_some<T: ?Sized + Serialize>(self, val: &T) -> Result<Self::Ok, Self::Error> {
        let sig = get_next_sub_signature(self.iter, Tag::Optional)?;
        let ser = AnySerializer::new(self.iter);
        let any = Serialize::serialize(val, ser)?;
        Ok(Any(AnyEnum::Optional(sig, Some(Box::new(any)))))
    }

    fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
        check_tag(self.iter, Tag::Unit)?;
        Ok(Any(AnyEnum::Unit))
    }

    fn serialize_unit_struct(self, _name: &'static str) -> Result<Self::Ok, Self::Error> {
        // could check signature struct name but libqi doesn't check struct names
        let sig = get_next_sub_signature(self.iter, Tag::Tuple)?;
        check_tag(self.iter, Tag::TupleEnd)?;
        Ok(Any(AnyEnum::Tuple(sig, vec![])))
    }

    fn serialize_unit_variant(
        self,
        _name: &'static str,
        idx: u32,
        _variant: &'static str,
    ) -> Result<Self::Ok, Self::Error> {
        // C like enum variant
        use std::convert::TryFrom;
        match self.iter.next() {
            Some(item) => match item.tag() {
                Tag::Int8 => match i8::try_from(idx as i32) {
                    Ok(idx) => Ok(Any(AnyEnum::Int8(idx))),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::Int8)),
                },
                Tag::UInt8 => match u8::try_from(idx) {
                    Ok(idx) => Ok(Any(AnyEnum::UInt8(idx))),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::UInt8)),
                },
                Tag::Int16 => match i16::try_from(idx as i32) {
                    Ok(idx) => Ok(Any(AnyEnum::Int16(idx))),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::Int16)),
                },
                Tag::UInt16 => match u16::try_from(idx) {
                    Ok(idx) => Ok(Any(AnyEnum::UInt16(idx))),
                    Err(_) => Err(Error::EnumDoesntFit(idx, Tag::UInt16)),
                },
                Tag::Int32 => Ok(Any(AnyEnum::Int32(idx as i32))),
                Tag::UInt32 => Ok(Any(AnyEnum::UInt32(idx))),
                Tag::Int64 => Ok(Any(AnyEnum::Int64(idx as i64))),
                Tag::UInt64 => Ok(Any(AnyEnum::UInt64(idx as u64))),
                _ => Err(Error::IncompatibleEnumType),
            },
            None => Err(Error::IncompatibleEnumType),
        }
    }

    fn serialize_newtype_struct<T: ?Sized + Serialize>(
        self,
        _name: &'static str,
        val: &T,
    ) -> Result<Self::Ok, Self::Error> {
        // serialize as if not wrapped
        // example: struct Millimetters(u8)
        Serialize::serialize(val, self)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<Self::SerializeSeq, Error> {
        let sig = get_next_sub_signature(self.iter, Tag::List)?;
        Ok(AnySeqSerializer::new(self.iter, sig, len))
    }

    fn serialize_tuple(self, len: usize) -> Result<Self::SerializeTuple, Error> {
        // anonymous tuple: (u32, i16, f64)
        let sig = get_next_sub_signature(self.iter, Tag::Tuple)?;
        super::SERIALIZE_SIGNATURE.with(|s| *s.borrow_mut() = Some(sig.clone()));
        Ok(AnyStructSerializer::new(self.iter, sig, len))
    }

    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeTupleStruct, Error> {
        // tuple struct: Rgb(u8, u8, u8)
        // could check signature struct name but libqi doesn't check struct names
        self.serialize_tuple(len)
    }

    fn serialize_struct(
        self,
        name: &'static str,
        len: usize,
    ) -> Result<Self::SerializeStruct, Error> {
        // struct: Rgb{ r: u8, g: u8, b: u8}
        // could check signature struct name but libqi doesn't check struct names
        match name {
            super::OBJECT_TOKEN => {
                check_tag(self.iter, Tag::Object)?;
                Ok(AnyStructSerializer::Object)
            }
            super::ANYVALUE_TOKEN => {
                check_tag(self.iter, Tag::Dynamic)?;
                Ok(AnyStructSerializer::Dynamic)
            }
            _ => self.serialize_tuple(len),
        }
    }

    fn serialize_map(self, len: Option<usize>) -> Result<Self::SerializeMap, Self::Error> {
        let sig = get_next_sub_signature(self.iter, Tag::Map)?;
        Ok(AnyMapSerializer::new(self.iter, sig, len))
    }

    fn serialize_newtype_variant<T: ?Sized + Serialize>(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _val: &T,
    ) -> Result<Self::Ok, Self::Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant, Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _idx: u32,
        _variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant, Error> {
        Err(Error::UnsupportedVariant)
    }

    fn serialize_i128(self, _val: i128) -> Result<Self::Ok, Self::Error> {
        Err(Error::UnsupportedI128)
    }

    fn serialize_u128(self, _val: u128) -> Result<Self::Ok, Self::Error> {
        Err(Error::UnsupportedU128)
    }
}

pub(crate) struct AnySeqSerializer<'iter, 'sig> {
    iter: &'iter mut SIter<'sig>,
    sig: Signature,
    anys: Vec<Any>,
}

impl<'iter, 'sig> AnySeqSerializer<'iter, 'sig> {
    fn new(iter: &'iter mut SIter<'sig>, sig: Signature, len: Option<usize>) -> Self {
        Self {
            iter,
            sig,
            anys: match len {
                Some(len) => Vec::with_capacity(len),
                None => Vec::new(),
            },
        }
    }
}

impl<'iter, 'sig> ser::SerializeSeq for AnySeqSerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error;

    fn serialize_element<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Self::Error> {
        let mut iter = self.iter.clone();
        let ser = AnySerializer::new(&mut iter);
        let any = Serialize::serialize(val, ser)?;
        self.anys.push(any);
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        signature::consume_next_element(self.iter);
        check_tag(self.iter, Tag::ListEnd)?;
        Ok(Any(AnyEnum::List(self.sig, self.anys)))
    }
}

pub(crate) struct AnyMapSerializer<'iter, 'sig> {
    iter: &'iter mut SIter<'sig>,
    value_iter: SIter<'sig>,
    sig: Signature,
    anys: Vec<(Any, Any)>,
}

impl<'iter, 'sig> AnyMapSerializer<'iter, 'sig> {
    fn new(iter: &'iter mut SIter<'sig>, sig: Signature, len: Option<usize>) -> Self {
        let value_iter = iter.clone();
        Self {
            iter,
            value_iter,
            sig,
            anys: match len {
                Some(len) => Vec::with_capacity(len),
                None => Vec::new(),
            },
        }
    }
}

impl<'iter, 'sig> ser::SerializeMap for AnyMapSerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error;

    fn serialize_entry<K: ?Sized + Serialize, V: ?Sized + Serialize>(
        &mut self,
        key: &K,
        value: &V,
    ) -> Result<(), Self::Error> {
        let mut iter = self.iter.clone();
        let ser = AnySerializer::new(&mut iter);
        let key_any = Serialize::serialize(key, ser)?;
        let ser = AnySerializer::new(&mut iter);
        let value_any = Serialize::serialize(value, ser)?;
        self.anys.push((key_any, value_any));
        Ok(())
    }

    fn serialize_key<T: ?Sized + Serialize>(&mut self, key: &T) -> Result<(), Self::Error> {
        let mut iter = self.iter.clone();
        let ser = AnySerializer::new(&mut iter);
        let any = Serialize::serialize(key, ser)?;
        self.anys.push((any, Any(AnyEnum::None)));
        self.value_iter = iter;
        Ok(())
    }

    fn serialize_value<T: ?Sized + Serialize>(&mut self, value: &T) -> Result<(), Self::Error> {
        let mut iter = self.value_iter.clone();
        let ser = AnySerializer::new(&mut iter);
        let any = Serialize::serialize(value, ser)?;
        self.anys.last_mut().unwrap().1 = any;
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        signature::consume_next_element(self.iter);
        signature::consume_next_element(self.iter);
        check_tag(self.iter, Tag::MapEnd)?;
        Ok(Any(AnyEnum::Map(self.sig, self.anys)))
    }
}

pub(crate) enum AnyStructSerializer<'iter, 'sig> {
    Tuple {
        iter: &'iter mut SIter<'sig>,
        sig: Signature,
        anys: Vec<Any>,
    },
    Object,
    Dynamic,
}

impl<'iter, 'sig> AnyStructSerializer<'iter, 'sig> {
    fn new(iter: &'iter mut SIter<'sig>, sig: Signature, len: usize) -> Self {
        AnyStructSerializer::Tuple {
            iter,
            sig,
            anys: Vec::with_capacity(len),
        }
    }
}

impl<'iter, 'sig> ser::SerializeTuple for AnyStructSerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error;

    fn serialize_element<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Self::Error> {
        match self {
            AnyStructSerializer::Tuple {
                iter,
                sig: _sig,
                anys,
            } => {
                let ser = AnySerializer::new(iter);
                let any = Serialize::serialize(val, ser)?;
                anys.push(any);
            }
            AnyStructSerializer::Object => { /* error ?*/ }
            AnyStructSerializer::Dynamic => { /* error ?*/ }
        }
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {
            AnyStructSerializer::Tuple {
                iter: _iter,
                sig,
                anys,
            } => Ok(Any(AnyEnum::Tuple(sig, anys))),
            AnyStructSerializer::Object => unimplemented!(),
            AnyStructSerializer::Dynamic => {
                let any = super::SERIALIZE_ANY.with(|a| a.borrow_mut().take());
                match any {
                    Some(any) => Ok(any),
                    None => Err(Error::ExpectedAny),
                }
            }
        }
    }
}

impl<'iter, 'sig> ser::SerializeTupleStruct for AnyStructSerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error;

    fn serialize_field<T: ?Sized + Serialize>(&mut self, val: &T) -> Result<(), Self::Error> {
        ser::SerializeTuple::serialize_element(self, val)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        ser::SerializeTuple::end(self)
    }
}

impl<'iter, 'sig> ser::SerializeStruct for AnyStructSerializer<'iter, 'sig> {
    type Ok = Any;
    type Error = Error;

    fn serialize_field<T: ?Sized + Serialize>(
        &mut self,
        name: &'static str,
        val: &T,
    ) -> Result<(), Self::Error> {
        match self {
            AnyStructSerializer::Tuple { iter, .. } => {
                if let Some(item) = iter.clone().next() {
                    let expected = item.field_name();
                    if !expected.is_empty() && name != expected {
                        return Err(Error::UnexpectedField(expected.to_string(), name));
                    }
                }
            }
            AnyStructSerializer::Object => {}
            AnyStructSerializer::Dynamic => {}
        }
        ser::SerializeTuple::serialize_element(self, val)
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        ser::SerializeTuple::end(self)
    }

    fn skip_field(&mut self, name: &'static str) -> Result<(), Self::Error> {
        match self {
            AnyStructSerializer::Tuple { iter, anys, .. } => {
                if let Some(item) = iter.clone().next() {
                    let expected = item.field_name();
                    if !expected.is_empty() && name != expected {
                        return Err(Error::UnexpectedField(expected.to_string(), name));
                    }
                }
                anys.push(default_any(iter));
            }
            AnyStructSerializer::Object => { /* error ?*/ }
            AnyStructSerializer::Dynamic => { /* error ?*/ }
        }
        Ok(())
    }
}

pub(crate) fn default_any(iter: &mut SIter<'_>) -> Any {
    match iter.next() {
        Some(item) => match item.tag() {
            Tag::None => Any(AnyEnum::None),
            Tag::Unit => Any(AnyEnum::Unit),
            Tag::Bool => Any(AnyEnum::Boolean(false)),
            Tag::Int8 => Any(AnyEnum::Int8(0)),
            Tag::UInt8 => Any(AnyEnum::UInt8(0)),
            Tag::Int16 => Any(AnyEnum::Int16(0)),
            Tag::UInt16 => Any(AnyEnum::UInt16(0)),
            Tag::Int32 => Any(AnyEnum::Int32(0)),
            Tag::UInt32 => Any(AnyEnum::UInt32(0)),
            Tag::Int64 => Any(AnyEnum::Int64(0)),
            Tag::UInt64 => Any(AnyEnum::UInt64(0)),
            Tag::Float => Any(AnyEnum::Float(0.)),
            Tag::Double => Any(AnyEnum::Double(0.)),
            Tag::String => Any(AnyEnum::String(String::new())),
            Tag::List => {
                let sig = item.sub_signature().unwrap();
                Any(AnyEnum::List(sig, Vec::new()))
            }
            Tag::Map => {
                let sig = item.sub_signature().unwrap();
                Any(AnyEnum::Map(sig, Vec::new()))
            }
            Tag::Tuple => {
                let sig = item.sub_signature().unwrap();
                let mut vec = Vec::new();
                loop {
                    match iter.clone().next().unwrap().tag() {
                        Tag::TupleEnd => {
                            let _ = iter.next();
                            break;
                        }
                        _ => vec.push(default_any(iter)),
                    }
                }
                Any(AnyEnum::Tuple(sig, vec))
            }
            Tag::Dynamic => Any(AnyEnum::Dynamic(Box::new(Any(AnyEnum::None)))),
            Tag::Raw => Any(AnyEnum::Raw(Buffer::new())),
            Tag::Optional => {
                let sig = item.sub_signature().unwrap();
                Any(AnyEnum::Optional(sig, None))
            }
            Tag::Object => unimplemented!(),
            Tag::ListEnd | Tag::MapEnd | Tag::TupleEnd => unreachable!(),
            Tag::Unknown => Any(AnyEnum::Unknown),
        },
        None => Any(AnyEnum::None),
    }
}
