use {
    crate::signature::Tag,
    serde::{de, ser},
    std::{fmt, io},
};

#[derive(Debug)]
pub enum Error {
    EndOfBuffer,
    UnsupportedVariant,
    UnsupportedI128,
    UnsupportedU128,
    ExpectedAny,
    ExpectedSignatureEnd,
    SizeToBig(usize),
    UnexpectedWriteAfterEnd,
    UnexpectedEndOfSignature,
    UnexpectedType(Tag, Tag),
    IncompatibleEnumType,
    IncompatibleWithU32,
    EnumDoesntFit(u32, Tag),
    UnexpectedField(String, &'static str),

    Io(std::io::Error),
    Custom(String),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::EndOfBuffer => write!(f, "unexpected end of buffer"),
            Error::UnsupportedVariant => {
                write!(f, "qimessaging serialization only support C like enums")
            }
            Error::UnsupportedI128 => write!(f, "qimessaging serialization doesn't support i128"),
            Error::UnsupportedU128 => write!(f, "qimessaging serialization doesn't support u128"),
            Error::ExpectedSignatureEnd => {
                write!(f, "the driving signature has not be completely consumed")
            }
            Error::ExpectedAny => write!(f, "expected to serialize an Any"),
            Error::SizeToBig(size) => write!(f, "size doesn't fit in u32 ({})", size),
            Error::UnexpectedWriteAfterEnd => write!(f, "unexpected write after end"),
            Error::UnexpectedEndOfSignature => write!(f, "unexpected end of signature"),
            Error::UnexpectedType(expected, actual) => write!(
                f,
                "unexpected type (signature: {}, processed: {})",
                expected, actual
            ),
            Error::IncompatibleEnumType => write!(f, "incompatble type for enum"),
            Error::IncompatibleWithU32 => write!(f, "the value is incompatble with u32"),
            Error::EnumDoesntFit(value, expected) => write!(
                f,
                "the enum value doesn't fit in the type (value: {}, signature: {})",
                value, expected
            ),
            Error::UnexpectedField(expected, actual) => write!(
                f,
                "unexpected field name (signature: {}, processed: {})",
                expected, actual
            ),
            Error::Io(e) => write!(f, "io error ({})", e),
            Error::Custom(msg) => write!(f, "{}", msg),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Io(e) => Some(e),
            _ => None,
        }
    }
}

impl ser::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Error::Custom(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T: fmt::Display>(msg: T) -> Self {
        Error::Custom(msg.to_string())
    }

    // fn invalid_type(unexp: de::Unexpected<'_>, exp: &dyn de::Expected) -> Self { ... }
    // fn invalid_value(unexp: de::Unexpected<'_>, exp: &dyn de::Expected) -> Self { ... }
    // fn invalid_length(len: usize, exp: &dyn de::Expected) -> Self { ... }
    // fn unknown_variant(variant: &str, expected: &'static [&'static str]) -> Self { ... }
    // fn unknown_field(field: &str, expected: &'static [&'static str]) -> Self { ... }
    // fn missing_field(field: &'static str) -> Self { ... }
    // fn duplicate_field(field: &'static str) -> Self { ... }
}
