use {
    crate::{
        buffer::Buffer,
        serde::{self as ser, binary_serializer::BinarySerializer},
        signature::Signature,
    },
    byteorder::{LittleEndian, WriteBytesExt},
    serde::{Serialize, Serializer},
    std::io::{Cursor, Write},
};

pub fn to_payload<T: Serialize>(sig: &Signature, val: &T) -> Result<Vec<u8>, ser::Error> {
    let mut serializer = BinarySerializer::new(sig.iter());

    ser::SERIALIZE_BINARY.with(|b| *b.borrow_mut() = true);
    let res = Serialize::serialize(val, &mut serializer);
    ser::SERIALIZE_SIGNATURE.with(|s| *s.borrow_mut() = None);
    ser::SERIALIZE_BINARY.with(|b| *b.borrow_mut() = false);

    res?;
    serializer.check_reached_end()?;
    Ok(serializer.into_vec())
}

#[test]
fn is_human_readable() {
    let sig = Signature::new("v".to_string()).unwrap();
    let ser = &mut BinarySerializer::new(sig.iter());
    assert_eq!(false, ser.is_human_readable());
}

fn to_sig(sig: &str) -> Signature {
    Signature::new(sig.to_string()).unwrap()
}

#[test]
fn serialize_bool() {
    let val = true;
    let sig = to_sig("b");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1));
    cursor.write_u8(1).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_i8() {
    let val = 5i8;
    let sig = to_sig("c");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1));
    cursor.write_i8(5).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_u8() {
    let val = 5u8;
    let sig = to_sig("C");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1));
    cursor.write_u8(5).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

macro_rules! test_number {
    ($name:ident, $write:ident, $t:ty, $sig:expr, $capa:expr) => {
        #[test]
        fn $name() {
            let val = 5 as $t;
            let sig = to_sig($sig);
            let res = to_payload(&sig, &val);
            assert!(res.is_ok());

            let mut cursor = Cursor::new(Vec::with_capacity($capa));
            cursor.$write::<LittleEndian>(5 as $t).unwrap();
            assert_eq!(res.unwrap(), cursor.into_inner());
        }
    };
}

test_number!(serialize_i16, write_i16, i16, "w", 2);
test_number!(serialize_u16, write_u16, u16, "W", 2);
test_number!(serialize_i32, write_i32, i32, "i", 4);
test_number!(serialize_u32, write_u32, u32, "I", 4);
test_number!(serialize_i64, write_i64, i64, "l", 8);
test_number!(serialize_u64, write_u64, u64, "L", 7);
test_number!(serialize_f32, write_f32, f32, "f", 4);
test_number!(serialize_f64, write_f64, f64, "d", 8);

#[test]
fn serialize_empty_string() {
    let val = String::new();
    let sig = to_sig("s");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4));
    cursor.write_u32::<LittleEndian>(0).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_string() {
    let val = "Some text";
    let sig = to_sig("s");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4 + val.len()));
    cursor.write_u32::<LittleEndian>(val.len() as u32).unwrap();
    cursor.write_all(val.as_bytes()).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_char() {
    let c = 'b';
    let sig = to_sig("s");
    let res = to_payload(&sig, &c);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4 + 1));
    cursor.write_u32::<LittleEndian>(1).unwrap();
    cursor.write_all(c.to_string().as_bytes()).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_empty_buffer() {
    let val = Buffer::new();
    let sig = to_sig("r");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4));
    cursor.write_u32::<LittleEndian>(0).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_buffer() {
    let data = &[1, 2, 3, 4, 5];
    let val = Buffer::from_vec(data.to_vec());
    let sig = to_sig("r");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4 + data.len()));
    cursor.write_u32::<LittleEndian>(data.len() as u32).unwrap();
    cursor.write_all(data).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_none() {
    let val: Option<u32> = None;
    let sig = to_sig("+I");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1));
    cursor.write_u8(0).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_some() {
    let val: Option<u32> = Some(42);
    let sig = to_sig("+I");
    let res = to_payload(&sig, &val);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1 + 4));
    cursor.write_u8(1).unwrap();
    cursor.write_u32::<LittleEndian>(42).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_empty_vec() {
    let vec: Vec<u32> = Vec::new();
    let sig = to_sig("[I]");
    let res = to_payload(&sig, &vec);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4));
    cursor.write_u32::<LittleEndian>(0).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_vec() {
    let vec = vec![45u32, 63, 28];
    let sig = to_sig("[I]");
    let res = to_payload(&sig, &vec);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4 + 4 * vec.len()));
    cursor.write_u32::<LittleEndian>(vec.len() as u32).unwrap();
    for v in vec {
        cursor.write_u32::<LittleEndian>(v).unwrap();
    }
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_empty_map() {
    use std::collections::BTreeMap;
    let map: BTreeMap<u32, bool> = BTreeMap::new();
    let sig = to_sig("{Ib}");
    let res = to_payload(&sig, &map);
    println!("{:?}", res);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4));
    cursor.write_u32::<LittleEndian>(0).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_map() {
    use std::collections::BTreeMap;
    let mut map: BTreeMap<u32, i8> = BTreeMap::new();
    map.insert(5, 6);
    map.insert(8, 3);
    map.insert(84, 32);
    let sig = to_sig("{Ic}");
    let res = to_payload(&sig, &map);
    println!("{:?}", res);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(4 + (4 + 1) * map.len()));
    // BTreeMap is an ordered map
    cursor.write_u32::<LittleEndian>(map.len() as u32).unwrap();
    for (key, val) in map {
        cursor.write_u32::<LittleEndian>(key).unwrap();
        cursor.write_i8(val).unwrap();
    }
    assert_eq!(res.unwrap(), cursor.into_inner());
}

#[test]
fn serialize_tuple_1() {
    #[derive(Serialize)]
    struct MyStruct {
        x: u8,
        y: u32,
        z: bool,
    };

    let val = MyStruct {
        x: 4,
        y: 42,
        z: true,
    };
    let sig = to_sig("(CIb)<MyStruct,x,y,z>");
    let res = to_payload(&sig, &val);
    println!("{:?}", res);
    assert!(res.is_ok());

    let mut cursor = Cursor::new(Vec::with_capacity(1 + 4 + 1));
    cursor.write_u8(val.x).unwrap();
    cursor.write_u32::<LittleEndian>(val.y).unwrap();
    cursor.write_u8(1).unwrap();
    assert_eq!(res.unwrap(), cursor.into_inner());
}
