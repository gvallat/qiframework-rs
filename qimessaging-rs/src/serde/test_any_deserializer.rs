use crate::{
    any::{Any, AnyEnum},
    buffer::Buffer,
    serde::from_any,
    signature::Signature,
};

#[test]
fn deserialize_bool() {
    let any = Any(AnyEnum::Boolean(true));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: bool = res.unwrap();
    assert_eq!(true, val);
}

macro_rules! test_deserializer_integer {
    ($name:ident, $enum:ident => $t:ty) => {
        #[test]
        fn $name() {
            let any = Any(AnyEnum::$enum(42 as $t));
            let res = from_any(&any);
            assert!(res.is_ok());
            let val: $t = res.unwrap();
            assert_eq!(42 as $t, val);
        }
    };
}

test_deserializer_integer!(deserialize_i8, Int8 => i8);
test_deserializer_integer!(deserialize_u8, UInt8 => u8);
test_deserializer_integer!(deserialize_i16, Int16 => i16);
test_deserializer_integer!(deserialize_u16, UInt16 => u16);
test_deserializer_integer!(deserialize_i32, Int32 => i32);
test_deserializer_integer!(deserialize_u32, UInt32 => u32);
test_deserializer_integer!(deserialize_i64, Int64 => i64);
test_deserializer_integer!(deserialize_u64, UInt64 => u64);

#[test]
fn deserialize_f32() {
    let any = Any(AnyEnum::Float(42.));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: f32 = res.unwrap();
    assert!((42. - val).abs() < std::f32::EPSILON);
}

#[test]
fn deserialize_f64() {
    let any = Any(AnyEnum::Double(42.));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: f64 = res.unwrap();
    assert!((42. - val).abs() < std::f64::EPSILON);
}

#[test]
fn deserialize_string() {
    let text = "Some text".to_string();
    let any = Any(AnyEnum::String(text.clone()));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: String = res.unwrap();
    assert_eq!(text, val);
}

#[test]
fn deserialize_buffer() {
    let buffer = Buffer::from_vec(b"Some text".to_vec());
    let any = Any(AnyEnum::Raw(buffer.clone()));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Buffer = res.unwrap();
    assert_eq!(buffer, val);
}

#[test]
fn deserialize_none() {
    let sig = Signature::new("+I".to_string()).unwrap();
    let any = Any(AnyEnum::Optional(sig, None));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Option<u32> = res.unwrap();
    assert_eq!(None, val);
}

#[test]
fn deserialize_some() {
    let sig = Signature::new("+I".to_string()).unwrap();
    let any = Any(AnyEnum::Optional(
        sig,
        Some(Box::new(Any(AnyEnum::UInt32(42)))),
    ));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Option<u32> = res.unwrap();
    assert_eq!(Some(42u32), val);
}

#[test]
fn deserialize_seq() {
    let vec = vec![42u32, 5, 6, 9];
    let anys = vec.iter().map(|i| Any(AnyEnum::UInt32(*i))).collect();
    let sig = Signature::new("[I]".to_string()).unwrap();
    let any = Any(AnyEnum::List(sig, anys));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Vec<u32> = res.unwrap();
    assert_eq!(vec, val);
}

#[test]
fn deserialize_map() {
    let mut map = std::collections::HashMap::new();
    map.insert(42u32, "Some text".to_string());
    map.insert(5, "Stuff".to_string());
    let anys = map
        .iter()
        .map(|(k, v)| (Any(AnyEnum::UInt32(*k)), Any(AnyEnum::String(v.clone()))))
        .collect();
    let sig = Signature::new("{Is}".to_string()).unwrap();
    let any = Any(AnyEnum::Map(sig, anys));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: std::collections::HashMap<u32, String> = res.unwrap();
    assert_eq!(map, val);
}

#[test]
fn deserialize_tuple() {
    #[derive(serde::Deserialize, PartialEq, Eq, Debug)]
    struct Data {
        x: u32,
        y: u8,
    }

    let sig = Signature::new("(IC)<Data,x,y>".to_string()).unwrap();
    let any = Any(AnyEnum::Tuple(
        sig,
        vec![Any(AnyEnum::UInt32(42)), Any(AnyEnum::UInt8(5))],
    ));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Data = res.unwrap();
    assert_eq!(Data { x: 42, y: 5 }, val);
}

#[test]
fn deserialize_unordered_tuple() {
    #[derive(serde::Deserialize, PartialEq, Eq, Debug)]
    struct Data {
        y: u8,
        x: u32,
    }

    let sig = Signature::new("(IC)<Data,x,y>".to_string()).unwrap();
    let any = Any(AnyEnum::Tuple(
        sig,
        vec![Any(AnyEnum::UInt32(42)), Any(AnyEnum::UInt8(5))],
    ));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Data = res.unwrap();
    assert_eq!(Data { x: 42, y: 5 }, val);
}

#[test]
fn deserialize_anonymous_tuple() {
    #[derive(serde::Deserialize, PartialEq, Eq, Debug)]
    struct Data {
        x: u32,
        y: u8,
    }

    let sig = Signature::new("(IC)".to_string()).unwrap();
    let any = Any(AnyEnum::Tuple(
        sig,
        vec![Any(AnyEnum::UInt32(42)), Any(AnyEnum::UInt8(5))],
    ));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: Data = res.unwrap();
    assert_eq!(Data { x: 42, y: 5 }, val);
}

#[test]
fn deserialize_dynamic() {
    let any = Any(AnyEnum::Dynamic(Box::new(Any(AnyEnum::UInt32(42)))));
    let res = from_any(&any);
    assert!(res.is_ok());
    let val: u32 = res.unwrap();
    assert_eq!(42, val);
}

#[test]
fn deserialize_enum() {
    #[derive(serde_repr::Deserialize_repr, PartialEq, Eq, Debug)]
    #[repr(u16)]
    enum Fruit {
        Apple = 1,
        Banana = 5,
        Peach = 42,
    }

    let any = Any(AnyEnum::UInt32(42));
    let res = from_any(&any);
    println!("{:?}", res);
    assert!(res.is_ok());
    let val: Fruit = res.unwrap();
    assert_eq!(Fruit::Peach, val);
}
