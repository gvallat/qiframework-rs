use {
    crate::{
        any::{Any, AnyEnum},
        buffer::Buffer,
        serde::{any_serializer::AnySerializer, to_any, Error},
        signature::{GetTypeSignature, Signature},
    },
    serde::ser::{SerializeMap, SerializeSeq, Serializer},
};

#[test]
fn is_human_readable() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    assert_eq!(false, ser.is_human_readable());
}

#[test]
fn serialize_bool() {
    let sig = Signature::new("b".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_bool(true);
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), Any(AnyEnum::Boolean(true)));
}

#[test]
fn serialize_bool_wrong_signature() {
    let sig = Signature::new("I".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_bool(true);
    assert!(res.is_err());
}

macro_rules! test_integer {
    ($name:ident, $sig:expr, $t:ty, $enum:ident) => {
        mod $name {
            use super::*;

            #[test]
            fn success() {
                let sig = Signature::new($sig.to_string()).unwrap();
                let mut iter = sig.iter();
                let ser = AnySerializer::new(&mut iter);
                let res = ser.$name(5 as $t);
                assert!(res.is_ok());
                assert_eq!(res.unwrap(), Any(AnyEnum::$enum(5 as $t)));
            }

            #[test]
            fn to_any_success() {
                let res = to_any(&(5 as $t));
                assert!(res.is_ok());
                assert_eq!(res.unwrap(), Any(AnyEnum::$enum(5 as $t)));
            }

            #[test]
            fn wrong_signature() {
                let sig = Signature::new("b".to_string()).unwrap();
                let mut iter = sig.iter();
                let ser = AnySerializer::new(&mut iter);
                let res = ser.$name(5 as $t);
                assert!(res.is_err());
            }
        }
    };
}

test_integer!(serialize_i8, "c", i8, Int8);
test_integer!(serialize_u8, "C", u8, UInt8);
test_integer!(serialize_i16, "w", i16, Int16);
test_integer!(serialize_u16, "W", u16, UInt16);
test_integer!(serialize_i32, "i", i32, Int32);
test_integer!(serialize_u32, "I", u32, UInt32);
test_integer!(serialize_i64, "l", i64, Int64);
test_integer!(serialize_u64, "L", u64, UInt64);
test_integer!(serialize_f32, "f", f32, Float);
test_integer!(serialize_f64, "d", f64, Double);

mod serialize_char {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("s".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_char('f');
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::String("f".to_string())));
    }

    // TODO add getTypeSignature to char ?
    // fn to_any_success()

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_char('f');
        assert!(res.is_err());
    }
}

mod serialize_str {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("s".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_str("some text");
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::String("some text".to_string())));
    }

    #[test]
    fn to_any_success() {
        let t = "Some text".to_string();
        let res = to_any(&t);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::String(t)));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_str("some text");
        assert!(res.is_err());
    }
}

mod serialize_bytes {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("r".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let data = vec![0, 1, 2, 3, 4];
        let res = ser.serialize_bytes(&data);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::Raw(Buffer::from_vec(data))));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let data = vec![0, 1, 2, 3, 4];
        let res = ser.serialize_bytes(&data);
        assert!(res.is_err());
    }
}

mod serialize_none {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("+I".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_none();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::Optional(sig, None)));
    }

    #[test]
    fn to_any_success() {
        let o: Option<u32> = None;
        let res = to_any(&o);
        assert!(res.is_ok());
        let sig = Signature::new("+I".to_string()).unwrap();
        assert_eq!(res.unwrap(), Any(AnyEnum::Optional(sig, None)));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_none();
        assert!(res.is_err());
    }
}

mod serialize_some {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("+I".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_some(&5u32);
        assert!(res.is_ok());
        assert_eq!(
            res.unwrap(),
            Any(AnyEnum::Optional(
                sig,
                Some(Box::new(Any(AnyEnum::UInt32(5))))
            ))
        );
    }

    #[test]
    fn to_any_success() {
        let o: Option<u32> = Some(5);
        let res = to_any(&o);
        assert!(res.is_ok());
        let sig = Signature::new("+I".to_string()).unwrap();
        assert_eq!(
            res.unwrap(),
            Any(AnyEnum::Optional(
                sig,
                Some(Box::new(Any(AnyEnum::UInt32(5))))
            ))
        );
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_some(&5u32);
        assert!(res.is_err());
    }
}

mod serialize_unit {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("v".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit();
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::Unit));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit();
        assert!(res.is_err());
    }
}

mod serialize_unit_struct {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("()<MytTagStruct>".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_struct("MyTagStruct");
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::Tuple(sig, vec![])));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("(b)".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_struct("MyTagStruct");
        assert!(res.is_err());
    }
}

mod serialize_unit_variant {
    use super::*;

    macro_rules! test_to_integer {
        ($name:ident, $sig:expr => $enum:ident) => {
            #[test]
            fn $name() {
                let sig = Signature::new($sig.to_string()).unwrap();
                let mut iter = sig.iter();
                let ser = AnySerializer::new(&mut iter);
                let res = ser.serialize_unit_variant("MyENum", 2, "Blue");
                assert!(res.is_ok());
                assert_eq!(res.unwrap(), Any(AnyEnum::$enum(2)));
            }
        };
    }

    test_to_integer!(i8_success, "c" => Int8);
    test_to_integer!(u8_success, "C" => UInt8);
    test_to_integer!(i16_success, "w" => Int16);
    test_to_integer!(u16_success, "W" => UInt16);
    test_to_integer!(i32_success, "i" => Int32);
    test_to_integer!(u32_success, "I" => UInt32);
    test_to_integer!(i64_success, "l" => Int64);
    test_to_integer!(u64_success, "L" => UInt64);

    #[test]
    fn doesnt_fit_i8() {
        let sig = Signature::new("c".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_variant("MyENum", u8::max_value() as u32 + 4, "Blue");
        assert!(res.is_err());
    }

    #[test]
    fn doesnt_fit_u8() {
        let sig = Signature::new("C".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_variant("MyENum", u8::max_value() as u32 + 4, "Blue");
        assert!(res.is_err());
    }

    #[test]
    fn doesnt_fit_i16() {
        let sig = Signature::new("w".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_variant("MyENum", u16::max_value() as u32 + 4, "Blue");
        assert!(res.is_err());
    }

    #[test]
    fn doesnt_fit_u16() {
        let sig = Signature::new("W".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_variant("MyENum", u16::max_value() as u32 + 4, "Blue");
        assert!(res.is_err());
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("s".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_unit_variant("MyENum", 2, "Blue");
        assert!(res.is_err());
    }
}

mod serialize_seq {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("[I]".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let vec = vec![0, 1, 2, 3, 4];
        let mut t = ser.serialize_seq(Some(5)).unwrap();
        for x in vec.iter() {
            t.serialize_element(&x).unwrap();
        }
        let res = t.end();
        assert!(res.is_ok());
        let vec = vec.iter().map(|x| Any(AnyEnum::UInt32(*x))).collect();
        assert_eq!(res.unwrap(), Any(AnyEnum::List(sig, vec)));
    }

    #[test]
    fn no_lenght_success() {
        let sig = Signature::new("[I]".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let vec = vec![0, 1, 2, 3, 4];
        let mut t = ser.serialize_seq(None).unwrap();
        for x in vec.iter() {
            t.serialize_element(&x).unwrap();
        }
        let res = t.end();
        assert!(res.is_ok());
        let vec = vec.iter().map(|x| Any(AnyEnum::UInt32(*x))).collect();
        assert_eq!(res.unwrap(), Any(AnyEnum::List(sig, vec)));
    }

    #[test]
    fn to_any_success() {
        let vec = vec![0u32, 1, 2, 3, 4];
        let res = to_any(&vec);
        assert!(res.is_ok());
        let sig = Signature::new("[I]".to_string()).unwrap();
        let vec = vec.iter().map(|x| Any(AnyEnum::UInt32(*x))).collect();
        assert_eq!(res.unwrap(), Any(AnyEnum::List(sig, vec)));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let t = ser.serialize_seq(Some(5));
        assert!(t.is_err());
    }
}

mod serialize_map {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("{Is}".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let map = vec![(0u32, "stuff".to_string()), (1, "bidule".to_string())];
        let mut m = ser.serialize_map(Some(2)).unwrap();
        for (i, s) in map.iter() {
            m.serialize_entry(&i, &s).unwrap();
        }
        let res = m.end();
        assert!(res.is_ok());
        let map = map
            .iter()
            .map(|(i, s)| (Any(AnyEnum::UInt32(*i)), Any(AnyEnum::String(s.clone()))))
            .collect();
        assert_eq!(res.unwrap(), Any(AnyEnum::Map(sig, map)));
    }

    #[test]
    fn no_lenght_success() {
        let sig = Signature::new("{Is}".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let map = vec![(0u32, "stuff".to_string()), (1, "bidule".to_string())];
        let mut m = ser.serialize_map(None).unwrap();
        for (i, s) in map.iter() {
            m.serialize_entry(&i, &s).unwrap();
        }
        let res = m.end();
        assert!(res.is_ok());
        let map = map
            .iter()
            .map(|(i, s)| (Any(AnyEnum::UInt32(*i)), Any(AnyEnum::String(s.clone()))))
            .collect();
        assert_eq!(res.unwrap(), Any(AnyEnum::Map(sig, map)));
    }

    #[test]
    fn to_any_success() {
        let mut map = std::collections::HashMap::new();
        map.insert(5u32, "stuff".to_string());
        map.insert(2u32, "bidule".to_string());
        let res = to_any(&map);
        assert!(res.is_ok());
        let any = res.unwrap();
        let expected_sig = Signature::new("{Is}".to_string()).unwrap();
        match any.0 {
            AnyEnum::Map(sig, _vec) => {
                assert_eq!(expected_sig, sig);
            }
            _ => unreachable!(),
        }
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let t = ser.serialize_map(Some(5));
        assert!(t.is_err());
    }
}

mod serialize_newtype_struct {
    use super::*;

    #[test]
    fn success() {
        let sig = Signature::new("I".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_newtype_struct("MyInt", &(5 as u32));
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::UInt32(5)));
    }

    #[derive(serde::Serialize)]
    struct MyInt(u32);

    impl GetTypeSignature for MyInt {
        fn signature() -> String {
            "I".to_string()
        }
    }

    #[test]
    fn to_any_success() {
        let val = MyInt(5);
        let res = to_any(&val);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), Any(AnyEnum::UInt32(5)));
    }

    #[test]
    fn wrong_signature() {
        let sig = Signature::new("b".to_string()).unwrap();
        let mut iter = sig.iter();
        let ser = AnySerializer::new(&mut iter);
        let res = ser.serialize_newtype_struct("MyInt", &(5 as u32));
        assert!(res.is_err());
    }
}

#[test]
fn serialize_newtype_variant() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let val = 5;
    let res = ser.serialize_newtype_variant("Enum", 5, "green", &val);
    match res {
        Err(Error::UnsupportedVariant) => {}
        _ => unreachable!(),
    }
}

#[test]
fn serialize_tuple_variant() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_tuple_variant("Enum", 5, "green", 2);
    match res {
        Err(Error::UnsupportedVariant) => {}
        _ => unreachable!(),
    }
}

#[test]
fn serialize_struct_variant() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_struct_variant("Enum", 5, "green", 2);
    match res {
        Err(Error::UnsupportedVariant) => {}
        _ => unreachable!(),
    }
}

#[test]
fn serialize_i128() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_i128(152);
    match res {
        Err(Error::UnsupportedI128) => {}
        _ => unreachable!(),
    }
}

#[test]
fn serialize_u128() {
    let sig = Signature::new("v".to_string()).unwrap();
    let mut iter = sig.iter();
    let ser = AnySerializer::new(&mut iter);
    let res = ser.serialize_u128(152);
    match res {
        Err(Error::UnsupportedU128) => {}
        _ => unreachable!(),
    }
}

// TODO serialize_tuple()
// TODO serialize_tuple_struct()
// TODO serialize_struct()
