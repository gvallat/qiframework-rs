use super::Signature;

pub trait GetTypeSignature {
    fn signature() -> String;
}

pub fn get_type_signature<T: GetTypeSignature>() -> String {
    T::signature()
}

pub fn get_value_type_signature<T: GetTypeSignature>(_val: &T) -> String {
    T::signature()
}

macro_rules! decl_basic_get_type_signature {
    ($T:ty, $sig:expr) => {
        impl GetTypeSignature for $T {
            fn signature() -> String {
                $sig.to_string()
            }
        }
    };
}

decl_basic_get_type_signature!(bool, "b");
decl_basic_get_type_signature!(i8, "c");
decl_basic_get_type_signature!(u8, "C");
decl_basic_get_type_signature!(i16, "w");
decl_basic_get_type_signature!(u16, "W");
decl_basic_get_type_signature!(i32, "i");
decl_basic_get_type_signature!(u32, "I");
decl_basic_get_type_signature!(i64, "l");
decl_basic_get_type_signature!(u64, "L");
decl_basic_get_type_signature!(f32, "f");
decl_basic_get_type_signature!(f64, "d");
decl_basic_get_type_signature!(String, "s");
decl_basic_get_type_signature!(&str, "s");
decl_basic_get_type_signature!(Signature, "s");

impl<T: GetTypeSignature> GetTypeSignature for Option<T> {
    fn signature() -> String {
        "+".to_string() + &get_type_signature::<T>()
    }
}

impl<T: GetTypeSignature> GetTypeSignature for Vec<T> {
    fn signature() -> String {
        "[".to_string() + &get_type_signature::<T>() + "]"
    }
}

impl<K, V, S> GetTypeSignature for std::collections::HashMap<K, V, S>
where
    K: GetTypeSignature + std::cmp::Eq + std::hash::Hash,
    V: GetTypeSignature,
    S: std::hash::BuildHasher,
{
    fn signature() -> String {
        "{".to_string() + &get_type_signature::<K>() + &get_type_signature::<V>() + "}"
    }
}
