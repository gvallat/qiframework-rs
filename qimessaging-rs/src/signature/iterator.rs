use crate::signature::{parser::char_to_tag, *};
use std::iter::{FusedIterator, Iterator};

#[derive(Clone, Copy, Debug)]
enum IterState {
    Tuple(usize, usize),
    Map,
    List,
}

#[derive(Clone, Debug)]
pub struct Iter<'s> {
    sig: &'s Signature,
    idx: usize,
    current_state: Vec<IterState>,
    new_struct_index: usize,
}

impl<'s> Iter<'s> {
    pub(crate) fn new(sig: &'s Signature) -> Iter<'s> {
        Iter {
            sig,
            idx: 0,
            current_state: Vec::with_capacity(sig.struct_names().len()),
            new_struct_index: 0,
        }
    }

    fn get_current_indexes(&mut self, tag: Tag) -> (Option<usize>, Option<(usize, usize)>) {
        let size = self.current_state.len();
        if size > 0 {
            if let IterState::Tuple(s_idx, f_idx) = self.current_state[size - 1] {
                if let Tag::Tuple = tag {
                    if size > 1 {
                        if let IterState::Tuple(p_s_idx, p_f_idx) = self.current_state[size - 2] {
                            self.current_state[size - 2] = IterState::Tuple(p_s_idx, p_f_idx + 1);
                            (Some(s_idx), Some((p_s_idx, p_f_idx)))
                        } else {
                            (Some(s_idx), None)
                        }
                    } else {
                        (Some(s_idx), None)
                    }
                } else if let Tag::TupleEnd = tag {
                    (None, None)
                } else {
                    self.current_state[size - 1] = IterState::Tuple(s_idx, f_idx + 1);
                    (None, Some((s_idx, f_idx)))
                }
            } else {
                (None, None)
            }
        } else {
            (None, None)
        }
    }
}

impl<'s> FusedIterator for Iter<'s> {}

impl<'s> Iterator for Iter<'s> {
    type Item = IterItem<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.idx == self.sig.simplified_string().len() {
            return None;
        }

        let index = self.idx;
        let tag = char_to_tag(self.sig.simplified_string().chars().nth(self.idx).unwrap());
        self.idx += 1;

        match tag {
            Tag::Tuple => {
                self.current_state
                    .push(IterState::Tuple(self.new_struct_index, 0));
                self.new_struct_index += 1;
            }
            Tag::TupleEnd => {
                self.current_state.pop();
            }
            _ => {}
        }

        let (struct_index, field_index) = self.get_current_indexes(tag);

        match tag {
            Tag::Map => self.current_state.push(IterState::Map),
            Tag::List => self.current_state.push(IterState::List),
            Tag::MapEnd | Tag::ListEnd => {
                self.current_state.pop();
            }
            _ => {}
        }

        Some(IterItem {
            sig: self.sig,
            tag,
            index,
            struct_index,
            field_index,
        })
    }
}

#[derive(Clone)]
pub struct IterItem<'s> {
    sig: &'s Signature,
    tag: Tag,
    index: usize,
    struct_index: Option<usize>,
    field_index: Option<(usize, usize)>,
}

impl<'s> IterItem<'s> {
    pub fn tag(&self) -> Tag {
        self.tag
    }

    pub fn index(&self) -> usize {
        self.index
    }

    pub fn tag_char(&self) -> char {
        self.sig
            .simplified_string()
            .chars()
            .nth(self.index)
            .unwrap()
    }

    pub fn struct_name(&self) -> &str {
        match self.struct_index {
            Some(idx) => {
                if idx < self.sig.struct_names().len() {
                    &self.sig.struct_names()[idx].name
                } else {
                    ""
                }
            }
            None => "",
        }
    }

    pub fn field_names(&self) -> Option<&'s [&'s str]> {
        match self.struct_index {
            Some(idx) => {
                if idx < self.sig.struct_names().len() {
                    Some(&self.sig.struct_names()[idx].field_names)
                } else {
                    None
                }
            }
            None => None,
        }
    }

    pub fn field_name(&self) -> &'s str {
        match self.field_index {
            Some((struct_idx, idx)) => {
                if struct_idx < self.sig.struct_names().len() {
                    let names = &self.sig.struct_names()[struct_idx];
                    if idx < names.field_names.len() {
                        &names.field_names[idx]
                    } else {
                        ""
                    }
                } else {
                    ""
                }
            }
            None => "",
        }
    }

    pub fn sub_signature(&self) -> Option<Signature> {
        match find_sub_signature_indexes(self.sig, self.index) {
            None => None,
            Some((r1, r2, r3)) => Some(Signature {
                inner: self.sig.inner.clone(),
                full_str: &self.sig.full_str[r1],
                simplified_str: &self.sig.simplified_str[r2],
                struct_names: &self.sig.struct_names[r3],
            }),
        }
    }
}

type Range = std::ops::Range<usize>;

#[allow(clippy::range_plus_one)]
fn find_sub_signature_indexes(sig: &Signature, sig_idx: usize) -> Option<(Range, Range, Range)> {
    let tag = char_to_tag(sig.simplified_string().chars().nth(sig_idx).unwrap());
    match tag {
        Tag::ListEnd | Tag::MapEnd | Tag::TupleEnd => None,
        Tag::List | Tag::Map | Tag::Tuple => {
            let full_begin = find_full_string_begin(sig, sig_idx);
            let (sig_end, tuple_count) = find_simplified_string_end(sig, sig_idx);
            let full_end = find_full_string_end(sig, full_begin);

            Some((
                (full_begin..full_end + 1),
                (sig_idx..sig_end + 1),
                (0..tuple_count),
            ))
        }
        Tag::Optional => {
            let (mut r1, mut r2, r3) = find_sub_signature_indexes(sig, sig_idx + 1).unwrap();
            r1.start -= 1;
            r2.start -= 1;
            Some((r1, r2, r3))
        }

        Tag::None
        | Tag::Unit
        | Tag::Bool
        | Tag::Int8
        | Tag::UInt8
        | Tag::Int16
        | Tag::UInt16
        | Tag::Int32
        | Tag::UInt32
        | Tag::Int64
        | Tag::UInt64
        | Tag::Float
        | Tag::Double
        | Tag::String
        | Tag::Raw
        | Tag::Object
        | Tag::Unknown
        | Tag::Dynamic => {
            let idx = find_full_string_begin(sig, sig_idx);
            Some(((idx..idx + 1), (sig_idx..sig_idx + 1), (0..0)))
        }
    }
}

fn find_full_string_begin(sig: &Signature, simplified_idx: usize) -> usize {
    let mut idx = 0;
    let mut counted = 0;
    let full_str = sig.full_string().as_bytes();
    loop {
        if full_str[idx] == b'<' {
            // consume stucture names
            let mut opened = 0;
            loop {
                idx += 1;
                if full_str[idx] == b'<' {
                    opened += 1;
                } else if full_str[idx] == b'>' && opened > 0 {
                    opened -= 1;
                } else if full_str[idx] == b'>' {
                    break;
                }
            }
        }

        if counted == simplified_idx {
            break;
        }
        counted += 1;
        idx += 1;
    }
    idx
}

fn find_simplified_string_end(sig: &Signature, simplified_idx: usize) -> (usize, usize) {
    let mut idx = simplified_idx;
    let mut tuple_count = 0;
    let mut opened = 0;
    let full_str = sig.simplified_string().as_bytes();
    loop {
        if full_str[idx] == b'(' {
            tuple_count += 1;
        }
        if b"([{<".iter().any(|c| *c == full_str[idx]) {
            opened += 1;
        } else if b")]}>".iter().any(|c| *c == full_str[idx]) {
            opened -= 1;
        }

        if opened == 0 {
            break;
        }
        idx += 1;
    }
    (idx, tuple_count)
}

fn find_full_string_end(sig: &Signature, idx_begin: usize) -> usize {
    let mut idx = idx_begin;
    let mut type_opened = 0;
    let full_str = sig.full_string().as_bytes();
    loop {
        if b"([{".iter().any(|c| *c == full_str[idx]) {
            type_opened += 1;
        } else if b")]}".iter().any(|c| *c == full_str[idx]) {
            type_opened -= 1;
        }

        if full_str[idx] == b'<' {
            // consume stucture names
            let mut opened = 0;
            loop {
                idx += 1;
                if full_str[idx] == b'<' {
                    opened += 1;
                } else if full_str[idx] == b'>' && opened > 0 {
                    opened -= 1;
                } else if full_str[idx] == b'>' {
                    break;
                }
            }
        }

        if type_opened == 0 {
            break;
        }
        idx += 1;
    }

    // consume following stucture names
    if idx + 1 < full_str.len() && full_str[idx + 1] == b'<' {
        idx += 1;
        let mut opened = 0;
        loop {
            idx += 1;
            if full_str[idx] == b'<' {
                opened += 1;
            } else if full_str[idx] == b'>' && opened > 0 {
                opened -= 1;
            } else if full_str[idx] == b'>' {
                break;
            }
        }
    }
    idx
}
