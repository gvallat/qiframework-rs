use super::*;

pub fn char_to_tag(c: char) -> Tag {
    match c {
        '_' => Tag::None,
        'v' => Tag::Unit,
        'b' => Tag::Bool,
        'c' => Tag::Int8,
        'C' => Tag::UInt8,
        'w' => Tag::Int16,
        'W' => Tag::UInt16,
        'i' => Tag::Int32,
        'I' => Tag::UInt32,
        'l' => Tag::Int64,
        'L' => Tag::UInt64,
        'f' => Tag::Float,
        'd' => Tag::Double,
        's' => Tag::String,
        '[' => Tag::List,
        ']' => Tag::ListEnd,
        '{' => Tag::Map,
        '}' => Tag::MapEnd,
        '(' => Tag::Tuple,
        ')' => Tag::TupleEnd,
        'm' => Tag::Dynamic,
        'r' => Tag::Raw,
        'o' => Tag::Object,
        '+' => Tag::Optional,
        'X' | _ => Tag::Unknown,
        // '*' => Tag::Pointer,
        // '#' => Tag::VarArgs,
        // '~' => Tag::KwArgs,
    }
}

pub fn is_valid_tag_char(c: char) -> bool {
    const VALID: &str = "_bcCvwWiIlLfds[]{}()mro+X"; // *#~
    VALID.chars().any(|v| c == v)
}

pub fn parse(raw: String) -> std::io::Result<Signature> {
    let full_str = raw;
    let mut simplified_str = String::with_capacity(full_str.len());
    let mut names = Vec::new();

    let mut iter = full_str.chars();
    read_element(&mut iter, &mut simplified_str, &mut names)?;

    if iter.next().is_some() {
        return error("Signature: Unparsed data.");
    }

    // Safety: moving the full_str will not invalidate the references (heap data doesn't move)
    // public APIs will cast the lifetime back to the true lifetime
    let struct_names = names
        .into_iter()
        .map(|names| unsafe { std::mem::transmute::<StructNames<'_>, StructNames<'static>>(names) })
        .collect();

    let inner = std::sync::Arc::new(SignatureData {
        full_str,
        simplified_str,
        struct_names,
    });
    let full_str = unsafe { std::mem::transmute::<&'_ str, &'static str>(&inner.full_str[..]) };
    let simplified_str =
        unsafe { std::mem::transmute::<&'_ str, &'static str>(&inner.simplified_str[..]) };
    let struct_names = unsafe {
        std::mem::transmute::<&'_ [StructNames<'static>], &'static [StructNames<'static>]>(
            &inner.struct_names[..],
        )
    };

    Ok(Signature {
        inner,
        full_str,
        simplified_str,
        struct_names,
    })
}

#[inline(always)]
fn error<T>(text: &str) -> std::io::Result<T> {
    Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, text))
}

#[inline(always)]
fn error_eof<T>() -> std::io::Result<T> {
    error("Signature: Unexpected end.")
}

#[inline(always)]
fn error_invalid(c: char) -> std::io::Result<()> {
    Err(std::io::Error::new(
        std::io::ErrorKind::InvalidInput,
        format!("Signature: Invalid tag '{}'.", c),
    ))
}

fn read_element<'a>(
    iter: &mut std::str::Chars<'a>,
    sig: &mut String,
    names: &mut Vec<StructNames<'a>>,
) -> std::io::Result<()> {
    match iter.next() {
        None => error_eof(),
        Some(c) => {
            if !is_valid_tag_char(c) {
                return error_invalid(c);
            }
            sig.push(c);
            let tag = char_to_tag(c);
            match tag {
                Tag::None | Tag::Unknown => Ok(()),

                Tag::Unit
                | Tag::Bool
                | Tag::Int8
                | Tag::UInt8
                | Tag::Int16
                | Tag::UInt16
                | Tag::Int32
                | Tag::UInt32
                | Tag::Int64
                | Tag::UInt64
                | Tag::Float
                | Tag::Double
                | Tag::String
                | Tag::Dynamic
                | Tag::Raw => Ok(()),

                Tag::List => read_list(iter, sig, names),
                Tag::Map => read_map(iter, sig, names),
                Tag::Tuple => read_tuple(iter, sig, names),

                Tag::ListEnd => error("Signature: Unexpected end list."),
                Tag::MapEnd => error("Signature: Unexpected end map."),
                Tag::TupleEnd => error("Signature: Unexpected end tuple."),

                Tag::Optional => read_element(iter, sig, names),

                Tag::Object => Ok(()),
            }
        }
    }
}

fn read_list<'a>(
    iter: &mut std::str::Chars<'a>,
    sig: &mut String,
    names: &mut Vec<StructNames<'a>>,
) -> std::io::Result<()> {
    read_element(iter, sig, names)?;
    match iter.next() {
        None => error_eof(),
        Some(c) => {
            if !is_valid_tag_char(c) {
                return error_invalid(c);
            }
            sig.push(c);
            let tag = char_to_tag(c);
            match tag {
                Tag::ListEnd => Ok(()),
                _ => error("Signature: Expected end list."),
            }
        }
    }
}

fn read_map<'a>(
    iter: &mut std::str::Chars<'a>,
    sig: &mut String,
    names: &mut Vec<StructNames<'a>>,
) -> std::io::Result<()> {
    read_element(iter, sig, names)?;
    read_element(iter, sig, names)?;
    match iter.next() {
        None => error_eof(),
        Some(c) => {
            if !is_valid_tag_char(c) {
                return error_invalid(c);
            }
            sig.push(c);
            let tag = char_to_tag(c);
            match tag {
                Tag::MapEnd => Ok(()),
                _ => error("Signature: Expected end map."),
            }
        }
    }
}

fn read_tuple<'a>(
    iter: &mut std::str::Chars<'a>,
    sig: &mut String,
    names: &mut Vec<StructNames<'a>>,
) -> std::io::Result<()> {
    let idx = names.len();
    names.push(StructNames {
        name: "",
        field_names: Vec::new(),
    });

    // read until tuple end
    let mut count = 0;
    let mut iter_2 = iter.clone();
    loop {
        match iter_2.next() {
            None => return error_eof(),
            Some(c) => {
                if !is_valid_tag_char(c) {
                    return error_invalid(c);
                }
                let tag = char_to_tag(c);
                match tag {
                    Tag::TupleEnd => {
                        sig.push(c);
                        *iter = iter_2.clone();
                        break;
                    }
                    _ => {
                        read_element(iter, sig, names)?;
                        count += 1;
                        iter_2 = iter.clone();
                    }
                }
            }
        }
    }

    // try read annotation
    if let Some(c) = iter_2.next() {
        if c == '<' {
            read_names(&mut iter_2, count, &mut names[idx])?;
            *iter = iter_2;
        }
    }

    Ok(())
}

fn is_valid_name_char(c: char) -> bool {
    c.is_ascii_alphanumeric() || c == '_'
}

#[inline(always)]
fn error_invalid_char(c: char) -> std::io::Result<()> {
    Err(std::io::Error::new(
        std::io::ErrorKind::InvalidInput,
        format!("Signature: Invalid name charater '{}'.", c),
    ))
}

fn read_names<'a>(
    iter: &mut std::str::Chars<'a>,
    count: usize,
    struct_name: &mut StructNames<'a>,
) -> std::io::Result<()> {
    // read struct name
    let mut len = 0;
    let mut bracket_count = 0;
    let mut base_iter = iter.clone();
    let mut with_names = true;
    struct_name.name = loop {
        match iter.next() {
            None => return error_eof(),
            Some(c) => match c {
                '<' => {
                    len += 1;
                    bracket_count += 1;
                }
                '>' if bracket_count > 0 => {
                    len += 1;
                    bracket_count -= 1;
                }
                ',' if bracket_count == 0 => {
                    break &base_iter.as_str()[..len];
                }
                '>' if bracket_count == 0 => {
                    with_names = false;
                    break &base_iter.as_str()[..len];
                }
                _ => {
                    if is_valid_name_char(c) {
                        len += 1;
                    } else {
                        return error_invalid_char(c);
                    }
                }
            },
        }
    };

    let mut names = Vec::with_capacity(count);
    if with_names {
        len = 0;
        base_iter = iter.clone();
        loop {
            match iter.next() {
                None => return error_eof(),
                Some(c) => match c {
                    '>' => {
                        names.push(&base_iter.as_str()[..len]);
                        break;
                    }
                    ',' => {
                        names.push(&base_iter.as_str()[..len]);
                        len = 0;
                        base_iter = iter.clone();
                    }
                    _ => {
                        if is_valid_name_char(c) {
                            len += 1;
                        } else {
                            return error_invalid_char(c);
                        }
                    }
                },
            }
        }
    }

    if names.len() == count {
        struct_name.field_names = names;
        Ok(())
    } else {
        Err(std::io::Error::new(
            std::io::ErrorKind::InvalidInput,
            format!("Signature: Unexpected count of names '{}'.", names.len()),
        ))
    }
}
