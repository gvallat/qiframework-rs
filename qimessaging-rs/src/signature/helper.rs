use super::{iterator::Iter, Tag};

// assumed to be an iterator on a valid signature
pub fn consume_next_element(iter: &mut Iter<'_>) {
    match iter.next() {
        None => unreachable!(),
        Some(item) => match item.tag() {
            Tag::None | Tag::Unknown => {}

            Tag::Unit
            | Tag::Bool
            | Tag::Int8
            | Tag::UInt8
            | Tag::Int16
            | Tag::UInt16
            | Tag::Int32
            | Tag::UInt32
            | Tag::Int64
            | Tag::UInt64
            | Tag::Float
            | Tag::Double
            | Tag::String
            | Tag::Dynamic
            | Tag::Raw => {}

            Tag::List => consume_next_list(iter),
            Tag::Map => consume_next_map(iter),
            Tag::Tuple => consume_next_tuple(iter),

            Tag::ListEnd => unreachable!(),
            Tag::MapEnd => unreachable!(),
            Tag::TupleEnd => unreachable!(),

            Tag::Optional => consume_next_element(iter),

            Tag::Object => unimplemented!(),
        },
    }
}

fn consume_next_list(iter: &mut Iter<'_>) {
    consume_next_element(iter);
    let next = iter.next();
    match next {
        None => unreachable!(),
        Some(item) => match item.tag() {
            Tag::ListEnd => {}
            _ => unreachable!(),
        },
    }
}

fn consume_next_map(iter: &mut Iter<'_>) {
    consume_next_element(iter);
    consume_next_element(iter);
    let next = iter.next();
    match next {
        None => unreachable!(),
        Some(item) => match item.tag() {
            Tag::MapEnd => {}
            _ => unreachable!(),
        },
    }
}

fn consume_next_tuple(iter: &mut Iter<'_>) {
    let mut iter_2 = iter.clone();
    loop {
        match iter_2.next() {
            None => unreachable!(),
            Some(item) => match item.tag() {
                Tag::TupleEnd => {
                    *iter = iter_2;
                    break;
                }
                Tag::ListEnd | Tag::MapEnd => unreachable!(),
                _ => {
                    consume_next_element(iter);
                    iter_2 = iter.clone();
                }
            },
        }
    }
}

pub fn next_element_string_signature(iter: Iter<'_>) -> (String, Iter<'_>) {
    let mut end_iter = iter.clone();
    consume_next_element(&mut end_iter);
    let mut sig_str = String::new();
    match end_iter.clone().next() {
        None => {
            for item in iter {
                sig_str.push(item.tag_char());
            }
        }
        Some(end_item) => {
            let end_index = end_item.index();
            for item in iter {
                if item.index() >= end_index {
                    break;
                }
                sig_str.push(item.tag_char());
            }
        }
    }
    (sig_str, end_iter)
}
