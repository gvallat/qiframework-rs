use std::net::{SocketAddr, ToSocketAddrs};
use std::str::FromStr;

bitflags::bitflags! {
    #[derive(Default)]
    struct UrlComponent: u8 {
        const SCHEME = 0b0000_0001;
        const HOST = 0b0000_0010;
        const PORT = 0b0000_0100;
    }
}

/// Supported `Url` scheme
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum UrlScheme {
    /// Connection over TCP without encryption
    Tcp,
    /// Connection over TCP with TLS encryption
    Tcps,
}

const DEFAULT_HOST: &str = "127.0.0.1";

/// Simple url struct, good enough for our usage
///
/// Url can parse the following formats:
/// - scheme://host:port
/// - scheme://host
/// - host:port
/// - host
/// - scheme://:port
/// - scheme://
/// - :port
/// - *empty string*
///
/// Only support 'tcp' and 'tcps' as schemes.
///
/// **Warning:** The class isn't compliant with RFC 3986.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Url {
    component: UrlComponent,
    scheme: UrlScheme,
    host: String,
    port: u16,
}

impl std::fmt::Display for Url {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        let scheme = match &self.scheme {
            UrlScheme::Tcp => "tcp",
            UrlScheme::Tcps => "tcps",
        };
        write!(f, "{}://{}:{}", scheme, self.host, self.port)
    }
}

impl ToSocketAddrs for Url {
    type Iter = std::vec::IntoIter<SocketAddr>;
    fn to_socket_addrs(&self) -> std::io::Result<Self::Iter> {
        (&self.host[..], self.port).to_socket_addrs()
    }
}

impl Default for Url {
    fn default() -> Url {
        Url::new()
    }
}

impl Url {
    /// create a default Url: "tcp://127.0.0.1:9559"
    pub fn new() -> Url {
        Url {
            component: UrlComponent::default(),
            scheme: UrlScheme::Tcp,
            host: DEFAULT_HOST.to_string(),
            port: 9559,
        }
    }

    /// Returns an `Url` builder
    pub fn builder() -> UrlBuilder {
        UrlBuilder { url: Url::new() }
    }

    /// checks if the scheme use TLS encryption (`true` if the scheme is 'tcps')
    pub fn use_tls(&self) -> bool {
        match self.scheme {
            UrlScheme::Tcp => false,
            UrlScheme::Tcps => true,
        }
    }

    /// Checks the scheme has been explicitly set
    pub fn has_scheme(&self) -> bool {
        self.component.contains(UrlComponent::SCHEME)
    }

    /// Checks the host has been explicitly set
    pub fn has_host(&self) -> bool {
        self.component.contains(UrlComponent::HOST)
    }

    /// Checks the port has been explicitly set
    pub fn has_port(&self) -> bool {
        self.component.contains(UrlComponent::PORT)
    }

    /// Returns the scheme
    pub fn scheme(&self) -> &UrlScheme {
        &self.scheme
    }

    /// Returns the host
    pub fn host(&self) -> &String {
        &self.host
    }

    /// Returns the port
    pub fn port(&self) -> u16 {
        self.port
    }

    /// Set the scheme
    pub fn set_scheme(&mut self, scheme: UrlScheme) {
        self.scheme = scheme;
        self.component.set(UrlComponent::SCHEME, true);
    }

    /// Set the host
    pub fn set_host(&mut self, host: &str) {
        self.host = host.to_string();
        self.component.set(UrlComponent::HOST, true);
    }

    /// Set the port
    pub fn set_port(&mut self, port: u16) {
        self.port = port;
        self.component.set(UrlComponent::PORT, true);
    }

    // tokio::ToSocketAddrs is not implementable (sealed)
    // but is implemented for (&str, u16)
    /// Returns a tuple that can be use as Into<tokio::ToSocketAddrs>
    pub fn as_socket_addrs_base(&self) -> (&str, u16) {
        (&self.host, self.port)
    }
}

impl FromStr for Url {
    type Err = String;

    fn from_str(mut s: &str) -> Result<Self, Self::Err> {
        let mut component = UrlComponent::default();
        let mut iter = s.match_indices("://");
        let scheme = if let Some((pos, _)) = iter.next() {
            let scheme = &s[0..pos];
            component |= UrlComponent::SCHEME;
            s = &s[pos + 3..];
            match scheme {
                "tcp" => UrlScheme::Tcp,
                "tcps" => UrlScheme::Tcps,
                _ => return Err("Unsupported scheme (tcp, tcps)".to_string()),
            }
        } else {
            UrlScheme::Tcp
        };

        if iter.next().is_some() {
            return Err("Multiple scheme delimitation".to_string());
        }

        let mut iter = s.split(':');
        let host = if let Some(host) = iter.next() {
            if !host.is_empty() {
                component |= UrlComponent::HOST;
                host.to_string()
            } else {
                DEFAULT_HOST.to_string()
            }
        } else {
            DEFAULT_HOST.to_string()
        };

        let port = if let Some(port) = iter.next() {
            component |= UrlComponent::PORT;
            match port.parse() {
                Ok(port) => port,
                Err(e) => return Err(format!("Could not parse port: {}", e)),
            }
        } else {
            9559
        };

        if iter.next().is_some() {
            return Err("Multiple port delimitation".to_string());
        }

        Ok(Url {
            component,
            scheme,
            host,
            port,
        })
    }
}

/// An `Url` builder
pub struct UrlBuilder {
    url: Url,
}

impl UrlBuilder {
    /// Set the scheme
    pub fn scheme(mut self, scheme: UrlScheme) -> Self {
        self.url.set_scheme(scheme);
        self
    }

    /// Set the host
    pub fn host(mut self, host: &str) -> Self {
        self.url.set_host(host);
        self
    }

    /// Set the port
    pub fn port(mut self, port: u16) -> Self {
        self.url.set_port(port);
        self
    }

    /// Returns the builded url
    pub fn url(self) -> Url {
        self.url
    }
}

impl From<UrlBuilder> for Url {
    fn from(builder: UrlBuilder) -> Self {
        builder.url()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_scheme_host_port() {
        let url: Url = "tcps://host.my:563".parse().unwrap();
        assert!(url.has_scheme());
        assert!(url.has_host());
        assert!(url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcps);
        assert_eq!(&url.host()[..], "host.my");
        assert_eq!(url.port(), 563);
    }

    #[test]
    fn test_parse_scheme_host() {
        let url: Url = "tcps://host.my".parse().unwrap();
        assert!(url.has_scheme());
        assert!(url.has_host());
        assert!(!url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcps);
        assert_eq!(&url.host()[..], "host.my");
        assert_eq!(url.port(), 9559);
    }

    #[test]
    fn test_parse_host_port() {
        let url: Url = "host.my:563".parse().unwrap();
        assert!(!url.has_scheme());
        assert!(url.has_host());
        assert!(url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcp);
        assert_eq!(&url.host()[..], "host.my");
        assert_eq!(url.port(), 563);
    }

    #[test]
    fn test_parse_host() {
        let url: Url = "host.my".parse().unwrap();
        assert!(!url.has_scheme());
        assert!(url.has_host());
        assert!(!url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcp);
        assert_eq!(&url.host()[..], "host.my");
        assert_eq!(url.port(), 9559);
    }

    #[test]
    fn test_parse_scheme_port() {
        let url: Url = "tcps://:563".parse().unwrap();
        assert!(url.has_scheme());
        assert!(!url.has_host());
        assert!(url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcps);
        assert_eq!(&url.host()[..], DEFAULT_HOST);
        assert_eq!(url.port(), 563);
    }

    #[test]
    fn test_parse_scheme() {
        let url: Url = "tcps://".parse().unwrap();
        assert!(url.has_scheme());
        assert!(!url.has_host());
        assert!(!url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcps);
        assert_eq!(&url.host()[..], DEFAULT_HOST);
        assert_eq!(url.port(), 9559);
    }

    #[test]
    fn test_parse_port() {
        let url: Url = ":563".parse().unwrap();
        assert!(!url.has_scheme());
        assert!(!url.has_host());
        assert!(url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcp);
        assert_eq!(&url.host()[..], DEFAULT_HOST);
        assert_eq!(url.port(), 563);
    }

    #[test]
    fn test_parse_empty() {
        let url: Url = "".parse().unwrap();
        assert!(!url.has_scheme());
        assert!(!url.has_host());
        assert!(!url.has_port());
        assert_eq!(url.scheme(), &UrlScheme::Tcp);
        assert_eq!(&url.host()[..], DEFAULT_HOST);
        assert_eq!(url.port(), 9559);
    }

    #[test]
    fn test_parse_known_schemes() {
        let url: Url = "tcp://".parse().unwrap();
        assert!(url.has_scheme());
        assert_eq!(url.scheme(), &UrlScheme::Tcp);

        let url: Url = "tcps://".parse().unwrap();
        assert!(url.has_scheme());
        assert_eq!(url.scheme(), &UrlScheme::Tcps);
    }

    use proptest::prelude::*;

    fn valid_option_scheme_strategy() -> impl Strategy<Value = Option<String>> {
        prop_oneof![
            Just(None),
            Just(Some("tcp".to_string())),
            Just(Some("tcps".to_string()))
        ]
    }

    fn valid_host_strategy() -> impl Strategy<Value = String> {
        // valid url label "[a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?"
        // valid host is a dot separated list of labels
        // valid host should be no more than 255 characters
        "[a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(\\.[a-zA-Z0-9]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?){0,4}"
    }

    fn valid_option_host_strategy() -> impl Strategy<Value = Option<String>> {
        prop_oneof![Just(None), valid_host_strategy().prop_map(Some)]
    }

    fn valid_port_strategy() -> impl Strategy<Value = u16> {
        1u16..u16::max_value()
    }

    fn valid_option_port_strategy() -> impl Strategy<Value = Option<u16>> {
        prop_oneof![Just(None), valid_port_strategy().prop_map(Some)]
    }

    proptest! {
        #[test]
        fn doesnt_crash(url_str in "\\PC*") {
            let _res = url_str.parse::<Url>();
        }

        #[test]
        fn valid_url(
            scheme in valid_option_scheme_strategy(),
            host in valid_option_host_strategy(),
            port in valid_option_port_strategy()) {

            let url_str = match (&scheme, &host, &port) {
                (Some(scheme), Some(host), Some(port)) => format!("{}://{}:{}", scheme, host, port),
                (Some(scheme), Some(host), None) => format!("{}://{}", scheme, host),
                (Some(scheme), None, Some(port)) => format!("{}://:{}", scheme, port),
                (Some(scheme), None, None) => format!("{}://", scheme),
                (None, Some(host), Some(port)) => format!("{}:{}", host, port),
                (None, Some(host), None) => host.clone(),
                (None, None, Some(port)) => format!(":{}", port),
                (None, None, None) => String::new(),
            };

            let res = url_str.parse::<Url>();
            assert!(res.is_ok());
            let url = res.unwrap();

            if let Some(scheme) = scheme {
                assert!(url.has_scheme());
                if &scheme[..] == "tcp" {
                    assert_eq!(UrlScheme::Tcp, *url.scheme());
                } else if &scheme[..] == "tcps" {
                    assert_eq!(UrlScheme::Tcps, *url.scheme());
                } else {
                    unreachable!()
                }
            } else {
                assert!(!url.has_scheme());
                assert_eq!(UrlScheme::Tcp, *url.scheme());
            }

            if let Some(host) = host {
                assert!(url.has_host());
                assert_eq!(host, *url.host());
            } else {
                assert!(!url.has_host());
                assert_eq!(DEFAULT_HOST, &url.host()[..]);
            }

            if let Some(port) = port {
                assert!(url.has_port());
                assert_eq!(port, url.port());
            } else {
                assert!(!url.has_port());
                assert_eq!(9559, url.port());
            }
        }
    }
} // mod tests
