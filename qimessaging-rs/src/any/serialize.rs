use {
    crate::{
        any::{Any, AnyEnum},
        serialization::{
            serializer::{error_doesnt_fit_u32, write_bytes_array},
            Serialize, Serializer,
        },
        signature::Tag,
    },
    byteorder::{LittleEndian, WriteBytesExt},
    serde::ser::{SerializeMap, SerializeSeq, SerializeStruct, SerializeTuple},
    std::io::{Cursor, Result},
};

// TODO AnyWrap<T: Serialize + GetTypeSignature>(T) => serialize as if converted in Any

impl Serialize for Any {
    fn serialize_into(&self, serializer: &mut Serializer<'_, '_>) -> Result<()> {
        serializer.check_tag(Tag::Dynamic)?;
        let sig = self.signature();
        write_bytes_array(serializer.buf_mut(), sig.as_bytes())?;
        serialize_any(self, serializer.buf_mut())
    }
}

#[inline(always)]
fn error_str(text: &str) -> std::io::Result<()> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, text))
}

pub fn serialize_any(any: &Any, buf: &mut Cursor<Vec<u8>>) -> Result<()> {
    match &any.0 {
        AnyEnum::None => error_str("Any: can't serialize None"),
        AnyEnum::Unit => Ok(()),
        AnyEnum::Boolean(val) => {
            if *val {
                buf.write_u8(1)
            } else {
                buf.write_u8(0)
            }
        }
        AnyEnum::Int8(val) => buf.write_i8(*val),
        AnyEnum::UInt8(val) => buf.write_u8(*val),
        AnyEnum::Int16(val) => buf.write_i16::<LittleEndian>(*val),
        AnyEnum::UInt16(val) => buf.write_u16::<LittleEndian>(*val),
        AnyEnum::Int32(val) => buf.write_i32::<LittleEndian>(*val),
        AnyEnum::UInt32(val) => buf.write_u32::<LittleEndian>(*val),
        AnyEnum::Int64(val) => buf.write_i64::<LittleEndian>(*val),
        AnyEnum::UInt64(val) => buf.write_u64::<LittleEndian>(*val),
        AnyEnum::Float(val) => buf.write_f32::<LittleEndian>(*val),
        AnyEnum::Double(val) => buf.write_f64::<LittleEndian>(*val),
        AnyEnum::String(val) => write_bytes_array(buf, val.as_bytes()),
        AnyEnum::List(_, vec) => {
            if vec.len() > u32::max_value() as usize {
                return error_doesnt_fit_u32(vec.len());
            }
            buf.write_u32::<LittleEndian>(vec.len() as u32)?;
            for val in vec {
                serialize_any(val, buf)?;
            }
            Ok(())
        }
        AnyEnum::Map(_, vec) => {
            if vec.len() > u32::max_value() as usize {
                return error_doesnt_fit_u32(vec.len());
            }
            buf.write_u32::<LittleEndian>(vec.len() as u32)?;
            for (key, val) in vec {
                serialize_any(key, buf)?;
                serialize_any(val, buf)?;
            }
            Ok(())
        }
        AnyEnum::Tuple(_, vec) => {
            for val in vec {
                serialize_any(val, buf)?;
            }
            Ok(())
        }
        AnyEnum::Dynamic(val) => {
            let sig = val.signature();
            write_bytes_array(buf, sig.as_bytes())?;
            serialize_any(val, buf)
        }
        AnyEnum::Raw(val) => write_bytes_array(buf, &val[..]),
        AnyEnum::Optional(_, val) => match val {
            Some(val) => {
                buf.write_u8(1)?;
                serialize_any(val, buf)
            }
            None => buf.write_u8(0),
        },
        AnyEnum::Object => unimplemented!(),
        AnyEnum::Unknown => error_str("Any: can't serialize Unknown"),
    }
}

impl serde::Serialize for Any {
    fn serialize<S: serde::Serializer>(
        &self,
        serializer: S,
    ) -> std::result::Result<S::Ok, S::Error> {
        if crate::serde::is_serializing_to_any() {
            let s = serializer.serialize_struct(crate::serde::ANYVALUE_TOKEN, 2)?;
            crate::serde::SERIALIZE_ANY.with(|a| *a.borrow_mut() = Some(self.clone()));
            s.end()
        } else {
            let mut s = if crate::serde::is_serializing_to_binary() {
                serializer.serialize_struct(crate::serde::ANYVALUE_TOKEN, 2)?
            } else {
                serializer.serialize_struct("Any", 2)?
            };
            let sig = self.signature();
            s.serialize_field("signature", &sig)?;
            s.serialize_field("data", &SerializeAny(self))?;
            s.end()
        }
    }
}

struct SerializeAny<'a>(&'a Any);
impl<'a> serde::Serialize for SerializeAny<'a> {
    fn serialize<S: serde::Serializer>(&self, ser: S) -> std::result::Result<S::Ok, S::Error> {
        _serialize_any(&self.0, ser)
    }
}

pub(crate) fn _serialize_any<S: serde::Serializer>(
    any: &Any,
    ser: S,
) -> std::result::Result<S::Ok, S::Error> {
    use serde::ser::Error;
    match &any.0 {
        AnyEnum::None => ser.serialize_unit(),
        AnyEnum::Unit => ser.serialize_unit(),
        AnyEnum::Boolean(val) => ser.serialize_bool(*val),
        AnyEnum::Int8(val) => ser.serialize_i8(*val),
        AnyEnum::UInt8(val) => ser.serialize_u8(*val),
        AnyEnum::Int16(val) => ser.serialize_i16(*val),
        AnyEnum::UInt16(val) => ser.serialize_u16(*val),
        AnyEnum::Int32(val) => ser.serialize_i32(*val),
        AnyEnum::UInt32(val) => ser.serialize_u32(*val),
        AnyEnum::Int64(val) => ser.serialize_i64(*val),
        AnyEnum::UInt64(val) => ser.serialize_u64(*val),
        AnyEnum::Float(val) => ser.serialize_f32(*val),
        AnyEnum::Double(val) => ser.serialize_f64(*val),
        AnyEnum::String(val) => ser.serialize_str(val.as_str()),
        AnyEnum::List(_, vec) => {
            let mut list = ser.serialize_seq(Some(vec.len()))?;
            for val in vec {
                list.serialize_element(&SerializeAny(val))?;
            }
            list.end()
        }
        AnyEnum::Map(_, vec) => {
            let mut map = ser.serialize_map(Some(vec.len()))?;
            for (key, val) in vec {
                map.serialize_entry(&SerializeAny(key), &SerializeAny(val))?;
            }
            map.end()
        }
        AnyEnum::Tuple(_, vec) => {
            // TODO follow constraining signature if needed
            let mut tuple = ser.serialize_tuple(vec.len())?;
            for val in vec {
                tuple.serialize_element(&SerializeAny(val))?;
            }
            tuple.end()
        }
        AnyEnum::Dynamic(val) => {
            let mut s = ser.serialize_struct("Any", 2)?;
            let sig = val.signature();
            s.serialize_field("signature", &sig)?;
            s.serialize_field("data", &SerializeAny(val))?;
            s.end()
        }
        AnyEnum::Raw(val) => ser.serialize_bytes(&val[..]),
        AnyEnum::Optional(_, val) => match val {
            Some(val) => ser.serialize_some(&SerializeAny(val)),
            None => ser.serialize_none(),
        },
        AnyEnum::Object => unimplemented!(),
        AnyEnum::Unknown => Err(S::Error::custom("Any: can't serialize Unknown")),
    }
}
