use crate::{
    any::{Any, AnyEnum},
    buffer::Buffer,
    signature::GetTypeSignature,
};

// TODO 'From<_> for Any' for basic types
// TODO 'TryFrom<Any> for _' for basic types

pub trait IntoAny {
    fn into_any(self) -> Any;
}

#[derive(Debug)]
pub struct InvalidAnyConversion;

impl std::fmt::Display for InvalidAnyConversion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "invalid any conversion")
    }
}

impl std::error::Error for InvalidAnyConversion {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

pub trait TryFromAny: Sized {
    fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion>;
}

macro_rules! decl_basic_conversion {
    ($t:ty, $from:ident, $name:ident) => {
        impl IntoAny for $t {
            fn into_any(self) -> Any {
                Any::$from(self)
            }
        }

        impl TryFromAny for $t {
            fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion> {
                if let AnyEnum::$name(v) = any.0 {
                    Ok(v)
                } else {
                    Err(InvalidAnyConversion)
                }
            }
        }
    };
}

decl_basic_conversion!(bool, from_bool, Boolean);
decl_basic_conversion!(i8, from_i8, Int8);
decl_basic_conversion!(u8, from_u8, UInt8);
decl_basic_conversion!(i16, from_i16, Int16);
decl_basic_conversion!(u16, from_u16, UInt16);
decl_basic_conversion!(i32, from_i32, Int32);
decl_basic_conversion!(u32, from_u32, UInt32);
decl_basic_conversion!(i64, from_i64, Int64);
decl_basic_conversion!(u64, from_u64, UInt64);
decl_basic_conversion!(f32, from_f32, Float);
decl_basic_conversion!(f64, from_f64, Double);

impl IntoAny for String {
    fn into_any(self) -> Any {
        Any::from_string(self)
    }
}

impl TryFromAny for String {
    fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion> {
        if let AnyEnum::String(v) = &any.0 {
            Ok(v.clone())
        } else {
            Err(InvalidAnyConversion)
        }
    }
}

impl IntoAny for Buffer {
    fn into_any(self) -> Any {
        Any::from_buffer(self)
    }
}

impl TryFromAny for Buffer {
    fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion> {
        if let AnyEnum::Raw(v) = &any.0 {
            Ok(v.clone())
        } else {
            Err(InvalidAnyConversion)
        }
    }
}
// TODO add implementation for Bytes and BytesMut

impl<T: IntoAny + GetTypeSignature + Clone> IntoAny for &[T] {
    fn into_any(self) -> Any {
        let sig = <Vec<T> as GetTypeSignature>::signature().parse().unwrap();
        let mut vec = Vec::with_capacity(self.len());
        for val in self {
            vec.push(IntoAny::into_any(val.clone()));
        }
        Any(AnyEnum::List(sig, vec))
    }
}

impl<T: IntoAny + GetTypeSignature> IntoAny for Vec<T> {
    fn into_any(self) -> Any {
        let sig = <Vec<T> as GetTypeSignature>::signature().parse().unwrap();
        let mut vec = Vec::with_capacity(self.len());
        for val in self.into_iter() {
            vec.push(IntoAny::into_any(val));
        }
        Any(AnyEnum::List(sig, vec))
    }
}

impl<T: TryFromAny> TryFromAny for Vec<T> {
    fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion> {
        if let AnyEnum::List(_sig, vec_any) = &any.0 {
            let mut vec = Vec::with_capacity(vec_any.len());
            for val in vec_any {
                vec.push(TryFromAny::try_from_any(val)?);
            }
            Ok(vec)
        } else {
            Err(InvalidAnyConversion)
        }
    }
}

impl<K, V, S> IntoAny for std::collections::HashMap<K, V, S>
where
    K: IntoAny + GetTypeSignature + std::cmp::Eq + std::hash::Hash,
    V: IntoAny + GetTypeSignature,
    S: std::hash::BuildHasher,
{
    fn into_any(self) -> Any {
        let sig = <std::collections::HashMap<K, V, S> as GetTypeSignature>::signature()
            .parse()
            .unwrap();
        let mut vec = Vec::with_capacity(self.len());
        for (key, val) in self.into_iter() {
            vec.push((IntoAny::into_any(key), IntoAny::into_any(val)));
        }
        Any(AnyEnum::Map(sig, vec))
    }
}

impl<K, V> TryFromAny for std::collections::HashMap<K, V, std::collections::hash_map::RandomState>
where
    K: TryFromAny + std::cmp::Eq + std::hash::Hash,
    V: TryFromAny,
{
    fn try_from_any(
        any: &Any,
    ) -> Result<
        std::collections::HashMap<K, V, std::collections::hash_map::RandomState>,
        InvalidAnyConversion,
    > {
        if let AnyEnum::Map(_sig, any_map) = &any.0 {
            let mut map = std::collections::HashMap::with_capacity(any_map.len());
            for (key, val) in any_map {
                let k = TryFromAny::try_from_any(key)?;
                let v = TryFromAny::try_from_any(val)?;
                map.insert(k, v);
            }
            Ok(map)
        } else {
            Err(InvalidAnyConversion)
        }
    }
}

impl<T: IntoAny + GetTypeSignature> IntoAny for Option<T> {
    fn into_any(self) -> Any {
        let sig = <Option<T> as GetTypeSignature>::signature()
            .parse()
            .unwrap();
        let val = match self {
            Some(val) => Some(Box::new(IntoAny::into_any(val))),
            None => None,
        };
        Any(AnyEnum::Optional(sig, val))
    }
}

impl<T: TryFromAny> TryFromAny for Option<T> {
    fn try_from_any(any: &Any) -> Result<Self, InvalidAnyConversion> {
        if let AnyEnum::Optional(_sig, val) = &any.0 {
            match val {
                Some(val) => Ok(Some(TryFromAny::try_from_any(val)?)),
                None => Ok(None),
            }
        } else {
            Err(InvalidAnyConversion)
        }
    }
}
