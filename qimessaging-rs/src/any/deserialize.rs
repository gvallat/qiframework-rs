use {
    crate::{
        any::{Any, AnyEnum},
        buffer::Buffer,
        serialization::{
            deserializer::{error_eof, error_str, error_tag, read_bytes_array},
            Deserialize, Deserializer,
        },
        signature::{consume_next_element, iterator::Iter as SIter, Signature, Tag},
    },
    byteorder::{LittleEndian, ReadBytesExt},
    bytes::Bytes,
    std::{
        fmt,
        io::{Cursor, Result},
    },
};

impl Deserialize for Any {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        deserializer.check_tag(Tag::Dynamic)?;
        let sig = read_string(deserializer.buf())?;
        let sig = Signature::new(sig)?;
        let mut iter = sig.iter();
        deserialize_any(deserializer.buf(), &mut iter)
    }
}

fn read_string(buf: &mut Cursor<&Bytes>) -> Result<String> {
    let data = read_bytes_array(buf)?;
    String::from_utf8(data).map_err(|_| {
        std::io::Error::new(
            std::io::ErrorKind::Other,
            "Deserializer: Invalid UTF8 string.",
        )
    })
}

pub fn deserialize_any(buf: &mut Cursor<&Bytes>, iter: &mut SIter<'_>) -> Result<Any> {
    match iter.next() {
        None => error_eof(),
        Some(item) => match item.tag() {
            Tag::None => Ok(Any(AnyEnum::None)),
            Tag::Unit => Ok(Any(AnyEnum::Unit)),
            Tag::Bool => {
                let val = buf.read_u8()? != 0;
                Ok(Any(AnyEnum::Boolean(val)))
            }
            Tag::Int8 => {
                let val = buf.read_i8()?;
                Ok(Any(AnyEnum::Int8(val)))
            }
            Tag::UInt8 => {
                let val = buf.read_u8()?;
                Ok(Any(AnyEnum::UInt8(val)))
            }
            Tag::Int16 => {
                let val = buf.read_i16::<LittleEndian>()?;
                Ok(Any(AnyEnum::Int16(val)))
            }
            Tag::UInt16 => {
                let val = buf.read_u16::<LittleEndian>()?;
                Ok(Any(AnyEnum::UInt16(val)))
            }
            Tag::Int32 => {
                let val = buf.read_i32::<LittleEndian>()?;
                Ok(Any(AnyEnum::Int32(val)))
            }
            Tag::UInt32 => {
                let val = buf.read_u32::<LittleEndian>()?;
                Ok(Any(AnyEnum::UInt32(val)))
            }
            Tag::Int64 => {
                let val = buf.read_i64::<LittleEndian>()?;
                Ok(Any(AnyEnum::Int64(val)))
            }
            Tag::UInt64 => {
                let val = buf.read_u64::<LittleEndian>()?;
                Ok(Any(AnyEnum::UInt64(val)))
            }
            Tag::Float => {
                let val = buf.read_f32::<LittleEndian>()?;
                Ok(Any(AnyEnum::Float(val)))
            }
            Tag::Double => {
                let val = buf.read_f64::<LittleEndian>()?;
                Ok(Any(AnyEnum::Double(val)))
            }
            Tag::String => {
                let text = read_string(buf)?;
                Ok(Any(AnyEnum::String(text)))
            }
            Tag::List => deserialize_any_list(buf, iter, item.sub_signature().unwrap()),
            Tag::Map => deserialize_any_map(buf, iter, item.sub_signature().unwrap()),
            Tag::Tuple => deserialize_any_tuple(buf, iter, item.sub_signature().unwrap()),
            Tag::Dynamic => {
                let sig = read_string(buf)?;
                let sig = Signature::new(sig)?;
                let mut sig_iter = sig.iter();
                let val = deserialize_any(buf, &mut sig_iter)?;
                Ok(Any(AnyEnum::Dynamic(Box::new(val))))
            }
            Tag::Raw => {
                let bytes = read_bytes_array(buf)?;
                Ok(Any(AnyEnum::Raw(Buffer::from_vec(bytes))))
            }
            Tag::Object => unimplemented!(),
            Tag::Optional => {
                let has_val = buf.read_u8()? != 0;
                let sig = item.sub_signature().unwrap();
                if has_val {
                    let val = deserialize_any(buf, iter)?;
                    Ok(Any(AnyEnum::Optional(sig, Some(Box::new(val)))))
                } else {
                    consume_next_element(iter);
                    Ok(Any(AnyEnum::Optional(sig, None)))
                }
            }
            Tag::Unknown => error_str("Any: can't deserialize Unknown"),
            Tag::ListEnd | Tag::MapEnd | Tag::TupleEnd => unreachable!(),
        },
    }
}

fn deserialize_any_list(
    buf: &mut Cursor<&Bytes>,
    iter: &mut SIter<'_>,
    sig: Signature,
) -> Result<Any> {
    let len = buf.read_u32::<LittleEndian>()? as usize;
    let mut vec = Vec::with_capacity(len);

    let begin_iter = iter.clone();
    if len != 0 {
        for _ in 0..len {
            *iter = begin_iter.clone();
            let any = deserialize_any(buf, iter)?;
            vec.push(any);
        }
    } else {
        consume_next_element(iter);
    }

    match iter.next() {
        None => return error_eof(),
        Some(item) => match item.tag() {
            Tag::ListEnd => {}
            tag => return error_tag(tag, Tag::ListEnd),
        },
    }
    Ok(Any(AnyEnum::List(sig, vec)))
}

fn deserialize_any_map(
    buf: &mut Cursor<&Bytes>,
    iter: &mut SIter<'_>,
    sig: Signature,
) -> Result<Any> {
    let len = buf.read_u32::<LittleEndian>()? as usize;
    let mut vec = Vec::with_capacity(len);

    let begin_iter = iter.clone();
    if len != 0 {
        for _ in 0..len {
            *iter = begin_iter.clone();
            let key = deserialize_any(buf, iter)?;
            let value = deserialize_any(buf, iter)?;
            vec.push((key, value));
        }
    } else {
        consume_next_element(iter);
        consume_next_element(iter);
    }

    match iter.next() {
        None => return error_eof(),
        Some(item) => match item.tag() {
            Tag::ListEnd => {}
            tag => return error_tag(tag, Tag::ListEnd),
        },
    }
    Ok(Any(AnyEnum::Map(sig, vec)))
}

fn deserialize_any_tuple(
    buf: &mut Cursor<&Bytes>,
    iter: &mut SIter<'_>,
    sig: Signature,
) -> Result<Any> {
    let mut iter_2 = iter.clone();
    let mut vec = Vec::new();
    loop {
        match iter_2.next() {
            None => return error_eof(),
            Some(item) => match item.tag() {
                Tag::TupleEnd => {
                    *iter = iter_2.clone();
                    break;
                }
                _ => {
                    let field = deserialize_any(buf, iter)?;
                    vec.push(field);
                    iter_2 = iter.clone();
                }
            },
        }
    }
    Ok(Any(AnyEnum::Tuple(sig, vec)))
}

impl<'de> serde::Deserialize<'de> for Any {
    fn deserialize<D: serde::Deserializer<'de>>(de: D) -> std::result::Result<Self, D::Error> {
        enum Field {
            Signature,
            Data,
        }
        const FIELDS: &[&str] = &["signature", "data"];

        impl<'de> serde::Deserialize<'de> for Field {
            fn deserialize<D: serde::Deserializer<'de>>(
                de: D,
            ) -> std::result::Result<Self, D::Error> {
                struct FieldVisitor;
                impl<'de> serde::de::Visitor<'de> for FieldVisitor {
                    type Value = Field;
                    fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                        f.write_str("'signature' or 'data'")
                    }

                    fn visit_str<E: serde::de::Error>(
                        self,
                        name: &str,
                    ) -> std::result::Result<Field, E> {
                        match name {
                            "signature" => Ok(Field::Signature),
                            "data" => Ok(Field::Data),
                            _ => Err(E::unknown_field(name, FIELDS)),
                        }
                    }
                }

                de.deserialize_identifier(FieldVisitor)
            }
        }

        struct AnyVisitor;
        impl<'de> serde::de::Visitor<'de> for AnyVisitor {
            type Value = Any;
            fn expecting(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("any value")
            }

            fn visit_seq<A: serde::de::SeqAccess<'de>>(
                self,
                mut seq: A,
            ) -> std::result::Result<Any, A::Error> {
                let sig: String = seq
                    .next_element()?
                    .ok_or_else(|| serde::de::Error::invalid_length(0, &self))?;
                let sig = Signature::new(sig)
                    .map_err(|_| serde::de::Error::custom("could not parse signature"))?;
                let mut iter = sig.iter();
                seq.next_element_seed(with_signature::AnySeed::new(&mut iter))?
                    .ok_or_else(|| serde::de::Error::invalid_length(1, &self))
            }

            fn visit_map<A: serde::de::MapAccess<'de>>(
                self,
                mut map: A,
            ) -> std::result::Result<Any, A::Error> {
                let mut sig = None;
                let mut any = None;

                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Signature => {
                            if sig.is_some() {
                                return Err(serde::de::Error::duplicate_field("signature"));
                            }
                            let sig_str: String = map.next_value()?;
                            sig = Some(Signature::new(sig_str).map_err(|_| {
                                serde::de::Error::custom("could not parse signature")
                            })?);
                        }
                        Field::Data => {
                            if any.is_some() {
                                return Err(serde::de::Error::duplicate_field("data"));
                            }
                            match &sig {
                                Some(sig) => {
                                    let mut iter = sig.iter();
                                    any = Some(map.next_value_seed(
                                        with_signature::AnySeed::new(&mut iter),
                                    )?);
                                }
                                None => {
                                    // error ?
                                    any = Some(map.next_value_seed(no_signature::AnySeed)?);
                                }
                            }
                        }
                    }
                }

                let any =
                    any.ok_or_else(|| serde::de::Error::custom("could not deserialize an Any"))?;
                Ok(any)
            }
        }

        de.deserialize_struct("Any", FIELDS, AnyVisitor)
    }
}

mod no_signature {
    use crate::{
        any::{Any, AnyEnum},
        buffer::Buffer,
        signature::Signature,
    };
    use serde::de;

    pub(crate) struct AnySeed;
    impl<'de> de::DeserializeSeed<'de> for AnySeed {
        type Value = Any;
        fn deserialize<D: de::Deserializer<'de>>(
            self,
            de: D,
        ) -> std::result::Result<Any, D::Error> {
            de.deserialize_any(AnyVisitor)
        }
    }

    macro_rules! visit_simple {
        ($name:ident, $t:ty => $enum:ident) => {
            fn $name<E: de::Error>(self, val: $t) -> Result<Any, E> {
                Ok(Any(AnyEnum::$enum(val)))
            }
        };
    }

    struct AnyVisitor;
    impl<'de> serde::de::Visitor<'de> for AnyVisitor {
        type Value = Any;
        fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str("any value with no signature")
        }

        fn visit_unit<E: de::Error>(self) -> Result<Any, E> {
            Ok(Any(AnyEnum::Unit))
        }

        visit_simple!(visit_bool, bool => Boolean);
        visit_simple!(visit_i8, i8 => Int8);
        visit_simple!(visit_u8, u8 => UInt8);
        visit_simple!(visit_i16, i16 => Int16);
        visit_simple!(visit_u16, u16 => UInt16);
        visit_simple!(visit_i32, i32 => Int32);
        visit_simple!(visit_u32, u32 => UInt32);
        visit_simple!(visit_i64, i64 => Int64);
        visit_simple!(visit_u64, u64 => UInt64);
        visit_simple!(visit_f32, f32 => Float);
        visit_simple!(visit_f64, f64 => Double);
        visit_simple!(visit_string, String => String);

        fn visit_str<E: de::Error>(self, val: &str) -> Result<Any, E> {
            Ok(Any(AnyEnum::String(val.to_string())))
        }

        fn visit_bytes<E: de::Error>(self, val: &[u8]) -> Result<Any, E> {
            Ok(Any(AnyEnum::Raw(Buffer::from_vec(val.to_vec()))))
        }

        fn visit_byte_buf<E: de::Error>(self, val: Vec<u8>) -> Result<Any, E> {
            Ok(Any(AnyEnum::Raw(Buffer::from_vec(val))))
        }

        fn visit_none<E: de::Error>(self) -> Result<Any, E> {
            // unknown signature...
            let sig = Signature::new("+m".to_string()).unwrap();
            Ok(Any(AnyEnum::Optional(sig, None)))
        }

        fn visit_some<D: de::Deserializer<'de>>(self, de: D) -> Result<Any, D::Error> {
            let any = de.deserialize_any(AnyVisitor)?;
            let sig = Signature::new(any.signature()).unwrap();
            Ok(Any(AnyEnum::Optional(sig, Some(Box::new(any)))))
        }

        // visit_newtype_struct => not supported or as any ?

        fn visit_seq<A: de::SeqAccess<'de>>(self, mut access: A) -> Result<Any, A::Error> {
            let mut anys = match access.size_hint() {
                Some(len) => Vec::with_capacity(len),
                None => Vec::new(),
            };

            while let Some(any) = access.next_element_seed(AnySeed)? {
                anys.push(any);
            }

            // unknown signature...
            let sig = Signature::new("[m]".to_string()).unwrap();
            Ok(Any(AnyEnum::List(sig, anys)))
        }

        fn visit_map<A: de::MapAccess<'de>>(self, mut access: A) -> Result<Any, A::Error> {
            let mut anys = match access.size_hint() {
                Some(len) => Vec::with_capacity(len),
                None => Vec::new(),
            };

            while let Some(pair) = access.next_entry_seed(AnySeed, AnySeed)? {
                anys.push(pair);
            }

            // unknown signature...
            let sig = Signature::new("{mm}".to_string()).unwrap();
            Ok(Any(AnyEnum::Map(sig, anys)))
        }

        // visit_enum
    }
}

mod with_signature {
    use {
        crate::{
            any::{Any, AnyEnum},
            buffer::Buffer,
            signature::{consume_next_element, iterator::IterItem, Iter as SIter, Tag},
        },
        serde::de,
    };

    pub(crate) struct AnySeed<'iter, 'sig> {
        iter: &'iter mut SIter<'sig>,
    }

    impl<'iter, 'sig> AnySeed<'iter, 'sig> {
        pub(crate) fn new(iter: &'iter mut SIter<'sig>) -> Self {
            Self { iter }
        }
    }

    impl<'iter, 'sig, 'de> serde::de::DeserializeSeed<'de> for AnySeed<'iter, 'sig> {
        type Value = Any;
        fn deserialize<D: serde::de::Deserializer<'de>>(
            self,
            de: D,
        ) -> std::result::Result<Any, D::Error> {
            use serde::de::Error;
            match self.iter.next() {
                Some(item) => {
                    let visitor = AnyVisitor::new(self.iter, item.clone());
                    match item.tag() {
                        Tag::None => Ok(Any(AnyEnum::None)),
                        Tag::Unit => de.deserialize_unit(visitor),
                        Tag::Bool => de.deserialize_bool(visitor),
                        Tag::Int8 => de.deserialize_i8(visitor),
                        Tag::UInt8 => de.deserialize_u8(visitor),
                        Tag::Int16 => de.deserialize_i16(visitor),
                        Tag::UInt16 => de.deserialize_u16(visitor),
                        Tag::Int32 => de.deserialize_i32(visitor),
                        Tag::UInt32 => de.deserialize_u32(visitor),
                        Tag::Int64 => de.deserialize_i64(visitor),
                        Tag::UInt64 => de.deserialize_u64(visitor),
                        Tag::Float => de.deserialize_f32(visitor),
                        Tag::Double => de.deserialize_f64(visitor),
                        Tag::String => de.deserialize_string(visitor),
                        Tag::Raw => de.deserialize_byte_buf(visitor),
                        Tag::List => de.deserialize_seq(visitor),
                        Tag::Map => de.deserialize_map(visitor),
                        Tag::Optional => de.deserialize_option(visitor),
                        Tag::Tuple => Err(D::Error::custom("unimplemented tuple support")),
                        Tag::Dynamic => {
                            let any = serde::Deserialize::deserialize(de)?;
                            Ok(Any(AnyEnum::Dynamic(Box::new(any))))
                        }
                        Tag::Object => Err(D::Error::custom("unimplemented object support")),
                        Tag::ListEnd | Tag::MapEnd | Tag::TupleEnd => unreachable!(),
                        Tag::Unknown => Ok(Any(AnyEnum::None)), // error ?
                    }
                }
                None => Err(D::Error::custom("unexpected end of signature")),
            }
        }
    }

    pub(crate) struct AnyVisitor<'iter, 'sig> {
        iter: &'iter mut SIter<'sig>,
        item: IterItem<'sig>,
    }

    impl<'iter, 'sig> AnyVisitor<'iter, 'sig> {
        fn new(iter: &'iter mut SIter<'sig>, item: IterItem<'sig>) -> Self {
            Self { iter, item }
        }

        fn check_tag(&self, tag: Tag) -> Result<(), String> {
            if self.item.tag() == tag {
                Ok(())
            } else {
                Err(format!(
                    "Unexpected type (visit: {}, expected: {})",
                    tag,
                    self.item.tag()
                ))
            }
        }
    }

    macro_rules! visit_integer {
        ($name:ident, $t:ty) => {
            fn $name<E: de::Error>(self, val: $t) -> Result<Any, E> {
                use std::convert::TryFrom;
                match self.item.tag() {
                    Tag::Int8 => match i8::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::Int8(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit i8")),
                    },
                    Tag::UInt8 => match u8::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::UInt8(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit u8")),
                    },
                    Tag::Int16 => match i16::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::Int16(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit i16")),
                    },
                    Tag::UInt16 => match u16::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::UInt16(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit u16")),
                    },
                    Tag::Int32 => match i32::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::Int32(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit i32")),
                    },
                    Tag::UInt32 => match u32::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::UInt32(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit u32")),
                    },
                    Tag::Int64 => match i64::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::Int64(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit i64")),
                    },
                    Tag::UInt64 => match u64::try_from(val) {
                        Ok(val) => Ok(Any(AnyEnum::UInt64(val))),
                        Err(_) => Err(E::custom("the given value doesn't fit u64")),
                    },
                    tag => Err(E::custom(format!("unexpected integer (expected: {})", tag))),
                }
            }
        };
    }

    impl<'iter, 'sig, 'de> serde::de::Visitor<'de> for AnyVisitor<'iter, 'sig> {
        type Value = Any;
        fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str("any value")
        }

        fn visit_unit<E: de::Error>(self) -> Result<Any, E> {
            self.check_tag(Tag::Unit).map_err(E::custom)?;
            Ok(Any(AnyEnum::Unit))
        }

        fn visit_bool<E: de::Error>(self, val: bool) -> Result<Any, E> {
            self.check_tag(Tag::Bool).map_err(E::custom)?;
            Ok(Any(AnyEnum::Boolean(val)))
        }

        visit_integer!(visit_i8, i8);
        visit_integer!(visit_u8, u8);
        visit_integer!(visit_i16, i16);
        visit_integer!(visit_u16, u16);
        visit_integer!(visit_i32, i32);
        visit_integer!(visit_u32, u32);
        visit_integer!(visit_i64, i64);
        visit_integer!(visit_u64, u64);

        fn visit_f32<E: de::Error>(self, val: f32) -> Result<Any, E> {
            match self.item.tag() {
                Tag::Float => Ok(Any(AnyEnum::Float(val))),
                Tag::Double => Ok(Any(AnyEnum::Double(val.into()))),
                tag => Err(E::custom(format!(
                    "unexpected floating point value (expected: {})",
                    tag
                ))),
            }
        }

        fn visit_f64<E: de::Error>(self, val: f64) -> Result<Any, E> {
            match self.item.tag() {
                Tag::Float => Ok(Any(AnyEnum::Float(val as f32))),
                Tag::Double => Ok(Any(AnyEnum::Double(val))),
                tag => Err(E::custom(format!(
                    "unexpected floating point value (expected: {})",
                    tag
                ))),
            }
        }

        // visit_simple!(visit_f32, f32 => Float => Float);
        // visit_simple!(visit_f64, f64 => Double => Double);

        fn visit_string<E: de::Error>(self, val: String) -> Result<Any, E> {
            self.check_tag(Tag::String).map_err(E::custom)?;
            Ok(Any(AnyEnum::String(val)))
        }

        fn visit_str<E: de::Error>(self, val: &str) -> Result<Any, E> {
            self.check_tag(Tag::String).map_err(E::custom)?;
            Ok(Any(AnyEnum::String(val.to_string())))
        }

        fn visit_bytes<E: de::Error>(self, val: &[u8]) -> Result<Any, E> {
            self.check_tag(Tag::Raw).map_err(E::custom)?;
            Ok(Any(AnyEnum::Raw(Buffer::from_vec(val.to_vec()))))
        }

        fn visit_byte_buf<E: de::Error>(self, val: Vec<u8>) -> Result<Any, E> {
            self.check_tag(Tag::Raw).map_err(E::custom)?;
            Ok(Any(AnyEnum::Raw(Buffer::from_vec(val))))
        }

        fn visit_none<E: de::Error>(self) -> Result<Any, E> {
            self.check_tag(Tag::Optional).map_err(E::custom)?;
            let sig = self.item.sub_signature().unwrap();
            consume_next_element(self.iter);
            Ok(Any(AnyEnum::Optional(sig, None)))
        }

        fn visit_some<D: de::Deserializer<'de>>(self, de: D) -> Result<Any, D::Error> {
            use de::{DeserializeSeed, Error};
            self.check_tag(Tag::Optional).map_err(D::Error::custom)?;
            let sig = self.item.sub_signature().unwrap();
            let access = AnySeed::new(self.iter);
            let any = access.deserialize(de)?;
            Ok(Any(AnyEnum::Optional(sig, Some(Box::new(any)))))
        }

        // visit_newtype_struct => not supported or as any ?

        fn visit_seq<A: de::SeqAccess<'de>>(self, mut access: A) -> Result<Any, A::Error> {
            use de::Error;
            match self.item.tag() {
                Tag::List => {
                    let sig = self.item.sub_signature().unwrap();
                    let mut anys = match access.size_hint() {
                        Some(len) => Vec::with_capacity(len),
                        None => Vec::new(),
                    };

                    loop {
                        let mut iter = self.iter.clone();
                        match access.next_element_seed(AnySeed::new(&mut iter))? {
                            Some(any) => anys.push(any),
                            None => break,
                        }
                    }

                    consume_next_element(self.iter);
                    Ok(Any(AnyEnum::List(sig, anys)))
                }
                Tag::Tuple => unimplemented!(),
                Tag::Raw => {
                    let mut bytes = match access.size_hint() {
                        Some(len) => Vec::with_capacity(len),
                        None => Vec::new(),
                    };

                    while let Some(byte) = access.next_element()? {
                        bytes.push(byte);
                    }

                    Ok(Any(AnyEnum::Raw(Buffer::from_vec(bytes))))
                }
                tag => Err(A::Error::custom(format!(
                    "unexpected sequence visit (signature: {}",
                    tag
                ))),
            }
        }

        fn visit_map<A: de::MapAccess<'de>>(self, mut access: A) -> Result<Any, A::Error> {
            use de::Error;
            match self.item.tag() {
                Tag::Map => {
                    let sig = self.item.sub_signature().unwrap();
                    let mut anys = match access.size_hint() {
                        Some(len) => Vec::with_capacity(len),
                        None => Vec::new(),
                    };

                    loop {
                        let mut iter = self.iter.clone();
                        match access.next_key_seed(AnySeed::new(&mut iter))? {
                            Some(key) => {
                                let val = access.next_value_seed(AnySeed::new(&mut iter))?;
                                anys.push((key, val));
                            }
                            None => break,
                        }
                    }

                    consume_next_element(self.iter);
                    consume_next_element(self.iter);
                    Ok(Any(AnyEnum::Map(sig, anys)))
                }
                Tag::Tuple => unimplemented!(),
                tag => Err(A::Error::custom(format!(
                    "unexpected map visit (signature: {}",
                    tag
                ))),
            }
        }

        // visit_enum
    }
}

pub fn deserialize_to_any<'de, D: serde::Deserializer<'de>>(
    de: D,
) -> std::result::Result<Any, D::Error> {
    use serde::de::DeserializeSeed;
    no_signature::AnySeed.deserialize(de)
}

pub fn deserialize_to_any_with_signature<'de, D: serde::Deserializer<'de>>(
    sig: &Signature,
    de: D,
) -> std::result::Result<Any, D::Error> {
    use serde::de::DeserializeSeed;
    let mut iter = sig.iter();
    with_signature::AnySeed::new(&mut iter).deserialize(de)
}
