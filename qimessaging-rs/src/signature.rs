use std::str::FromStr;
use std::sync::Arc;

pub mod get_type_signature;
pub mod helper;
pub mod iterator;
pub mod parser;

pub use get_type_signature::GetTypeSignature;
pub use helper::consume_next_element;
pub use iterator::Iter;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Tag {
    None,
    Unit,
    Bool,
    Int8,
    UInt8,
    Int16,
    UInt16,
    Int32,
    UInt32,
    Int64,
    UInt64,
    Float,
    Double,
    String,
    List,
    ListEnd,
    Map,
    MapEnd,
    Tuple,
    TupleEnd,
    Dynamic,
    Raw,
    Object,
    Optional,
    Unknown,
    // Pointer, => legacy should be removed from libqi
    // TODO add support for VarArgs
    // TODO add support for KwArgs
}

impl std::fmt::Display for Tag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Tag::None => write!(f, "none"),
            Tag::Unit => write!(f, "unit"),
            Tag::Bool => write!(f, "bool"),
            Tag::Int8 => write!(f, "i8"),
            Tag::UInt8 => write!(f, "u8"),
            Tag::Int16 => write!(f, "i16"),
            Tag::UInt16 => write!(f, "u16"),
            Tag::Int32 => write!(f, "i32"),
            Tag::UInt32 => write!(f, "u32"),
            Tag::Int64 => write!(f, "i64"),
            Tag::UInt64 => write!(f, "u64"),
            Tag::Float => write!(f, "f32"),
            Tag::Double => write!(f, "f64"),
            Tag::String => write!(f, "string"),
            Tag::List => write!(f, "list begin"),
            Tag::ListEnd => write!(f, "list end"),
            Tag::Map => write!(f, "map begin"),
            Tag::MapEnd => write!(f, "map end"),
            Tag::Tuple => write!(f, "tuple begin"),
            Tag::TupleEnd => write!(f, "tuple end"),
            Tag::Dynamic => write!(f, "dynamic"),
            Tag::Raw => write!(f, "raw"),
            Tag::Object => write!(f, "object"),
            Tag::Optional => write!(f, "optional"),
            Tag::Unknown => write!(f, "unknown"),
        }
    }
}

#[derive(Default, Debug)]
pub(crate) struct StructNames<'a> {
    pub(crate) name: &'a str,
    pub(crate) field_names: Vec<&'a str>,
}

#[derive(Default, Debug)]
struct SignatureData {
    full_str: String,
    simplified_str: String,
    struct_names: Vec<StructNames<'static>>,
}

#[derive(Default, Clone)]
pub struct Signature {
    inner: Arc<SignatureData>,
    full_str: &'static str,
    simplified_str: &'static str,
    struct_names: &'static [StructNames<'static>],
}

impl std::fmt::Debug for Signature {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Signature {{shared:  \"{}\", full: \"{}\", simplified: \"{}\"}}",
            self.inner.full_str, self.full_str, self.simplified_str
        )
    }
}

impl PartialEq for Signature {
    fn eq(&self, other: &Signature) -> bool {
        Arc::ptr_eq(&self.inner, &other.inner) || self.full_str == other.full_str
    }
}

impl Eq for Signature {}

impl FromStr for Signature {
    type Err = std::io::Error;
    fn from_str(s: &str) -> Result<Signature, Self::Err> {
        parser::parse(s.to_string())
    }
}

impl ToString for Signature {
    fn to_string(&self) -> String {
        self.inner.full_str.clone()
    }
}

impl Signature {
    pub fn new(raw: String) -> std::io::Result<Signature> {
        parser::parse(raw)
    }

    pub fn full_string(&self) -> &str {
        &self.full_str
    }

    pub fn simplified_string(&self) -> &str {
        &self.simplified_str
    }

    pub fn iter(&self) -> Iter<'_> {
        Iter::new(self)
    }

    pub(crate) fn struct_names<'a>(&'a self) -> &[StructNames<'a>] {
        &self.struct_names
    }
}
