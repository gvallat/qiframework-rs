use crate::{
    buffer::Buffer,
    signature::{GetTypeSignature, Signature},
};

pub mod conversion;
mod deserialize;
mod serialize;
pub use conversion::{IntoAny, InvalidAnyConversion, TryFromAny};
pub use deserialize::deserialize_any;
pub use deserialize::{deserialize_to_any, deserialize_to_any_with_signature};

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum AnyEnum {
    None,
    Unit,
    Boolean(bool),
    Int8(i8),
    UInt8(u8),
    Int16(i16),
    UInt16(u16),
    Int32(i32),
    UInt32(u32),
    Int64(i64),
    UInt64(u64),
    Float(f32),
    Double(f64),
    String(String),
    List(Signature, Vec<Any>),
    Map(Signature, Vec<(Any, Any)>),
    Tuple(Signature, Vec<Any>),
    Dynamic(Box<Any>),
    Raw(Buffer),
    Optional(Signature, Option<Box<Any>>),
    #[allow(unused)]
    Object, // TODO add support for object
    #[allow(unused)]
    Unknown,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Any(pub(crate) AnyEnum);

impl GetTypeSignature for Any {
    fn signature() -> String {
        "m".to_string()
    }
}

impl AnyEnum {
    fn signature(&self) -> String {
        match self {
            AnyEnum::None => "_".to_string(),
            AnyEnum::Unit => "v".to_string(),
            AnyEnum::Boolean(_) => "b".to_string(),
            AnyEnum::Int8(_) => "c".to_string(),
            AnyEnum::UInt8(_) => "C".to_string(),
            AnyEnum::Int16(_) => "w".to_string(),
            AnyEnum::UInt16(_) => "W".to_string(),
            AnyEnum::Int32(_) => "i".to_string(),
            AnyEnum::UInt32(_) => "I".to_string(),
            AnyEnum::Int64(_) => "l".to_string(),
            AnyEnum::UInt64(_) => "L".to_string(),
            AnyEnum::Float(_) => "f".to_string(),
            AnyEnum::Double(_) => "d".to_string(),
            AnyEnum::String(_) => "s".to_string(),
            AnyEnum::List(sig, _) => sig.full_string().to_string(),
            AnyEnum::Map(sig, _) => sig.full_string().to_string(),
            AnyEnum::Tuple(sig, _) => sig.full_string().to_string(),
            AnyEnum::Dynamic(_) => "m".to_string(),
            AnyEnum::Raw(_) => "r".to_string(),
            AnyEnum::Optional(sig, _) => sig.full_string().to_string(),
            AnyEnum::Object => "o".to_string(),
            AnyEnum::Unknown => "X".to_string(),
        }
    }
}

impl Default for Any {
    fn default() -> Any {
        Any::new()
    }
}

macro_rules! decl_as_x {
    ($name:ident, $t:ty, $val:ident) =>
    {
        pub fn $name(&self) -> Option<& $t> {
            if let AnyEnum::$val(v) = &self.0 {
                Some(v)
            } else {
                None
            }
        }
    }
}

macro_rules! decl_from_x {
    ($name:ident, $t:ty, $val:ident) => {
        pub fn $name(v: $t) -> Any {
            Any(AnyEnum::$val(v))
        }
    };
}

impl Any {
    pub fn new() -> Any {
        Any(AnyEnum::None)
    }

    pub fn signature(&self) -> String {
        self.0.signature()
    }

    decl_as_x!(as_bool, bool, Boolean);
    decl_as_x!(as_i8, i8, Int8);
    decl_as_x!(as_u8, u8, UInt8);
    decl_as_x!(as_i16, i16, Int16);
    decl_as_x!(as_u16, u16, UInt16);
    decl_as_x!(as_i32, i32, Int32);
    decl_as_x!(as_u32, u32, UInt32);
    decl_as_x!(as_i64, i64, Int64);
    decl_as_x!(as_u64, u64, UInt64);
    decl_as_x!(as_f32, f32, Float);
    decl_as_x!(as_f64, f64, Double);
    decl_as_x!(as_string, String, String);
    decl_as_x!(as_dynamic, Box<Any>, Dynamic);
    decl_as_x!(as_buffer, Buffer, Raw);

    pub fn as_vec(&self) -> Option<&Vec<Any>> {
        if let AnyEnum::List(_, v) = &self.0 {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_map(&self) -> Option<&Vec<(Any, Any)>> {
        if let AnyEnum::Map(_, v) = &self.0 {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_tuple(&self) -> Option<&Vec<Any>> {
        if let AnyEnum::Tuple(_, v) = &self.0 {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_optional(&self) -> Option<&Any> {
        if let AnyEnum::Optional(_, v) = &self.0 {
            if let Some(val) = v {
                Some(val)
            } else {
                None
            }
        } else {
            None
        }
    }

    pub fn from<T: IntoAny>(val: T) -> Any {
        IntoAny::into_any(val)
    }

    pub fn try_into<T: TryFromAny>(&self) -> Result<T, InvalidAnyConversion> {
        TryFromAny::try_from_any(self)
    }

    pub fn from_unit(_: ()) -> Any {
        Any(AnyEnum::Unit)
    }

    decl_from_x!(from_bool, bool, Boolean);
    decl_from_x!(from_i8, i8, Int8);
    decl_from_x!(from_u8, u8, UInt8);
    decl_from_x!(from_i16, i16, Int16);
    decl_from_x!(from_u16, u16, UInt16);
    decl_from_x!(from_i32, i32, Int32);
    decl_from_x!(from_u32, u32, UInt32);
    decl_from_x!(from_i64, i64, Int64);
    decl_from_x!(from_u64, u64, UInt64);
    decl_from_x!(from_f32, f32, Float);
    decl_from_x!(from_f64, f64, Double);
    decl_from_x!(from_string, String, String);
    decl_from_x!(from_buffer, Buffer, Raw);

    pub fn into_dynamic(v: Any) -> Any {
        Any(AnyEnum::Dynamic(Box::new(v)))
    }

    // TODO Add helper for constructing and destructing tuples => needed for ToAny and FromAny
}
