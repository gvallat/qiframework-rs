use crate::{
    serialization::{Deserialize, Deserializer, Serialize, Serializer},
    signature::GetTypeSignature,
};

#[derive(Default, Debug, Clone, PartialEq, Eq)]
pub struct Buffer(pub Vec<u8>);

impl Buffer {
    pub fn new() -> Buffer {
        Buffer(Vec::new())
    }

    pub fn with_capacity(capa: usize) -> Buffer {
        Buffer(Vec::with_capacity(capa))
    }

    pub fn from_vec(vec: Vec<u8>) -> Buffer {
        Buffer(vec)
    }
}

impl std::ops::Deref for Buffer {
    type Target = Vec<u8>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl GetTypeSignature for Buffer {
    fn signature() -> String {
        "r".to_string()
    }
}

impl Serialize for Buffer {
    fn serialize_into(&self, serializer: &mut Serializer<'_, '_>) -> std::io::Result<()> {
        serializer.write_raw(&self.0[..])
    }
}

impl serde::Serialize for Buffer {
    fn serialize<S: serde::Serializer>(&self, ser: S) -> std::result::Result<S::Ok, S::Error> {
        ser.serialize_bytes(&self.0)
    }
}

impl<'de> serde::Deserialize<'de> for Buffer {
    fn deserialize<D: serde::Deserializer<'de>>(ser: D) -> std::result::Result<Self, D::Error> {
        struct BufferVisitor;

        impl<'de> serde::de::Visitor<'de> for BufferVisitor {
            type Value = Buffer;

            fn expecting(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "bytes")
            }

            fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E> {
                Ok(Buffer(v.to_vec()))
            }

            fn visit_borrowed_bytes<E>(self, v: &'de [u8]) -> Result<Self::Value, E> {
                Ok(Buffer(v.to_vec()))
            }

            fn visit_byte_buf<E>(self, v: Vec<u8>) -> Result<Self::Value, E> {
                Ok(Buffer(v))
            }
        }

        ser.deserialize_byte_buf(BufferVisitor)
    }
}

impl Deserialize for Buffer {
    fn deserialize_from(deserializer: &mut Deserializer<'_, '_>) -> std::io::Result<Self> {
        let vec = deserializer.read_raw()?;
        Ok(Buffer(vec))
    }
}

#[test]
fn buffer_deref() {
    let vec = vec![1, 2, 3];
    let buf = Buffer::from_vec(vec.clone());
    assert_eq!(&buf[..], &vec[..]);
}

#[test]
fn buffer_deref_iter() {
    let vec = vec![1, 2, 3];
    let buf = Buffer::from_vec(vec.clone());
    let vec2: Vec<u8> = buf.iter().copied().collect();
    assert_eq!(&vec2[..], &vec[..]);
}
