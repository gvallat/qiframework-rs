pub trait GetSignature {
    fn signature() -> String;
}

pub struct Serializer;
pub trait Serialize {
    fn serialize(&self, serializer: &mut Serializer) -> Result<(), ()>;
}

pub struct Deserializer;
pub trait Deserialize: Default {
    fn deserialize(deserializer: &mut Deserializer) -> Result<Self, ()>;
}

pub struct StructDeserializer;

impl Deserializer {
    fn begin_struct(&self) -> StructDeserializer {
        unimplemented!()
    }
}

impl StructDeserializer {
    fn fields(&self) -> &[&str] {
        unimplemented!()
    }
}

#[derive(Default)]
//#[derive(Serialize, Deserialize)]
//#[type_added("y")]
//#[type_removed("z")]
struct TestStruct {
    // x: u8
// y: u8 (Added)
// z: u8 (Removed)
}

impl Deserialize for TestStruct {
    fn deserialize(deserializer: &mut Deserializer) -> Result<Self, ()> {
        let res = TestStruct::default();
        let struct_deserializer = deserializer.begin_struct();
        let required = &mut [false; 1];
        let mut added = Vec::with_capacity(1);
        let mut dropped = Vec::with_capacity(1);

        for field in struct_deserializer.fields() {
            match *field {
                "x" => {
                    // required: read field, set member, mark required as read
                    required[0] = true;
                }
                "y" => {
                    // added: read field, set member, mark added as read
                    added.push("y");
                }
                "z" => {
                    // dropped: consume and save dropped buffer and signature
                    let buff = vec![0u8; 4];
                    let sig = "i".to_string();
                    dropped.push(("z", buff, sig));
                }
                _ => {
                    // unexpected field
                    return Err(());
                }
            }
        }

        // check all required field were read
        if !required.iter().all(|b| b) {
            return Err(());
        }
        // convert(&mut res, added, dropped);

        Ok(res)
    }
}
