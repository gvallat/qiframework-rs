use {
    crate::{
        transport::{dispatcher::Dispatcher, socket::Socket},
        url::{Url, UrlScheme},
    },
    std::marker::PhantomData,
    tokio::{io, net::TcpStream},
    tokio_tls::{TlsConnector, TlsStream},
    tracing_futures::Instrument,
};

async fn connect_tcp(url: &Url) -> Result<TcpStream, io::Error> {
    tracing::trace!("Connecting TCP");
    TcpStream::connect(url.as_socket_addrs_base()).await
}

async fn add_tls(stream: TcpStream, host: &str) -> Result<TlsStream<TcpStream>, io::Error> {
    tracing::trace!("Adding TLS");
    let connector = native_tls::TlsConnector::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();
    let connector = TlsConnector::from(connector);
    connector
        .connect(host, stream)
        .await
        .map_err(|e| io::Error::new(io::ErrorKind::ConnectionAborted, e))
}

/// Connect a `Socket` to the given url, messages will be dispatched using an instance of `D`.
pub async fn connect_socket<D: Dispatcher + 'static>(
    url: &Url,
    dispatcher_state: D::State,
) -> Result<Socket<D::Event>, io::Error> {
    let span = tracing::span!(tracing::Level::TRACE, "Connecting", "{}", url);
    match url.scheme() {
        UrlScheme::Tcp => {
            let stream = connect_tcp(url).instrument(span).await?;
            let dispatcher: PhantomData<D> = PhantomData;
            Ok(Socket::new(
                stream,
                url.clone(),
                dispatcher_state,
                dispatcher,
            ))
        }
        UrlScheme::Tcps => {
            let stream = connect_tcp(url).instrument(span.clone()).await?;
            let stream = add_tls(stream, url.host()).instrument(span).await?;
            let dispatcher: PhantomData<D> = PhantomData;
            Ok(Socket::new(
                stream,
                url.clone(),
                dispatcher_state,
                dispatcher,
            ))
        }
    }
}
