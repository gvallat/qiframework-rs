use bytes::Bytes;

/// *qi-messaging* message types
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MsgType {
    /// Unknown message type.
    Unknown = 0,
    /// Call message.
    Call = 1,
    /// Call reply message.
    Reply = 2,
    /// Error reply message.
    Error = 3,
    /// Post message.
    Post = 4,
    /// Event message.
    Event = 5,
    /// Protocol capability message.
    Capability = 6,
    /// Cancel message.
    Cancel = 7,
    /// Cancel confirmation message.
    Cancelled = 8,
}

impl Into<u8> for MsgType {
    fn into(self) -> u8 {
        self as u8
    }
}

impl From<u8> for MsgType {
    fn from(val: u8) -> MsgType {
        match val {
            1 => MsgType::Call,
            2 => MsgType::Reply,
            3 => MsgType::Error,
            4 => MsgType::Post,
            5 => MsgType::Event,
            6 => MsgType::Capability,
            7 => MsgType::Cancel,
            8 => MsgType::Cancelled,
            _ => MsgType::Unknown,
        }
    }
}

/// *qi-messaging* message header
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct MessageHeader {
    // magic: u32,
    /// The message ID.
    pub msg_id: u32,
    /// The data payload size.
    pub size: u32,
    // version: u16,
    /// The message type.
    pub msg_type: MsgType,
    /// The message flags.
    pub flags: u8,
    /// The ID of the destination service.
    pub service_id: u32,
    /// The ID of the destination object.
    pub object_id: u32,
    /// The ID of the destination action.
    pub action_id: u32,
}

/// Payload of type 'm' instead of expected type
#[allow(unused)]
pub const FLAG_DYNAMIC_PAYLOAD: u8 = 1;

/// Payload contain a type in wich the reply should be converted to (reply will be dynamic)
#[allow(unused)]
pub const FLAG_RETURN_TYPE: u8 = 2;

impl Default for MessageHeader {
    fn default() -> MessageHeader {
        MessageHeader::new()
    }
}

impl MessageHeader {
    /// The serialize bytes count of the *qi-messaging* message header.
    pub const SIZE: usize = 28;
    /// The *qi-messaging* message magic value.
    pub const MAGIC: u32 = 0x42de_ad42;
    /// The *qi-messaging* message version.
    pub const VERSION: u16 = 0;

    /// Returns a default contructed header.
    pub fn new() -> MessageHeader {
        MessageHeader {
            msg_id: 0,
            size: 0,
            msg_type: MsgType::Unknown,
            flags: 0,
            service_id: 0,
            object_id: 0,
            action_id: 0,
        }
    }
}

// TODO use Payload
#[allow(unused)]
enum Payload {
    Constrained(Bytes),
    Dynamic(Bytes),
}

/// A *qi-messaging* protocol package
#[derive(Clone)]
pub struct Message {
    payload: Bytes,
    /// The message header
    pub header: MessageHeader,
}

impl Message {
    /// Constructs a `Message` with the given payload and a default header.
    pub fn new(payload: Bytes) -> Self {
        Message {
            payload,
            header: MessageHeader::default(),
        }
    }

    /// Constructs an empty `Message` with a default header and an empty payload.
    pub fn empty() -> Self {
        Message {
            payload: Bytes::new(),
            header: MessageHeader::default(),
        }
    }

    /// Constructs a `Message` with the given payload and header.
    pub fn with_header(payload: Bytes, header: MessageHeader) -> Self {
        Message { payload, header }
    }

    /// Constructs a `Message` with the given header and an empty payload.
    pub fn empty_with_header(header: MessageHeader) -> Self {
        Message {
            payload: Bytes::new(),
            header,
        }
    }

    /// Returns the message data as a bytes slice.
    pub fn payload(&self) -> &[u8] {
        &self.payload
    }

    pub fn into_payload(self) -> Bytes {
        self.payload
    }
}
