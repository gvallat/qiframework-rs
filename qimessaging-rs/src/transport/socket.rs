use {
    crate::{
        transport::{
            dispatcher::{
                dispatch_task, io_read_message_task, io_write_message_task, Dispatcher,
                DispatcherMsg, WriterMsg,
            },
            message_codec::MessageCodec,
            Message,
        },
        url::Url,
    },
    futures::{
        channel::{mpsc, oneshot},
        StreamExt,
    },
    std::{
        marker::PhantomData,
        sync::{
            atomic::{AtomicBool, AtomicU32, AtomicU64, Ordering},
            Arc,
        },
    },
    tokio::io,
    tracing_futures::Instrument,
};

pub(crate) struct SocketInner<T> {
    url: Url,
    dispatch_sender: mpsc::UnboundedSender<DispatcherMsg<T>>,
    writer_sender: mpsc::UnboundedSender<WriterMsg>,
    next_msg_id: AtomicU32,
    next_signal_id: AtomicU64,
}

/// The handle to a network connection.
///
/// Linked to three tasks:
/// - Read message (from the inner stream)
/// - Write message (to the inner stream)
/// - Dispatch
pub struct Socket<T> {
    inner: Arc<SocketInner<T>>,
    connected: Arc<AtomicBool>,
}

impl<T> Clone for Socket<T> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            connected: self.connected.clone(),
        }
    }
}

impl<T> Socket<T>
where
    T: Send + 'static,
{
    /// Retuns a new `Socket` linked to the given stream.
    ///
    /// Creates a `Dispatcher` of type `D` with `dispatcher_state`.
    /// Spawns the associated tasks.
    pub fn new<S, D>(
        stream: S,
        url: Url,
        dispatcher_state: D::State,
        _dispatcher: PhantomData<D>,
    ) -> Self
    where
        S: io::AsyncRead + io::AsyncWrite + Send + 'static + std::marker::Unpin,
        D: Dispatcher<Event = T> + 'static,
    {
        let (dispatch_sender, dispatch_receiver) = mpsc::unbounded();
        let (writer_sender, writer_receiver) = mpsc::unbounded();
        let connected = Arc::new(AtomicBool::new(true));

        let stream = tokio_util::codec::Framed::new(stream, MessageCodec);
        let (writer, reader) = stream.split();
        tokio::spawn(
            io_read_message_task(reader, dispatch_sender.clone())
                .instrument(tracing::trace_span!("Read", "{}", url)),
        );
        tokio::spawn(
            io_write_message_task(writer, writer_receiver)
                .instrument(tracing::trace_span!("Write", "{}", url)),
        );

        let connected_2 = connected.clone();
        let writer_sender_2 = writer_sender.clone();
        tokio::spawn(
            dispatch_task::<D>(
                url.clone(),
                dispatcher_state,
                dispatch_receiver,
                writer_sender_2,
                connected_2,
            )
            .instrument(tracing::trace_span!("Dispatch", "{}", url)),
        );

        let inner = Arc::new(SocketInner {
            url,
            dispatch_sender,
            writer_sender,
            next_msg_id: AtomicU32::new(0),
            next_signal_id: AtomicU64::new(0),
        });

        Socket { inner, connected }
    }
}

impl<T> SocketInner<T> {
    #[inline(always)]
    pub fn next_message_id(&self) -> u32 {
        self.next_msg_id.fetch_add(1, Ordering::SeqCst)
    }

    #[inline(always)]
    pub fn next_signal_id(&self) -> u64 {
        self.next_signal_id.fetch_add(1, Ordering::SeqCst)
    }

    #[inline(always)]
    pub fn send_event(&self, event: T) -> Result<(), ()> {
        self.dispatch_sender
            .unbounded_send(DispatcherMsg::Event(event))
            .map_err(|_| ())
    }

    #[inline(always)]
    pub fn send_message(&self, msg: Message) -> Result<(), ()> {
        self.writer_sender
            .unbounded_send(WriterMsg::Send(msg))
            .map_err(|_| ())
    }

    #[inline(always)]
    pub fn post_message(&self, msg: Message) -> Result<oneshot::Receiver<()>, ()> {
        let (sender, receiver) = oneshot::channel();
        self.writer_sender
            .unbounded_send(WriterMsg::Post(msg, sender))
            .map(|_| receiver)
            .map_err(|_| ())
    }

    #[inline(always)]
    pub fn close(&self) {
        match self
            .dispatch_sender
            .unbounded_send(DispatcherMsg::CloseRequest)
        {
            _ => {}
        }
    }
}

impl<T> Drop for SocketInner<T> {
    /// Close the socket
    fn drop(&mut self) {
        self.close()
    }
}

impl<T> Socket<T> {
    #[allow(unused)]
    pub(crate) fn inner(&self) -> &SocketInner<T> {
        &self.inner
    }

    /// Returns a unique id to use for the next message to send
    pub fn next_message_id(&self) -> u32 {
        self.inner.next_message_id()
    }

    /// Returns a unique id to use for the next signal connection
    pub fn next_signal_id(&self) -> u64 {
        self.inner.next_signal_id()
    }

    pub(crate) fn url(&self) -> &Url {
        &self.inner.url
    }

    /// Returns `true` if the inner stream is still connected and the associated tasks are still running.
    pub fn is_connected(&self) -> bool {
        self.connected.load(Ordering::SeqCst)
    }

    /// Send an event to the dispatch task.
    pub fn send_event(&self, event: T) -> Result<(), ()> {
        self.inner.send_event(event)
    }

    /// Send a message to the writer task.
    pub fn send_message(&self, msg: Message) -> Result<(), ()> {
        self.inner.send_message(msg)
    }

    /// Send a message to the writer task and returns a future which complete when the
    /// message has been flushed into the inner stream.
    pub fn post_message(&self, msg: Message) -> Result<oneshot::Receiver<()>, ()> {
        self.inner.post_message(msg)
    }

    /// Close the socket by closing the inner stream and stopping the three associated tasks.
    pub fn close(&self) {
        self.inner.close();
    }
}
