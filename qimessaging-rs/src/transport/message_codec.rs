use crate::transport::message::{Message, MessageHeader, MsgType};
use byteorder::{BigEndian, LittleEndian, ReadBytesExt, WriteBytesExt};
use bytes::BytesMut;
use std::io::Cursor;
use tokio_util::codec::{Decoder, Encoder};

/// A tokio codec to transform a stream of bytes into a stream of `Message`.
pub struct MessageCodec;

impl Decoder for MessageCodec {
    type Item = Message;
    type Error = std::io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        if src.len() < MessageHeader::SIZE {
            return Ok(None);
        }
        // read header
        let mut cursor = Cursor::new(&src[..MessageHeader::SIZE]);

        let magic = cursor.read_u32::<BigEndian>()?;
        if magic != MessageHeader::MAGIC {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Invalid magic number: {}", magic),
            ));
        }

        let msg_id = cursor.read_u32::<LittleEndian>()?;
        let size = cursor.read_u32::<LittleEndian>()?;

        if src.len() < MessageHeader::SIZE + size as usize {
            return Ok(None);
        }

        let version = cursor.read_u16::<LittleEndian>()?;
        if version != MessageHeader::VERSION {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                format!("Invalid version number: {}", version),
            ));
        }

        let msg_type = MsgType::from(cursor.read_u8()?);
        let flags = cursor.read_u8()?;
        let service_id = cursor.read_u32::<LittleEndian>()?;
        let object_id = cursor.read_u32::<LittleEndian>()?;
        let action_id = cursor.read_u32::<LittleEndian>()?;

        let header = MessageHeader {
            msg_id,
            size,
            msg_type,
            flags,
            service_id,
            object_id,
            action_id,
        };

        // consume header
        src.split_to(MessageHeader::SIZE);

        // extract payload
        let payload = src.split_to(header.size as usize).freeze();
        Ok(Some(Message::with_header(payload, header)))
    }
}

impl Encoder for MessageCodec {
    type Item = Message;
    type Error = std::io::Error;

    fn encode(&mut self, msg: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
        dst.reserve(MessageHeader::SIZE + msg.payload().len());

        // write header
        let header = &msg.header;
        let bytes = &mut [0; MessageHeader::SIZE];
        let mut cursor = Cursor::new(&mut bytes[..]);
        cursor.write_u32::<BigEndian>(MessageHeader::MAGIC)?;
        cursor.write_u32::<LittleEndian>(header.msg_id)?;
        // TODO remove size from header, serialize msg.data().len()
        cursor.write_u32::<LittleEndian>(header.size)?;
        cursor.write_u16::<LittleEndian>(MessageHeader::VERSION)?;
        cursor.write_u8(header.msg_type.into())?;
        cursor.write_u8(header.flags)?;
        cursor.write_u32::<LittleEndian>(header.service_id)?;
        cursor.write_u32::<LittleEndian>(header.object_id)?;
        cursor.write_u32::<LittleEndian>(header.action_id)?;
        dst.extend_from_slice(&bytes[..]);

        // write payload
        dst.extend_from_slice(msg.payload());
        Ok(())
    }
}
