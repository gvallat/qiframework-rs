use {
    crate::{transport::message::Message, url::Url},
    futures::{channel::mpsc, channel::oneshot, Sink, SinkExt, Stream, StreamExt},
    std::sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    tokio::io,
};

pub(crate) enum DispatcherMsg<T> {
    MsgReceived(Message),
    CloseRequest,
    Event(T),
}

pub(crate) enum WriterMsg {
    Send(Message),
    Post(Message, oneshot::Sender<()>),
    CloseRequest,
}

pub trait Dispatcher
where
    Self: Send,
{
    type State: Send;
    type Event: Send;
    fn new(url: Url, state: Self::State) -> Self;
    fn handle_event(&mut self, event: Self::Event);
    fn handle_received_message(&mut self, msg: Message);
    fn close(&mut self);
}

pub(crate) async fn io_read_message_task<S, E>(
    mut reader: S,
    dispatch_sender: mpsc::UnboundedSender<DispatcherMsg<E>>,
) where
    S: Stream<Item = io::Result<Message>> + Send + 'static + std::marker::Unpin,
{
    while let Some(res) = reader.next().await {
        match res {
            Ok(msg) => {
                tracing::trace!("Message received");
                if dispatch_sender
                    .unbounded_send(DispatcherMsg::MsgReceived(msg))
                    .is_err()
                {
                    break;
                }
            }
            Err(e) => {
                tracing::error!("Reading error: {}", e);
                break;
            }
        }
    }
    let _ = dispatch_sender.unbounded_send(DispatcherMsg::CloseRequest);
}

pub(crate) async fn io_write_message_task<S>(
    mut writer: S,
    mut writer_receiver: mpsc::UnboundedReceiver<WriterMsg>,
) where
    S: Sink<Message, Error = io::Error> + Send + 'static + std::marker::Unpin,
{
    let task = async {
        while let Some(msg) = writer_receiver.next().await {
            match msg {
                WriterMsg::CloseRequest => {
                    writer.close().await?;
                    tracing::trace!("Stream closed");
                    break;
                }
                WriterMsg::Post(msg, acknowledgement) => {
                    writer.send(msg).await?;
                    writer.flush().await?;
                    tracing::trace!("Message posted");
                    let _ = acknowledgement.send(());
                }
                WriterMsg::Send(msg) => {
                    writer.send(msg).await?;
                    writer.flush().await?;
                    tracing::trace!("Message sent");
                }
            }
        }
        Ok(())
    };

    let res: io::Result<()> = task.await;
    if let Err(e) = res {
        tracing::error!("Writting error: {}", e);
    }
}

pub(crate) async fn dispatch_task<D>(
    url: Url,
    dispatcher_state: D::State,
    mut dispatch_receiver: mpsc::UnboundedReceiver<DispatcherMsg<D::Event>>,
    writer_sender: mpsc::UnboundedSender<WriterMsg>,
    connected: Arc<AtomicBool>,
) where
    D: Dispatcher + 'static,
{
    let mut dispatcher = D::new(url, dispatcher_state);
    while let Some(msg) = dispatch_receiver.next().await {
        match msg {
            DispatcherMsg::MsgReceived(msg) => {
                dispatcher.handle_received_message(msg);
            }
            DispatcherMsg::CloseRequest => {
                dispatcher.close();
                let _ = writer_sender.unbounded_send(WriterMsg::CloseRequest);
                break;
            }
            DispatcherMsg::Event(event) => {
                dispatcher.handle_event(event);
            }
        }
    }

    tracing::trace!("Dispatch task ending");
    dispatcher.close();
    connected.store(false, Ordering::SeqCst);
}
