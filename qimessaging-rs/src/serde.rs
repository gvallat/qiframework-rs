/// Two kind of payloads:
/// - constrained (usual, normal flow)
/// - dynamic (rare, for compat)
///
/// TODO to_*()
/// TODO add ConstrainedAny(Any)
///
/// Constrained, three ways of getting the same payload:
/// - to_payload<T: Serialize>(sig, val) => Payload::Constrained(_)
/// - to_any<T: Serialize + GetTypeSignature>(val) => any_to_payload(sig, any) => Payload::Constrained(_)
/// - to_constrained_any<T: Serialize>(val, sig) => constrained_any_to_payload(any) => Payload::Constrained(_)
///
/// Dynamic, two ways of getting the same payload:
/// - to_dynamic_payload<T: Serialize + GetTypeSignature>(val) => Payload::Dynamic(_)
/// - to_any<T: Serialize + GetTypeSignature>(val) => any_to_dynamic_payload(any) => Payload::Dynamic(_)
use {
    crate::{
        any::Any,
        rpc::RpcSocket,
        signature::{GetTypeSignature, Signature},
    },
    bytes::Bytes,
    serde::{Deserialize, Serialize},
    std::cell::RefCell,
    std::io::Cursor,
};

pub(crate) mod any_deserializer;
pub(crate) mod any_serializer;
pub(crate) mod binary_serializer;
pub mod error;

#[cfg(test)]
mod test_any_deserializer;
#[cfg(test)]
mod test_any_serializer;
#[cfg(test)]
mod test_binary_serializer;

use any_deserializer::AnyDeserializer;
use any_serializer::AnySerializer;
use binary_serializer::BinarySerializer;
pub use error::Error;

pub const OBJECT_TOKEN: &str = "$QI_OBJECT$";
pub const ANYVALUE_TOKEN: &str = "$QI_ANYVALUE$";

thread_local! {
static SERIALIZE_SOCKET: RefCell<Option<RpcSocket>> = RefCell::new(None);
static SERIALIZE_BINARY: RefCell<bool> = RefCell::new(false);
static SERIALIZE_SIGNATURE: RefCell<Option<Signature>> = RefCell::new(None);
}

pub fn is_serializing_to_binary() -> bool {
    SERIALIZE_BINARY.with(|b| *b.borrow())
}

pub fn serializing_socket() -> Option<RpcSocket> {
    SERIALIZE_SOCKET.with(|s| s.borrow().clone())
}

pub fn serializing_current_signature() -> Option<Signature> {
    SERIALIZE_SIGNATURE.with(|s| s.borrow().clone())
}

pub fn to_payload<T: Serialize>(
    socket: &RpcSocket,
    sig: &Signature,
    val: &T,
) -> Result<Bytes, Error> {
    let mut serializer = BinarySerializer::new(sig.iter());

    SERIALIZE_SOCKET.with(|s| *s.borrow_mut() = Some(socket.clone()));
    SERIALIZE_BINARY.with(|b| *b.borrow_mut() = true);

    let res = Serialize::serialize(val, &mut serializer);

    SERIALIZE_SOCKET.with(|s| *s.borrow_mut() = None);
    SERIALIZE_SIGNATURE.with(|s| *s.borrow_mut() = None);
    SERIALIZE_BINARY.with(|b| *b.borrow_mut() = false);

    res?;
    serializer.check_reached_end()?;
    Ok(serializer.into_vec().into())
}

pub fn to_dynamic_payload<T: Serialize + GetTypeSignature>(
    socket: &RpcSocket,
    val: &T,
) -> Result<Bytes, Error> {
    let sig = <T as GetTypeSignature>::signature();

    let vec = Vec::with_capacity(128);
    let mut buf = Cursor::new(vec);
    binary_serializer::write_bytes_array(&mut buf, sig.as_bytes())?;

    let sig: Signature = sig.parse().unwrap(); // TODO map to Error
    let mut serializer = BinarySerializer::with_buffer(sig.iter(), buf);

    SERIALIZE_SOCKET.with(|s| *s.borrow_mut() = Some(socket.clone()));
    SERIALIZE_BINARY.with(|b| *b.borrow_mut() = true);

    let res = Serialize::serialize(val, &mut serializer);

    SERIALIZE_SOCKET.with(|s| *s.borrow_mut() = None);
    SERIALIZE_SIGNATURE.with(|s| *s.borrow_mut() = None);
    SERIALIZE_BINARY.with(|b| *b.borrow_mut() = false);

    res?;
    serializer.check_reached_end()?;
    Ok(serializer.into_vec().into())
}

thread_local! {
static SERIALIZE_TO_ANY: RefCell<bool> = RefCell::new(false);
pub(crate) static SERIALIZE_ANY: RefCell<Option<Any>> = RefCell::new(None);
}

pub fn is_serializing_to_any() -> bool {
    SERIALIZE_TO_ANY.with(|b| *b.borrow())
}

pub fn to_any<T: Serialize + GetTypeSignature>(val: &T) -> Result<Any, Error> {
    let sig = <T as GetTypeSignature>::signature();
    let sig: Signature = sig.parse().unwrap(); // TODO map to Error
    let mut iter = sig.iter();
    let serializer = AnySerializer::new(&mut iter);
    SERIALIZE_TO_ANY.with(|b| *b.borrow_mut() = true);
    let res = Serialize::serialize(val, serializer);
    SERIALIZE_TO_ANY.with(|b| *b.borrow_mut() = false);
    SERIALIZE_ANY.with(|s| *s.borrow_mut() = None);
    // TODO check iter is consumed
    res
}

pub fn to_compatible_any<T: Serialize>(sig: &Signature, val: &T) -> Result<Any, Error> {
    let mut iter = sig.iter();
    let serializer = AnySerializer::new(&mut iter);
    SERIALIZE_TO_ANY.with(|b| *b.borrow_mut() = true);
    let res = Serialize::serialize(val, serializer);
    SERIALIZE_TO_ANY.with(|b| *b.borrow_mut() = false);
    SERIALIZE_ANY.with(|s| *s.borrow_mut() = None);
    // TODO check iter is consumed
    res
}

pub fn from_any<'de, T: Deserialize<'de>>(any: &'de Any) -> Result<T, Error> {
    let deserializer = AnyDeserializer::new(any);
    Deserialize::deserialize(deserializer)
}
