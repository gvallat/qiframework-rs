use {
    crate::url::Url,
    bytes::{Bytes, BytesMut},
    std::io::Result,
};

pub mod deserializer;
pub mod serializer;
pub use deserializer::{deserialize_value, Deserializer};
pub use serializer::{serialize_value, Serializer};

pub trait Serialize {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()>;
}

pub trait Deserialize
where
    Self: Sized,
{
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self>;
}

macro_rules! decl_basic_serialization {
    ($T:ty, $write:ident, $read:ident) => {
        impl Serialize for $T {
            fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
                serializer.$write(*self)
            }
        }

        impl Deserialize for $T {
            fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<$T> {
                deserializer.$read()
            }
        }
    };
}

decl_basic_serialization!(bool, write_bool, read_bool);
decl_basic_serialization!(i8, write_i8, read_i8);
decl_basic_serialization!(u8, write_u8, read_u8);
decl_basic_serialization!(i16, write_i16, read_i16);
decl_basic_serialization!(u16, write_u16, read_u16);
decl_basic_serialization!(i32, write_i32, read_i32);
decl_basic_serialization!(u32, write_u32, read_u32);
decl_basic_serialization!(i64, write_i64, read_i64);
decl_basic_serialization!(u64, write_u64, read_u64);
decl_basic_serialization!(f32, write_f32, read_f32);
decl_basic_serialization!(f64, write_f64, read_f64);

impl Serialize for &str {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        serializer.write_str(self)
    }
}

impl Serialize for String {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        serializer.write_str(self)
    }
}

impl Deserialize for String {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        deserializer.read_str()
    }
}

impl Serialize for BytesMut {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        serializer.write_raw(&self[..])
    }
}

impl Deserialize for BytesMut {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        let raw = deserializer.read_raw()?;
        Ok(BytesMut::from(&raw[..]))
    }
}

impl Serialize for Bytes {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        serializer.write_raw(&self[..])
    }
}

impl Deserialize for Bytes {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        let raw = deserializer.read_raw()?;
        Ok(Bytes::from(raw))
    }
}

impl<T: Serialize> Serialize for Option<T> {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        serializer.write_optional(self)
    }
}

impl<T: Deserialize> Deserialize for Option<T> {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        deserializer.read_optional()
    }
}

impl<T: Serialize> Serialize for [T] {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        let mut list_serializer = serializer.list_serializer(Some(self.len()))?;
        for item in self {
            list_serializer.write_element(item)?;
        }
        list_serializer.end()
    }
}

impl<T: Serialize> Serialize for Vec<T> {
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        let mut list_serializer = serializer.list_serializer(Some(self.len()))?;
        for item in self {
            list_serializer.write_element(item)?;
        }
        list_serializer.end()
    }
}

impl<T: Deserialize> Deserialize for Vec<T> {
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        let mut list_deserializer = deserializer.list_deserializer()?;
        let mut vec = Vec::with_capacity(list_deserializer.len());
        for _ in 0..list_deserializer.len() {
            vec.push(list_deserializer.read_element()?);
        }
        list_deserializer.end()?;
        Ok(vec)
    }
}

impl<K, V, S> Serialize for std::collections::HashMap<K, V, S>
where
    K: Serialize + std::cmp::Eq + std::hash::Hash,
    V: Serialize,
    S: std::hash::BuildHasher,
{
    fn serialize_into<'s, 'd>(&self, serializer: &mut Serializer<'s, 'd>) -> Result<()> {
        let mut map_serializer = serializer.map_serializer(Some(self.len()))?;
        for entry in self {
            map_serializer.write_entry(entry.0, entry.1)?;
        }
        map_serializer.end()
    }
}

impl<K, V> Deserialize for std::collections::HashMap<K, V, std::collections::hash_map::RandomState>
where
    K: Deserialize + std::cmp::Eq + std::hash::Hash,
    V: Deserialize,
{
    fn deserialize_from<'s, 'd>(deserializer: &mut Deserializer<'s, 'd>) -> Result<Self> {
        let mut map_deserializer = deserializer.map_deserializer()?;
        let mut map = std::collections::HashMap::with_capacity(map_deserializer.len());
        for _ in 0..map_deserializer.len() {
            let (key, value) = map_deserializer.read_entry()?;
            map.insert(key, value);
        }
        map_deserializer.end()?;
        Ok(map)
    }
}

impl Serialize for Url {
    fn serialize_into(&self, serializer: &mut Serializer<'_, '_>) -> Result<()> {
        serializer.write_str(&self.to_string())
    }
}

impl Deserialize for Url {
    fn deserialize_from(deserializer: &mut Deserializer<'_, '_>) -> Result<Self> {
        let url = deserializer.read_str()?;
        let url: Url = url
            .parse()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))?;
        Ok(url)
    }
}

impl<T1, T2, T3> Serialize for (T1, T2, T3)
where
    T1: Serialize,
    T2: Serialize,
    T3: Serialize,
{
    fn serialize_into(&self, serializer: &mut Serializer<'_, '_>) -> Result<()> {
        let mut tuple = serializer.tuple_serializer("")?;
        tuple.write_field("", &self.0)?;
        tuple.write_field("", &self.1)?;
        tuple.write_field("", &self.2)?;
        tuple.end()
    }
}

impl<T1, T2> Deserialize for (T1, T2)
where
    T1: Deserialize,
    T2: Deserialize,
{
    fn deserialize_from(deserializer: &mut Deserializer<'_, '_>) -> Result<Self> {
        let mut tuple = deserializer.tuple_deserializer("")?;
        let f1 = tuple.read_field("")?;
        let f2 = tuple.read_field("")?;
        tuple.end()?;
        Ok((f1, f2))
    }
}
