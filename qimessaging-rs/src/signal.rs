use {
    core::{
        pin::Pin,
        task::{Context, Poll},
    },
    futures::{
        channel::mpsc::{self, UnboundedReceiver, UnboundedSender},
        stream::{FusedStream, Stream},
    },
    std::{
        collections::HashMap,
        sync::{Arc, Mutex},
    },
};

type SignalTx<T> = UnboundedSender<T>;
type SignalRx<T> = UnboundedReceiver<T>;
pub type OnEnabledChanged<T> = Box<dyn FnMut(Option<Signal<T>>) + Send>;
pub type ArcSignalShared<T> = Arc<Mutex<SignalShared<T>>>;

pub struct SignalShared<T> {
    next_id: u64,
    senders: HashMap<u64, SignalTx<T>>,
    on_enabled_changed: Option<OnEnabledChanged<T>>,
}

impl<T: Clone> SignalShared<T> {
    pub fn send(&self, val: T) {
        for (_id, tx) in self.senders.iter() {
            let _ = tx.unbounded_send(val.clone());
        }
    }
}

fn add_receiver<T>(shared: ArcSignalShared<T>) -> SignalReceiver<T> {
    let (receiver, id) = {
        let mut guard = shared.lock().unwrap();
        let was_empty = guard.senders.is_empty();
        let (tx, receiver) = mpsc::unbounded();
        guard.next_id += 1;
        let id = guard.next_id;
        guard.senders.insert(id, tx);
        if was_empty && !guard.senders.is_empty() {
            if let Some(on_enabled_changed) = &mut guard.on_enabled_changed {
                on_enabled_changed(Some(Signal {
                    shared: shared.clone(),
                }));
            }
        }
        (receiver, id)
    };
    SignalReceiver {
        shared,
        receiver,
        id,
    }
}

fn remove_receiver<T>(shared: &ArcSignalShared<T>, id: u64) {
    let mut guard = shared.lock().unwrap();
    let was_empty = guard.senders.is_empty();
    guard.senders.remove(&id);
    if !was_empty && guard.senders.is_empty() {
        if let Some(on_enabled_changed) = &mut guard.on_enabled_changed {
            on_enabled_changed(None);
        }
    }
}

pub struct Signal<T> {
    shared: ArcSignalShared<T>,
}

impl<T> Default for Signal<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Signal<T> {
    pub fn new() -> Self {
        let shared = SignalShared {
            next_id: 0,
            senders: HashMap::new(),
            on_enabled_changed: None,
        };

        Self {
            shared: Arc::new(Mutex::new(shared)),
        }
    }

    pub fn with_enabled_callback(callback: OnEnabledChanged<T>) -> Self {
        let shared = SignalShared {
            next_id: 0,
            senders: HashMap::new(),
            on_enabled_changed: Some(callback),
        };

        Self {
            shared: Arc::new(Mutex::new(shared)),
        }
    }

    pub fn receiver(&self) -> SignalReceiver<T> {
        add_receiver(self.shared.clone())
    }
}

impl<T: Clone> Signal<T> {
    pub fn send(&self, val: T) {
        let shared = self.shared.lock().unwrap();
        shared.send(val);
    }
}

pub struct SignalReceiver<T> {
    shared: ArcSignalShared<T>,
    receiver: SignalRx<T>,
    id: u64,
}

impl<T> Drop for SignalReceiver<T> {
    fn drop(&mut self) {
        remove_receiver(&self.shared, self.id);
    }
}

impl<T> Stream for SignalReceiver<T> {
    type Item = T;
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let pinned = Pin::get_mut(self);
        Pin::new(&mut pinned.receiver).poll_next(cx)
    }
}

impl<T> FusedStream for SignalReceiver<T> {
    fn is_terminated(&self) -> bool {
        self.receiver.is_terminated()
    }
}
