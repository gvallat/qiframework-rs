use super::Serialize;
use crate::signature::{consume_next_element, iterator::Iter as SIter, Signature, Tag};
use byteorder::{LittleEndian, WriteBytesExt};
use bytes::Bytes;
use std::io::{Cursor, Result, Write};

pub struct Serializer<'s, 'd> {
    sig_iter: &'d mut SIter<'s>,
    buf: &'d mut Cursor<Vec<u8>>,
}

pub fn serialize_value<T: Serialize>(sig: &Signature, value: &T) -> Result<Bytes> {
    let mut buf = Cursor::new(Vec::new());
    {
        let mut iter = sig.iter();
        let mut serializer = Serializer::new(&mut iter, &mut buf);
        Serialize::serialize_into(value, &mut serializer)?;
        if iter.next().is_some() {
            return error_str("Serializer: Unexpected end of data");
        }
    }
    Ok(Bytes::from(buf.into_inner()))
}

#[inline(always)]
fn error_str<T>(text: &str) -> std::io::Result<T> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, text))
}

#[inline(always)]
fn error<T>(text: String) -> std::io::Result<T> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, text))
}

#[inline(always)]
fn error_tag<T>(sig_tag: Tag, write_tag: Tag) -> std::io::Result<T> {
    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        format!(
            "Serializer: Unexpected tag (write '{:?}', signature '{:?}').",
            write_tag, sig_tag
        ),
    ))
}

#[inline(always)]
pub(crate) fn error_doesnt_fit_u32<T>(len: usize) -> std::io::Result<T> {
    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        format!("Serializer: Length doesn't fit in u32 ({})", len),
    ))
}

pub(crate) fn write_bytes_array(buf: &mut Cursor<Vec<u8>>, src: &[u8]) -> Result<()> {
    let len = src.len();
    if len > u32::max_value() as usize {
        return error_doesnt_fit_u32(len);
    }
    buf.write_u32::<LittleEndian>(len as u32)?;
    let size = buf.write(src)?;
    if size == len {
        Ok(())
    } else {
        error_str("Serializer: Unexpected end of buffer.")
    }
}

impl<'s, 'd> Serializer<'s, 'd> {
    pub fn new<'ss, 'dd>(
        sig_iter: &'dd mut SIter<'ss>,
        buf: &'dd mut Cursor<Vec<u8>>,
    ) -> Serializer<'ss, 'dd> {
        Serializer { sig_iter, buf }
    }

    pub(crate) fn check_tag(&mut self, tag: Tag) -> Result<()> {
        match self.sig_iter.next() {
            Some(item) => {
                if item.tag() == tag {
                    Ok(())
                } else {
                    error_tag(item.tag(), tag)
                }
            }
            None => error_tag(Tag::None, tag),
        }
    }

    pub(crate) fn buf_mut(&mut self) -> &mut Cursor<Vec<u8>> {
        &mut self.buf
    }

    pub fn write_bool(&mut self, val: bool) -> Result<()> {
        self.check_tag(Tag::Bool)?;
        self.buf.write_u8(val as u8)
    }

    pub fn write_i8(&mut self, val: i8) -> Result<()> {
        self.check_tag(Tag::Int8)?;
        self.buf.write_i8(val)
    }

    pub fn write_u8(&mut self, val: u8) -> Result<()> {
        self.check_tag(Tag::UInt8)?;
        self.buf.write_u8(val)
    }

    pub fn write_i16(&mut self, val: i16) -> Result<()> {
        self.check_tag(Tag::Int16)?;
        self.buf.write_i16::<LittleEndian>(val)
    }

    pub fn write_u16(&mut self, val: u16) -> Result<()> {
        self.check_tag(Tag::UInt16)?;
        self.buf.write_u16::<LittleEndian>(val)
    }

    pub fn write_i32(&mut self, val: i32) -> Result<()> {
        self.check_tag(Tag::Int32)?;
        self.buf.write_i32::<LittleEndian>(val)
    }

    pub fn write_u32(&mut self, val: u32) -> Result<()> {
        self.check_tag(Tag::UInt32)?;
        self.buf.write_u32::<LittleEndian>(val)
    }

    pub fn write_i64(&mut self, val: i64) -> Result<()> {
        self.check_tag(Tag::Int64)?;
        self.buf.write_i64::<LittleEndian>(val)
    }

    pub fn write_u64(&mut self, val: u64) -> Result<()> {
        self.check_tag(Tag::UInt64)?;
        self.buf.write_u64::<LittleEndian>(val)
    }

    pub fn write_f32(&mut self, val: f32) -> Result<()> {
        self.check_tag(Tag::Float)?;
        self.buf.write_f32::<LittleEndian>(val)
    }

    pub fn write_f64(&mut self, val: f64) -> Result<()> {
        self.check_tag(Tag::Double)?;
        self.buf.write_f64::<LittleEndian>(val)
    }

    pub fn write_str(&mut self, val: &str) -> Result<()> {
        self.check_tag(Tag::String)?;
        write_bytes_array(self.buf, val.as_bytes())
    }

    pub fn write_raw(&mut self, buf: &[u8]) -> Result<()> {
        self.check_tag(Tag::Raw)?;
        write_bytes_array(self.buf, buf)
    }

    pub fn write_optional<T: Serialize>(&mut self, option: &Option<T>) -> Result<()> {
        self.check_tag(Tag::Optional)?;
        match option {
            Some(val) => {
                self.buf.write_u8(1)?;
                val.serialize_into(self)
            }
            None => {
                consume_next_element(self.sig_iter);
                self.buf.write_u8(0)
            }
        }
    }

    pub fn list_serializer<'ser, T: Serialize>(
        &'ser mut self,
        len: Option<usize>,
    ) -> Result<ListSerializer<'ser, 's, 'd, T>> {
        self.check_tag(Tag::List)?;

        let len_pos = self.buf.position();
        let written_len = match len {
            Some(size) => {
                if size > u32::max_value() as usize {
                    return error_doesnt_fit_u32(size);
                }
                self.buf.write_u32::<LittleEndian>(size as u32)?;
                size
            }
            None => {
                self.buf.write_u32::<LittleEndian>(0)?;
                0
            }
        };

        Ok(ListSerializer::new(self, written_len, len_pos))
    }

    pub fn map_serializer<'ser, K: Serialize, V: Serialize>(
        &'ser mut self,
        len: Option<usize>,
    ) -> Result<MapSerializer<'ser, 's, 'd, K, V>> {
        self.check_tag(Tag::Map)?;

        let len_pos = self.buf.position();
        let written_len = match len {
            Some(size) => {
                if size > u32::max_value() as usize {
                    return error_doesnt_fit_u32(size);
                }
                self.buf.write_u32::<LittleEndian>(size as u32)?;
                size
            }
            None => {
                self.buf.write_u32::<LittleEndian>(0)?;
                0
            }
        };

        Ok(MapSerializer::new(self, written_len, len_pos))
    }

    pub fn tuple_serializer<'ser>(
        &'ser mut self,
        _name: &str,
    ) -> Result<TupleSerializer<'ser, 's, 'd>> {
        let field_names = match self.sig_iter.next() {
            Some(item) => {
                if item.tag() == Tag::Tuple {
                    // name == item.struct_name() is not inforced by libqi
                    item.field_names().unwrap()
                } else {
                    return error_tag(item.tag(), Tag::Tuple);
                }
            }
            None => return error_tag(Tag::None, Tag::Tuple),
        };
        Ok(TupleSerializer::new(self, field_names))
    }
}

pub struct ListSerializer<'ser, 's, 'd, T: Serialize> {
    serializer: &'ser mut Serializer<'s, 'd>,
    begin_sig_iter: SIter<'s>,
    len: usize,
    written_len: usize,
    len_pos: u64,
    _type: std::marker::PhantomData<T>,
}

impl<'ser, 's, 'd, T: Serialize> ListSerializer<'ser, 's, 'd, T> {
    fn new(
        serializer: &'ser mut Serializer<'s, 'd>,
        written_len: usize,
        len_pos: u64,
    ) -> ListSerializer<'ser, 's, 'd, T> {
        let begin_sig_iter = serializer.sig_iter.clone();
        ListSerializer {
            serializer,
            begin_sig_iter,
            len: 0,
            written_len,
            len_pos,
            _type: std::marker::PhantomData,
        }
    }

    pub fn write_element(&mut self, value: &T) -> Result<()> {
        self.len += 1;
        *self.serializer.sig_iter = self.begin_sig_iter.clone();
        value.serialize_into(self.serializer)
    }

    pub fn end(self) -> Result<()> {
        if self.len != self.written_len {
            let pos = self.serializer.buf.position();
            self.serializer.buf.set_position(self.len_pos);
            if self.len > u32::max_value() as usize {
                return error_doesnt_fit_u32(self.len);
            }
            self.serializer
                .buf
                .write_u32::<LittleEndian>(self.len as u32)?;
            self.serializer.buf.set_position(pos);
        }
        // list might be empty
        *self.serializer.sig_iter = self.begin_sig_iter;
        consume_next_element(self.serializer.sig_iter);
        self.serializer.check_tag(Tag::ListEnd)?;
        Ok(())
    }
}

pub struct MapSerializer<'ser, 's, 'd, K: Serialize, V: Serialize> {
    serializer: &'ser mut Serializer<'s, 'd>,
    begin_sig_iter: SIter<'s>,
    len: usize,
    written_len: usize,
    len_pos: u64,
    _key: std::marker::PhantomData<K>,
    _value: std::marker::PhantomData<V>,
}

impl<'ser, 's, 'd, K: Serialize, V: Serialize> MapSerializer<'ser, 's, 'd, K, V> {
    fn new(
        serializer: &'ser mut Serializer<'s, 'd>,
        written_len: usize,
        len_pos: u64,
    ) -> MapSerializer<'ser, 's, 'd, K, V> {
        let begin_sig_iter = serializer.sig_iter.clone();
        MapSerializer {
            serializer,
            begin_sig_iter,
            len: 0,
            written_len,
            len_pos,
            _key: std::marker::PhantomData,
            _value: std::marker::PhantomData,
        }
    }

    pub fn write_entry(&mut self, key: &K, value: &V) -> Result<()> {
        self.len += 1;
        *self.serializer.sig_iter = self.begin_sig_iter.clone();
        key.serialize_into(self.serializer)?;
        value.serialize_into(self.serializer)
    }

    pub fn end(self) -> Result<()> {
        if self.len != self.written_len {
            let pos = self.serializer.buf.position();
            self.serializer.buf.set_position(self.len_pos);
            if self.len > u32::max_value() as usize {
                return error_doesnt_fit_u32(self.len);
            }
            self.serializer
                .buf
                .write_u32::<LittleEndian>(self.len as u32)?;
            self.serializer.buf.set_position(pos);
        }
        // map might be empty
        *self.serializer.sig_iter = self.begin_sig_iter;
        consume_next_element(self.serializer.sig_iter);
        consume_next_element(self.serializer.sig_iter);
        self.serializer.check_tag(Tag::MapEnd)?;
        Ok(())
    }
}

pub struct TupleSerializer<'ser, 's, 'd> {
    serializer: &'ser mut Serializer<'s, 'd>,
    field_names: &'s [&'s str],
}

impl<'ser, 's, 'd> TupleSerializer<'ser, 's, 'd> {
    fn new(
        serializer: &'ser mut Serializer<'s, 'd>,
        field_names: &'s [&'s str],
    ) -> TupleSerializer<'ser, 's, 'd> {
        TupleSerializer {
            serializer,
            field_names,
        }
    }

    pub fn field_names(&self) -> &'s [&'s str] {
        self.field_names
    }

    pub fn write_field<T: Serialize>(&mut self, name: &str, value: &T) -> Result<()> {
        let field_name = match self.serializer.sig_iter.clone().next() {
            Some(item) => item.field_name(),
            None => "",
        };
        if !field_name.is_empty() && !name.is_empty() && field_name != name {
            return error(format!(
                "Serialize: field name doesn't match (write: '{}', signature: '{}')",
                name, field_name,
            ));
        }
        value.serialize_into(self.serializer)
    }

    pub fn end(self) -> Result<()> {
        self.serializer.check_tag(Tag::TupleEnd)?;
        Ok(())
    }
}
