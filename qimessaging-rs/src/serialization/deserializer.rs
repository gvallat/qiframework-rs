use super::Deserialize;
use crate::signature::{consume_next_element, iterator::Iter as SIter, Signature, Tag};
use byteorder::{LittleEndian, ReadBytesExt};
use bytes::Bytes;
use std::io::{Cursor, Read, Result};

pub struct Deserializer<'s, 'd> {
    sig_iter: &'d mut SIter<'s>,
    buf: &'d mut Cursor<&'d Bytes>,
}

pub fn deserialize_value<T: Deserialize>(sig: &Signature, bytes: &Bytes) -> Result<T> {
    let mut iter = sig.iter();
    let len = bytes.len();
    let mut buf = Cursor::new(bytes);
    let mut deserializer = Deserializer::new(&mut iter, &mut buf);
    let val = Deserialize::deserialize_from(&mut deserializer)?;
    deserializer.end(len)?;
    Ok(val)
}

#[inline(always)]
pub(crate) fn error_str<T>(text: &str) -> std::io::Result<T> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, text))
}

#[inline(always)]
fn error<T>(text: String) -> std::io::Result<T> {
    Err(std::io::Error::new(std::io::ErrorKind::Other, text))
}

#[inline(always)]
pub(crate) fn error_tag<T>(sig_tag: Tag, read_tag: Tag) -> std::io::Result<T> {
    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        format!(
            "Deserializer: Unexpected tag (read '{:?}', signature '{:?}').",
            read_tag, sig_tag
        ),
    ))
}

#[inline(always)]
pub(crate) fn error_eof<T>() -> std::io::Result<T> {
    error_str("Deserializer: Unexpected end of data.")
}

pub(crate) fn read_bytes_array(buf: &mut Cursor<&Bytes>) -> Result<Vec<u8>> {
    let len = buf.read_u32::<LittleEndian>()? as usize;
    let mut dst = Vec::new();
    dst.resize(len, 0);
    let readen_len = buf.read(&mut dst)?;
    if readen_len != len {
        return error_eof();
    }
    Ok(dst)
}

impl<'v, 'd> Deserializer<'v, 'd> {
    pub fn new<'vv, 'dd>(
        sig_iter: &'dd mut SIter<'vv>,
        buf: &'dd mut Cursor<&'dd Bytes>,
    ) -> Deserializer<'vv, 'dd> {
        Deserializer { sig_iter, buf }
    }

    fn end(self, len: usize) -> Result<()> {
        if self.sig_iter.next().is_some() || self.buf.position() != len as u64 {
            error_str("Deserializer: Unconsumed data")
        } else {
            Ok(())
        }
    }

    pub(crate) fn check_tag(&mut self, tag: Tag) -> Result<()> {
        match self.sig_iter.next() {
            Some(item) => {
                if item.tag() == tag {
                    Ok(())
                } else {
                    error_tag(item.tag(), tag)
                }
            }
            None => error_tag(Tag::None, tag),
        }
    }

    pub(crate) fn buf(&mut self) -> &mut Cursor<&'d Bytes> {
        self.buf
    }

    pub fn read_bool(&mut self) -> Result<bool> {
        self.check_tag(Tag::Bool)?;
        let val = self.buf.read_u8()?;
        Ok(val != 0)
    }

    pub fn read_i8(&mut self) -> Result<i8> {
        self.check_tag(Tag::Int8)?;
        self.buf.read_i8()
    }

    pub fn read_u8(&mut self) -> Result<u8> {
        self.check_tag(Tag::UInt8)?;
        self.buf.read_u8()
    }

    pub fn read_i16(&mut self) -> Result<i16> {
        self.check_tag(Tag::Int16)?;
        self.buf.read_i16::<LittleEndian>()
    }

    pub fn read_u16(&mut self) -> Result<u16> {
        self.check_tag(Tag::UInt16)?;
        self.buf.read_u16::<LittleEndian>()
    }

    pub fn read_i32(&mut self) -> Result<i32> {
        self.check_tag(Tag::Int32)?;
        self.buf.read_i32::<LittleEndian>()
    }

    pub fn read_u32(&mut self) -> Result<u32> {
        self.check_tag(Tag::UInt32)?;
        self.buf.read_u32::<LittleEndian>()
    }

    pub fn read_i64(&mut self) -> Result<i64> {
        self.check_tag(Tag::Int64)?;
        self.buf.read_i64::<LittleEndian>()
    }

    pub fn read_u64(&mut self) -> Result<u64> {
        self.check_tag(Tag::UInt64)?;
        self.buf.read_u64::<LittleEndian>()
    }

    pub fn read_f32(&mut self) -> Result<f32> {
        self.check_tag(Tag::Float)?;
        self.buf.read_f32::<LittleEndian>()
    }

    pub fn read_f64(&mut self) -> Result<f64> {
        self.check_tag(Tag::Double)?;
        self.buf.read_f64::<LittleEndian>()
    }

    pub fn read_str(&mut self) -> Result<String> {
        self.check_tag(Tag::String)?;
        let buf = read_bytes_array(self.buf)?;
        String::from_utf8(buf).map_err(|_| {
            std::io::Error::new(
                std::io::ErrorKind::Other,
                "Deserializer: Invalid UTF8 string.",
            )
        })
    }

    pub fn read_raw(&mut self) -> Result<Vec<u8>> {
        self.check_tag(Tag::Raw)?;
        read_bytes_array(self.buf)
    }

    pub fn read_optional<T: Deserialize>(&mut self) -> Result<Option<T>> {
        self.check_tag(Tag::Optional)?;
        let has_value = self.buf.read_u8()? != 0;
        if has_value {
            let value = Deserialize::deserialize_from(self)?;
            Ok(Some(value))
        } else {
            consume_next_element(self.sig_iter);
            Ok(None)
        }
    }

    pub fn list_deserializer<'des, T: Deserialize>(
        &'des mut self,
    ) -> Result<ListDeserializer<'des, 'v, 'd, T>> {
        self.check_tag(Tag::List)?;
        let len = self.buf.read_u32::<LittleEndian>()? as usize;
        Ok(ListDeserializer::new(self, len))
    }

    pub fn map_deserializer<'des, K: Deserialize, V: Deserialize>(
        &'des mut self,
    ) -> Result<MapDeserializer<'des, 'v, 'd, K, V>> {
        self.check_tag(Tag::Map)?;
        let len = self.buf.read_u32::<LittleEndian>()? as usize;
        Ok(MapDeserializer::new(self, len))
    }

    pub fn tuple_deserializer<'des>(
        &'des mut self,
        _name: &str,
    ) -> Result<TupleDeserializer<'des, 'v, 'd>> {
        let field_names = match self.sig_iter.next() {
            Some(item) => {
                if item.tag() == Tag::Tuple {
                    // name == item.struct_name() is not inforced by libqi
                    item.field_names().unwrap()
                } else {
                    return error_tag(item.tag(), Tag::Tuple);
                }
            }
            None => return error_tag(Tag::None, Tag::Tuple),
        };
        Ok(TupleDeserializer::new(self, field_names))
    }
}

pub struct ListDeserializer<'des, 's, 'd, T: Deserialize> {
    deserializer: &'des mut Deserializer<'s, 'd>,
    begin_sig_iter: SIter<'s>,
    len: usize,
    readen_len: usize,
    _type: std::marker::PhantomData<T>,
}

impl<'des, 's, 'd, T: Deserialize> ListDeserializer<'des, 's, 'd, T> {
    fn new(
        deserializer: &'des mut Deserializer<'s, 'd>,
        len: usize,
    ) -> ListDeserializer<'des, 's, 'd, T> {
        let begin_sig_iter = deserializer.sig_iter.clone();
        ListDeserializer {
            deserializer,
            begin_sig_iter,
            len,
            readen_len: 0,
            _type: std::marker::PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn read_element(&mut self) -> Result<T> {
        self.readen_len += 1;
        *self.deserializer.sig_iter = self.begin_sig_iter.clone();
        Deserialize::deserialize_from(self.deserializer)
    }

    pub fn end(self) -> Result<()> {
        if self.len != self.readen_len {
            return error(format!(
                "Deserializer: Readen count doesn't match the list size (read: {}, data: {})",
                self.len, self.readen_len,
            ));
        }
        // list might be empty
        *self.deserializer.sig_iter = self.begin_sig_iter;
        consume_next_element(self.deserializer.sig_iter);
        self.deserializer.check_tag(Tag::ListEnd)?;
        Ok(())
    }
}

pub struct MapDeserializer<'des, 's, 'd, K: Deserialize, V: Deserialize> {
    deserializer: &'des mut Deserializer<'s, 'd>,
    begin_sig_iter: SIter<'s>,
    len: usize,
    readen_len: usize,
    _key: std::marker::PhantomData<K>,
    _value: std::marker::PhantomData<V>,
}

impl<'des, 's, 'd, K: Deserialize, V: Deserialize> MapDeserializer<'des, 's, 'd, K, V> {
    fn new(
        deserializer: &'des mut Deserializer<'s, 'd>,
        len: usize,
    ) -> MapDeserializer<'des, 's, 'd, K, V> {
        let begin_sig_iter = deserializer.sig_iter.clone();
        MapDeserializer {
            deserializer,
            begin_sig_iter,
            len,
            readen_len: 0,
            _key: std::marker::PhantomData,
            _value: std::marker::PhantomData,
        }
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn read_entry(&mut self) -> Result<(K, V)> {
        self.readen_len += 1;
        *self.deserializer.sig_iter = self.begin_sig_iter.clone();
        let key = Deserialize::deserialize_from(self.deserializer)?;
        let value = Deserialize::deserialize_from(self.deserializer)?;
        Ok((key, value))
    }

    pub fn end(self) -> Result<()> {
        if self.len != self.readen_len {
            return error(format!(
                "Deserializer: Readen count doesn't match the map size (read: {}, data: {})",
                self.len, self.readen_len,
            ));
        }
        // map might be empty
        *self.deserializer.sig_iter = self.begin_sig_iter;
        consume_next_element(self.deserializer.sig_iter);
        consume_next_element(self.deserializer.sig_iter);
        self.deserializer.check_tag(Tag::MapEnd)?;
        Ok(())
    }
}

pub struct TupleDeserializer<'des, 'v, 'd> {
    deserializer: &'des mut Deserializer<'v, 'd>,
    field_names: &'v [&'v str],
}

impl<'des, 'v, 'd> TupleDeserializer<'des, 'v, 'd> {
    fn new(
        deserializer: &'des mut Deserializer<'v, 'd>,
        field_names: &'v [&'v str],
    ) -> TupleDeserializer<'des, 'v, 'd> {
        TupleDeserializer {
            deserializer,
            field_names,
        }
    }

    pub fn field_names(&self) -> &'v [&'v str] {
        self.field_names
    }

    pub fn read_field<T: Deserialize>(&mut self, name: &str) -> Result<T> {
        let field_name = match self.deserializer.sig_iter.clone().next() {
            Some(item) => item.field_name(),
            None => "",
        };
        if !field_name.is_empty() && !name.is_empty() && field_name != name {
            return error(format!(
                "Deserialize: field name doesn't match (read: '{}', signature: '{}')",
                name, field_name,
            ));
        }
        Deserialize::deserialize_from(self.deserializer)
    }

    pub fn end(self) -> Result<()> {
        self.deserializer.check_tag(Tag::TupleEnd)?;
        Ok(())
    }
}
