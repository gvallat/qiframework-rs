type ServiceFuture<T> = Box<dyn Future<Output = Result<T, String>>>;

trait ObjectInterface {
    fn meta_object(&self) -> MetaObject;
}

struct RemoteObject {
    pub socket: Socket,
    pub service_id: u32,
    pub object_id: u32,
    pub meta: MetaObject,
}

impl Drop for RemoteObject {
    fn drop(&mut self) {
        // send destroyed to remote
    }
}

#[derive(Clone, Copy)]
struct CallId {
    msg_id: u32,
    service_id: u32,
    object_id: u32,
    action_id: u32,
}

trait GenericObject: ObjectInterface {
    fn call(&self, call_id: CallId, buf: Bytes, socket: &Socket);
    fn is_valid(&self) -> bool;
}

mod my_service {
    mod action_ids {
        const COMPUTE_BIDULE: u32 = 101;
        const DO_STUFF: u32 = 102;
    }

    trait Interface: ObjectInterface {
        fn compute_bidule(&self, i: u8) -> ServiceFuture<u8>;
        fn do_stuff(&self, name: String, fac: u32) -> ServiceFuture<bool>;
    }
    // used through Arc<dyn MyServiceInterface>
    // implemented by either:
    // - service struct
    // - handle using channels to tasks

    #[derive(clone)]
    enum ProxyInner {
        Remote(Arc<RemoteObject>),
        Local(Arc<dyn Interface>),
    }

    #[derive(clone)]
    struct Proxy {
        inner: ProxyInner,
    }

    // impl From<Arc<dyn Interface>> for Proxy
    // impl From<Arc<RemoteObject>> for Proxy

    impl Proxy {
        fn from_local(service: Arc<dyn Interface>) -> Self {
            Self {
                inner: ProxyInner::Local(service),
            }
        }

        fn from_remote(remote: Arc<RemoteObject>) -> Self {
            Self {
                inner: ProxyInner::Remote(remote),
            }
        }
    }

    impl ObjectInterface for Proxy {
        fn meta_object(&self) -> MetaObject {
            match self.inner {
                Remote(remote) => remote.meta_object.clone(),
                Local(object) => object.meta_object(),
            }
        }
    }

    impl Interface for Proxy {
        fn compute_bidule(&self, i: u8) -> ServiceFuture<u8> {
            match self.inner {
                Remote(remote) => Box::new(async move {
                    if !remote.socket.is_connected() {
                        return Err(/*..*/);
                    }
                    let sig = <u8 as GetTypeSignature>::signature();
                    let method = remote.meta_object.find_method("compute_bidule", &sig)?;
                    let buf = serialize(i, method.signature(), &remote.socket.context())?;
                    let mut msg = Message::new(buf);
                    msg.header.service_id = remote.service_id;
                    msg.header.object_id = remote.object_id;
                    msg.header.action_id = method.action_id();
                    let res = call_raw(&remote.socket, msg).await?;
                    Ok(deserialize(
                        i,
                        method.return_sig(),
                        &remote.socket.context(),
                    )?)
                }),
                Local(object) => object.compute_bidule(i),
            }
        }

        fn do_stuff(&self, name: String, fac: u32) -> ServiceFuture<bool> {
            match self.inner {
                Remote(remote) => {
                    call_remote::<bool, (String, u32)>(remote.clone(), "do_stuff", (name, fac))
                }
                Local(object) => object.do_stuff(name, fac),
            }
        }
    }

    impl GenericObject for Proxy {
        fn call(&self, call_id: CallId, buf: Bytes, socket: &Socket) {
            match call_id.action_id {
                action_ids::COMPUTE_BIDULE => {
                    generic_call!(1, u8, (u8), self, compute_bidule, call_id, buf, socket)
                }
                action_ids::DO_STUFF => {
                    let this = self.clone();
                    tokio::spawn(async move {
                        let task = async {
                            let sig = <(String, u32) as GetTypeSignature>::signature();
                            let args: (String, u32) = deserialize(i, &sig, &socket.context())?;
                            let res = this.do_stuff(args.0, args.1).await?;
                            let sig = <bool as GetTypeSignature>::signature();
                            let buf = serialize(res, &sig, &socket.context())?;
                            reply_ok(&socket, call_id, buf);
                        };
                        if task.await.is_err() {
                            reply_error(&socket, call_id, format!("Error..."));
                        }
                    });
                }
                _ => {
                    reply_error(
                        &socket,
                        call_id,
                        format!("Invalid method call (action id: {})", action_id),
                    );
                }
            }
        }

        fn is_valid(&self) -> bool {
            match self.inner {
                Remote(remote) => remote.socket.is_connected(),
                Local(_) => true,
            }
        }
    }
}
