# Using Serde

We could have a QI binary serialization, and Any serialization and use the json serialization.

Pros:

* Out of the box Json support (for logs and CLI arguments)
* Can use more of the ecosystem (might need to add Signature implementation)

Cons:

* Need support for Object
* Need support fo Any
* Need support for Struct evolution

## Solutions

### out-band signaling

`serialize()` and `deserialize()` can known we are using the QI codec and acces some
needed data using `std::thread_local` variables set in codec constructor and destructor (see `serde_out_band_signaling.rs`).

### in-band signaling

Abusing the `name` argument of `serialize_struct()`, we can use specialized serializer/deserializer.

```Rust
enum StructSerializer {
    Normal{/* ...*/},
    Object{/* ...*/}, // Noop serializer (use out-band data instead)
    Any{/* ...*/}, // serializer who doesn't check signature
}

fn serialize_struct(name: &str) -> StructSerializer {
    match name {
        TOKEN_OBJECT => /* check signature = 'o' */ StructSerializer::Object {},
        TOKEN_ANY => /* check signature = 'm' */ StructSerializer::Any {},
        _ => /* check signature = '(' */ StructSerializer::Normal {},
    }
}
```
