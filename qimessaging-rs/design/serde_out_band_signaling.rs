use std::cell::RefCell;

trait Deserializer<'d> {}

struct D1;
struct D2;

impl<'d> Deserializer<'d> for D1 {}
impl<'d> Deserializer<'d> for D2 {}

thread_local! {
    static USING_D1: RefCell<bool> = RefCell::new(false);
}

trait Deserialize {
    fn deserialize<'d, D: Deserializer<'d>>(&self, d: &D) -> Result<(), ()>;
}

fn deserialize_with_d1<T: Deserialize>(val: &T) -> Result<(), ()> {
    USING_D1.with(|using| *using.borrow_mut() = true);
    let d1 = D1;
    let res = Deserialize::deserialize(val, &d1);
    USING_D1.with(|using| *using.borrow_mut() = false);
    res
}

fn deserialize_with_d2<T: Deserialize>(val: &T) -> Result<(), ()> {
    let d2 = D2;
    let res = Deserialize::deserialize(val, &d2);
    res
}

struct S1;

impl Deserialize for S1 {
    fn deserialize<'d, D: Deserializer<'d>>(&self, _d: &D) -> Result<(), ()> {
        USING_D1.with(|using_d1| {
            if *using_d1.borrow() {
                println!("With D1");
            } else {
                println!("Without D1");
            }
            Ok(())
        })
    }
}

fn main() {
    let s = S1;
    let _ = deserialize_with_d1(&s);
    let _ = deserialize_with_d2(&s);
}
