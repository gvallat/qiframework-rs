# Libqi research

Some notes about `libqi` internals

## Object

Impacted by capabilities "MetaObjectCache" and "ObjectPtrUID"

### Serialization

```python
if metaObjectCache:
    id, inserted = cache(metaObject)
    write(inserted)
    if inserted:
        write(metaObject)
    write(id)
else:
    write(metaObject)
write(serviceId)
write(objectId)
if objectPtrUID:
    write(objectUid)
```
