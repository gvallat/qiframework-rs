use bytes::BytesMut;
use core::task::{Context, Poll, Waker};
use std::{
    pin::Pin,
    sync::{Arc, Mutex},
};
use tokio::io::{AsyncRead, AsyncWrite};

pub fn stream() -> (MockStream, MockStream) {
    let side_1 = Arc::new(Mutex::new(MockBuffer::new()));
    let side_2 = Arc::new(Mutex::new(MockBuffer::new()));
    let stream_1 = MockStream {
        read_side: side_1.clone(),
        write_side: side_2.clone(),
    };
    let stream_2 = MockStream {
        read_side: side_2.clone(),
        write_side: side_1.clone(),
    };
    (stream_1, stream_2)
}

struct MockBuffer {
    bytes: BytesMut,
    waker: Option<Waker>,
    closed: bool,
}

impl MockBuffer {
    fn new() -> MockBuffer {
        MockBuffer {
            bytes: BytesMut::new(),
            waker: None,
            closed: false,
        }
    }
}

pub struct MockStream {
    read_side: Arc<Mutex<MockBuffer>>,
    write_side: Arc<Mutex<MockBuffer>>,
}

impl MockStream {
    pub fn close(&mut self) {
        let mut guard = self.write_side.lock().unwrap();
        if !guard.closed {
            guard.closed = true;
            if let Some(waker) = guard.waker.take() {
                waker.wake();
            }
        }
    }

    pub fn write(&mut self, data: &[u8]) {
        let mut guard = self.write_side.lock().unwrap();
        write_priv(data, &mut *guard);
    }

    pub fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let mut guard = self.read_side.lock().unwrap();
        read_priv(buf, &mut *guard)
    }
}

fn write_priv(buf: &[u8], data: &mut MockBuffer) {
    data.bytes.extend_from_slice(buf);
    if let Some(waker) = data.waker.take() {
        waker.wake();
    }
}

impl AsyncWrite for MockStream {
    fn poll_write(
        self: Pin<&mut Self>,
        _cx: &mut Context,
        buf: &[u8],
    ) -> Poll<Result<usize, tokio::io::Error>> {
        let mut guard = self.write_side.lock().unwrap();
        write_priv(buf, &mut *guard);
        Poll::Ready(Ok(buf.len()))
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context) -> Poll<Result<(), tokio::io::Error>> {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(
        mut self: Pin<&mut Self>,
        _cx: &mut Context,
    ) -> Poll<Result<(), tokio::io::Error>> {
        self.as_mut().close();
        Poll::Ready(Ok(()))
    }
}

fn read_priv(buf: &mut [u8], data: &mut MockBuffer) -> std::io::Result<usize> {
    if !data.bytes.is_empty() {
        let len = std::cmp::min(buf.len(), data.bytes.len());
        let dst = &mut buf[0..len];
        let src = &data.bytes[0..len];
        dst.copy_from_slice(src);
        let _ = data.bytes.split_to(len);
        Ok(len)
    } else if data.closed {
        Ok(0)
    } else {
        Err(std::io::Error::new(
            std::io::ErrorKind::WouldBlock,
            "Not enought data",
        ))
    }
}

impl AsyncRead for MockStream {
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context,
        buf: &mut [u8],
    ) -> Poll<Result<usize, tokio::io::Error>> {
        let mut guard = self.read_side.lock().unwrap();
        match read_priv(buf, &mut *guard) {
            Ok(len) => Poll::Ready(Ok(len)),
            Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                guard.waker = Some(cx.waker().clone());
                Poll::Pending
            }
            Err(e) => Poll::Ready(Err(e)),
        }
    }
}

impl Drop for MockStream {
    fn drop(&mut self) {
        self.close();
    }
}

/* TODO tokio_test API changed => fix
#[cfg(test)]
mod tests {
    use super::*;
    use futures::pin_mut;
    use tokio::io::{AsyncRead, AsyncWrite};
    use tokio_test::{task, *};

    #[test]
    fn create_stream() {
        let (_in, _out) = stream();
    }

    #[test]
    fn sync_write() {
        let (mut stream_in, _stream_out) = stream();
        let data = &[0, 1, 2, 3, 4];
        stream_in.write(&data[..]);
    }

    #[test]
    fn sync_read() {
        let (mut stream_in, mut stream_out) = stream();

        let data = &[0, 1, 2, 3, 4];
        stream_in.write(&data[..]);

        let out = &mut [0; 4];
        let res = stream_out.read(&mut out[..]);
        assert!(res.is_ok());
        assert_eq!(res.unwrap(), 4);
        assert_eq!(&out[..], &data[..4]);
    }

    #[test]
    fn async_write() {
        task::mock(|mut cx| {
            let (stream_in, _stream_out) = stream();

            pin_mut!(stream_in);
            let data = &[0, 1, 2, 3, 4];
            let res = stream_in.poll_write(&mut cx, &data[..]);
            let len = assert_ready_ok!(res);
            assert_eq!(len, 5);
        });
    }

    #[test]
    fn async_flush() {
        task::mock(|mut cx| {
            let (stream_in, _stream_out) = stream();
            pin_mut!(stream_in);
            let res = stream_in.poll_flush(&mut cx);
            assert_ready_ok!(res);
        });
    }

    #[test]
    fn async_shutdown() {
        task::mock(|mut cx| {
            let (stream_in, _stream_out) = stream();
            pin_mut!(stream_in);
            let res = stream_in.poll_shutdown(&mut cx);
            assert_ready_ok!(res);
        });
    }

    #[test]
    fn async_read_less_than_length() {
        task::mock(|mut cx| {
            let (mut stream_in, stream_out) = stream();

            let data = &[0, 1, 2, 3, 4];
            stream_in.write(&data[..]);

            pin_mut!(stream_out);
            let out = &mut [0; 4];
            let res = stream_out.poll_read(&mut cx, &mut out[..]);
            let len = assert_ready_ok!(res);
            assert_eq!(len, 4);
            assert_eq!(&out[..], &data[..4]);
        });
    }

    #[test]
    fn async_read_more_than_length() {
        task::mock(|mut cx| {
            let (mut stream_in, stream_out) = stream();

            let data = &[0, 1, 2, 3, 4];
            stream_in.write(&data[..]);

            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let res = stream_out.poll_read(&mut cx, &mut out[..]);
            let len = assert_ready_ok!(res);
            assert_eq!(len, 5);
            assert_eq!(&out[..5], &data[..5]);
        });
    }

    #[test]
    fn async_read_empty() {
        task::mock(|mut cx| {
            let (_stream_in, stream_out) = stream();

            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let res = stream_out.poll_read(&mut cx, &mut out[..]);
            assert_pending!(res);
        });
    }

    #[test]
    fn async_read_empty_closed() {
        task::mock(|mut cx| {
            let (mut stream_in, stream_out) = stream();
            stream_in.close();

            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let res = stream_out.poll_read(&mut cx, &mut out[..]);
            let len = assert_ready_ok!(res);
            assert_eq!(len, 0);
        });
    }

    #[test]
    fn async_wake_on_sync_write() {
        let mut task = task::MockTask::new();

        let (mut stream_in, stream_out) = stream();
        task.enter(|mut cx| {
            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let _ = stream_out.poll_read(&mut cx, &mut out[..]);
        });
        assert_eq!(task.waker_ref_count(), 2);
        assert!(!task.is_woken());

        let data = &[0, 1, 2, 3, 4];
        stream_in.write(&data[..]);
        assert!(task.is_woken());
    }

    #[test]
    fn async_wake_on_write() {
        let mut task = task::MockTask::new();

        let (stream_in, stream_out) = stream();
        task.enter(|mut cx| {
            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let _ = stream_out.poll_read(&mut cx, &mut out[..]);
        });
        assert_eq!(task.waker_ref_count(), 2);
        assert!(!task.is_woken());

        task.enter(|mut cx| {
            pin_mut!(stream_in);
            let data = &[0, 1, 2, 3, 4];
            let _ = stream_in.poll_write(&mut cx, &data[..]);
        });
        assert!(task.is_woken());
    }

    #[test]
    fn async_wake_on_close() {
        let mut task = task::MockTask::new();

        let (mut stream_in, stream_out) = stream();
        task.enter(|mut cx| {
            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let _ = stream_out.poll_read(&mut cx, &mut out[..]);
        });
        assert_eq!(task.waker_ref_count(), 2);
        assert!(!task.is_woken());

        stream_in.close();
        assert!(task.is_woken());
    }

    #[test]
    fn async_wake_on_shutdown() {
        let mut task = task::MockTask::new();

        let (stream_in, stream_out) = stream();
        task.enter(|mut cx| {
            pin_mut!(stream_out);
            let out = &mut [0; 8];
            let _ = stream_out.poll_read(&mut cx, &mut out[..]);
        });
        assert_eq!(task.waker_ref_count(), 2);
        assert!(!task.is_woken());

        task.enter(|mut cx| {
            pin_mut!(stream_in);
            let _ = stream_in.poll_shutdown(&mut cx);
        });
        assert!(task.is_woken());
    }
}
*/
